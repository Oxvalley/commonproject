package interfaces;

public interface ISelected
{
   public IBusinessClass getSelectedBO();

   public IBusinessClass getNewInstance();
}
