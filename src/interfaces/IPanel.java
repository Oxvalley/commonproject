package interfaces;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;

public interface IPanel
{

   void add(IPanel panel);

   void add(IPanel panel, String direction);

   void setLayout(LayoutManager layout);

   void reLoad();

   void add(IComponent label);

   void removeAll();

   Component[] getComponents();

   void setVisible(boolean b);

   void setPreferredSize(Dimension dimension);

}
