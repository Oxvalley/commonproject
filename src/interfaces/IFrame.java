package interfaces;

import java.awt.Dimension;
import java.awt.LayoutManager;

import javax.swing.JPanel;

public interface IFrame
{

   void setVisible(boolean b);

   void setLayout(LayoutManager borderLayout);

   void pack();

   void setPreferredSize(Dimension dimension);

   void add(IPanel panel);

   void setTitle(String title);

   void add(JPanel panel);

   void reLoad();

   void setAsMainWindow(IFrame frame);

   void validate();

}
