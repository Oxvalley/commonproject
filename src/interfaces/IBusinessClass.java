package interfaces;

import java.util.List;

public interface IBusinessClass
{

   public String getTableName();

   public String toStringLine();

   public String toString();

   public void saveToDB();

   public List<IColumn> getColumnList();

   public void resetDirtyFlag();

   public IBOList getIBOInstance();

   public String getLangText(String compGroupName);

   public void deleteFromDB();

   public List<IBusinessClass> getBOList();

   public IBusinessClass getNewInstance();

   public List<IBusinessClass> getBOList(String fields, String where);

}
