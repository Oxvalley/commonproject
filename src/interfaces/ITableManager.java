package interfaces;

import java.util.List;

public interface ITableManager
{

   String getDBName();

   List<IBusinessClass> getTableList();

}
