package interfaces;

import java.awt.Dimension;

public interface IComponent
{

   void setPreferredSize(Dimension dimension);

   public String toString();

   void updateValue(String textValue);

   void setEnabled(boolean b);

}
