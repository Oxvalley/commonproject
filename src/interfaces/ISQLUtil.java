package interfaces;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

public interface ISQLUtil
{

   public void close();

   public void closeRS();

   public void closeRS(ResultSet rs);

   public ArrayList<String> getAllTableNames();

   public DatabaseMetaData getMetaDB();

   public boolean getRSBooleanValue(String key);

   public Date getRSDateValue(String key);

   public double getRSDoubleValue(String key);

   public int getRSIntValue(String key);

   public long getRSLongValue(String key);

   public String getRSValue(ResultSet rs, String key);

   public String getRSValue(String key);

   public void insertAdd(String key, long value);

   public void insertAdd(String key, String value);

   public void insertAdd(String key, String value, boolean isNumeric);

   public void insertExecute();

   public void insertInit(String tableName);

   public boolean next();

   public boolean next(ResultSet rs);

   public String safeString(long songID);

   public void SQLCall(String CallString);

   public ResultSet SQLCallRS(String CallString);

   public ResultSet SQLCallRS(String callString, boolean debug,
         boolean printValues);
}