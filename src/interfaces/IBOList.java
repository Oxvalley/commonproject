package interfaces;

import java.util.ArrayList;

public interface IBOList
{

   ArrayList<IBusinessClass> getBOList();

   IBusinessClass getNewInstance();

}
