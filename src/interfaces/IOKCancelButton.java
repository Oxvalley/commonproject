package interfaces;

public interface IOKCancelButton
{

   void doActionOK();

   void doActionCancel();

   void init();

}
