/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package interfaces;

import java.awt.Component;

/**
 * 
 * @author Lars Svensson
 */
public interface IColumn
{

   public String getColumnName();

   public String getColumnNameSafe();

   public String getColumnNameEqualsValue();

   public int getColumnType();

   public String getDefaultValue();

   public String getQuotedValue();

   public String getTextValue();

   public boolean isDirty();

   public boolean isNullable();

   public boolean isPrimaryKey();

   public void resetDirtyFlag();

   public void setValue(String defaultValue);

   public String toStringLine();

   public int getHorizontalAlignment();

   public void setComponent(Component component);

   public IComponent getIComponent(String compGroupName, int componentType);

   public void saveInstantly(boolean saveInstant);

   public boolean isSavedInstantly();

   public void updateComponents();

   public String getTableName();

   public String getLangText(String compGroupName);
}
