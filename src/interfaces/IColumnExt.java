package interfaces;

public interface IColumnExt extends IColumn
{

   public String getClassName();

   public String getColumnTypeName();

   public String getIColumnImplDeclaration();

   public String getJavaType();

   public String getVarName();

   public String getVarInstanceName();

   public String getGetFunctionName();

   public String getRSString();

   public String getExtraGetter();

   public String getExtraSetter();

   public String getExtraImports();
}
