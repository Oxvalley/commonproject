package resources;

import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Resources
{

   public static Icon getIcon(String iconName)
   {
      try
      {
         URL stream = new Resources().getClass().getResource("images/" + iconName);
         return new ImageIcon(stream);
      }
      catch (Exception e)
      {
         System.out.println("Icon " + iconName + " not found!");
         return null;
      }
   }

}
