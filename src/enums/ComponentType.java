package enums;

public class ComponentType
{
   public static final int ColumnNameLabel = 1;
   public static final int FormInput = 2;
   public static final int TableInputNotEditable = 3;
   public static final int TableInputEditable = 4;
}
