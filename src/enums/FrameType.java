package enums;

public class FrameType
{
   public static final int Table = 1;
   public static final int EditableTable = 2;
   public static final int FormEdit = 3;
   public static final int FormNew = 4;
   public static final int ScrollableTopTableMain = 5;

}
