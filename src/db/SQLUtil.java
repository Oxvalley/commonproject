package db;

import interfaces.ISQLUtil;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import db.tables.BasicTable;

public abstract class SQLUtil implements ISQLUtil
{
   private static ISQLUtil i_SQLObj = null;

   public static void connectAccessDB(String path)
   {
      i_SQLObj = new SQLACCESSUtil(path);
   }

   public static String getColumnInfo(ResultSetMetaData rsMeta, int i)
         throws SQLException
   {
      String retval = rsMeta.getColumnName(i);
      retval += " " + rsMeta.getColumnType(i);
      retval += " " + rsMeta.getColumnTypeName(i);
      retval += " " + rsMeta.getPrecision(i);

      if (rsMeta.isAutoIncrement(i))
      {
         retval += " AutoIncr";
      }
      if (rsMeta.isNullable(i) != 0)
      {
         retval += " Nullable";
      }
      else
      {
         retval += " Not Null";
      }
      return retval;
   }

   public static ISQLUtil getConnectedObject()
   {
      return i_SQLObj;
   }

   public static String getRSValue(ResultSet rs, int intKey, boolean isNumeric)
   {
      try
      {
         if (rs != null)
         {
            if (rs.getRow() == 0)
            {
               rs.next();
            }
            if (isNumeric)
            {
               return String.valueOf(rs.getLong(intKey));
            }
            else
            {
               return unSafeString(rs.getString(intKey));
            }
         }
         else
         {
            return "";
         }
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.getRSValue: SQL Exception " + e);
         System.out.println(intKey);
         return "";
      }
   }

   public static String getRSValue(ResultSet rs, int intKey, int columnType)
   {
      try
      {
         if (rs != null)
         {
            if (rs.getRow() == 0)
            {
               rs.next();
            }
            String retval = "";
            switch (BasicTable.getColumnTypeGroup(columnType))
            {
            case BasicTable.ColumnTypeGroup_INTEGER:
               retval = String.valueOf(rs.getInt(intKey));
               break;
            case BasicTable.ColumnTypeGroup_STRING:
               retval = rs.getString(intKey);
               break;
            case BasicTable.ColumnTypeGroup_DOUBLE:
               retval = String.valueOf(rs.getDouble(intKey));
               break;
            case BasicTable.ColumnTypeGroup_DATE:
               retval = String.valueOf(rs.getDate(intKey));
               break;
            case BasicTable.ColumnTypeGroup_UNKNOWN:
               retval = "???Unknown???";
               break;
            case BasicTable.ColumnTypeGroup_BLOB:
               retval = "???Blob???";
               break;
            default:
               break;
            // retval = rs.getString(intKey);
            }

            return retval;
         }
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.getRSValue: SQL Exception " + e);
         System.out.println(intKey);
         return "";
      }
      return "";
   }

   @SuppressWarnings("unused")
   private static boolean isNumericType(ResultSet rs, int i)
         throws SQLException
   {
      ResultSetMetaData rsMeta = rs.getMetaData();
      boolean isNumeric;
      int type = rsMeta.getColumnType(i);

      if (type == Types.BIGINT || type == Types.BIT || type == Types.INTEGER
            || type == Types.NUMERIC || type == Types.SMALLINT
            || type == Types.TINYINT)
      {
         isNumeric = true;
      }
      else
      {
         isNumeric = false;
      }
      return isNumeric;
   }

   public static void printTableInfo(ResultSet rs, String callString,
         boolean printValues)
   {
      String Line = "";
      int i = 0;
      try
      {
         ResultSetMetaData rsMeta = rs.getMetaData();
         String Headings = "";
         Line = "";
         for (i = 1; i <= rsMeta.getColumnCount(); i++)
         {
            try
            {
               Headings += getColumnInfo(rsMeta, i) + "\n";
            }
            catch (SQLException e)
            {
               Headings += "<NA> ";
            }
         }
         System.out.println(Headings + "\n");
         if (printValues)
         {
            int c = 0;
            while (rs.next())
            {
               Line = "";
               for (i = 1; i <= rsMeta.getColumnCount(); i++)
               {
                  Line += "<" + getRSValue(rs, i, rsMeta.getColumnType(i))
                        + "> ";
               }
               System.out.println(Line);
               c++;
            }
            System.out.println(c + " lines found.");

         }
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.SQLCallRS: Debug SQL Exception " + e);
         System.out.println(callString);
         System.out.println(Line);
         System.out.println(i + "\n");
      }
   }

   public static void printTableInfo(ISQLUtil sqlobj, String tableName)
   {
      printTableInfo(sqlobj, "*", tableName, "");
   }

   public static void printTableInfo(ISQLUtil sqlobj, String fields,
         String tableName, String where)
   {
      String whereString = where;
      if (where == null || where.length() == 0)
      {
         whereString = "";
      }
      else
      {
         if (!whereString.trim().toUpperCase().startsWith("WHERE "))
         {
            whereString = " where " + whereString;
         }
      }
      String SQL = "select " + fields + " from [" + tableName + "] "
            + whereString;
      ResultSet rs = sqlobj.SQLCallRS(SQL);
      SQLUtil.printTableInfo(rs, SQL, false);
   }

   /**
    * @param string
    * @return
    */
   public static String safeString(String Text)
   {
      return safeString(Text, false);
   }

   /**
    * @param Text
    * @param b
    * @return
    */
   public static String safeString(String Text, boolean isNumeric)
   {
      Text = safeStringReplace(Text).trim();
      if (!isNumeric)
      {
         Text = "'" + Text + "'";
      }
      return Text;
   }

   /**
    * @param string
    * @return
    */
   public static String safeStringReplace(String Text)
   {
      Text = Text.replace("'", "''");
      Text = Text.replace("&", "&&");
      Text = Text.replace("\\", "\\\\");
      return Text;
   }

   /**
    * @param string
    * @return
    */
   public static String unSafeString(String Text)
   {
      if (Text == null)
      {
         return Text;
      }

      Text = Text.replace("''", "'");
      Text = Text.replace("&&", "&");
      Text = Text.replace("\\\\", "\\");
      return Text;
   }

   private String i_CallString = "";

   private Connection i_Conn = null;

   private HashMap<String, String> i_hm = null;

   private String i_InsertTableName;

   private DatabaseMetaData i_MetaDB;

   private int i_RowCount = 0;

   private ResultSet i_RS = null;

   private Statement i_Statement;

   private String i_DBPath;

   /**
    * @param string
    */
   public SQLUtil(String DBPath)
   {
      try
      {
         Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
         String connString = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ="
               + DBPath + ";DriverID=22;READONLY=true}";
         i_Conn = DriverManager.getConnection(connString, "", "");
         i_Statement = i_Conn.createStatement();
         i_MetaDB = i_Conn.getMetaData();
         i_DBPath = DBPath;

      }
      catch (Exception err)
      {
         System.out.println("SQLUtil.SQLUtill: Connection Error: " + err);
         System.out.println(DBPath);
      }
      if (i_SQLObj == null)
      {
         i_SQLObj = this;
      }

   }

   /**
     *
     */
   public void close()
   {
      if (i_Conn != null)
      {
         try
         {
            i_Conn.close();
         }
         catch (SQLException e)
         {
            System.out.println("SQLclose : SQL Exception " + e);
         }
      }
   }

   public void closeRS()
   {
      if (i_RS != null)
      {
         // try
         // {
         // rs.close();
         // }
         // catch (SQLException e)
         // {
         // System.out.println("SQLUtil.closeRS: SQL Exception " + e);
         // }
      }
   }

   /**
    * @param rs
    */
   public void closeRS(ResultSet rs)
   {
      if (rs != null)
      {
         // try
         // {
         // rs.close();
         // }
         // catch (SQLException e)
         // {
         // System.out.println("SQLUtil.closeRS: SQL Exception " + e);
         // }
      }
   }

   public ArrayList<String> getAllTableNames()
   {
      ArrayList<String> lst = new ArrayList<String>();
      ResultSet rs;
      try
      {
         rs = i_MetaDB.getTables(null, null, "%", null);
         while (rs.next())
         {
            String table = rs.getString(3);
            if (!table.startsWith("MSys"))
            {
               lst.add(table);
            }
         }
      }
      catch (SQLException ex)
      {
         Logger.getLogger(ISQLUtil.class.getName()).log(Level.SEVERE, null, ex);
      }
      return lst;
   }

   public DatabaseMetaData getMetaDB()
   {
      return i_MetaDB;
   }

   private boolean getRSBooleanValue(ResultSet rs, String key,
         boolean defaultvalue)
   {
      try
      {
         if (rs != null)
         {
            if (rs.getRow() == 0)
            {
               rs.next();
            }

            try
            {
               return rs.getBoolean(key);
            }
            catch (SQLException e)
            {
               System.out.println("SQLUtil.getRSDoubleValue: SQL Exception "
                     + e);
               System.out.println("This Key (" + key + ") is not in the row");
               System.out.println(i_CallString);
               return defaultvalue;
            }
         }
         else
         {
            System.out.println("Rs is null (Key=" + key + ")");
            return defaultvalue;
         }
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.getRSDoubleValue: SQL Exception " + e);
         System.out.println(key);
         return defaultvalue;
      }

   }

   public boolean getRSBooleanValue(String key)
   {
      return getRSBooleanValue(i_RS, key, false);
   }

   private Date getRSDateValue(ResultSet rs, String key, Date defaultvalue)
   {
      try
      {
         if (rs != null)
         {
            if (rs.getRow() == 0)
            {
               rs.next();
            }

            try
            {
               return rs.getDate(key);
            }
            catch (SQLException e)
            {
               System.out.println("SQLUtil.getRSDoubleValue: SQL Exception "
                     + e);
               System.out.println("This Key (" + key + ") is not in the row");
               System.out.println(i_CallString);
               return defaultvalue;
            }
         }
         else
         {
            System.out.println("Rs is null (Key=" + key + ")");
            return defaultvalue;
         }
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.getRSDoubleValue: SQL Exception " + e);
         System.out.println(key);
         return defaultvalue;
      }

   }

   public Date getRSDateValue(String key)
   {
      return getRSDateValue(i_RS, key, new Date());
   }

   private double getRSDoubleValue(ResultSet rs, String key, double defaultvalue)
   {
      try
      {
         if (rs != null)
         {
            if (rs.getRow() == 0)
            {
               rs.next();
            }

            try
            {
               return rs.getDouble(key);
            }
            catch (SQLException e)
            {
               System.out.println("SQLUtil.getRSDoubleValue: SQL Exception "
                     + e);
               System.out.println("This Key (" + key + ") is not in the row");
               System.out.println(i_CallString);
               return defaultvalue;
            }
         }
         else
         {
            System.out.println("Rs is null (Key=" + key + ")");
            return defaultvalue;
         }
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.getRSDoubleValue: SQL Exception " + e);
         System.out.println(key);
         return defaultvalue;
      }

   }

   public double getRSDoubleValue(String key)
   {
      return getRSDoubleValue(i_RS, key, -1);
   }

   private int getRSIntValue(ResultSet rs, String key, int defaultvalue)
   {
      try
      {
         if (rs != null)
         {
            if (rs.getRow() == 0)
            {
               rs.next();
            }

            try
            {
               return rs.getInt(key);
            }
            catch (SQLException e)
            {
               System.out.println("SQLUtil.getRSIntValue: SQL Exception " + e);
               System.out.println("This Key (" + key + ") is not in the row");
               System.out.println(i_CallString);
               return defaultvalue;
            }
         }
         else
         {
            System.out.println("Rs is null (Key=" + key + ")");
            return defaultvalue;
         }
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.getRSIntValue: SQL Exception " + e);
         System.out.println(key);
         return defaultvalue;
      }

   }

   public int getRSIntValue(String key)
   {
      return getRSIntValue(i_RS, key, -1);
   }

   private long getRSLongValue(ResultSet rs, String key, long defaultvalue)
   {
      try
      {
         if (rs != null)
         {
            if (rs.getRow() == 0)
            {
               rs.next();
            }

            try
            {
               return rs.getLong(key);
            }
            catch (SQLException e)
            {
               System.out.println("SQLUtil.getRSLongValue: SQL Exception " + e);
               System.out.println("This Key (" + key + ") is not in the row");
               System.out.println(i_CallString);
               return defaultvalue;
            }
         }
         else
         {
            System.out.println("Rs is null (Key=" + key + ")");
            return defaultvalue;
         }
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.getRSLongValue: SQL Exception " + e);
         System.out.println(key);
         return defaultvalue;
      }

   }

   public long getRSLongValue(String key)
   {
      return getRSLongValue(i_RS, key, -1);
   }

   public String getRSValue(ResultSet rs, String key)
   {
      return getRSValue(rs, key, "");
   }

   private String getRSValue(ResultSet rs, String key, String defaultvalue)
   {
      try
      {
         if (rs != null)
         {
            if (rs.getRow() == 0)
            {
               rs.next();
            }

            try
            {
               return unSafeString(rs.getString(key));
            }
            catch (SQLException e)
            {
               System.out.println("SQLUtil.getRSLongValue: SQL Exception " + e);
               System.out.println("This Key (" + key + ") is not in the row");
            }
            return defaultvalue;
         }
         else
         {
            System.out.println("Rs is null (Key=" + key + ")");
            return defaultvalue;
         }
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.getRSLongValue: SQL Exception " + e);
         System.out.println(key);
         return defaultvalue;
      }

   }

   public String getRSValue(String key)
   {
      return getRSValue(i_RS, key, "");
   }

   /**
    * @param string
    * @param string2
    */
   public void insertAdd(String key, long value)
   {
      insertAdd(key, String.valueOf(value), true);
   }

   /**
    * @param string
    * @param string2
    */
   public void insertAdd(String key, String value)
   {
      insertAdd(key, value, false);
   }

   /**
    * @param string
    * @param string2
    */
   public void insertAdd(String key, String value, boolean isNumeric)
   {
      if (i_hm == null)
      {
         System.out.println("SQLUtil.insertAdd: insertInit() not run!!");
      }
      else
      {
         i_hm.put(key, safeString(value, isNumeric));
      }
   }

   /**
     *
     */
   @SuppressWarnings("unchecked")
   public void insertExecute()
   {
      if (i_hm == null)
      {
         System.out.println("SQLUtil.insertExecute: insertInit() not run!!");
      }
      else
      {
         if (i_hm.size() == 0)
         {
            System.out.println("SQLUtil.insertExecute: insertAdd() not run!!");
         }
         else
         {
            Set<Entry<String, String>> set = i_hm.entrySet();
            Map.Entry<String, String> entry = null;
            String CallString = "INSERT INTO " + i_InsertTableName + " (";
            String CallString2 = "VALUES(";
            for (Iterator<?> iter = set.iterator(); iter.hasNext();)
            {
               entry = (Map.Entry<String, String>) iter.next();
               if (!CallString.endsWith("("))
               {
                  // Not the first element, add a comma before it
                  CallString += ", ";
                  CallString2 += ", ";
               }
               CallString += "[" + entry.getKey() + "]";
               CallString2 += entry.getValue();
            }
            CallString += ") " + CallString2 + ")";
            SQLCall(CallString);
         }
      }
   }

   /**
    * @param string
    */
   public void insertInit(String tableName)
   {
      i_InsertTableName = tableName;
      i_hm = new HashMap<String, String>();
   }

   public boolean next()
   {
      if (i_RS == null)
      {
         return false;
      }
      try
      {
         i_RowCount++;
         // System.out.println("Line no: " + i_RowCount);
         return i_RS.next();
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.next: SQL Exception " + e);
         System.out.println(i_CallString);
         return false;
      }
   }

   /**
    * @param rs
    * @return
    */
   public boolean next(ResultSet rs)
   {
      if (rs == null)
      {
         return false;
      }
      try
      {
         i_RowCount++;
         // System.out.println("Line no: " + i_RowCount);
         return rs.next();
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.next: SQL Exception " + e);
         System.out.println(i_CallString);
         return false;
      }
   }

   /**
    * @param songID
    * @return
    */
   public String safeString(long songID)
   {
      return safeString(String.valueOf(songID), true);
   }

   void setRS(ResultSet rs)
   {
      i_RS = rs;
   }

   /**
    * @param string
    */
   public void SQLCall(String CallString)
   {
      i_CallString = CallString;
      System.out.println(CallString);
      try
      {
         Statement statement = i_Conn.createStatement();
         // Select the data from the table
         statement.execute(CallString);
         statement.close();
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.SQLCall: SQL Exception " + e);
      }
   }

   public ResultSet SQLCallRS(String CallString)
   {
      System.out.println(CallString);
      i_CallString = CallString;
      i_RowCount = 0;
      try
      {
         if (i_Statement != null)
         {
            i_Statement.close();
            if(!i_Conn.isClosed()){
            i_Statement = i_Conn.createStatement();}
            else
            {
               System.out.println("Error: Connection was closed for call "+ CallString);
            }
         }
         // Select the data from the table
         i_Statement.execute(CallString);
         i_RS = i_Statement.getResultSet();
         return i_RS;
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.SQLCallRS: SQL Exception " + e);
         System.out.println(CallString);
         return null;
      }
      catch (Exception e)
      {
         System.out.println("SQLUtil.SQLCallRS: SQL Exception " + e);
         System.out.println(CallString);
         return null;
      }
   }

   public ResultSet SQLCallRS(String callString, boolean debug,
         boolean printValues)
   {
      ResultSet rs = SQLCallRS(callString);
      if (debug)
      {
         printTableInfo(rs, callString, printValues);
      }
      i_RS = SQLCallRS(callString);
      return i_RS;
   }

   public String getDBPath()
   {
      return i_DBPath;
   }

}
