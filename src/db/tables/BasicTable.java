/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db.tables;

import interfaces.IBOList;
import interfaces.IColumn;
import interfaces.ISQLUtil;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import program.properties.Language;
import db.SQLACCESSUtil;
import db.columns.BasicColumn;
import db.columns.ColumnAutoIncr;

/**
 * 
 * @author Lars Svensson
 */
public abstract class BasicTable implements IBOList
{

   public static final int ColumnTypeGroup_STRING = 1;
   public static final int ColumnTypeGroup_INTEGER = 2;
   public static final int ColumnTypeGroup_DOUBLE = 3;
   public static final int ColumnTypeGroup_BLOB = 4;
   public static final int ColumnTypeGroup_DATE = 5;
   public static final int ColumnTypeGroup_BOOLEAN = 6;

   public static final int ColumnTypeGroup_UNKNOWN = 99;

   protected String i_TableName;

   public String getLangText(String compGroupName, String defaultValue)
   {
      return Language.getText(compGroupName, "tbl." + i_TableName + ".name",
            defaultValue);
   }

   protected static void deleteFromDB(ISQLUtil sqlObj, String tableName,
         List<IColumn> lst)
   {
      if (rowExists(sqlObj, tableName, lst))
      {
         sqlObj.SQLCall("delete from " + tableName + " where "
               + getQuotedPrimaryKeyValues(lst));
      }

   }

   public IBOList getIBOInstance()
   {
      return this;
   }

   private static String getColumnNames(List<IColumn> lst)
   {
      String retval = "";
      for (IColumn col : lst)
      {
         if (col.isDirty())
         {
            if (retval.length() != 0)
            {
               retval += ", ";
            }
            retval += col.getColumnName();
         }
      }
      return retval;
   }

   public static int getColumnTypeGroup(int columnType)
   {

      switch (columnType)
      {
      case Types.ARRAY:
      case Types.BINARY:
      case Types.BLOB:
      case Types.VARBINARY:
      case Types.CLOB:
      case Types.STRUCT:
      case Types.NCLOB:
      case Types.LONGVARBINARY:
         return ColumnTypeGroup_BLOB;

      case Types.JAVA_OBJECT:
      case Types.DATALINK:
      case Types.NULL:
      case Types.OTHER:
      case Types.REF:
         return ColumnTypeGroup_UNKNOWN;

      case Types.FLOAT:
      case Types.REAL:
      case Types.DOUBLE:
      case Types.DECIMAL:
         return ColumnTypeGroup_DOUBLE;

      case Types.DATE:
      case Types.TIME:
      case Types.TIMESTAMP:
         return ColumnTypeGroup_DATE;

      case Types.DISTINCT:
      case Types.ROWID:
      case Types.BIGINT:
      case Types.INTEGER:
      case Types.NUMERIC:
      case Types.SMALLINT:
      case Types.TINYINT:
         return ColumnTypeGroup_INTEGER;

      case Types.BIT:
      case Types.BOOLEAN:
         return ColumnTypeGroup_BOOLEAN;

      case Types.SQLXML:
      case Types.CHAR:
      case Types.NCHAR:
      case Types.NVARCHAR:
      case Types.LONGVARCHAR:
      case Types.LONGNVARCHAR:
      case Types.VARCHAR:
      default:
         return ColumnTypeGroup_STRING;
      }

   }

   private static ColumnAutoIncr getPrimaryKeyAutoIncrement(List<IColumn> lst)
   {
      for (IColumn col : lst)
      {
         if (col instanceof ColumnAutoIncr)
         {
            return (ColumnAutoIncr) col;
         }
      }

      // The primary key is no autoincrement
      return null;
   }

   public static String getQuoted(BasicColumn basicColumn)
   {
      return getQuote(basicColumn.getColumnType());
   }

   public static String getQuoted(IColumn col)
   {
      return getQuoted((BasicColumn) col);
   }

   public static String getQuote(int columnType)
   {
      int columnGroupType = getColumnTypeGroup(columnType);
      switch (columnGroupType)
      {
      case ColumnTypeGroup_DATE:
         return "#";
      case ColumnTypeGroup_BLOB:
      case ColumnTypeGroup_STRING:
      case ColumnTypeGroup_UNKNOWN:
         return "'";
      case ColumnTypeGroup_DOUBLE:
      case ColumnTypeGroup_INTEGER:
      case ColumnTypeGroup_BOOLEAN:
         return "";
      default:
         return "'";
      }
   }

   public static String getQuotedColumnValues(List<IColumn> lst)
   {
      String retval = "";
      for (IColumn col : lst)
      {
         if (col.isDirty())
         {
            if (retval.length() != 0)
            {
               retval += ", ";
            }
            retval += col.getQuotedValue();
         }
      }
      return retval;
   }

   public static String getQuotedPrimaryKeyValues(List<IColumn> lst)
   {
      String retval = "";
      for (IColumn col : lst)
      {
         if (col.isPrimaryKey())
         {
            if (retval.length() != 0)
            {
               retval += " and ";
            }
            retval += col.getColumnNameEqualsValue();
         }
      }
      return retval;
   }

   // public static String getWhereString(String where) {
   // This do not allow omly 'Order by'. Require 'where'
   // String whereString = where;
   // if (where == null) {
   // whereString = "";
   // } else {
   // if (!whereString.trim().toUpperCase().startsWith("WHERE ")) {
   // whereString = " where " + whereString;
   // }
   //
   // }
   // return whereString;
   // }

   private static void insertRow(ISQLUtil sqlObj, String tableName,
         List<IColumn> lst)
   {
      sqlObj.SQLCall("insert into [" + tableName + "](" + getColumnNames(lst)
            + ") values(" + getQuotedColumnValues(lst) + ")");
   }

   protected static void readAutoIncrementValueFromDB(ISQLUtil sqlObj,
         List<IColumn> lst, String tableName)
   {
      ColumnAutoIncr auto = getPrimaryKeyAutoIncrement(lst);
      if (auto != null)
      {
         // This is an auto increment. Get new automatic id by reloading
         String SQL = "select top 1 ";
         for (IColumn col : lst)
         {
            if (col == auto)
            {
               SQL += col.getColumnNameSafe() + " from [" + tableName + "]";
            }
            else
            {
               if (col.isDirty())
               {
                  if (!SQL.contains(" where "))
                  {
                     SQL += " where ";
                  }
                  else
                  {
                     SQL += " and ";
                  }
                  SQL += col.getColumnNameEqualsValue();
               }
            }
         }

         SQL += " order by " + auto.getColumnNameSafe() + " desc";

         sqlObj.SQLCallRS(SQL);
         if (sqlObj.next())
         {
            auto.setValue(sqlObj.getRSIntValue(auto.getColumnName()));
         }
         else
         {
            System.out
                  .println("Error! Could not find id of newly created auto increment row.");
            return;
         }
      }
   }

   public void saveInstantly(boolean saveInstant)
   {
      saveInstantly(saveInstant, i_ColumnList);
   }

   private static void saveInstantly(boolean saveInstant, List<IColumn> lst)
   {
      for (IColumn column : lst)
      {
         column.saveInstantly(saveInstant);
      }
   }

   private static void resetDirtyFlag(List<IColumn> lst)
   {
      for (IColumn column : lst)
      {
         column.resetDirtyFlag();
      }
   }

   public void resetDirtyFlag()
   {
      resetDirtyFlag(i_ColumnList);
   }

   protected static boolean rowExists(ISQLUtil sqlObj, String tableName,
         List<IColumn> lst)
   {

      ColumnAutoIncr auto = getPrimaryKeyAutoIncrement(lst);

      if (auto != null && auto.getTextValue() == null)
      {
         return false;
      }

      sqlObj.SQLCallRS("select * from [" + tableName + "] where "
            + getQuotedPrimaryKeyValues(lst));

      return sqlObj.next();

   }

   protected static void saveToDB(ISQLUtil sqlObj, String tableName,
         List<IColumn> lst)
   {
      if (!rowExists(sqlObj, tableName, lst))
      {
         insertRow(sqlObj, tableName, lst);
         // setDefaultValues(lst);
         readAutoIncrementValueFromDB(sqlObj, lst, tableName);
      }
      else
      {
         // Update
         updateRow(sqlObj, tableName, lst);
      }
      resetDirtyFlag(lst);
   }

   private static void updateRow(ISQLUtil sqlObj, String tableName,
         List<IColumn> lst)
   {
      String SQL = "";
      for (IColumn col : lst)
      {
         if (!col.isPrimaryKey() && col.isDirty())
         {
            if (SQL.length() != 0)
            {
               SQL += ", ";
            }
            SQL += col.getColumnNameEqualsValue();
         }
      }
      if (SQL.length() > 0)
      {
         SQL = "update [" + tableName + "] set " + SQL + " where "
               + getQuotedPrimaryKeyValues(lst);
         sqlObj.SQLCall(SQL);

      }
   }

   protected List<IColumn> i_ColumnList = new ArrayList<IColumn>();

   protected ISQLUtil i_SQLObj = null;

   public List<IColumn> getColumnList()
   {
      return i_ColumnList;
   }

   protected ISQLUtil getConnection()
   {
      if (i_SQLObj != null)
      {
         return i_SQLObj;
      }
      else
      {
         // Check if Global Connection is set
         if (SQLACCESSUtil.getConnectedObject() != null)
         {
            i_SQLObj = SQLACCESSUtil.getConnectedObject();
            return i_SQLObj;
         }
         else
         {
            return null;
         }
      }
   }

   protected void setColumnList(List<IColumn> columnList)
   {
      i_ColumnList = columnList;
   }

   public void saveToDB()
   {
      saveToDB(i_SQLObj, i_TableName, i_ColumnList);
      if (!isSavedInstantly())
      {
         updateComponents();
      }
   }

   private void updateComponents()
   {
      for (IColumn column : i_ColumnList)
      {
         column.updateComponents();
      }
   }

   private boolean isSavedInstantly()
   {
      for (IColumn column : i_ColumnList)
      {
         if (!column.isSavedInstantly())
         {
            return false;
         }
      }
      return true;
   }

}
