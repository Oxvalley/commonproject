package db.tables;

import interfaces.IBusinessClass;
import interfaces.IColumn;
import interfaces.ISQLUtil;
import interfaces.ITableManager;

import java.util.ArrayList;
import java.util.List;

import db.SQLACCESSUtil;

public class BasicTableManager
{
   protected static ISQLUtil i_SQLObj;

   protected String i_DBName = null;

   protected List<IBusinessClass> i_TableList = new ArrayList<IBusinessClass>();

   public String getDBName()
   {
      return i_DBName;
   }

   protected ISQLUtil getConnection()
   {
      if (i_SQLObj != null)
      {
         return i_SQLObj;
      }
      else
      {
         // Check if Global Connection is set
         if (SQLACCESSUtil.getConnectedObject() != null)
         {
            i_SQLObj = SQLACCESSUtil.getConnectedObject();
            return i_SQLObj;
         }
         else
         {
            return null;
         }
      }
   }

   public List<IBusinessClass> getTableList()
   {
      return i_TableList;
   }

   public static void printDB(ITableManager tabman)
   {
      System.out.println("DB name: " + tabman.getDBName());

      for (IBusinessClass table : tabman.getTableList())
      {
         System.out.println("\nTable name: " + table.getTableName());
         System.out.println("===============================================");
         List<IColumn> cols = table.getColumnList();
         for (IColumn column : cols)
         {
            System.out.println(column.toStringLine());
         }
      }
   }

}
