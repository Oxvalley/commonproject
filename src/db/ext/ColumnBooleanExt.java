package db.ext;

import interfaces.IColumnExt;
import common.utils.StringUtil;
import db.columns.ColumnBoolean;

public class ColumnBooleanExt extends ColumnBoolean implements IColumnExt
{

   private String i_ColumnTypeName;

   private String i_VarName;

   private String i_VarInstanceName;

   private String i_getGetFunctionName;

   public ColumnBooleanExt(String tableName, String colName, int colType,
         String columnTypeName, boolean isThisPrimaryKey, boolean defbool)
   {
      super(tableName, colName, colType, isThisPrimaryKey, defbool);
      i_ColumnTypeName = columnTypeName;
      i_VarName = StringUtil.getVarName(i_ColumnName);
      i_VarInstanceName = StringUtil.getVarInstanceName(i_ColumnName,
            i_VarName);
      i_getGetFunctionName = "is" + i_VarName;
   }

   public String getColumnTypeName()
   {
      return i_ColumnTypeName;
   }

   public String getClassName()
   {
      return "ColumnBoolean";
   }

   public String getIColumnImplDeclaration()
   {
      String retVal = "   private " + getClassName() + " ic_" + getVarName()
            + " = new " + getClassName() + "(i_TableName, \"" + getColumnName()
            + "\", Types." + getColumnTypeName() + ", " + isPrimaryKey() + ", "
            + getDefaultValue() + ");";
      return retVal;
   }

   public String getJavaType()
   {
      return "boolean";
   }

   public String getVarInstanceName()
   {
      return i_VarInstanceName;
   }

   public String getVarName()
   {
      return i_VarName;
   }

   public String getGetFunctionName()
   {
      return i_getGetFunctionName;
   }

   public String getRSString()
   {
      return "getRSBooleanValue";
   }

   public String getExtraGetter()
   {
      return "";
   }

   public String getExtraSetter()
   {
      return "";
   }

   public String getExtraImports()
   {
      return "";
   }

}
