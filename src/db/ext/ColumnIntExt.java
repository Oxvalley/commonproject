package db.ext;

import interfaces.IColumnExt;
import common.utils.StringUtil;
import db.columns.ColumnInt;

public class ColumnIntExt extends ColumnInt implements IColumnExt
{

   private String i_ColumnTypeName;

   private String i_VarName;

   private String i_VarInstanceName;

   private String i_getgetFunctionName;

   public ColumnIntExt(String tableName, String colName, int colType,
         String columnTypeName, boolean isThisNullable,
         boolean isThisPrimaryKey, int defint)
   {
      super(tableName, colName, colType, isThisNullable, isThisPrimaryKey,
            defint);
      i_ColumnTypeName = columnTypeName;
      i_VarName = StringUtil.getVarName(i_ColumnName);
      i_VarInstanceName = StringUtil.getVarInstanceName(i_ColumnName,
            i_VarName);
      i_getgetFunctionName = "get" + i_VarName;
   }

   public String getColumnTypeName()
   {
      return i_ColumnTypeName;
   }

   @Override
   public String getClassName()
   {
      return "ColumnInt";
   }

   public String getIColumnImplDeclaration()
   {
      String retVal = "   private " + getClassName() + " ic_" + getVarName()
            + " = new " + getClassName() + "(i_TableName, \"" + getColumnName()
            + "\", Types." + getColumnTypeName() + ", " + isNullable() + ", "
            + isPrimaryKey() + ", " + getDefaultValue() + ");";
      return retVal;
   }

   public String getJavaType()
   {
      return "int";
   }

   public String getVarInstanceName()
   {
      return i_VarInstanceName;
   }

   public String getVarName()
   {
      return i_VarName;
   }

   public String getGetFunctionName()
   {
      return i_getgetFunctionName;
   }

   public String getRSString()
   {
      return "getRSIntValue";
   }

   public String getExtraGetter()
   {
      return "";
   }

   public String getExtraSetter()
   {
      return "";
   }

   public String getExtraImports()
   {
      return "";
   }
}
