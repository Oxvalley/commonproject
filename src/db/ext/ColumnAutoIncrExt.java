package db.ext;

import interfaces.IColumnExt;
import common.utils.StringUtil;
import db.columns.ColumnAutoIncr;

public class ColumnAutoIncrExt extends ColumnAutoIncr implements IColumnExt
{

   private String i_ColumnTypeName;

   private String i_VarName;

   private String i_VarInstanceName;

   private String i_getgetFunctionName;

   public ColumnAutoIncrExt(String tableName, String columnName,
         int columnType, String columnTypeName)
   {
      super(tableName, columnName, columnType);
      i_ColumnTypeName = columnTypeName;
      i_VarName = StringUtil.getVarName(i_ColumnName);
      i_VarInstanceName = StringUtil.getVarInstanceName(i_ColumnName,
            i_VarName);
      i_getgetFunctionName = "get" + i_VarName;
   }

   public String getColumnTypeName()
   {
      return i_ColumnTypeName;
   }

   public String getClassName()
   {
      return "ColumnAutoIncr";
   }

   public String getIColumnImplDeclaration()
   {
      String retVal = "   private " + getClassName() + " ic_" + getVarName()
            + " = new " + getClassName() + "(i_TableName, \"" + getColumnName()
            + "\", Types." + getColumnTypeName() + ");";
      return retVal;
   }

   public String getJavaType()
   {
      return "int";
   }

   public String getVarInstanceName()
   {
      return i_VarInstanceName;
   }

   public String getVarName()
   {
      return i_VarName;
   }

   public String getGetFunctionName()
   {
      return i_getgetFunctionName;
   }

   public String getRSString()
   {
      return "getRSIntValue";
   }

   public String getExtraGetter()
   {
      return "";
   }

   public String getExtraSetter()
   {
      return "";
   }

   public String getExtraImports()
   {
      return "";
   }
}
