package db.ext;

import interfaces.IColumnExt;
import common.utils.StringUtil;

import java.util.Date;

import db.columns.ColumnDate;

public class ColumnDateExt extends ColumnDate implements IColumnExt
{

   private String i_ColumnTypeName;

   private String i_VarName;

   private String i_VarInstanceName;

   private String i_getgetFunctionName;

   public ColumnDateExt(String tableName, String colName, int colType,
         String columnTypeName, boolean isThisNullable,
         boolean isThisPrimaryKey, Date defdate)
   {
      super(tableName, colName, colType, isThisNullable, isThisPrimaryKey,
            defdate);
      i_ColumnTypeName = columnTypeName;
      i_VarName = StringUtil.getVarName(i_ColumnName);
      i_VarInstanceName = StringUtil.getVarInstanceName(i_ColumnName,
            i_VarName);
      i_getgetFunctionName = "get" + i_VarName;
   }

   public String getColumnTypeName()
   {
      return i_ColumnTypeName;
   }

   @Override
   public String getClassName()
   {
      return "ColumnDate";
   }

   public String getIColumnImplDeclaration()
   {
      String retVal = "   private " + getClassName() + " ic_" + getVarName()
            + " = new " + getClassName() + "(i_TableName, \"" + getColumnName()
            + "\", Types." + getColumnTypeName() + ", " + isNullable() + ", "
            + isPrimaryKey() + ", " + getDefaultValue() + ");";
      return retVal;
   }

   public String getJavaType()
   {
      return "Date";
   }

   public String getVarInstanceName()
   {
      return i_VarInstanceName;
   }

   public String getVarName()
   {
      return i_VarName;
   }

   public String getGetFunctionName()
   {
      return i_getgetFunctionName;
   }

   public String getRSString()
   {
      return "getRSDateValue";
   }

   public String getExtraGetter()
   {
      String retVal = "public String " + getGetFunctionName() + "String() {\n";
      retVal += "      return DateHelper.getStringFromDate("
            + getGetFunctionName() + "());\n";
      retVal += "   }\n\n   ";
      return retVal;
   }

   public String getExtraSetter()
   {
      String retVal = "public void setBirthDateString(String value) {\n";
      retVal += "      Date date = DateHelper.getDateFromString(value);\n";
      retVal += "      set" + getVarName() + "(date);\n";
      retVal += "   }\n\n   ";
      return retVal;
   }

   public String getExtraImports()
   {
      String retVal = "import java.util.Date;\n";
      retVal += "import utils.DateHelper;";
      return retVal;
   }

}
