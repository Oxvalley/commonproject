package db.ext;

import interfaces.IColumnExt;
import common.utils.StringUtil;
import db.columns.ColumnString;

public class ColumnStringExt extends ColumnString implements IColumnExt
{

   private String i_ColumnTypeName;

   private int i_Precision;

   private String i_VarName;

   private String i_VarInstanceName;

   private String i_getgetFunctionName;

   public ColumnStringExt(String tableName, String colName, int precision,
         int colType, String columnTypeName, boolean isThisNullable,
         boolean isThisPrimaryKey, String def)
   {
      super(tableName, colName, precision, colType, isThisNullable,
            isThisPrimaryKey, def);
      i_ColumnTypeName = columnTypeName;
      i_Precision = precision;
      i_VarName = StringUtil.getVarName(i_ColumnName);
      i_VarInstanceName = StringUtil.getVarInstanceName(i_ColumnName,
            i_VarName);
      i_getgetFunctionName = "get" + i_VarName;
   }

   public String getColumnTypeName()
   {
      return i_ColumnTypeName;
   }

   public String getClassName()
   {
      return "ColumnString";
   }

   public String getIColumnImplDeclaration()
   {
      String retVal = "   private " + getClassName() + " ic_" + getVarName()
            + " = new " + getClassName() + "(i_TableName, \"" + getColumnName()
            + "\", Types." + getColumnTypeName() + ", " + getPrecision() + ", "
            + isNullable() + ", " + isPrimaryKey() + ", " + getDefaultValue()
            + ");";
      return retVal;
   }

   public int getPrecision()
   {
      return i_Precision;
   }

   public String getJavaType()
   {
      return "String";
   }

   public String getVarInstanceName()
   {
      return i_VarInstanceName;
   }

   public String getVarName()
   {
      return i_VarName;
   }

   public String getGetFunctionName()
   {
      return i_getgetFunctionName;
   }

   public String getRSString()
   {
      return "getRSValue";
   }

   public String getExtraGetter()
   {
      return "";
   }

   public String getExtraSetter()
   {
      return "";
   }

   public String getExtraImports()
   {
      return "";
   }

}
