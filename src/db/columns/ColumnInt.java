/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package db.columns;

import javax.swing.SwingConstants;

/**
 * 
 * @author Lars Svensson
 */
public class ColumnInt extends BasicColumn
{
   private int i_Value;

   public ColumnInt(String tableName, String columnName, int columnType,
         boolean isThisNullable, boolean isThisPrimaryKey, int defaultValue)
   {
      super(tableName, columnName, columnType, isThisNullable,
            isThisPrimaryKey, String.valueOf(defaultValue));
   }

   public int getValue()
   {
      return i_Value;
   }

   public void setValue(int value)
   {
      i_Value = value;
      i_TextValue = String.valueOf(value);
      i_IsDirty = true;
   }

   @Override
   public void setValue(String value)
   {
      // TODO prio 3 Take care of too large numbers or characters, or text in
      // number inputs
      if (value != null)
      {
         try
         {
            setValue(Integer.parseInt(value));
         }
         catch (NumberFormatException e)
         {
            System.out.println(value + " is not a number (ColumnInt). ");
         }
      }
   }

   @Override
   public int getHorizontalAlignment()
   {
      return SwingConstants.RIGHT;
   }

   @Override
   public String getLangText(String compGroupName)
   {
      return super.getLangText(compGroupName, getColumnName());
   }

}
