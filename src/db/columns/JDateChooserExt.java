package db.columns;

import java.beans.PropertyChangeEvent;

import com.toedter.calendar.JDateChooser;

import common.utils.DateUtil;
import interfaces.IColumn;
import interfaces.IComponent;

public class JDateChooserExt extends JDateChooser implements IComponent
{

   private IColumn i_Col = null;

   public JDateChooserExt(IColumn col)
   {
      super(null, DateUtil.getDateFormatString());
      i_Col = col;
      if (col != null)
      {
         setDate(DateUtil.getDateFromString(col.getTextValue()));
         col.setComponent(this);
         addPropertyChangeListener(this);
      }
   }

   @Override
   public String toString()
   {
      return i_Col.getTextValue();
   }

   @Override
   public void updateValue(String textValue)
   {
      setDate(DateUtil.getDateFromString(textValue));
   }

   @Override
   public void propertyChange(PropertyChangeEvent propertyChangeEvent)
   {
      super.propertyChange(propertyChangeEvent);
      if ((i_Col != null && i_Col.getTextValue() != null && !i_Col
            .getTextValue().equals(DateUtil.getStringFromDate(getDate())))
            || (i_Col != null && i_Col.getTextValue() == null && getDate() != null))
      {
         i_Col.setValue(DateUtil.getStringFromDate(getDate()));
      }

   }

}
