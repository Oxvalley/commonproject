package db.columns;

import interfaces.IColumn;
import interfaces.IComponent;

import java.awt.event.ActionEvent;

import javax.swing.JComboBox;

public class JComboBoxBoolean extends JComboBox implements IComponent
{

   private IColumn i_Col = null;
   private static final String[] i_Values = { "true", "false" };

   public JComboBoxBoolean(IColumn col)
   {
      super(i_Values);
      i_Col = col;
      if (col != null)
      {
         String value = col.getTextValue();
         setValue(value);

         addActionListener(this);

         col.setComponent(this);
      }
   }

   public void setValue(String value)
   {
      if (value != null && value.equalsIgnoreCase("true"))
      {
         setSelectedIndex(0);
      }
      else
      {
         setSelectedIndex(1);
      }
   }

   @Override
   public void actionPerformed(ActionEvent e)
   {
      JComboBox cb = (JComboBox) e.getSource();
      String value = (String) cb.getSelectedItem();
      i_Col.setValue(value);
   }

   @Override
   public String toString()
   {
      return i_Col.getTextValue();
   }

   public void updateValue(String textValue)
   {
      setValue(textValue);
   }

}
