/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db.columns;

import interfaces.IColumn;
import interfaces.IComponent;

import java.awt.Component;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import db.SQLUtil;
import db.tables.BasicTable;

import enums.ComponentType;
import program.components.extension.JLabelExtension;
import program.components.extension.JTextFieldExtension;
import program.properties.Language;

/**
 * 
 * @author Lars Svensson
 */
public abstract class BasicColumn implements IColumn
{

   protected String i_ColumnName;
   protected String i_TableName;
   protected int i_ColumnType;
   private String i_DefaultValue;

   protected boolean i_IsDirty = false;
   protected String i_TextValue = null;
   protected boolean i_IsInitialised = false;
   protected boolean i_SaveInstantly = false;

   protected ArrayList<IComponent> i_Components = new ArrayList<IComponent>();

   public void updateComponents()
   {
      for (IComponent component : i_Components)
      {
         component.updateValue(getTextValue());
      }
   }

   public String getTableName()
   {
      return i_TableName;
   }

   public boolean isSavedInstantly()
   {
      return i_SaveInstantly;
   }

   public void saveInstantly(boolean saveInstant)
   {
      this.i_SaveInstantly = saveInstant;
   }

   public String getLangText(String compGroupName, String defaultValue)
   {
      return Language.getText(compGroupName, "tbl." + getTableName() + ".col."
            + getColumnName() + ".name", defaultValue);

   }

   protected boolean isNullable;
   protected boolean isPrimaryKey;
   @SuppressWarnings("unused")
   private Component i_Component = null;

   protected BasicColumn(String tableName, String columnName, int columnType,
         boolean isThisNullable, boolean isThisPrimaryKey, String defaultValue)
   {
      i_TableName = tableName;
      i_ColumnName = columnName;
      i_ColumnType = columnType;
      this.isNullable = isThisNullable;
      this.isPrimaryKey = isThisPrimaryKey;
      i_DefaultValue = defaultValue;
   }

   public int getHorizontalAlignment()
   {
      return SwingConstants.LEFT;
   }

   public String getColumnNameSafe()
   {
      return "[" + i_ColumnName + "]";
   }

   public String getColumnName()
   {
      return i_ColumnName;
   }

   public String getColumnNameEqualsValue()
   {
      return getColumnNameSafe() + "=" + getQuotedValue();
   }

   public int getColumnType()
   {
      return i_ColumnType;
   }

   public String getDefaultValue()
   {
      return i_DefaultValue;
   }

   public String getQuotedValue()
   {
      return BasicTable.getQuoted(this) + SQLUtil.safeString(getTextValue(),true)
            + BasicTable.getQuoted(this);
   }

   @Override
   public String toString()
   {
      return getTextValue();
   }

   public String getTextValue()
   {
      return i_TextValue;
   }

   public boolean isDirty()
   {
      return i_IsDirty;
   }

   public boolean isNullable()
   {
      return isNullable;
   }

   public boolean isPrimaryKey()
   {
      return isPrimaryKey;
   }

   public void resetDirtyFlag()
   {
      i_IsDirty = false;
   }

   public String toStringLine()
   {
      return getColumnNameSafe() + "\t" + getColumnType() + "\t" + isNullable()
            + "\t" + isPrimaryKey();
   }

   public void setComponent(Component component)
   {
      i_Component = component;
   }

   public IComponent getIComponent(String compGroupName, int componentType)
   {
      return getListedIComponent(getInnerIComponent(compGroupName,
            componentType));
   }

   // override this to change behavior
   public IComponent getInnerIComponent(String compGroupName, int componentType)
   {
      switch (componentType)
      {
      case ComponentType.ColumnNameLabel:
         return new JLabelExtension(getLangText(compGroupName));

      case ComponentType.FormInput:
         return new JTextFieldExtension(this);

      case ComponentType.TableInputEditable:
         return new JTextFieldExtension(this);

      case ComponentType.TableInputNotEditable:
         return new JTextFieldExtension(this);

      default:
         return null;
      }
   }

   private IComponent getListedIComponent(IComponent component)
   {
      i_Components.add(component);
      return component;
   }

}
