package db.columns;

import interfaces.IComponent;
import program.components.extension.JLabelExtension;
import program.components.extension.JTextFieldExtension;
import enums.ComponentType;

public class ColumnBoolean extends BasicColumn
{

   private boolean i_Value;

   public ColumnBoolean(String tableName, String columnName, int columnType,
         boolean isThisPrimaryKey, boolean defaultValue)
   {
      super(tableName, columnName, columnType, false, isThisPrimaryKey, String
            .valueOf(defaultValue));
   }

   public boolean getValue()
   {
      return i_Value;
   }

   public void setValue(boolean value)
   {
      i_Value = value;
      i_TextValue = String.valueOf(value);
      i_IsDirty = true;
   }

   @Override
   public void setValue(String value)
   {
      if (value != null)
      {
         setValue(Boolean.parseBoolean(value));
      }
   }

   @Override
   public IComponent getInnerIComponent(String compGroupName, int componentType)
   {
      switch (componentType)
      {
      case ComponentType.ColumnNameLabel:
         return new JLabelExtension(getLangText(compGroupName));

      case ComponentType.FormInput:
         return new JComboBoxBoolean(this);

      case ComponentType.TableInputEditable:
         return new JTextFieldExtension(this);

      case ComponentType.TableInputNotEditable:
         return new JCheckBoxExt();

      default:
         return null;
      }
   }

   @Override
   public String getLangText(String compGroupName)
   {
      return super.getLangText(compGroupName, getColumnName());
   }

}
