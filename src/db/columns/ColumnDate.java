package db.columns;

import java.util.Date;

import common.utils.DateUtil;
import enums.ComponentType;
import interfaces.IComponent;
import program.components.extension.JLabelExtension;
import program.components.extension.JTextFieldExtension;

public class ColumnDate extends BasicColumn
{

   private Date i_Value;

   public ColumnDate(String tableName, String columnName, int columnType,
         boolean isThisNullable, boolean isThisPrimaryKey, Date defaultValue)
   {
      super(tableName, columnName, columnType, isThisNullable,
            isThisPrimaryKey, String.valueOf(defaultValue));
   }

   public Date getValue()
   {
      return i_Value;
   }

   public void setValue(Date value)
   {
      i_Value = value;

      if (value == null)
      {
         i_TextValue = null;
      }
      else
      {
         i_TextValue = DateUtil.getStringFromDate(value);
      }
      i_IsDirty = true;
   }

   @Override
   public void setValue(String value)
   {
      Date thisDate = null;
      thisDate = DateUtil.getDateFromString(value);
      setValue(thisDate);
   }

   @Override
   public IComponent getInnerIComponent(String compGroupName, int componentType)
   {
      switch (componentType)
      {
      case ComponentType.ColumnNameLabel:
         return new JLabelExtension(getLangText(compGroupName));

      case ComponentType.FormInput:
         return new JDateChooserExt(this);

      case ComponentType.TableInputEditable:
         return new JTextFieldExtension(this);

      case ComponentType.TableInputNotEditable:
         return new JTextFieldExtension(this);

      default:
         return null;
      }
   }

   @Override
   public String getLangText(String compGroupName)
   {
      return super.getLangText(compGroupName, getColumnName());
   }

}
