/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package db.columns;

import interfaces.IComponent;
import program.components.extension.JLabelExtension;
import program.components.extension.JTextFieldExtension;
import program.components.extension.JTextFieldLocked;

import javax.swing.SwingConstants;

import enums.ComponentType;

/**
 * 
 * @author Lars Svensson
 */
public class ColumnAutoIncr extends BasicColumn
{

   private int i_Value;

   public ColumnAutoIncr(String tableName, String columnName, int columnType)
   {
      super(tableName, columnName, columnType, false, true, "-1");
   }

   public int getValue()
   {
      return i_Value;
   }

   public void setValue(int value)
   {
      i_Value = value;
      i_TextValue = String.valueOf(value);
      i_IsDirty = true;
   }

   @Override
   public void setValue(String value)
   {
      if (value != null)
      {
         setValue(Integer.parseInt(value));
      }
   }

   @Override
   public int getHorizontalAlignment()
   {
      return SwingConstants.RIGHT;
   }

   @Override
   public IComponent getInnerIComponent(String compGroupName, int componentType)
   {
      switch (componentType)
      {
      case ComponentType.ColumnNameLabel:
         return new JLabelExtension(getLangText(compGroupName));

      case ComponentType.FormInput:
         return new JTextFieldLocked(this);

      case ComponentType.TableInputEditable:
         return new JTextFieldExtension(this);

      case ComponentType.TableInputNotEditable:
         return new JTextFieldExtension(this);

      default:
         return null;
      }
   }

   @Override
   public String getLangText(String compGroupName)
   {
      return super.getLangText(compGroupName, getColumnName());
   }

}
