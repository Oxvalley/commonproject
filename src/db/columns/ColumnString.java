/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package db.columns;

/**
 * 
 * @author Lars Svensson
 */
public class ColumnString extends BasicColumn
{
   private int i_length;

   public ColumnString(String tableName, String columnName, int columnType,
         int length, boolean isThisNullable, boolean isThisPrimaryKey,
         String defaultValue)
   {
      super(tableName, columnName, columnType, isThisNullable,
            isThisPrimaryKey, defaultValue);
      i_length = length;
   }

   public int getLength()
   {
      return i_length;
   }

   public String getValue()
   {
      return i_TextValue;
   }

   public void setValue(String value)
   {
      if (value != null && value.length() > i_length)
      {
         System.out.println("Too long value, cutting it: " + value + " "
               + toStringLine());
         value = value.substring(0, i_length);
      }

      i_TextValue = value;
      if (i_SaveInstantly)
      {
         fireSaveBOEvent();
         // TODO prio 4 Create an event to save and change dirty to false
         i_IsDirty = true;
      }
      else
      {
         i_IsDirty = true;
      }

   }

   private void fireSaveBOEvent()
   {
   }

   @Override
   public String toStringLine()
   {
      return getColumnName() + "\t" + getColumnType() + "\t" + getLength()
            + "\t" + isNullable() + "\t" + isPrimaryKey();
   }

   @Override
   public String getLangText(String compGroupName)
   {
      return super.getLangText(compGroupName, getColumnName());
   }

}
