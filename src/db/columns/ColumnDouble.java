/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package db.columns;

import javax.swing.SwingConstants;

/**
 * 
 * @author Lars Svensson
 */
public class ColumnDouble extends BasicColumn
{
   private double i_Value;

   public ColumnDouble(String tableName, String columnName, int columnType,
         boolean isThisNullable, boolean isThisPrimaryKey, double defaultValue)
   {
      super(tableName, columnName, columnType, isThisNullable,
            isThisPrimaryKey, String.valueOf(defaultValue));
   }

   public double getValue()
   {
      return i_Value;
   }

   public void setValue(double value)
   {
      i_Value = value;
      i_TextValue = String.valueOf(value);
      i_IsDirty = true;
   }

   @Override
   public void setValue(String value)
   {
      if (value != null)
      {
         try
         {
            setValue(Double.parseDouble(value));
         }
         catch (NumberFormatException e)
         {
            System.out.println(value + " is not a number (ColumnDouble). ");
         }
      }
   }

   @Override
   public int getHorizontalAlignment()
   {
      return SwingConstants.RIGHT;
   }

   @Override
   public String getLangText(String compGroupName)
   {
      return super.getLangText(compGroupName, getColumnName());
   }

}
