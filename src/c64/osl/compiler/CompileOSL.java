package c64.osl.compiler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Stack;

import org.apache.commons.lang3.StringUtils;

import common.utils.FileUtil;

public class CompileOSL
{

   private String iOutputFileName;
   private String iInputFileName;
   private boolean write = false;
   private Stack<Integer> iStack = new Stack<>();

   public static void main(String[] args)
   {
      CompileOSL comp = new CompileOSL();
      comp.getArguments(args);
      comp.compile();
   }

   private void compile()
   {
      BufferedReader reader = null;
      File file = new File(iOutputFileName);
      Writer w = null;
      try
      {
         reader = new BufferedReader(new FileReader(iInputFileName));

         if (write)
         {
            try
            {
               w = new FileWriter(file);
            }
            catch (IOException e)
            {
               System.out.println("Could not open file " + iInputFileName
                     + " for writing. Exiting.");
               System.exit(1);
            }

         }

         String line = reader.readLine();
         int indent = 0;
         int oldindent = -1;
         int origIndent = -1;
         boolean isInComment = false;
         int spaces = 3;
         String spaceString = StringUtils.repeat(" ", spaces);

         while (line != null)
         {
            if (!line.trim().isEmpty())
            {

               if (!isInComment)
               {
                  if ((StringUtils.countMatches(line, "/*")
                        - StringUtils.countMatches(line, "*/")) > 0)
                  {
                     isInComment = true;
                  }
               }

               if (!isInComment)
               {
                  line = line.trim();

                  if (!line.endsWith(";") && !line.equals("{")
                        && !line.endsWith(">"))
                  {
                     if (!line.startsWith("{") && !line.startsWith("//")
                           && !line.startsWith("/*"))
                     {
                        if (origIndent == -1)
                        {
                           origIndent = indent;
                        }
                        indent++;
                     }
                  }
                  else
                  {
                     if (origIndent > -1)
                     {
                        if (line.equals("{") && origIndent < indent - 1)
                        {
                           indent--;
                        }
                        else
                        {
                           indent = origIndent;
                        }

                        origIndent = -1;
                     }
                  }

                  String line2 = getNonCommentedLine(line);
                  indent += ((StringUtils.countMatches(line2, "{")
                        + StringUtils.countMatches(line2, "(")
                        + StringUtils.countMatches(line2, "/*")
                        + StringUtils.countMatches(line2, "["))
                        - (StringUtils.countMatches(line2, "}")
                              + StringUtils.countMatches(line2, ")")
                              + StringUtils.countMatches(line2, "*/")
                              + StringUtils.countMatches(line2, "]")));

                  if (line.equals("{"))
                  {
                     oldindent--;
                  }

                  if (line.startsWith("}"))
                  {
                     oldindent = (int) Math.floor(iStack.peek() / spaces);
                     indent = oldindent;
                     origIndent = -1;
                  }

                  line = StringUtils.repeat(" ", oldindent * spaces) + line;

                  // System.out.println(
                  // line + " " + oldindent + " " + indent + " " + origIndent+ "
                  // "
                  // + extra);

                  if (!line2.isEmpty())
                  {
                     System.out.println(line2);
                  }
                  
                  if (write)
                  {
                     w.write(line + "\n");
                  }

               }

               if (!isInComment)
               {
                  iStack = addOpenBracketstoStack(iStack, line);
               }

               if (isInComment)
               {
                  if ((StringUtils.countMatches(line, "*/"))
                        - (StringUtils.countMatches(line, "/*")) > 0)
                  {
                     isInComment = false;
                  }
               }

               oldindent = indent;
            }

            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }

            if (write)
            {
               if (w != null)
               {
                  w.close();
               }
            }
         }
         catch (Exception e)
         {
         }
      }

   }

   private static Stack<Integer> addOpenBracketstoStack(Stack<Integer> iStack,
         String line)
   {
      String line2 = getNonCommentedLine(line);
      for (int i = 0; i < line2.length(); i++)
      {
         char c = line2.charAt(i);
         if (c == '{')
         {
            iStack.push(i);
         }

         if (c == '}')
         {
            iStack.pop();
         }
      }
      return iStack;
   }

   public static String getNonCommentedLine(String line)
   {
      String line2 = line;

      int j = line2.indexOf("//");
      if (j > -1)
      {
         line2 = line2.substring(0, j);
      }
      return line2;
   }

   public void getArguments(String[] args)
   {
      boolean oFlag = false;

      if (args.length == 0)
      {
         System.out.println("Usage: osl <Filename.osl> [-o <outputFileName>]");
         System.exit(1);
      }

      for (String arg : args)
      {

         if (oFlag)
         {
            // Name of output file
            iOutputFileName = arg;
            oFlag = false;
            break;
         }

         if (arg.equals("-o"))
         {
            oFlag = true;
            break;
         }

         iInputFileName = arg;
      }

      if (oFlag)
      {
         System.out.println("-o needs an argument after");
         System.exit(1);
      }

      if (iOutputFileName == null)
      {
         iOutputFileName = FileUtil.getFileNameNoTag(iInputFileName);
      }
   }

}
