package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class OSFileWriter extends FileWriter
{

   public OSFileWriter(File file) throws IOException
   {
      super(file);
   }

   public OSFileWriter(String filepath) throws IOException
   {
      super(filepath);
   }

   public void writeln(String string) throws IOException
   {
      write(string + "\n");
   }

}
