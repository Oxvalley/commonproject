package program.components;

import interfaces.IButton;
import interfaces.IPanel;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import program.components.extension.JPanelExtension;
import program.properties.Language;
import resources.Resources;

public class Button extends JPanelExtension
{

   private JButton i_Button;
   private IButton i_Helper;

   public Button(String compGroupName, IButton helper, String label)
   {
      this(compGroupName, helper, label, null, label.toLowerCase());
   }

   public Button(String compGroupName, IButton helper, String label,
         String iconName)
   {
      this(compGroupName, helper, label, iconName, label.toLowerCase());
   }

   public Button(String compGroupName, IButton helper, String label,
         String iconName, String key)
   {
      super();
      i_Helper = helper;
      createPanel(compGroupName, label, iconName, key);
   }

   @SuppressWarnings("unused")
   private IPanel createPanel(String compGroupName, String label)
   {
      return createPanel(compGroupName, label, null, label.toLowerCase());
   }

   private IPanel createPanel(String compGroupName, String label,
         String iconName, String key)
   {
      i_Helper.init();
      setLayout(new FlowLayout());
      if (key == null)
      {
         key = label.toLowerCase();
      }
      String caption = Language.getText(compGroupName, "btn." + key,
            label);
      i_Button = new JButton(caption);
      
      i_Button.setToolTipText(caption);
      
      i_Button.addActionListener(new ActionListener()
      {
         public void actionPerformed(ActionEvent e)
         {
            // OK code goes here
            i_Helper.doAction();
            System.out.println("actionPerformed");
         }
      });

      if (iconName != null)
      {
         i_Button.setIcon(Resources.getIcon(iconName));
      }
      i_Button.setPreferredSize(new Dimension(200, 70));
      //i_Button.setMinimumSize(new Dimension(200, 70));

      add(i_Button);
      return this;
   }

   public IPanel getIPanelInstance()
   {
      return this;
   }

}
