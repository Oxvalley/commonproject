package program.components;

import java.util.Vector;

public class DefaultTableModelNotEditable extends CheckboxTableModel
{

   public DefaultTableModelNotEditable()
   {
   }

   @SuppressWarnings("unchecked")
   public DefaultTableModelNotEditable(Vector data, Vector<String> headers)
   {
      super(data, headers, 6);
   }

   @Override
   public boolean isCellEditable(int arg0, int arg1)
   {
      return false;
   }

}
