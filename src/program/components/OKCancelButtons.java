package program.components;

import interfaces.IOKCancelButton;
import interfaces.IPanel;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import program.components.extension.JPanelExtension;
import program.properties.Language;

public class OKCancelButtons extends JPanelExtension
{

   private JButton i_OKButton;
   private JButton i_CancelButton;
   private IOKCancelButton i_Helper;

   public OKCancelButtons(String compGroupName, IOKCancelButton helper)
   {
      super();
      i_Helper = helper;
      createPanel(compGroupName);
   }

   // TODO prio 3 Return will OK and close form

   private void createPanel(String compGroupName)
   {
      i_Helper.init();
      JPanel invisible = new JPanel();
      invisible.setPreferredSize(new Dimension(300, 20));
      JPanel okCancel = new JPanel(new FlowLayout());
      i_OKButton = new JButton(Language.getText(compGroupName, "btn.ok", "OK"));
      i_OKButton.addActionListener(new ActionListener()
      {
         public void actionPerformed(ActionEvent e)
         {
            // OK code goes here
            i_Helper.doActionOK();
         }
      });

      i_CancelButton = new JButton(Language.getText(compGroupName,
            "btn.cancel", "Cancel"));
      i_CancelButton.addActionListener(new ActionListener()
      {
         public void actionPerformed(ActionEvent e)
         {
            // Cancel code goes here
            i_Helper.doActionCancel();
         }
      });

      okCancel.add(i_OKButton);
      okCancel.add(i_CancelButton);

      add(invisible);
      add(okCancel);
   }

   public IPanel getIPanelInstance()
   {
      return this;
   }

}
