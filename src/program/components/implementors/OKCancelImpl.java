package program.components.implementors;

import java.util.ArrayList;
import java.util.List;

import program.ProgramCreator;

import interfaces.IBusinessClass;
import interfaces.IColumn;
import interfaces.IFrame;
import interfaces.IOKCancelButton;

public class OKCancelImpl implements IOKCancelButton
{

   private IBusinessClass i_BO;
   private List<String> i_OldBOList;
   private IFrame i_Frame;
   private boolean i_isNew = false;

   public OKCancelImpl(IBusinessClass bo, IFrame frame, boolean isNew)
   {
      i_BO = bo;
      i_Frame = frame;
      i_isNew  = isNew;
   }

   private void saveOldValues(IBusinessClass bo)
   {
      i_OldBOList = new ArrayList<String>();
      for (IColumn col : bo.getColumnList())
      {
         i_OldBOList.add(col.getTextValue());
      }
   }

   public void doActionCancel()
   {
      System.out.println("Cancelled clicked");

      // Undo changes
      List<IColumn> boList = i_BO.getColumnList();
      for (int i = 0; i < boList.size(); i++)
      {
         IColumn col = boList.get(i);
         col.setValue(i_OldBOList.get(i));
      }
      i_BO.resetDirtyFlag();
      i_Frame.setVisible(false);
   }

   public void doActionOK()
   {
      i_BO.saveToDB();
      i_Frame.setVisible(false);
      
      // Really ugly
      if (i_isNew)
      {
         ProgramCreator.GlobalFrame.reLoad();
      }
      else
      {
         ProgramCreator.GlobalTable.repaint();
      }
   }

   public void init()
   {
      saveOldValues(i_BO);
   }

}
