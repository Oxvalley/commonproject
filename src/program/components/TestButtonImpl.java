package program.components;

import interfaces.IButton;

public class TestButtonImpl implements IButton
{

   @Override
   public void doAction()
   {
      System.out.println("Button clicked");
   }

   @Override
   public void init()
   {
      System.out.println("Init Button");
   }

}
