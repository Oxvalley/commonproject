package program.components.extension;

import interfaces.IBusinessClass;
import interfaces.IComponent;
import interfaces.IPanel;
import interfaces.ISQLUtil;

import java.awt.Component;
import java.awt.LayoutManager;
import java.util.List;

import javax.swing.JPanel;

public class JPanelExtension extends JPanel implements IPanel
{
   protected List<IBusinessClass> i_BOList = null;

   protected ISQLUtil i_SQLUtil = null;

   protected IBusinessClass i_BO = null;

   public JPanelExtension(LayoutManager layout)
   {
      super(layout);
   }

   public JPanelExtension()
   {
      super();
   }

   public JPanelExtension(List<IBusinessClass> list, ISQLUtil sqlobj)
   {
      super();
      i_BOList = list;
      i_SQLUtil = sqlobj;
   }

   public JPanelExtension(IBusinessClass bo, ISQLUtil sqlobj)
   {
      i_BO = bo;
      i_SQLUtil = sqlobj;
    }

   @Override
   public void add(IPanel panel)
   {
      add((JPanel) panel);
   }

   public void add(IPanel panel, String direction)
   {
      add((JPanel) panel, direction);
   }

   public void reLoad()
   {
      JFrameExtension.reLoadRecursive(this);
   }

   @Override
   public void add(IComponent label)
   {
      add((Component) label);
   }

}
