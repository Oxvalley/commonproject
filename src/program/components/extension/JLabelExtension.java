package program.components.extension;

import javax.swing.JLabel;

import interfaces.IComponent;

public class JLabelExtension extends JLabel implements IComponent
{

   private String I_Text = null;

   public JLabelExtension(String text)
   {
      super(text);
      I_Text = text;
   }

   @Override
   public String toString()
   {
      return I_Text;
   }

   public void updateValue(String textValue)
   {
      setText(textValue);
   }

}
