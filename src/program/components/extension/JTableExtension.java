/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package program.components.extension;

import javax.swing.JTable;

/**
 * 
 * @author Lars Svensson
 */
public class JTableExtension extends JTable
{

   private Object[][] i_Values;
   private String[] i_Headers;

   public JTableExtension()
   {
      super();
      init();
   }

   @SuppressWarnings("unchecked")
   Class[] types = new Class[] { java.lang.String.class,
         java.lang.String.class, java.lang.String.class,
         java.lang.String.class, java.lang.String.class,
         java.lang.String.class, java.lang.String.class };
   boolean[] canEdit = new boolean[] { false, false, false, false, false,
         false, false };

   @SuppressWarnings("unchecked")
   @Override
   public Class getColumnClass(int columnIndex)
   {
      return types[columnIndex];
   }

   @Override
   public boolean isCellEditable(int rowIndex, int columnIndex)
   {
      return canEdit[columnIndex];
   }

   private void init()
   {
      i_Values = new Object[][] { { null, null, null, null, null, null, null },
            { null, null, null, null, null, null, null },
            { null, null, null, null, null, null, null },
            { null, null, null, null, null, null, null } };

      i_Headers = new String[] { "Ordernr", "Förnamn", "Efternamn", "Adress",
            "Postnummer", "Postadress", "Epostadress" };

      setModel(new javax.swing.table.DefaultTableModel(i_Values, i_Headers));
   }
}
