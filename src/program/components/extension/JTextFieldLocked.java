package program.components.extension;

import interfaces.IColumn;
import interfaces.IComponent;

import javax.swing.JTextField;

public class JTextFieldLocked extends JTextField implements IComponent
{

   private IColumn i_Col = null;

   public JTextFieldLocked(IColumn col)
   {
      super();
      i_Col = col;
      if (col != null)
      {
         setText(col.getTextValue());
         col.setComponent(this);
         setEditable(false);
      }
   }

   @Override
   public String toString()
   {
      return i_Col.getTextValue();
   }

   @Override
   public void updateValue(String textValue)
   {
      // Do nothing. This control is locked
   }

}
