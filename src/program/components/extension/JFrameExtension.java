package program.components.extension;

import interfaces.IFrame;
import interfaces.IPanel;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import program.properties.PropertiesExt;

public class JFrameExtension extends JFrame implements IFrame, WindowListener
{

   private static IFrame i_MainWindow = null;

   public JFrameExtension(String title)
   {
      super(title);
      init();
   }

   public JFrameExtension()
   {
      init();
   }

   private void init()
   {
      addWindowListener(this);
   }

   public void add(IPanel panel)
   {
      add((Component) panel);
   }

   public void add(JPanel panel)
   {
      super.add(panel);
   }

   public void reLoad()
   {
      reLoadRecursive(this);
   }

   public static void reLoadRecursive(Container comp)
   {
      for (Component component : comp.getComponents())
      {
         if (component instanceof IPanel)
         {
            ((IPanel) component).reLoad();
         }

         if (component instanceof Container)
         {
            reLoadRecursive((Container)component);
         }        
         
      }
   }

 
   public void windowActivated(WindowEvent e)
   {
   }

   public void windowClosed(WindowEvent e)
   {
   }

   public void windowClosing(WindowEvent e)
   {
      PropertiesExt.StoreAll();

      // Only shut down program if this is the main window
      if (i_MainWindow == this)
      {
         System.exit(1);
      }
   }

   public void windowDeactivated(WindowEvent e)
   {
   }

   public void windowDeiconified(WindowEvent e)
   {
   }

   public void windowIconified(WindowEvent e)
   {
   }

   public void windowOpened(WindowEvent e)
   {
   }

   @Override
   public void setAsMainWindow(IFrame frame)
   {
      i_MainWindow = frame;
   }

}
