package program.components.extension;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import interfaces.IColumn;
import interfaces.IComponent;

import javax.swing.JTextField;

public class JTextFieldExtension extends JTextField implements IComponent,
      FocusListener
{

   private IColumn i_Col = null;

   public JTextFieldExtension(IColumn col)
   {
      super();
      i_Col = col;
      if (col != null)
      {
         setText(col.getTextValue());
         col.setComponent(this);
         addFocusListener(this);
      }
   }

   @Override
   public void focusGained(FocusEvent arg0)
   {

   }

   @Override
   public void focusLost(FocusEvent arg0)
   {
      if ((i_Col != null && i_Col.getTextValue() != null && !i_Col
            .getTextValue().equals(getText()))
            || (i_Col != null && i_Col.getTextValue() == null && getText() != null))
      {
         i_Col.setValue(getText());
      }
   }

   @Override
   public String toString()
   {
      return i_Col.getTextValue();
   }

   public void updateValue(String textValue)
   {
      setText(textValue);
   }

}
