package program.components;

import interfaces.IBusinessClass;
import interfaces.IFrame;
import interfaces.ISQLUtil;
import interfaces.ISelected;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import program.ProgramCreator;
import program.components.extension.JPanelExtension;
import program.properties.Language;
import enums.FrameType;

public class EditButtons extends JPanelExtension
{

   private JButton i_ButtonAdd;
   private JButton i_ButtonEdit;
   private JButton i_ButtonDelete;
   private ISelected i_SelectedBO = null;
   private ProgramCreator i_PC = null;

   public EditButtons(String compGroupName, List<IBusinessClass> list,
         ISQLUtil sqlobj, ISelected selected, ProgramCreator pc)
   {
      i_BOList = list;
      i_SQLUtil = sqlobj;
      i_SelectedBO = selected;
      i_PC = pc;
      addButtons(compGroupName);
   }

   // TODO prio 3 Make add edit new buttons by new framework (and OK Cancel too)

   public static void openEditFrame(String compGroupName, ProgramCreator pc,
         ISelected selected)
   {
      if (selected != null)
      {
         IFrame editFrame = pc.getIFrameFromBO(compGroupName,
               FrameType.FormEdit, selected.getSelectedBO());
         if (editFrame != null)
         {
            editFrame.setVisible(true);
         }
      }
   }

   private void openNewFrame(String compGroupName, ProgramCreator pc,
         ISelected selected)
   {
      if (selected != null)
      {
         IFrame newFrame = pc.getIFrameFromBO(compGroupName,
               FrameType.FormNew, selected.getNewInstance());
         if (newFrame != null)
         {

            newFrame.setVisible(true);
         }
      }
   }

   private void addButtons(String compGroupName)
   {
      final String name = compGroupName;
      i_ButtonAdd = new JButton(Language.getText(compGroupName, "btn.add",
            "Add"));
      add(i_ButtonAdd);
      i_ButtonAdd.addActionListener(new ActionListener()
      {
         public void actionPerformed(ActionEvent e)
         {
            openNewFrame(name, i_PC, i_SelectedBO);
         }
      });

      i_ButtonEdit = new JButton(Language.getText(compGroupName, "btn.edit",
            "Edit"));
      add(i_ButtonEdit);
      i_ButtonEdit.addActionListener(new ActionListener()
      {
         public void actionPerformed(ActionEvent e)
         {
            // Edit code goes here
            openEditFrame(name, i_PC, i_SelectedBO);
         }

      });

      i_ButtonDelete = new JButton(Language.getText(compGroupName,
            "btn.delete", "Delete"));
      add(i_ButtonDelete);
      i_ButtonDelete.addActionListener(new ActionListener()
      {

         public void actionPerformed(ActionEvent e)
         {
            if (i_SelectedBO.getSelectedBO() != null)
            {
               // TODO prio 2 use getLangText on confirm delete question
               // TODO prio 3 us getLangText on BO.toString text
               int answer = JOptionPane.showConfirmDialog(null,
                     "Are you really sure you want to delete "
                           + i_SelectedBO.getSelectedBO() + " ?");
               if (answer == JOptionPane.OK_OPTION)
               {
                  i_SelectedBO.getSelectedBO().deleteFromDB();
                  ProgramCreator.GlobalFrame.reLoad();
               }
            }
         }
      });
   }

}
