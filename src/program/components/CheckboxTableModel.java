package program.components;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class CheckboxTableModel extends DefaultTableModel
{
   private int i_columnindex;

   /**
    * @param columnNames
    * @param i
    */
   public CheckboxTableModel(String[] columnNames, int columnindexForCheckbox)

   {
      super(columnNames, columnNames.length);
      i_columnindex = columnindexForCheckbox;
   }

   @SuppressWarnings("unchecked")
   public CheckboxTableModel(Vector columnNames, Vector values,
         int columnindexForCheckbox)
   {
      super(columnNames, values);
      i_columnindex = columnindexForCheckbox;
   }

   public CheckboxTableModel()
   {
      super();
   }

   @Override
   public Object getValueAt(int row, int col)
   {
      if (col == i_columnindex)
      {
         Object temp = super.getValueAt(row, col);
         if (temp == null
               || (temp instanceof Boolean && temp.equals(Boolean.FALSE)))
         {
            return Boolean.FALSE;
         }
         else
         {
            return Boolean.TRUE;
         }
      }
      else
      {
         return super.getValueAt(row, col);
      }
   } /*
      * JTable uses this method to determine the default renderer/ editor for
      * each cell. If we didn't implement this method, then the last column
      * would contain text ("true"/"false"), rather than a check box.
      */

   @SuppressWarnings("unchecked")
   @Override
   public Class getColumnClass(int c)
   {
      if (c == i_columnindex)
      {
         return Boolean.class;
      }
      else
      {
         return super.getColumnClass(c);
      }
   } /*
      * Don't need to implement this method unless your table's data can change.
      */

   @Override
   public void setValueAt(Object value, int row, int col)
   {
      if (col == i_columnindex)
      {
         super.setValueAt(value, row, col);
      }
      else
      {
         super.setValueAt(value, row, col);
      }

   }

}
