package program.components;

import interfaces.IBOList;
import interfaces.IBusinessClass;
import interfaces.IColumn;
import interfaces.ISQLUtil;
import interfaces.ISelected;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.util.List;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

import program.ProgramCreator;
import program.components.extension.JPanelExtension;
import renderer.FormatRenderer;
import enums.ComponentType;

public class TablePanel extends JPanelExtension implements ISelected
{

   @Override
   public void reLoad()
   {
      super.reLoad();
      i_BOList = i_IBOList.getBOList();
      initTable(i_BOList);
   }

   private JTable i_JTable;

   private IBusinessClass i_SelectedBO = null;

   private IBOList i_IBOList = null;

   private ProgramCreator i_PC = null;

   private final String i_CompGroupName;

   public IBusinessClass getSelectedBO()
   {
      return i_SelectedBO;
   }

   public TablePanel(String compGroupName, List<IBusinessClass> list,
         ISQLUtil sqlobj, IBOList ibo, ProgramCreator pc)
   {
      i_CompGroupName = compGroupName;
      i_BOList = list;
      i_SQLUtil = sqlobj;
      i_IBOList = ibo;
      i_PC = pc;
      addTable();
   }

   private void mouseClickedAction(MouseEvent evt)
   {
      int row = i_JTable.rowAtPoint(evt.getPoint());

      if (row == -1)
      {
         return;
      }

      if (evt.getClickCount() == 1)
      {
         // Single Click
         i_SelectedBO = ((IBusinessClass) i_JTable.getValueAt(row, 0));
      }

      // Double Click
      if (evt.getClickCount() == 2)
      {
         i_SelectedBO = ((IBusinessClass) i_JTable.getValueAt(row, 0));
         // TODO prio 3 Move openEditFrame
         EditButtons.openEditFrame(i_CompGroupName, i_PC, this);
      }
   }

   private void addTable()
   {
      i_JTable = new JTable();
      ProgramCreator.GlobalTable = i_JTable;

      // TODO prio 2 Do not allow multiple line select in JTable
      initTable(i_BOList);

      Dimension sizedim = new Dimension(1000, 600);
      i_JTable.setPreferredScrollableViewportSize(sizedim);
      i_JTable.setFillsViewportHeight(true);

      i_JTable.addMouseListener(new MouseAdapter()
      {
         @Override
         public void mouseClicked(MouseEvent evt)
         {
            mouseClickedAction(evt);
         }
      });

      // Create the scroll pane and add the table to it.
      JScrollPane scrollPane = new JScrollPane(i_JTable);
      scrollPane.setPreferredSize(sizedim);

      // Add the scroll pane to this panel.
      add(scrollPane);

   }

   private void initTable(List<IBusinessClass> list)
   {
      Vector<String> headers = getHeaderVector(list);
      Vector<Object> data = getDataVector(list);

      i_JTable.setModel(new DefaultTableModelNotEditable(data, headers));
      // TableColumn var_col;
      // var_col = i_JTable.getColumnModel().getColumn(5);
      // final JCheckBox check = new JCheckBox();
      // var_col.setCellEditor(new DefaultCellEditor(check));
      //
      // var_col.setCellRenderer(new DefaultTableCellRenderer()
      // {
      // @Override
      // public Component getTableCellRendererComponent(JTable table,
      // Object value, boolean isSelected, boolean hasFocus, int row,
      // int column)
      // {
      // check.setSelected(((Boolean) value).booleanValue());
      // return check;
      // }
      // });

      TableColumnModel tcm = i_JTable.getColumnModel();
      alignColumns(tcm);
   }

   private Vector<Object> getDataVector(List<IBusinessClass> list)
   {
      Vector<Object> data = new Vector<Object>();
      for (IBusinessClass bo2 : list)
      {
         Vector<Object> subdata = new Vector<Object>();
         subdata.add(bo2);
         for (IColumn item : bo2.getColumnList())
         {
            subdata.add(item.getIComponent(i_CompGroupName,
                  ComponentType.TableInputNotEditable));
         }
         data.add(subdata);
      }
      return data;
   }

   private Vector<String> getHeaderVector(List<IBusinessClass> list)
   {
      Vector<String> headers = new Vector<String>();
      if (list.size() > 0)
      {
         headers.add("");
         IBusinessClass bo = list.get(0);
         for (IColumn item : bo.getColumnList())
         {
            headers.add(item.getLangText(i_CompGroupName));
         }
      }
      return headers;
   }

   private void alignColumns(TableColumnModel tcm)
   {
      // Get first instance in list
      IBusinessClass bo = i_BOList.get(0);

      // 0 is hidden object column, start with 1
      int i = 1;
      // TODO prio 3 column widths as well
      tcm.getColumn(0).setMaxWidth(0);
      tcm.getColumn(0).setPreferredWidth(0);
      for (IColumn col : bo.getColumnList())
      {
         NumberFormat formatter = NumberFormat.getInstance();
         FormatRenderer formatRend = new FormatRenderer(formatter);
         formatRend.setHorizontalAlignment(col.getHorizontalAlignment());
         tcm.getColumn(i++).setCellRenderer(formatRend);
      }
   }

   @Override
   public IBusinessClass getNewInstance()
   {
      if (i_IBOList == null)
      {
         if (i_BOList != null && i_BOList.size() > 0)
         {
            IBusinessClass bo = i_BOList.get(0);
            return bo.getNewInstance();
         }
         else
         {
            return null;
         }
      }
      else
      {
         return i_IBOList.getNewInstance();
      }

   }

}
