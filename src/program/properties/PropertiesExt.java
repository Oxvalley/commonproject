package program.properties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

public class PropertiesExt extends Properties
{

   @Override
   public String getProperty(String key, String defaultValue)
   {
      return super.getProperty(key.toLowerCase(), defaultValue);
   }

   @Override
   public String getProperty(String key)
   {
      return super.getProperty(key.toLowerCase());
   }

   private String i_Filename = null;

   private boolean i_isDirty = false;

   private static HashMap<String, PropertiesExt> i_Map = new HashMap<String, PropertiesExt>();

   public PropertiesExt(String filename)
   {
      load(filename);
   }

   private void load(String filename)
   {
      try
      {
         i_Filename = filename + ".properties";

         // if file is not found, it will be created
         if (new File(i_Filename).exists())
         {
            super.load(new FileReader(i_Filename));
         }

      }
      catch (FileNotFoundException e)
      {
         System.out.println("File " + filename
               + " not found. Should never happen...");
      }
      catch (IOException e)
      {
         System.out.println("File " + i_Filename + " not found. IOException");
      }
   }

   public PropertiesExt()
   {
      super();
   }

   @Override
   @SuppressWarnings("unchecked")
   public synchronized Enumeration keys()
   {
      Enumeration keysEnum = super.keys();
      Vector keyList = new Vector();
      while (keysEnum.hasMoreElements())
      {
         keyList.add(keysEnum.nextElement());
      }
      Collections.sort(keyList);
      return keyList.elements();
   }

   public static void StoreAll()
   {
      Collection<PropertiesExt> set = i_Map.values();
      for (PropertiesExt prop : set)
      {
         if (prop.isDirty())
         {
            prop.Store();
         }
      }
   }

   public void Store()
   {
      try
      {
         store(new FileWriter(i_Filename), "Created by DB2BO framework");
      }
      catch (IOException e)
      {
         System.out.println("File " + i_Filename
               + " could not be saved. IOException");
      }
      i_isDirty = false;
   }

   public boolean isDirty()
   {
      return i_isDirty;
   }

   @Override
   public synchronized Object setProperty(String key, String value)
   {
      i_isDirty = true;
      return super.setProperty(key.toLowerCase(), value);
   }

   public static PropertiesExt getPropFile(String propFilename)
   {
      PropertiesExt temp = i_Map.get(propFilename);
      if (temp == null)
      {
         temp = new PropertiesExt(propFilename);
         i_Map.put(propFilename, temp);
      }
      return temp;
   }

   public static List<String> getListFromProps(PropertiesExt props,
         String prefix)
   {
      ArrayList<String> retVal = new ArrayList<String>();

      String key = prefix + "count";
      String count = props.getProperty(key);
      if (count == null)
      {
         System.out.println("Expected to find tag " + key + ", but didn't.");
         return null;
      }
      try
      {
         int countInt = Integer.parseInt(count);
         for (int i = 0; i < countInt; i++)
         {
            String key2 = prefix + i;
            String tag = props.getProperty(key2);
            if (tag == null)
            {
               System.out.println("Expected to find tag " + key2
                     + ", but didn't.");
            }
            retVal.add(tag);
         }
      }
      catch (NumberFormatException e)
      {
         System.out.println(key + " is not an integer.");
         return null;
      }
      
      return retVal;
   }

}
