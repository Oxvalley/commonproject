package program.properties;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import common.utils.SystemUtil;

public class ComputerSpecificProperies extends Properties
{
   private String i_ComputerName = SystemUtil.getComputerName();
   private String i_PropFile;

   public ComputerSpecificProperies(String propFile)
   {
      File f = new File(propFile);
      i_PropFile = propFile;
      if (f.exists())
      {
         try
         {
            load(new FileReader(propFile));
         }
         catch (FileNotFoundException e)
         {
         }
         catch (IOException e)
         {
         }
      }
   }

   @Override
   public String getProperty(String keyPart, String defaultValue)
   {
      String key = i_ComputerName + "." + keyPart;
      String value = super.getProperty(key);
      if (value == null)
      {
         value = defaultValue;
         setProperty(key, value);
         try
         {
            store(new FileWriter(i_PropFile), "Saved by osutils2");
         }
         catch (IOException e)
         {
         }
      }
      return value;
   }

   @Override
   public synchronized Object setProperty(String key, String value)
   {
      return super.setProperty(i_ComputerName + "." + key, value);
   }

   public void store()
   {
      try
      {
         store(new FileWriter(i_PropFile), "saving");
      }
      catch (IOException e)
      {
         System.out.println("Could not save property file:\n" + e.getMessage());
      }

   }

}
