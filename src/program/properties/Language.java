package program.properties;

public class Language
{

   public static String getLanguage()
   {
      String retVal = getGeneralText("language");
      if (retVal == null)
      {
         retVal = getGeneralText("language.default");
         if (retVal == null)
         {
            // Still no language, set English as default
            retVal = "eng";
            setGeneralText("language", retVal);
            setGeneralText("language.default", retVal);
         }
      }
      return retVal;
   }

   private static void setGeneralText(String key, String value)
   {
      PropertiesExt prop = PropertiesExt.getPropFile("general");
      prop.setProperty(key, value);
   }

   private static String getAddedAtAt(String columnName)
   {
      return "@@" + columnName.toLowerCase() + "@@";
   }

   // TODO prio 3 Make compGroupName a class instead

   public static String getText(String compGroupName, String key)
   {
      return getText(compGroupName, key, null);
   }

   public static String getLangText(String key, String defaultValue)
   {
      PropertiesExt lang = PropertiesExt.getPropFile(getLanguage());
      String retVal = lang.getProperty(key);
      if (retVal != null)
      {
         return retVal;
      }
      else
      {
         if (defaultValue != null)
         {
            lang.setProperty(key, defaultValue);
         }
         return defaultValue;
      }

   }

   public static String getLangText(String key)
   {
      return getLangText(key, null);
   }

   public static String getGeneralText(String key, String defaultValue)
   {
      PropertiesExt prop = PropertiesExt.getPropFile("general");
      String retVal = prop.getProperty(key);
      if (retVal == null)
      {
         return defaultValue;
      }
      else
      {
         return retVal;
      }
   }

   public static String getGeneralText(String key)
   {
      return getGeneralText(key, null);
   }

   public static String getText(String compGroupName, String key,
         String defaultValue)
   {
      return getTextWithLangWord(compGroupName, key, defaultValue, defaultValue);
   }

   private static String replaceAtAt(String retVal)
   {
      String value = replaceFirstAtAt(retVal);
      while (value.contains("@@"))
      {
         value = replaceFirstAtAt(value);
      }
      return value;
   }

   private static String replaceFirstAtAt(String retVal)
   {
      int i = retVal.indexOf("@@");
      if (i == -1)
      {
         // No @@ exists
         return retVal;
      }
      else
      {
         if (i >= retVal.length() - 5)
         {
            // No room for start and stop @@ and something in between
            return retVal;
         }
         int i2 = retVal.substring(i + 2).indexOf("@@") + 2;
         if (i2 == -1)
         {
            // No second @@ exists
            return retVal;
         }
         else
         {
            String replacable = retVal.substring(i + 2, i2);
            String newText = getLangText(replacable, replacable);
            if (newText.contains("@@"))
            {
               // TODO prio 3 Allow @@ in lang.properties values
               System.out.println("Error! Lang value can not contain @@ ("
                     + replacable + "=" + newText + ")");
               newText = newText.replace("@@", "");
            }

            return retVal.substring(0, i) + newText + retVal.substring(i2 + 2);
         }
      }
   }

   public static String getTextWithLangWord(String compGroupName, String key,
         String defaultValue, String langword)
   {
      if (compGroupName.equalsIgnoreCase("general"))
      {
         String ret = "Group 'general' not allowed!!";
         System.out.println(ret);
         return ret;
      }

      PropertiesExt prop = PropertiesExt.getPropFile(compGroupName);
      String retVal = prop.getProperty(key);
      if (retVal != null)
      {
         // Is there @@...@@ in the file?
         return replaceAtAt(retVal);
      }
      else
      {
         // Property not found
         // Set @@key@@...
         if (defaultValue != null)
         {
            prop.setProperty(key, getAddedAtAt(defaultValue));

            // ...and return the real value in Lang file
            return getLangText(defaultValue, langword);
         }
         else
         {
            return null;
         }
      }
   }

}
