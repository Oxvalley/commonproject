package program;

import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import common.utils.DateUtil;
import common.utils.FileUtil;
import common.utils.StringUtil;
import db.SQLACCESSUtil;
import db.ext.ColumnAutoIncrExt;
import db.ext.ColumnBooleanExt;
import db.ext.ColumnDateExt;
import db.ext.ColumnDoubleExt;
import db.ext.ColumnIntExt;
import db.ext.ColumnStringExt;
import db.tables.BasicTable;
import interfaces.IColumn;
import interfaces.IColumnExt;
import interfaces.ISQLUtil;
import main.OSFileWriter;

public class CreateBOs
{

   // TODO prio 4 check DB: - Warn if table has no primary key

   // TODO prio 4 check DB: - Warn if column contains world 'Date' but is no
   // Date column

   // TODO prio 4 MySQL and SQLServer as well

   // TODO prio 2 Generate test class to edit all tables

   // TODO prio 1 Scrollbar under buttons

   // TODO prio 1 List of icons

   // TODO prio 2 Make top 100 an option in MainProgram

   // TODO prio 1 Title and icon over table

   // TODO prio 1 Main window higher or table lower

   // TODO prio 3 First kolumn visible yet

   // TODO prio 2 Test everything

   // TODO prio 4 Own directories for general and lang property files

   // TODO prio 3 Browse for file

   // TODO prio 3 ComboBox when required

   // TODO prio 3 Make getVersion and show in property file comments (and
   // property)

   // TODO prio 3 jar of JCalendar

   // TODO prio 4 Test medlem06.mdb

   // TODO prio 3 Graphical interface for creating classes

   // TODO prio 4 T in listing

   // TODO prio 2 Check package for auto-generated classes

   // TODO prio 3 remove FrameType and make functions of all frames

   // TODO prio 3 Be able to Filter listings

   // TODO prio 4 Write protect generated files

   // TODO prio 3 Sort of JTable on column header click

   // TODO prio 4 Graphical interface for maintaining property files

   // TODO prio 3 User Manual

   // TODO prio 3 Change Icon for program

   // TODO prio 4 Make TabelFrame and FormFrame own classes

   // TODO prio 4 Check if DB and source folder exists

   private String i_SourceFolder;
   private String i_DBPath;

   public CreateBOs(String dbPath, String sourceFolder)
   {
      i_DBPath = dbPath;
      i_SourceFolder = sourceFolder;
   }

   /**
    * @param args
    */
   public static void main(String[] args)
   {
      CreateBOs
            .createFiles("C:/All Projects/svn/vb/bestalla/mb1.mdb",
                  "C:/Documents and Settings/Lars Svensson/workspace34RC1/db2bo/src/bestalla/"); // Eclipse
      // "C:/All Projects/netbeansworkspace/db2bo/src/bestalla/"); // Netbeans

   }

   public static void createFiles(String dbPath, String sourceFolder)
   {
      CreateBOs creBO = new CreateBOs(dbPath, sourceFolder);
      creBO.createFiles();
   }

   private void createFiles()
   {
      SQLACCESSUtil.connectAccessDB(i_DBPath);
      ISQLUtil sqlobj = SQLACCESSUtil.getConnectedObject();

      ArrayList<String> lst = sqlobj.getAllTableNames();

      createTableManager(lst);

      for (String tableName : lst)
      {
         String className = StringUtil.getVarName(tableName);
         creteTableClass(sqlobj, className);
      }

   }

   private void createTableManager(ArrayList<String> lst)
   {
      OSFileWriter fw;
      try
      {
         fw = new OSFileWriter(i_SourceFolder + "TableManager.java");

         fw.writeln("package "
               + FileUtil.getLastFolder(i_SourceFolder).toLowerCase()
               + ";\n");

         fw.writeln("import interfaces.IBusinessClass;");
         fw.writeln("import interfaces.ITableManager;");
         fw.writeln("import db.tables.BasicTableManager;");
         fw.writeln("import interfaces.ISQLUtil;\n");
         fw.writeln("import db.SQLACCESSUtil;\n");

         fw.writeln("");

         fw.writeln("// Created " + new Date());

         fw
               .writeln("public class TableManager extends BasicTableManager implements ITableManager{");

         String tableNames = "";
         fw.writeln("   public static enum TableNames {");
         for (String tableName : lst)
         {
            String className = StringUtil.getVarName(tableName);
            if (tableNames.length() > 0)
            {
               tableNames += ", ";
            }
            tableNames += className;
         }
         fw.writeln("      " + tableNames);
         fw.writeln("   }\n");

         fw.writeln("   public TableManager(ISQLUtil sqlobj) {");
         fw.writeln("      i_SQLObj = sqlobj;");
         fw.writeln("      init();");
         fw.writeln("   }\n");

         fw.writeln("   public TableManager() {");
         fw.writeln("      init();");
         fw.writeln("   }\n");

         fw.writeln("   public TableManager(String DBName) {");
         fw.writeln("      i_DBName = DBName;");
         fw.writeln("      i_SQLObj = new SQLACCESSUtil(DBName);");
         fw.writeln("      init();");
         fw.writeln("   }\n");

         fw.writeln("   private void init() {");
         fw.writeln("      i_SQLObj = getConnection();");
         for (String tableName : lst)
         {
            String className = StringUtil.getVarName(tableName);
            fw.writeln("      i_TableList.add(new " + className + "());");
         }
         fw.writeln("   }\n");

         fw.writeln("   public IBusinessClass getTable(TableNames id) {");
         fw.writeln("      return i_TableList.get(id.ordinal());");
         fw.writeln("   }\n");

         fw.writeln("}");
         fw.flush();
         fw.close();
      }
      catch (IOException e)
      {
         System.out.println("File TableManager.java could not be created");
         e.printStackTrace();
      }
   }

   private void creteTableClass(ISQLUtil sqlobj, String className)
   {
      List<IColumnExt> lstCol = getColumnList(sqlobj, className);
      // List<IColumn> lstIcol = getIColumnList(lstCol);

      boolean hasPrimaryKey = hasPrimaryKey(lstCol);

      OSFileWriter fw;
      try
      {
         fw = new OSFileWriter(i_SourceFolder + className + ".java");

         fw.writeln("package "
               + FileUtil.getLastFolder(i_SourceFolder).toLowerCase()
               + ";\n");

         fw.writeln("import interfaces.IBusinessClass;");
         fw.writeln("import interfaces.IColumn;\n");

         fw.writeln("import java.sql.ResultSet;");
         fw.writeln("import java.sql.Types;");
         fw.writeln("import java.util.ArrayList;");
         fw.writeln("import java.util.List;");
         fw.writeln("import db.tables.BasicTable;");
         fw.writeln("import program.properties.Language;\n");

         for (IColumnExt col : lstCol)
         {
            String extraImports = col.getExtraImports();
            if (extraImports != null && extraImports.length() != 0)
            {
               fw.writeln(extraImports);
            }
         }

         fw.writeln("");

         ArrayList<String> tmplst = new ArrayList<String>();
         for (IColumnExt col : lstCol)
         {
            if (!tmplst.contains(col.getClassName()))
            {
               fw.writeln("import db.columns." + col.getClassName() + ";");
               tmplst.add(col.getClassName());
            }
         }

         fw.writeln("import interfaces.ISQLUtil;\n");
         fw.writeln("import db.SQLACCESSUtil;\n");
         fw.writeln("// Created " + new Date());

         fw.writeln("public class " + className
               + " extends BasicTable implements IBusinessClass {\n");

         fw.writeln("   public  static enum ColumnNames {");
         String colNames = "";
         for (IColumnExt col : lstCol)
         {
            if (colNames.length() > 0)
            {
               colNames += ", ";
            }
            colNames += col.getVarName();
         }
         fw.writeln("      " + colNames);
         fw.writeln("   }\n");

         fw.writeln("   private static final String i_TableName = \""
               + className + "\";\n");

         for (IColumnExt col : lstCol)
         {
            fw.writeln(col.getIColumnImplDeclaration() + "\n");
         }

         fw.writeln("\n   public " + className + "() {");
         fw.writeln("		init();");
         fw.writeln("   }");

         String primaryKeysAndType = getJavaTypeAndVar(lstCol, true);
         String primaryKeys = getVarInstances(lstCol, true);

         String keysAndType = getJavaTypeAndVar(lstCol, false);

         if (hasPrimaryKey)
         {
            // If there is no primary keys this will duplicate previous
            // function

            // also, if all are primary keys, this will never be used
            if (!keysAndType.equals(primaryKeysAndType))
            {
               fw.writeln("\n   public " + className + "(" + primaryKeysAndType
                     + ") {");
               fw.writeln("		init" + className + "(" + primaryKeys + ");");
               fw.writeln("   }");

               String primaryKeysCompared = getPrimaryColumnsSQL(lstCol);

               fw.writeln("\n   private void init" + className + "("
                     + primaryKeysAndType + ") {");
               fw
                     .writeln("   ArrayList<IBusinessClass> lst = getBOList(i_SQLObj, \""
                           + primaryKeysCompared + "\");");
               fw.writeln("   if (lst.size() != 1) {");
               fw.writeln("      System.out.println(\"Bad number of "
                     + className + " objects (\" + lst.size()");
               fw.writeln("      + \"). One expected.\");");
               fw.writeln("      return;");
               fw.writeln("      }");
               fw.writeln("   copy((" + className + ")lst.get(0));");
               fw.writeln("   }");
            }
         }
         // Will be duplicate if these are the same
         fw.writeln("\n   public " + className + "(" + keysAndType + ") {");
         fw.writeln("		this();");
         for (IColumnExt col : lstCol)
         {
            fw.writeln("      ic_" + col.getVarName() + ".setValue("
                  + col.getVarInstanceName() + ");");
         }
         fw.writeln("   }");

         fw.writeln("\n   public " + className + "(ISQLUtil sqlobj) {");
         fw.writeln("		this();");
         fw.writeln("      i_SQLObj = sqlobj;");
         fw.writeln("   }");

         fw.writeln("\n   private void init(){");
         fw.writeln("      i_SQLObj = getConnection();");
         for (IColumnExt col : lstCol)
         {
            fw.writeln("      i_ColumnList.add(ic_" + col.getVarName() + ");");
         }
         fw.writeln("   }");

         fw.writeln("\n   public static String getListAsString(ArrayList<"
               + className + "> lst) {");
         fw.writeln("      String line = \"\";");
         fw.writeln("      for (IBusinessClass businessObject : lst) {");
         fw.writeln("         line += businessObject.toStringLine();");
         fw.writeln("      }");
         fw.writeln("      return line;");
         fw.writeln("   }");

         fw.writeln("\n   public ArrayList<IBusinessClass> getBOList() {");
         fw.writeln("      return getBOList(\"*\", \"\");");
         fw.writeln("   }");

         fw
               .writeln("\n   public ArrayList<IBusinessClass> getBOList(ISQLUtil sqlobj, String where) {");
         fw.writeln("      return getBOList(sqlobj, \"*\", where);");
         fw.writeln("   }");

         fw
               .writeln("\n   public ArrayList<IBusinessClass> getBOList(ISQLUtil sqlobj,");
         fw.writeln("   		String fields, String where) {");
         fw
               .writeln("   	ArrayList<IBusinessClass> lst = new ArrayList<IBusinessClass>();");
         fw
               .writeln("   	String SQL = \"SELECT \" + fields + \" FROM [\" + i_TableName + \"]\" + where;");
         fw.writeln("   	ResultSet rs = sqlobj.SQLCallRS(SQL);");
         fw.writeln("\n      while (sqlobj.next(rs)) {");
         fw.writeln("         " + className + " "
               + StringUtil.getVarInstanceName(className, className)
               + " = new " + className + "(");
         String RSString = "";
         for (IColumnExt col : lstCol)
         {
            if (RSString.length() != 0)
            {
               RSString += ", \n";
            }
            RSString += "            sqlobj." + col.getRSString() + "(\""
                  + col.getColumnName() + "\")";
         }

         fw.writeln(RSString);
         fw.writeln("         );");
         fw.writeln("         lst.add("
               + StringUtil.getVarInstanceName(className, className) + ");");
         fw.writeln("      }");
         fw.writeln("      return lst;");
         fw.writeln("   }");

         fw
               .writeln("\n   public ArrayList<IBusinessClass> getBOList(String where) {");
         fw.writeln("   	   return getBOList(\"*\", where);");
         fw.writeln("   }");

         fw
               .writeln("\n   public ArrayList<IBusinessClass> getBOList(String fields, String where) {");
         fw
               .writeln("   	  ISQLUtil sqlobj = SQLACCESSUtil.getConnectedObject();");
         fw.writeln("      return getBOList(sqlobj, fields, where);");
         fw.writeln("   }");

         fw.writeln("\n   public static void printList(ArrayList<" + className
               + "> lst) {");
         fw.writeln("      String line = \"\";");
         fw.writeln("      if (lst.size() > 0) {");
         fw
               .writeln("         List<IColumn> cols = lst.get(0).getColumnList();");
         fw.writeln("         for (IColumn column : cols) {");
         fw.writeln("            line += column.getColumnName() + \"\t\";");
         fw.writeln("         }");
         fw.writeln("      }");
         fw
               .writeln("      System.out.println(line + \"\\n\" + getListAsString(lst));");
         fw.writeln("   }");

         fw.writeln("\n   public void copy(" + className + " source) {");
         fw.writeln("      init();");

         for (IColumnExt col : lstCol)
         {
            fw.writeln("      this.set" + col.getVarName() + "(source."
                  + col.getGetFunctionName() + "());");
         }
         fw.writeln("   }");

         for (IColumnExt col : lstCol)
         {
            fw.writeln("\n   " + getGetter(col));
            fw.writeln("\n   " + getSetter(col));
         }

         fw.writeln("\n   public String getTableName() {");
         fw.writeln("      return i_TableName;");
         fw.writeln("   }");

         fw.writeln("\n   @Override");
         fw.writeln("   public void saveToDB() {");
         fw.writeln("         saveToDB(i_SQLObj, i_TableName, i_ColumnList);");
         fw.writeln("   }");

         fw.writeln("\n   public IColumn getColumn(ColumnNames id) {");
         fw.writeln("         return i_ColumnList.get(id.ordinal());");
         fw.writeln("   }");

         fw.writeln("\n   @Override");
         fw.writeln("   public String toString() {");
         fw.writeln("      String retval = \"" + className
               + " Object data:\\n\";");
         for (IColumnExt col : lstCol)
         {
            fw.writeln("      retval += \"" + col.getVarName() + ": \" + "
                  + col.getGetFunctionName() + "() + \"\\n\";");
         }
         fw.writeln("      return retval;");
         fw.writeln("   }");

         fw.writeln("\n   public String toStringLine() {");
         fw.writeln("      String retval = \"\";");
         for (IColumnExt col : lstCol)
         {
            fw.writeln("      retval += " + col.getGetFunctionName()
                  + "() + \"\\t\";");
         }
         fw.writeln("      return retval + \"\\n\";");
         fw.writeln("   }");

         fw.writeln("\n   @Override");
         fw.writeln("   public String getLangText(String compGroupName) {");
         fw
               .writeln("      return Language.getText(compGroupName, \"tbl.\" + i_TableName + \".name\",i_TableName);");
         fw.writeln("   }");

         fw.writeln("\n   public IBusinessClass getNewInstance() {");
         fw.writeln("      return new " + className + "();");
         fw.writeln("   }");

         fw.writeln("\n   public void deleteFromDB() {");
         fw.writeln("      deleteFromDB(i_SQLObj, i_TableName, i_ColumnList);");
         fw.writeln("   }");

         fw.writeln("}");
         fw.flush();
         fw.close();
      }
      catch (IOException e)
      {
         System.out.println("File " + className + ".java could not be created");
         e.printStackTrace();
      }
   }

   private String getPrimaryColumnsSQL(List<IColumnExt> lstCol)
   {
      String primaryKeysCompared = "";
      for (IColumnExt col : lstCol)
      {
         if (col.isPrimaryKey())
         {
            if (primaryKeysCompared.length() != 0)
            {
               primaryKeysCompared += " and ";
            }
            primaryKeysCompared += col.getColumnNameSafe() + "="
                  + BasicTable.getQuoted(col) + "\" + "
                  + col.getVarInstanceName() + " + \""
                  + BasicTable.getQuoted(col);
         }
      }

      primaryKeysCompared = "where " + primaryKeysCompared;
      return primaryKeysCompared;
   }

   private boolean hasPrimaryKey(List<IColumnExt> lstCol)
   {
      for (IColumnExt columnExt : lstCol)
      {
         if (columnExt.isPrimaryKey())
         {
            return true;
         }
      }
      return false;
   }

   private String getGetter(IColumnExt col)
   {
      String retVal = col.getExtraGetter();

      retVal += "public " + col.getJavaType() + " " + col.getGetFunctionName()
            + "() {\n";
      retVal += "      return ic_" + col.getVarName() + ".getValue();\n";
      retVal += "   }";
      return retVal;
   }

   private String getSetter(IColumnExt col)
   {
      String retVal = col.getExtraSetter();

      retVal += "public void set" + col.getVarName() + "(" + col.getJavaType()
            + " value) {\n";
      retVal += "      ic_" + col.getVarName() + ".setValue(value);\n";
      retVal += "   }";
      return retVal;
   }

   @SuppressWarnings("unused")
   private List<IColumn> getIColumnList(List<IColumnExt> lstCol)
   {
      List<IColumn> lst = new ArrayList<IColumn>();
      for (IColumnExt columnExt : lstCol)
      {
         lst.add(columnExt);
      }
      return lst;
   }

   private static String getJavaTypeAndVar(List<IColumnExt> lstCol,
         boolean onlyPrimaryKeys)
   {
      String primaryKeys = "";
      for (IColumnExt col : lstCol)
      {
         if (!onlyPrimaryKeys || col.isPrimaryKey())
         {
            if (primaryKeys.length() > 0)
            {
               primaryKeys += ", ";
            }
            primaryKeys += col.getJavaType() + " " + col.getVarInstanceName();
         }
      }
      return primaryKeys;
   }

   private static String getVarInstances(List<IColumnExt> lstCol,
         boolean onlyPrimaryKeys)
   {
      String primaryKeys = "";
      for (IColumnExt col : lstCol)
      {
         if (!onlyPrimaryKeys || col.isPrimaryKey())
         {
            if (primaryKeys.length() > 0)
            {
               primaryKeys += ", ";
            }
            primaryKeys += col.getVarInstanceName();
         }
      }
      return primaryKeys;
   }

   private List<IColumnExt> getColumnList(ISQLUtil sqlobj, String tableName)
   {
      return getColumnList(sqlobj, "*", tableName, "");
   }

   public static List<IColumnExt> getColumnList(ISQLUtil sqlobj, String fields,
         String tableName, String where)
   {
      String whereString = where;
      if (where == null || where.length() == 0)
      {
         whereString = "";
      }
      else
      {
         if (!whereString.trim().toUpperCase().startsWith("WHERE "))
         {
            whereString = " where " + whereString;
         }
      }
      String SQL = "select " + fields + " from [" + tableName + "] where 1=0"
            + whereString;
      ResultSet rs = sqlobj.SQLCallRS(SQL);

      return getColumnList(sqlobj, rs, SQL, tableName);
   }

   public static List<IColumnExt> getColumnList(ISQLUtil sqlobj, ResultSet rs,
         String callString, String tableName)
   {
      List<IColumnExt> lst = new ArrayList<IColumnExt>();

      int i = 0;
      try
      {

         ArrayList<String> PKList = getPrimaryKeys(sqlobj, tableName);

         HashMap<String, String> DefList = getDefaultValues(sqlobj, tableName);

         ResultSetMetaData rsMeta = rs.getMetaData();
         for (i = 1; i <= rsMeta.getColumnCount(); i++)
         {
            lst
                  .add(getColumnImplementor(rsMeta, i, PKList, DefList,
                        tableName));
         }
      }
      catch (SQLException e)
      {
         System.out.println("SQLUtil.SQLCallRS: Debug SQL Exception " + e);
         System.out.println(callString);
         System.out.println(i + "\n");
         return null;
      }
      return lst;
   }

   private static ArrayList<String> getPrimaryKeys(ISQLUtil sqlobj,
         String tableName) throws SQLException
   {
      // Use meta.getIndexInfo() will get you the PK index. Once
      // you know the index, retrieve its column name

      DatabaseMetaData meta = sqlobj.getMetaDB();
      ArrayList<String> lst = new ArrayList<String>();

      String key_colname = null;

      // get the primary key information
      ResultSet rset = meta.getIndexInfo(null, null, tableName, true, true);

      // SQLUtil.printTableInfo(rset, "???", true);
      rset = meta.getIndexInfo(null, null, tableName, true, true);
      while (rset.next())
      {
         String idx = rset.getString(6);
         if (idx != null)
         {
            // Note: index "PrimaryKey" is Access DB specific
            // other db server has diff. index syntax.
            if (idx.equalsIgnoreCase("PrimaryKey"))
            {
               key_colname = rset.getString(9);
               lst.add(key_colname);
            }
         }
      }
      return lst;
   }

   private static HashMap<String, String> getDefaultValues(ISQLUtil sqlobj,
         String tableName)
   {
      return getDefaultValues(sqlobj.getMetaDB(), tableName);

   }

   private static HashMap<String, String> getDefaultValues(
         DatabaseMetaData meta, String tableName)
   {
      HashMap<String, String> lst = new HashMap<String, String>();
      ResultSet rs;
      try
      {
         rs = meta.getColumns(null, null, tableName, null);
         // SQLUtil.printTableInfo(rs, "???", true);
         rs = meta.getColumns(null, null, tableName, null);
         while (rs.next())
         {
            // TABLE_CAT, TABLE_SCHEM, TABLE_NAME, COLUMN_NAME, DATA_TYPE,
            // TYPE_NAME,
            // COLUMN_SIZE, BUFFER_LENGTH, DECIMAL_DIGITS, NUM_PREC_RADIX,
            // NULLABLE,
            // REMARKS, COLUMN_DEF, SQL_DATA_TYPE, SQL_DATETIME_SUB,
            // CHAR_OCTET_LENGTH,
            // ORDINAL_POSITION, IS_NULLABLE, SS_DATA_TYPE

            String table = rs.getString("COLUMN_NAME");
            String def = rs.getString("COLUMN_DEF");
            // if (!table.startsWith("MSys")) {
            // System.out.println(table + " - " + def);
            lst.put(table, def);
            // }
         }
      }
      catch (SQLException ex)
      {
         Logger.getLogger(ISQLUtil.class.getName()).log(Level.SEVERE, null, ex);
      }
      return lst;
   }

   public static ArrayList<String> getColumns(DatabaseMetaData meta,
         String tablename)
   {
      ArrayList<String> lst = new ArrayList<String>();
      ResultSet rs;
      try
      {
         rs = meta.getColumns(null, null, tablename, null);
         SQLACCESSUtil.printTableInfo(rs, "???", true);
         rs = meta.getColumns(null, null, tablename, null);
         while (rs.next())
         {
            // TABLE_CAT, TABLE_SCHEM, TABLE_NAME, COLUMN_NAME, DATA_TYPE,
            // TYPE_NAME,
            // COLUMN_SIZE, BUFFER_LENGTH, DECIMAL_DIGITS, NUM_PREC_RADIX,
            // NULLABLE,
            // REMARKS, COLUMN_DEF, SQL_DATA_TYPE, SQL_DATETIME_SUB,
            // CHAR_OCTET_LENGTH,
            // ORDINAL_POSITION, IS_NULLABLE, SS_DATA_TYPE

            String table = rs.getString("COLUMN_NAME");
            // String def = rs.getString("COLUMN_DEF");
            // if (!table.startsWith("MSys")) {
            // System.out.println(table + " - " + def);
            lst.add(table);
            // }
         }
      }
      catch (SQLException ex)
      {
         Logger.getLogger(ISQLUtil.class.getName()).log(Level.SEVERE, null, ex);
      }
      return lst;
   }

   public static IColumnExt getColumnImplementor(ResultSetMetaData rsMeta,
         int index, ArrayList<String> PKList, HashMap<String, String> defList,
         String tableName) throws SQLException
   {
      int colType = rsMeta.getColumnType(index);
      int colGroup = BasicTable.getColumnTypeGroup(colType);
      IColumnExt retval = null;
      boolean isNullable;

      if (rsMeta.isNullable(index) != 0)
      {
         isNullable = true;
      }
      else
      {
         isNullable = false;
      }
      String colName = rsMeta.getColumnName(index);

      boolean isAutoIncr;
      String columnTypeName = rsMeta.getColumnTypeName(index);

      if (columnTypeName.equalsIgnoreCase("COUNTER"))
      {
         isAutoIncr = true;
      }
      else
      {
         isAutoIncr = false;
      }

      columnTypeName = getCorrectColumnTypeName(colType, columnTypeName);

      String def = defList.get(colName);

      switch (colGroup)
      {
      case BasicTable.ColumnTypeGroup_STRING:
         retval = new ColumnStringExt(tableName, colName, rsMeta
               .getPrecision(index), colType, columnTypeName, isNullable,
               PKList.contains(colName), def);
         break;

      case BasicTable.ColumnTypeGroup_INTEGER:
         int defint = 0;
         if (def != null)
         {
            defint = Integer.parseInt(def);
         }

         if (isAutoIncr)
         {
            retval = new ColumnAutoIncrExt(tableName, colName, colType,
                  columnTypeName);
         }
         else
         {
            retval = new ColumnIntExt(tableName, colName, colType,
                  columnTypeName, isNullable, PKList.contains(colName), defint);
         }
         break;

      case BasicTable.ColumnTypeGroup_DOUBLE:
         double defdouble = 0.0;
         if (def != null)
         {
            defdouble = Double.parseDouble(def);
         }

         retval = new ColumnDoubleExt(tableName, colName, colType,
               columnTypeName, isNullable, PKList.contains(colName), defdouble);
         break;

      case BasicTable.ColumnTypeGroup_DATE:
         Date defdate = null;
         if (def != null)
         {
            defdate = DateUtil.getDateFromString(def);
         }
         retval = new ColumnDateExt(tableName, colName, colType,
               columnTypeName, isNullable, PKList.contains(colName), defdate);
         break;

      case BasicTable.ColumnTypeGroup_BLOB:
         retval = new ColumnStringExt(tableName, colName, rsMeta
               .getPrecision(index), colType, columnTypeName, isNullable,
               PKList.contains(colName), def);
         break;

      case BasicTable.ColumnTypeGroup_BOOLEAN:
         boolean defbool = false;
         if (def != null)
         {
            defbool = Boolean.parseBoolean(def);
         }
         retval = new ColumnBooleanExt(tableName, colName, colType,
               columnTypeName, PKList.contains(colName), defbool);
         break;

      case BasicTable.ColumnTypeGroup_UNKNOWN:
         retval = new ColumnStringExt(tableName, colName, rsMeta
               .getPrecision(index), colType, columnTypeName, isNullable,
               PKList.contains(colName), def);
         break;

      default:
         retval = new ColumnStringExt(tableName, colName, rsMeta
               .getPrecision(index), colType, columnTypeName, isNullable,
               PKList.contains(colName), def);
         break;
      }

      return retval;
   }

   public static String getCorrectColumnTypeName(int colType, String colTypeName)
   {
      switch (colType)
      {
      case 4:
         return "DISTINCT";

      case 93:
         return "DATE";

      default:
         return colTypeName;

      }
   }
}
