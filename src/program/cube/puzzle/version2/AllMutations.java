package program.cube.puzzle.version2;

import java.util.HashMap;
import java.util.Map;

public class AllMutations
{

   private int iCount;
   private int iStartsWith;
   private boolean setup = false;
   private int iEndsWith;
   private int[] iNext;
   private boolean isDone = false;

   public AllMutations(int count)
   {
      this(count, 0);
   }

   public AllMutations(int count, int startsWith)
   {
      iCount = count;
      iStartsWith = startsWith;
      iEndsWith = iStartsWith + iCount - 1;
   }

   public static void main(String[] args)
   {
      AllMutations all = new AllMutations(4, 6);
      int[] next = all.getNext();
      while (next != null)
      {
         printMutation(next);
         next = all.getNext();
      }
   }

   public static void printMutation(int[] next)
   {
      String result = "";
      for (int i : next)
      {
         result += i + ", ";
      }

      System.out.println(result.substring(0, result.length() - 2));
   }

   public int[] getNext()
   {
      if (isDone)
      {
         return null;
      }

      if (!setup)
      {
         iNext = new int[iCount];
         for (int i = 0; i < iNext.length; i++)
         {
            iNext[i] = iStartsWith;
         }

         // Compensate for first add
         iNext[iCount - 1] = iStartsWith - 1;
         setup = true;
      }

      return getNextRecursive();
   }

   private int[] getNextRecursive()
   {
      int index = iCount - 1;
      addOne(index);

      if (isDone)
      {
         return null;
      }

      if (!isAllDifferent())
      {
         return getNextRecursive();
      }
      return iNext;
   }

   private boolean isAllDifferent()
   {
      boolean isOk = true;

      Map<Integer, Integer> freqCount = new HashMap<>();
      for (int i : iNext)
      {
         Integer node = freqCount.get(i);
         int value = 1;
         if (node != null)
         {
            value = freqCount.get(i) + 1;
         }

         freqCount.put(i, value);
      }

      for (Integer key : freqCount.keySet())
      {
         int value = 0;
         if (freqCount.get(key) != null)
         {
            value = freqCount.get(key);
         }

         if (value != 1)
         {
            // System.out.print("Bad: ");
            // printMutation(iNext);
            // System.out.println(key + " is not 1 occurency, but " + value);
            isOk = false;
         }

      }

      return isOk;
   }

   private void addOne(int index)
   {
      iNext[index]++;
      if (iNext[index] > iEndsWith)
      {
         if (index > 0)
         {
            addOne(index - 1);
            iNext[index] = iStartsWith;
         }
         else
         {
            iNext[index]--;
            isDone = true;
         }
      }
   }

}
