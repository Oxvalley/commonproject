package program.cube.puzzle.version2;

public enum OpenScadColors
{

   VIOLET, GREEN, YELLOW, BLACK, PINK, ORANGE, BLUE, RED, GRAY, BROWN, WHITE, LIME, LIGHTBLUE, SILVER, PURPLE
}
