package program.cube.puzzle.version2;

import java.util.ArrayList;
import java.util.List;

public class PixelPiece
{

   private PixelDirectionEnum iDirection;

   public PixelDirectionEnum getDirection()
   {
      return iDirection;
   }

   @Override
   public String toString()
   {
      String line = "Piece " + iId + "\nDirection: " + iDirection + "\n";

      for (int z = 0; z < iPuzzlePixels; z++)
      {
         for (int y = 0; y < iPuzzlePixels; y++)
         {
            for (int x = 0; x < iPuzzlePixels; x++)
            {
               if (isInList(x, y, z))
               {
                  line += "X";
               }
               else
               {
                  line += ".";
               }
            }
            line += "\n";
         }
         line += "\n";
      }
      line += "\n";

      return line;
   }

   private boolean isInList(int x, int y, int z)
   {
      for (Pixel pixel : iPixelList)
      {
         if (pixel.getX() == x && pixel.getY() == y && pixel.getZ() == z)
         {
            return true;
         }
      }
      return false;
   }

   private int iId;
   private int iPuzzlePixels;
   private List<Pixel> iPixelList = new ArrayList<>();
   private String iDimensions = null;
   private String iStartPixel = null;

   public PixelPiece(int id, int pixelCount)
   {
      iId = id;
      iPuzzlePixels = pixelCount;
   }

   public void addPixel(Pixel pixel)
   {
      iPixelList.add(pixel);
   }

   public void setDirection(PixelDirectionEnum direction)
   {
      iDirection = direction;
   }

   public List<Pixel> getPixelList()
   {
      return iPixelList;
   }

   public String getDimensions()
   {
      if (iDimensions == null)
      {
         iDimensions = getCoordinate(getXWidth(), getYWidth(), getZWidth());
      }

      return iDimensions;
   }

   private static String getCoordinate(int x, int y, int z)
   {
      return "[" + x + "," + y + "," + z + "]";
   }

   private int getZWidth()
   {
      return getHighestZ() - getLowestZ() + 1;
   }

   private int getLowestZ()
   {
      int lowest = Integer.MAX_VALUE;
      for (Pixel pixel : iPixelList)
      {
         if (pixel.getZ() < lowest)
         {
            lowest = pixel.getZ();
         }
      }
      return lowest;
   }

   private int getHighestZ()
   {
      int highest = Integer.MIN_VALUE;
      for (Pixel pixel : iPixelList)
      {
         if (pixel.getZ() > highest)
         {
            highest = pixel.getZ();
         }
      }
      return highest;
   }

   private int getXWidth()
   {
      return getHighestX() - getLowestX() + 1;
   }

   private int getLowestX()
   {
      int lowest = Integer.MAX_VALUE;
      for (Pixel pixel : iPixelList)
      {
         if (pixel.getX() < lowest)
         {
            lowest = pixel.getX();
         }
      }
      return lowest;
   }

   private int getHighestX()
   {
      int highest = Integer.MIN_VALUE;
      for (Pixel pixel : iPixelList)
      {
         if (pixel.getX() > highest)
         {
            highest = pixel.getX();
         }
      }
      return highest;
   }

   private int getYWidth()
   {
      return getHighestY() - getLowestY() + 1;
   }

   private int getLowestY()
   {
      int lowest = Integer.MAX_VALUE;
      for (Pixel pixel : iPixelList)
      {
         if (pixel.getY() < lowest)
         {
            lowest = pixel.getY();
         }
      }
      return lowest;
   }

   private int getHighestY()
   {
      int highest = Integer.MIN_VALUE;
      for (Pixel pixel : iPixelList)
      {
         if (pixel.getY() > highest)
         {
            highest = pixel.getY();
         }
      }
      return highest;
   }

   public String getStartPixel()
   {
      if (iStartPixel == null)
      {
         iStartPixel = getCoordinate(getLowestX(), getLowestY(), getLowestZ());
      }

      return iStartPixel;
   }

   public void clearPixels()
   {
      for (Pixel pixel : iPixelList)
      {
         pixel.setValue(0);  
      }
      iPixelList.clear();      
   }
}
