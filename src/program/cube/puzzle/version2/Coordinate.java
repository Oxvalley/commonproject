package program.cube.puzzle.version2;

public class Coordinate
{

   private int iX;
   private int iY;
   private int iZ;

   public Coordinate(int x, int y, int z)
   {
      iX = x;
      iY = y;
      iZ = z;
   }

   public int getX()
   {
      return iX;
   }

   public void setX(int x)
   {
      iX = x;
   }

   public int getY()
   {
      return iY;
   }

   public void setY(int y)
   {
      iY = y;
   }

   public int getZ()
   {
      return iZ;
   }

   public void setZ(int z)
   {
      iZ = z;
   }

}
