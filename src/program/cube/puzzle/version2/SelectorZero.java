package program.cube.puzzle.version2;

public class SelectorZero implements SelectedInterface
{

   public boolean isSelected(Pixel pixel)
   {

      if (pixel == null)
      {
         return true;
      }
      return (pixel.getValue() != 0);
   }


}
