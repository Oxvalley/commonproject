package program.cube.puzzle.version2;

import java.util.List;

public class Pixel extends Coordinate
{

   @Override
   public String toString()
   {
      return "(" + getX() + "," + getY() + "," + getZ() + ")=" + iValue;
   }

   private int iValue;

   public int getValue()
   {
      return iValue;
   }

   public void setValue(int value)
   {
      iValue = value;
   }

   public Pixel(int x, int y, int z, int value)
   {
      super(x, y, z);
      iValue = value;
   }

   public boolean isNextTo(List<Pixel> list)
   {
      if (list == null || list.size() == 0)
      {
         return true;
      }

      for (Pixel pixel : list)
      {
         if (isNextTo(this, pixel))
         {
            // System.out.println("...and also next to " + pixel);
            return true;
         }
      }

      return false;
   }

   private static boolean isNextTo(Pixel pixel, Pixel pixel2)
   {
      return (Math.abs(pixel.getX() - pixel2.getX()) + Math.abs(pixel.getY() - pixel2.getY())
            + Math.abs(pixel.getZ() - pixel2.getZ()) < 2);
   }

   public boolean isNextTo(Pixel pixel2)
   {
      return isNextTo(this, pixel2);
   }

}
