package program.cube.puzzle.version2;

import java.util.Random;

public class RandomUndoer extends Random
{
   
   private static int count = 0;

   @Override
   public int nextInt(int bound)
   {
      
      return (count++ % bound);
      
   }

}
