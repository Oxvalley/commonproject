package program.cube.puzzle.version2;

import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import org.jaudiotagger.audio.iff.IffHeaderChunk;

import program.cube.puzzle.common.CubePuzzlePiece;

public class PixelMatrix
{

   public static final int UNUSED_START_ID = 100;
   private static Instant start;
   private int iPuzzlePixels;
   private List<CubePuzzlePiece> iPieces;
   private int iGood;
   private int iPiecesCount;
   private int iPixelMax;
   private Map<String, Pixel> iMatrix;
   private String iFileName;
   private int iPixelMin;

   private Map<Integer, List<Pixel>> unusedPiecesMap;
   private List<Pixel> unusedPixels;

   public PixelMatrix(int puzzlePixels, int piecesCount)
   {
      iPuzzlePixels = puzzlePixels;
      iPiecesCount = piecesCount;
      int iMatrixCount = (int) Math.pow(iPuzzlePixels, 3);
      iPixelMax = (int) Math.round(1.7 * iMatrixCount / piecesCount);
      iPixelMin = (int) Math.round(.7 * iMatrixCount / piecesCount);

      iMatrix = new HashMap<>();

      for (int x = 0; x < iPuzzlePixels; x++)
      {
         for (int y = 0; y < iPuzzlePixels; y++)
         {
            for (int z = 0; z < iPuzzlePixels; z++)
            {
               addPixel(x, y, z, 0);
            }
         }
      }
   }

   private void addPixel(int x, int y, int z, int value)
   {
      iMatrix.put(getKey(x, y, z), new Pixel(x, y, z, value));
   }

   private static String getKey(int x, int y, int z)
   {
      return x + "," + y + "," + z;
   }

   public static void main(String[] args)
   {
      int puzzlePixels = 4;
      int piecesCount = 10;
      start = Instant.now();
      PixelMatrix cp = new PixelMatrix(puzzlePixels, piecesCount);
      List<PixelPiece> pieces = cp.makePieces();
      cp.printSolution(pieces);
      cp.createOpenScad(pieces);
      System.out.println("Execution time in seconds: " + (Duration.between(start, Instant.now()).toMillis() / 1000));
      cp.openOpenScad();
   }

   private void openOpenScad()
   {
     CubePuzzlePiece.browseURL(iFileName);
   }

   private void createOpenScad(List<PixelPiece> pieces)
   {
      String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
      iFileName = "matrix_puzzel_" + iPuzzlePixels + "_" + timeStamp.replace(" ", "_").replace(":", ".") + ".scad";
      Random rand = new Random();

      try
      {
         FileWriter fw = new FileWriter(iFileName);

         fw.write("// Generated " + timeStamp + " by CubePuzzler\n");
         fw.write("PUZZLE_PIXELS = " + iPuzzlePixels + ";\n");
         fw.write("CUBE_THICKNESS = 5;\n");
         fw.write("NONE = -1;\n");
         fw.write("XPLUS = 0;\n");
         fw.write("YMINUS = 1;\n");
         fw.write("YPLUS = 2;\n");
         fw.write("XMINUS = 3;\n");
         fw.write("ZPLUS = 4;\n");
         fw.write("ZMINUS = 5;\n\n");

         String line = "";
         for (PixelPiece piece : pieces)
         {
            if (!line.isEmpty())
            {
               line += ", " + piece.getDirection().toString().toUpperCase();
            }
            else
            {
               line = "NONE";
            }
         }
         fw.write("Position = [" + line + "];\n");

         line = "";
         for (PixelPiece piece : pieces)
         {
            if (!line.isEmpty())
            {
               line += ", ";
            }

            line += piece.getDimensions();
         }
         fw.write("SizesArray = [" + line + "];\n\n");

         line = "";
         for (PixelPiece piece : pieces)
         {
            if (!line.isEmpty())
            {
               line += ", ";
            }

            line += piece.getStartPixel();
         }
         fw.write("StartPosition = [" + line + "];\n\n");

         fw.write("show_all_cubescube();\n\n");
         int i = 1;
         for (PixelPiece piece : pieces)
         {
            fw.write("module piece_" + i + "()\n");
            fw.write("{\n");
            if (i == 1)
            {
               fw.write("   // NONE\n");
            }
            else
            {
               fw.write("   // " + piece.getDirection().toString().toUpperCase() + "\n");
            }

            fw.write("   // Dimensions = " + piece.getDimensions() + "\n");
            fw.write("   // StartPixel = " + piece.getStartPixel() + "\n");
            fw.write("   color(\"" + OpenScadColors.values()[(i % OpenScadColors.values().length)] + "\")\n");
            fw.write("   {\n");
            for (Pixel pixelPiece : piece.getPixelList())
            {
               fw.write("      write_pixel(" + pixelPiece.getX() + ", " + pixelPiece.getY() + ", " + pixelPiece.getZ()
                     + ");\n");
            }

            fw.write("   }\n");
            fw.write("}\n\n");
            i++;
         }

         fw.write("\nmodule show_all_cubescube()\n");
         fw.write("{\n");
         fw.write("  for(i=[0:" + pieces.size() + "])\n");
         fw.write("    translate([0, " + (pieces.size() + 2) + " * i * CUBE_THICKNESS,0])\n");
         fw.write("      show_cube(i, Position[i], SizesArray[i],, StartPosition[i])\n");
         fw.write("      {\n");
         i = 1;
         for (PixelPiece piece : pieces)
         {
            fw.write("        piece_" + (i++) + "();\n");
         }

         fw.write("      }\n");
         fw.write("}\n\n");

         fw.write("module show_cube(id, position, sizes, startPixel)\n");
         fw.write("{\n");
         fw.write("  if (id >= 1)\n");
         fw.write("  {\n");
         fw.write("    for(i=[0:id-1])\n");
         fw.write("      children(i);\n");
         fw.write("  }\n\n");
         fw.write("  if(position == NONE)\n");
         fw.write("  {\n");
         fw.write("    children(id);\n");
         fw.write("  }\n\n");

         fw.write("  if(position == XPLUS)\n");
         fw.write("  {\n");
         fw.write(
               "    translate([(PUZZLE_PIXELS-sizes[0]+1) * CUBE_THICKNESS, (0) * CUBE_THICKNESS, (0) * CUBE_THICKNESS])\n");
         fw.write("      children(id);\n\n");
         fw.write(
               "    translate([(PUZZLE_PIXELS-sizes[0]+7) * CUBE_THICKNESS, (startPixel[1]+sizes[1]/2) * CUBE_THICKNESS, (startPixel[2]+sizes[2]/2) * CUBE_THICKNESS])\n");
         fw.write("      rotate([0,-90,0])\n");
         fw.write("        arrow();\n");
         fw.write("  }\n\n");

         fw.write("  if(position == XMINUS)\n");
         fw.write("  {\n");
         fw.write(
               "    translate([(PUZZLE_PIXELS-sizes[0]-5) * CUBE_THICKNESS, (0) * CUBE_THICKNESS, (0) * CUBE_THICKNESS])\n");
         fw.write("      children(id);\n\n");
         fw.write(
               "    translate([(PUZZLE_PIXELS-sizes[0]-7) * CUBE_THICKNESS, (startPixel[1]+sizes[1]/2) * CUBE_THICKNESS, (startPixel[2]+sizes[2]/2) * CUBE_THICKNESS])\n");
         fw.write("      rotate([0,90,0])\n");
         fw.write("        arrow();\n");
         fw.write("  }\n\n");

         fw.write("  if(position == YPLUS)\n");
         fw.write("  {\n");
         fw.write("    translate([(0) * CUBE_THICKNESS, (sizes[1]+1) * CUBE_THICKNESS, (0) * CUBE_THICKNESS])\n");
         fw.write("      children(id);\n\n");
         fw.write(
               "    translate([(startPixel[0]+sizes[0]/2) * CUBE_THICKNESS, (sizes[1]+3) * CUBE_THICKNESS, (startPixel[2]+sizes[2]/2) * CUBE_THICKNESS])\n");
         fw.write("      rotate([90,0,0])\n");
         fw.write("        arrow();\n");
         fw.write("  }\n\n");

         fw.write("  if(position == YMINUS)\n");
         fw.write("  {\n");
         fw.write(
               "    translate([(0) * CUBE_THICKNESS, (PUZZLE_PIXELS-sizes[1]-5) * CUBE_THICKNESS, (0) * CUBE_THICKNESS])\n");
         fw.write("      children(id);\n\n");
         fw.write(
               "    translate([(startPixel[0]+sizes[0]/2) * CUBE_THICKNESS, (PUZZLE_PIXELS-sizes[1]-7) * CUBE_THICKNESS, (startPixel[2]+sizes[2]/2) * CUBE_THICKNESS])\n");
         fw.write("      rotate([-90,0,0])\n");
         fw.write("        arrow();\n");
         fw.write("  }\n\n");

         fw.write("  if(position == ZPLUS)\n");
         fw.write("  {\n");
         fw.write("    translate([(0) * CUBE_THICKNESS, (0) * CUBE_THICKNESS, (sizes[2]+1) * CUBE_THICKNESS])\n");
         fw.write("      children(id);\n\n");
         fw.write(
               "    translate([(startPixel[0]+sizes[0]/2) * CUBE_THICKNESS, (startPixel[1]+sizes[1]/2) * CUBE_THICKNESS, (startPixel[2] + 3) * CUBE_THICKNESS])\n");
         fw.write("      rotate([0,180,0])\n");
         fw.write("        arrow();\n");
         fw.write("  }\n\n");

         fw.write("  if(position == ZMINUS)\n");
         fw.write("  {\n");
         fw.write(
               "    translate([(0) * CUBE_THICKNESS, (0) * CUBE_THICKNESS, (PUZZLE_PIXELS - sizes[2]-5) * CUBE_THICKNESS])\n");
         fw.write("      children(id);\n\n");
         fw.write(
               "    translate([(startPixel[0]+sizes[0]/2) * CUBE_THICKNESS, (startPixel[1]+sizes[1]/2) * CUBE_THICKNESS, (PUZZLE_PIXELS - sizes[2]-7) * CUBE_THICKNESS])\n");
         fw.write("      rotate([0,0,0])\n");
         fw.write("        arrow();\n");
         fw.write("  }\n");
         fw.write("}\n\n");

         fw.write("module arrow()\n");
         fw.write("{\n");
         fw.write("  cylinder($fn=64, d=1.5,h=3);\n\n");
         fw.write("  translate([0,0,3])\n");
         fw.write("    cylinder($fn=64, r1=1.5, r2=0 ,h=2);\n");
         fw.write("}\n\n");

         fw.write("module write_pixel(x, y, z)\n");
         fw.write("{\n");
         fw.write("   translate([x * CUBE_THICKNESS , y * CUBE_THICKNESS , z * CUBE_THICKNESS])\n");
         fw.write("      cube([CUBE_THICKNESS, CUBE_THICKNESS, CUBE_THICKNESS]);\n");
         fw.write("}\n\n");

         fw.close();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   public List<PixelPiece> makePieces()
   {
      Random rand = new Random();
      int minLeft = Integer.MAX_VALUE;

      List<PixelPiece> puzzelList = new ArrayList<>();
      for (int i = 0; i < iPiecesCount; i++)
      {
         puzzelList.add(0, new PixelPiece(iPiecesCount - i, iPuzzlePixels));
      }

      for (int id = 0; id < iPiecesCount; id++)
      {
         PixelPiece piece = puzzelList.get(id);
         int loopcount = 0;
         boolean isOK = false;

         while (loopcount < 1000 && !isOK)
         {
            // Take a direction, Left, Right, Front, Back, Up or Down
            PixelDirectionEnum direction =
                  PixelDirectionEnum.values()[rand.nextInt(PixelDirectionEnum.values().length)];
            piece.setDirection(direction);
            System.out.println(direction);

            int pixels = rand.nextInt(iPixelMax - iPixelMin + 1) + iPixelMin;
            System.out.println("Size: " + pixels);
            List<Pixel> list = null;

            for (int i = 0; i < pixels; i++)
            {
               list = getFreePixels(direction, piece.getPixelList());

               if (list.size() > 0)
               {
                  boolean isOKChoice = false;

                  int loop = list.size() * 3;
                  while (!isOKChoice && loop > 0)
                  {
                     int selected = rand.nextInt(list.size());
                     Pixel pixel = list.get(selected);
                     pixel.setValue(i + 1);
                     loop--;

                     if (isBankirAlgorithmOK(pixel))
                     {
                        System.out.println("Selected " + pixel);
                        piece.addPixel(pixel);
                        isOKChoice = true;

                        int pieceCount = i + 1 + unusedPiecesMap.keySet().size();

                        if (pieceCount == iPiecesCount)
                        {
                           boolean isDone = true;

                           for (int j = 0; j <= i; j++)
                           {
                              PixelPiece piecej = puzzelList.get(j);
                              if (piecej.getPixelList().size() < iPixelMin || piecej.getPixelList().size() > iPixelMax)
                              {
                                 isDone = false;
                              }
                           }

                           if (isDone)
                           {
                              for (Integer key : unusedPiecesMap.keySet())
                              {
                                 List<Pixel> lst = unusedPiecesMap.get(key);
                                 if (lst.size() < iPixelMin || lst.size() > iPixelMax)
                                 {
                                    isDone = false;
                                 }
                              }
                           }

                           if (isDone)
                           {
                              // Back door. Do we have the correct number of
                              // pieces
                              // and sizes?

                              int[] order = pullOutOrder();
                              if (order != null)
                              {
                                 System.out.println("We have found a backdoor");
                                 for (int j = 0; j <= i; j++)
                                 {
                                    PixelPiece piecej = puzzelList.get(j);
                                    System.out.println("Piece " + j + ". " + piecej.getPixelList().size());
                                 }

                                 for (Integer key : unusedPiecesMap.keySet())
                                 {
                                    List<Pixel> lst = unusedPiecesMap.get(key);
                                    System.out.println("Unused " + key + ". " + lst.size());
                                 }

                                 AllMutations.printMutation(order);
                                 System.exit(0);
                              }

                           }
                        }
                     }
                     else
                     {
                        // Undo
                        pixel.setValue(0);
                     }
                  }
               }
            }

            if (list != null && list.size() >= iPixelMin)
            {
               isOK = true;
            }
            else
            {
               piece.clearPixels();
            }

            loopcount++;
            System.out.println("loopcount: " + loopcount);
         }

         int left = getUnusedPiecesCount();

         if (left < minLeft)
         {
            minLeft = left;

            System.out.println("New min of left pixels: " + left);
            System.out.println("Nu!");
         }

         if (minLeft <= 14)
         {
            return puzzelList;
         }

         if (loopcount == 1000)
         {
            // Remove this piece and the piece before
            puzzelList.get(id - 1).clearPixels();
            id -= 2;
         }
      }

      return puzzelList;
   }

   private int[] pullOutOrder()
   {
      AllMutations mut = new AllMutations(unusedPiecesMap.size(), UNUSED_START_ID);

      int[] order = mut.getNext();
      while (order != null && !canPullOutAll(order))
      {
         order = mut.getNext();
      }

      return order;
   }

   private boolean canPullOutAll(int[] order)
   {
      // clearUnusedFromMatrix();

      // Can first one find a direction to pull out?
      List<Integer> usedUnused = new ArrayList<>();
      boolean canPull = true;

      for (int i = 0; i < order.length; i++)
      {
         int thisPieceIndex = order[i];

         if (!canBePulledOut(thisPieceIndex, usedUnused))
         {
            return false;
         }

         usedUnused.add(thisPieceIndex);
         // If so, can next?
      }

      return canPull;
   }

   private boolean canBePulledOut(int key, List<Integer> usedUnusedList)
   {
      List<Pixel> lst = unusedPiecesMap.get(key);
      SelectedInterface sel2 = new SelectorZerAndNotInList(usedUnusedList);

      for (PixelDirectionEnum direction : PixelDirectionEnum.values())
      {
         boolean okDirection = true;
         for (Pixel pixel : lst)
         {
            if (!isFree(pixel, direction, sel2))
            {
               okDirection = false;
            }
         }

         if (okDirection)
         {
            System.out.println(key + " can be Pulled out as " + direction);
            return true;
         }
      }

      System.out.println(key + " has no direction to pull out");
      return false;
   }

   private boolean isBankirAlgorithmOK(Pixel pixel)
   {
      int nextPieceId = UNUSED_START_ID;

      // Get a list of empty pixels
      unusedPixels = getUnusedPixels();

      // Check if each pixel has relatives in the other pixels in list
      for (int i = 0; i < unusedPixels.size() - 1; i++)
      {
         Pixel a = unusedPixels.get(i);
         // Get a list of relatives
         List<Pixel> lst = new ArrayList<>();
         lst.add(a);
         for (int j = i + 1; j < unusedPixels.size(); j++)
         {
            Pixel b = unusedPixels.get(j);
            if (a.isNextTo(b))
            {
               lst.add(b);
            }
         }

         // Which relative has value larger than 0 but the lowest piece number?
         int pieceId = getLowestNoneZeroValue(lst);

         if (pieceId == -1)
         {
            // No value set, used next id
            pieceId = nextPieceId++;
         }

         // Set all in list to this low number
         for (Pixel pixel2 : lst)
         {
            pixel2.setValue(pieceId);
         }
      }

      // When all is done, make a list with all the new pieces
      unusedPiecesMap = new TreeMap<>();

      for (Pixel pixel2 : unusedPixels)
      {
         int key = pixel2.getValue();
         List<Pixel> lst = unusedPiecesMap.get(key);
         if (lst == null)
         {
            lst = new ArrayList<>();
            unusedPiecesMap.put(key, lst);
         }

         lst.add(pixel2);
      }

      // Are all pieces at least in the count of pixel_min level?
      // return that answer

      boolean retVal = true;

      for (Integer key : unusedPiecesMap.keySet())
      {
         int size = unusedPiecesMap.get(key).size();
         System.out.println(key + ". " + size + " pixels");

         if (size < iPixelMin)
         {
            retVal = false;
         }
      }

      clearUnusedFromMatrix();

      return retVal;
   }

   public void clearUnusedFromMatrix()
   {      
      // (remove ids from UNUSED_START_ID and above)
      for (Pixel pixel2 : unusedPixels)
      {
         pixel2.setValue(0);
      }
   }

   // Which relative has value larger than 0 but the lowest piece number?
   private static int getLowestNoneZeroValue(List<Pixel> lst)
   {
      int retVal = Integer.MAX_VALUE;
      for (Pixel pixel : lst)
      {
         if (pixel.getValue() > 0 && pixel.getValue() < retVal)
         {
            retVal = pixel.getValue();
         }
      }

      if (retVal == Integer.MAX_VALUE)
      {
         retVal = -1;
      }

      return retVal;
   }

   private List<Pixel> getUnusedPixels()
   {

      List<Pixel> lst = new ArrayList<>();

      for (int x = 0; x < iPuzzlePixels; x++)
      {
         for (int y = 0; y < iPuzzlePixels; y++)
         {
            for (int z = 0; z < iPuzzlePixels; z++)
            {
               Pixel pixel = getPixel(x, y, z);

               if (pixel.getValue() == 0)
               {
                  lst.add(pixel);
               }
            }
         }
      }

      return lst;
   }

   private int getUnusedPiecesCount()
   {
      return getUnusedPixels().size();
   }

   private List<Pixel> getFreePixels(PixelDirectionEnum direction, List<Pixel> list)
   {
      SelectedInterface selector = new SelectorZero();
      List<Pixel> retVal = new ArrayList<>();

      for (int x = 0; x < iPuzzlePixels; x++)
      {
         for (int y = 0; y < iPuzzlePixels; y++)
         {
            for (int z = 0; z < iPuzzlePixels; z++)
            {
               Pixel pixel = getPixel(x, y, z);
               if (isFree(pixel, direction, selector) && (list.size() == 0 || pixel.isNextTo(list)))
               {
                  // System.out.println("Adding " + pixel);
                  retVal.add(pixel);
               }
            }
         }
      }

      return retVal;
   }

   /**
    * Free meaning is 0 and all to from that and out of cube is free
    * 
    * @param pixel
    * @param direction
    * @return
    */

   private boolean isFree(Pixel pixel, PixelDirectionEnum direction, SelectedInterface sel)
   {

      Pixel temp = pixel;
      // The pixel must not be selected
      if (sel.isSelected(pixel))
      {
         return false;
      }

      // All pixels from that on must be selected
      while (temp != null)
      {
         temp = getPixel(plus(temp, direction));
         if (temp == null)
         {
            // System.out.println("Tested " + pixel + " OK in direction " +
            // direction);
            return true;
         }

         if (sel.isSelected(temp))
         {
            // System.out.println(pixel + " taken in direction " + direction);
            return false;
         }
      }

      System.out.println("Tested " + pixel + " OK(2) in direction " + direction);
      return true;
   }

   private Pixel getPixel(Coordinate cord)
   {
      return iMatrix.get(getKey(cord));
   }

   private static String getKey(Coordinate cord)
   {
      return getKey(cord.getX(), cord.getY(), cord.getZ());
   }

   private static Coordinate plus(Pixel pixel, PixelDirectionEnum direction)
   {
      return new Coordinate(pixel.getX() + direction.getX(), pixel.getY() + direction.getY(),
            pixel.getZ() + direction.getZ());
   }

   private Pixel getPixel(int x, int y, int z)
   {
      return iMatrix.get(getKey(x, y, z));
   }

   public void printSolution(List<PixelPiece> pieces)
   {
      for (PixelPiece piece : pieces)
      {
         System.out.println("\n" + piece);
      }
   }

}
