package program.cube.puzzle.version2;

public interface SelectedInterface
{

   boolean isSelected(Pixel pixel);

}
