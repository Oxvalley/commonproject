package program.cube.puzzle.version2;

public enum PixelDirectionEnum
{
   xPlus(1, 0, 0), xMinus(-1, 0, 0), yPlus(0, 1, 0), yMinus(0, -1, 0), zPlus(0, 0, 1), zMinus(0, 0, -1);

   private int iX;
   private int iY;
   private int iZ;

   PixelDirectionEnum(int x, int y, int z)
   {
      iX = x;
      iY = y;
      iZ = z;
   }

   public int getX()
   {
      return iX;
   }

   public int getY()
   {
      return iY;
   }

   public int getZ()
   {
      return iZ;
   }
}
