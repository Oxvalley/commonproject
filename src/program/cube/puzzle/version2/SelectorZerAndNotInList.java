package program.cube.puzzle.version2;

import java.util.List;

public class SelectorZerAndNotInList implements SelectedInterface
{

   private List<Integer> iUsedUnusedList;

   public SelectorZerAndNotInList(List<Integer> usedUnusedList)
   {
      iUsedUnusedList = usedUnusedList;
   }

   @Override
   public boolean isSelected(Pixel pixel)
   {

      if (pixel == null)
      {
         return true;
      }

      int value = pixel.getValue();
      return ((value != 0 && value < PixelMatrix.UNUSED_START_ID) || iUsedUnusedList.contains(value));
   }

}
