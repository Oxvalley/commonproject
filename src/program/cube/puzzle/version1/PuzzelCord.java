package program.cube.puzzle.version1;

import program.cube.puzzle.common.CubePuzzlePiece;

public class PuzzelCord
{

   private CubePuzzlePiece iPuzzelPiece;
   private int iX;
   private int iY;

   public PuzzelCord(CubePuzzlePiece pc, int x, int y)
   {
      iPuzzelPiece = pc;
      iX = x;
      iY = y;

   }

   public CubePuzzlePiece getPuzzlePiece()
   {
      return iPuzzelPiece;
   }

   public int getX()
   {
      return iX;
   }

   public int getY()
   {
      return iY;
   }

   public String getKeyString()
   {
      return "P" + iPuzzelPiece.getIdNumber() + "-" + iX + "," + iY;
   }

   public int getValue()
   {
      return iPuzzelPiece.getPixel(iX, iY);
   }
}
