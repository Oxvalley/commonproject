package program.cube.puzzle.version1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import program.cube.puzzle.common.CubePuzzlePiece;

public class VariationCounter
{

   private List<String> iAllTurns = new ArrayList<>();
   private Set<String> iDifferentEdges = new HashSet<>();

   public void add(CubePuzzlePiece piece)
   {
      iAllTurns.add(piece.toString());
      iDifferentEdges.add(piece.toString().substring(0, piece.getPixelsWide()));
   }

   public List<String> getAllTurns()
   {
      return iAllTurns;
   }

   public void resetAllTurns()
   {
      iAllTurns.clear();
   }

   public void resetDifferentEdges()
   {
      iDifferentEdges.clear();
   }

   public int getDifferentEdgesCount()
   {
      return iDifferentEdges.size();
   }

}
