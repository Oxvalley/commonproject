package program.cube.puzzle.version1;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import program.cube.puzzle.common.CubePuzzlePiece;
import program.cube.puzzle.common.CubePuzzleUI;
import program.cube.puzzle.version2.OpenScadColors;

public class CubePuzzler
{

   private static Instant start;
   private int iPuzzlePixels;
   private List<CubePuzzlePiece> iPieces;
   private int iGood;
   private String iFileName;

   public CubePuzzler(int puzzlePixels)
   {
      iPuzzlePixels = puzzlePixels;
   }

   public static void main(String[] args)
   {

      int puzzlePixels = 5;
      int sizeinMM = 30;
      int maxTurns = 100000;

      CubePuzzler cp = new CubePuzzler(puzzlePixels);
      try
      {
         if (args.length == 0)
         {
            showGUI(puzzlePixels, sizeinMM, maxTurns, cp);
         }

         if (args.length > 0)
         {
            puzzlePixels = Integer.parseInt(args[0]);
         }

         if (args.length > 1)
         {
            sizeinMM = Integer.parseInt(args[1]);
         }

         if (args.length > 2)
         {
            maxTurns = Integer.parseInt(args[2]);
         }

         if (args.length > 3)
         {
            usage();
         }
      }
      catch (NumberFormatException e)
      {
         usage();
      }

      if (args.length != 0)
      {
         createAll(cp, sizeinMM, maxTurns);
      }
   }

   public static double createAll(CubePuzzler cp, int sizeinMM, int maxTurns)
   {
      start = Instant.now();
      List<CubePuzzlePiece> pieces = cp.makePieces(maxTurns);
      System.out.println("Pieces: " + pieces.get(0));
      cp.printSolution();
      cp.createOpenScad(sizeinMM);
      double time = (Duration.between(start, Instant.now()).toMillis() / 1000);
      System.out.println("Execution time in seconds: " + time);
      cp.openOpenScad();
      return time;
   }

   private static void showGUI(int puzzlePixels, int sizeinMM, int maxTurns, CubePuzzler cp)
   {
      CubePuzzleUI cubePuzzleUI = new CubePuzzleUI(puzzlePixels, sizeinMM, maxTurns, cp);
      cubePuzzleUI.setVisible(true);
   }

   private void openOpenScad()
   {
      CubePuzzlePiece.browseURL(iFileName);
   }

   public static void usage()
   {
      System.out.println("Usage: CubePuzzler    (will show GUI)");
      System.out.println("or");
      System.out.println(
            "Usage: CubePuzzler [NumberOfPizels(default=5)]  [SizeinMM(default=30)] [NumberOfLoops(default=100000)]");
      System.exit(0);
   }

   private void createOpenScad(int sizeinMM)
   {
      String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
      iFileName = "cube_puzzel_" + iPuzzlePixels + "_" + timeStamp.replace(" ", "_").replace(":", ".") + ".scad";
      Random rand = new Random();

      try
      {
         FileWriter fw = new FileWriter(iFileName);
         double sizePerPiece = Math.round(sizeinMM * 1.0 / iPuzzlePixels * 100.0) / 100.0;

         fw.write("// Generated " + timeStamp + " by CubePuzzler\n");
         fw.write("// SHOW_SOLUTION can be changed to false to just show the 6 pieces\n");
         fw.write("SHOW_SOLUTION=true;\n\n");

         fw.write("CUBE_THICKNESS = " + sizePerPiece + ";\n");
         fw.write("THINNER = .05;\n\n");

         for (int i = 0; i < 6; i++)
         {
            int rotate = rand.nextInt(2);
            fw.write("translate([" + (i % 3) + " * " + (iPuzzlePixels + 1) + " * CUBE_THICKNESS + "
                  + (rotate * iPuzzlePixels) + " * CUBE_THICKNESS, " + Math.round(i / 3) + " * " + (iPuzzlePixels + 1)
                  + " * CUBE_THICKNESS, " + rotate + " * CUBE_THICKNESS])\n");

            if (rotate == 1)
            {
               fw.write("   rotate([0,180,0])\n");
               fw.write("      piece_" + (i + 1) + "();\n\n");
            }
            else
            {
               fw.write("   piece_" + (i + 1) + "();\n\n");
            }
         }

         fw.write("if(SHOW_SOLUTION)\n");
         fw.write("{\n");
         fw.write("  scaled=1.02;\n\n");
         fw.write("  translate([0,-CUBE_THICKNESS * " + (iPuzzlePixels + 1) + ",0])\n");
         fw.write("  {\n");
         fw.write("    MOVE_IN = THINNER * 3;\n\n");
         fw.write("    scale([scaled,scaled,scaled])\n");
         fw.write("      piece_1();\n\n");
         fw.write("    translate([CUBE_THICKNESS * " + iPuzzlePixels + " - MOVE_IN, MOVE_IN, MOVE_IN])\n");
         fw.write("      rotate([0,-90,0])\n");
         fw.write("        scale([scaled,scaled,scaled])\n");
         fw.write("          piece_2();\n\n");
         fw.write("    translate([CUBE_THICKNESS * " + iPuzzlePixels
               + "  - MOVE_IN -.1, MOVE_IN - .2, CUBE_THICKNESS *  " + iPuzzlePixels + "  + MOVE_IN])\n");
         fw.write("      rotate([0,180,0])\n");
         fw.write("        scale([scaled,scaled,scaled])\n");
         fw.write("          piece_3();\n");
         fw.write("    translate([MOVE_IN, MOVE_IN, CUBE_THICKNESS * " + iPuzzlePixels + " - MOVE_IN])\n");
         fw.write("      rotate([0,90,0])\n");
         fw.write("        scale([scaled,scaled,scaled])\n");
         fw.write("          piece_4();\n\n");
         fw.write("    translate([MOVE_IN + .1, MOVE_IN -.1, CUBE_THICKNESS * " + iPuzzlePixels + " - MOVE_IN])\n");
         fw.write("      rotate([-90,0,0])\n");
         fw.write("        scale([scaled,scaled,scaled])\n");
         fw.write("          piece_5();\n\n");
         fw.write("    translate([-MOVE_IN, CUBE_THICKNESS * " + iPuzzlePixels + " - MOVE_IN, MOVE_IN -.1])\n");
         fw.write("      rotate([90,0,0])\n");
         fw.write("        scale([scaled,scaled,scaled])\n");
         fw.write("          piece_6();\n");
         fw.write("  }\n");
         fw.write("}\n");

         int i = 1;
         for (CubePuzzlePiece piece : iPieces)
         {
            fw.write("module piece_" + i + "()\n");
            fw.write("{\n");
            fw.write("   color(\"" + OpenScadColors.values()[(i % OpenScadColors.values().length)] + "\")\n");
            fw.write("      difference()\n");
            fw.write("      {\n");
            fw.write("         union()\n");
            fw.write("         {\n");
            fw.write("             cube([" + iPuzzlePixels + " * CUBE_THICKNESS, " + iPuzzlePixels
                  + " * CUBE_THICKNESS, CUBE_THICKNESS]);\n");
            fw.write("         }\n");

            for (int y = 0; y < iPuzzlePixels; y++)
            {
               for (int x = 0; x < iPuzzlePixels; x++)
               {
                  if (piece.getPixel(x, y) != 1)
                  {
                     fw.write("         box_remove(" + x + ", " + y + ");\n");
                  }
               }
            }
            fw.write("      }\n");
            fw.write("   }\n");
            fw.write("\n");
            i++;
         }

         fw.write("module box(x, y, lessLeft = 0, lessRight = 0,lessUp = 0,lessDown = 0)\n");
         fw.write("{\n");
         fw.write(
               "   translate([x * CUBE_THICKNESS + lessLeft * THINNER, y * CUBE_THICKNESS + lessUp * THINNER, 0])\n");
         fw.write(
               "      cube([CUBE_THICKNESS - (lessLeft + lessRight) * THINNER, CUBE_THICKNESS - (lessUp + lessDown) * THINNER, CUBE_THICKNESS]);\n");
         fw.write("}\n\n");

         fw.write("module box_remove(x, y)\n");
         fw.write("{\n");
         fw.write("   translate([x * CUBE_THICKNESS - THINNER, y * CUBE_THICKNESS - THINNER, -.01])\n");
         fw.write("      cube([CUBE_THICKNESS + 2 * THINNER, CUBE_THICKNESS + 2 * THINNER, CUBE_THICKNESS + .02]);\n");
         fw.write("}\n\n");

         fw.close();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   public List<CubePuzzlePiece> makePieces(int maxTurns)
   {
      int last = iPuzzlePixels - 1;

      List<CubePuzzlePiece> puzzelList = new ArrayList<>();
      for (int i = 0; i < 6; i++)
      {
         puzzelList.add(new CubePuzzlePiece(iPuzzlePixels, i));
      }

      PuzzelCordMap pcm = new PuzzelCordMap(puzzelList);

      pcm.addCord(0, 0, 0, 3, last, 0, 4, 0, last);
      pcm.addCord(0, last, 0, 1, 0, 0, 4, last, last);
      pcm.addCord(0, 0, last, 3, last, last, 5, 0, 0);
      pcm.addCord(0, last, last, 1, 0, last, 5, last, 0);

      pcm.addCord(1, last, 0, 2, 0, 0, 4, last, 0);
      pcm.addCord(1, last, last, 2, 0, last, 5, last, last);

      pcm.addCord(2, last, 0, 3, 0, 0, 4, 0, 0);
      pcm.addCord(2, last, last, 3, 0, last, 5, 0, last);

      for (int i = 1; i < last; i++)
      {
         pcm.addCord(0, i, 0, 4, i, last);
         pcm.addCord(1, i, 0, 4, last, last - i);
         pcm.addCord(2, i, 0, 4, last - i, 0);
         pcm.addCord(3, i, 0, 4, 0, i);

         pcm.addCord(0, i, last, 5, i, 0);
         pcm.addCord(1, i, last, 5, last, i);
         pcm.addCord(2, i, last, 5, last - i, last);
         pcm.addCord(3, i, last, 5, 0, last - i);

         pcm.addCord(0, last, i, 1, 0, i);
         pcm.addCord(1, last, i, 2, 0, i);
         pcm.addCord(2, last, i, 3, 0, i);
         pcm.addCord(3, last, i, 0, 0, i);
      }

      // pcm.listAll();

      long maxLoops = maxTurns;

      iGood = 0;
      int lowestDiff = Integer.MAX_VALUE;
      List<CubePuzzlePiece> lowestSolution = null;
      List<CubePuzzlePiece> pieces;
      for (int i = 0; i < maxLoops; i++)
      {
         pieces = pcm.getSolution(false, false);
         if (pieces != null)
         {
            if (PuzzelCordMap.hasFullRow(pieces))
            {
               System.out.println("Stop!");
               System.exit(1);
            }

            iGood++;
            int different = pcm.getDifferentEdgesCount();
            // System.out.println(different);
            if (different < lowestDiff)
            {
               lowestDiff = different;
               lowestSolution = new ArrayList<>();
               for (CubePuzzlePiece cubePuzzlePiece : pieces)
               {
                  lowestSolution.add(cubePuzzlePiece.clone());
               }

               if (PuzzelCordMap.hasFullRow(lowestSolution))
               {
                  System.out.println("Stop2!");
                  System.exit(1);
               }

               System.out.println("Lowest: " + lowestDiff);
               System.out.println(lowestSolution.get(0));
            }

            if (lowestSolution != null)
            {
               System.out.println(lowestSolution.get(0));
            }
            System.out.println(iGood + ". " + different);
         }

         System.out.println(i + 1);
      }

      iPieces = lowestSolution;

      System.out.println("Low diff:" + lowestDiff);

      System.out.println("Returning: " + lowestSolution.get(0));

      return lowestSolution;
   }

   public void printSolution()
   {
      System.out.println("Found solutions: " + iGood);

      for (CubePuzzlePiece piece : iPieces)
      {
         System.out.println("\n" + piece);
      }

      if (PuzzelCordMap.hasFullRow(iPieces))
      {
         System.out.println("Stop3!");
         System.exit(1);
      }
   }

   public double createCube(int cubes, int size, int turns)
   {
      return createAll(new CubePuzzler(cubes), size, turns);
   }

}
