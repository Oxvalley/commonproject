package program.cube.puzzle.version1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import program.cube.puzzle.common.CubePuzzlePiece;

public class PuzzelCordMap
{

   private List<CubePuzzlePiece> iPuzzelList;
   private List<String> iCorners = new ArrayList<>();

   private HashMap<String, List<PuzzelCord>> iCordMap = new HashMap<>();
   private VariationCounter iVc = new VariationCounter();

   public PuzzelCordMap(List<CubePuzzlePiece> puzzelList)
   {
      iPuzzelList = puzzelList;
   }

   public void addCord(int id1, int x1, int y1, int id2, int x2, int y2, int id3, int x3, int y3)
   {
      List<PuzzelCord> list = new ArrayList<>();
      CubePuzzlePiece piece = iPuzzelList.get(id1);
      PuzzelCord cord = new PuzzelCord(piece, x1, y1);
      list.add(cord);
      saveAsKey(cord, list);
      iCorners.add(cord.getKeyString());
      piece = iPuzzelList.get(id2);
      cord = new PuzzelCord(piece, x2, y2);
      list.add(cord);
      saveAsKey(cord, list);
      piece = iPuzzelList.get(id3);
      cord = new PuzzelCord(piece, x3, y3);
      list.add(cord);
      saveAsKey(cord, list);
   }

   private void saveAsKey(PuzzelCord cord, List<PuzzelCord> list)
   {
      // System.out.println(cord.getKeyString());
      if (iCordMap.get(cord.getKeyString()) != null)
      {
         System.out.println("Bad!" + cord.getKeyString());
      }

      iCordMap.put(cord.getKeyString(), list);
   }

   public void addCord(int id1, int x1, int y1, int id2, int x2, int y2)
   {
      List<PuzzelCord> list = new ArrayList<>();
      CubePuzzlePiece piece = iPuzzelList.get(id1);
      PuzzelCord cord = new PuzzelCord(piece, x1, y1);
      list.add(cord);
      saveAsKey(cord, list);
      iCorners.add(cord.getKeyString());
      piece = iPuzzelList.get(id2);
      cord = new PuzzelCord(piece, x2, y2);
      list.add(cord);
      saveAsKey(cord, list);
   }

   public List<CubePuzzlePiece> getSolution(boolean allowEquals, boolean allowFullRow)
   {
      // TODO Make random choices here
      Random rand = new Random();

      for (String key : iCorners)
      {
         List<PuzzelCord> list = iCordMap.get(key);
         // Find all with 3 cords
         if (list.size() == 3)
         {
            // Select one
            int selected = rand.nextInt(3);
            //System.out.println("\nNew Random:");
            for (int i = 0; i < list.size(); i++)
            {
               PuzzelCord cord = list.get(i);
               if (i == selected)
               {
                  //System.out.println("Selected:");
                  setCordPixel(cord, 1);
                  // Add the one next to selected as well
                  PuzzelCord p2 = getCordNextToThis(cord);
                  setPixelDisallowOthers(p2);

               }
               else
               {
                  // Tell not selected they can not be 1
                  setCordPixel(cord, 2);
               }
            }

         }
      }

      // Find all with 2 cords
      for (String key : iCorners)
      {
         List<PuzzelCord> list = iCordMap.get(key);
         // If they can be selected, select one of them
         int freeCount = getFreeCount(list);

         if (freeCount == 0)
         {
            //System.out.println("Not correct.");

         }

         if (list.size() == 2 && freeCount > 0)
         {
            // Select one
            int selected = rand.nextInt(freeCount);
            //System.out.println("\nNew random 2");
            PuzzelCord selectedCord = getCord(list, selected);
            if (selectedCord == null)
            {
               System.out.println("Bad cord");
            }

            for (PuzzelCord cord : list)
            {
               if (cord.equals(selectedCord))
               {
                  //System.out.println("Selected 2");
                  setCordPixel(cord, 1);
               }
               else
               {
                  // Tell not selected they can not be 1
                  setCordPixel(cord, 2);
               }
            }

         }
      }

      // Now test solution

      // Are to pieces equal (They can turn i 4 ways)

      if (!allowEquals && hasEqualPieces())
      {
         // If so, make another puzzle
         // System.out.println("EqualPieces");
         return null;
      }

      if (!allowFullRow && hasFullRow())
      {
         // If so, make another puzzle
         // System.out.println("FullRow");
         return null;
      }

      return iPuzzelList;
   }

   private boolean hasFullRow()
   {
      return hasFullRow(iPuzzelList);
   }

   public static boolean hasFullRow(List<CubePuzzlePiece> puzzelList)
   {
      for (CubePuzzlePiece piece : puzzelList)
      {
        // System.out.println(piece);
         
         // Check if any side has 0 or max pixels in a row. Avoid that
         int count = 0;
         for (int x = 0; x < piece.getPixelsWide(); x++)
         {
            if (piece.getPixel(x, 0) == 1)
            {
               count++;
            }
         }

         if (count == 0 || count == piece.getPixelsWide())
         {
            return true;
         }

         count = 0;
         for (int x = 0; x < piece.getPixelsWide(); x++)
         {
            if (piece.getPixel(x, piece.getPixelsWide() - 1) == 1)
            {
               count++;
            }
         }

         if (count == 0 || count == piece.getPixelsWide())
         {
            return true;
         }

         count = 0;
         for (int y = 0; y < piece.getPixelsWide(); y++)
         {
            if (piece.getPixel(0, y) == 1)
            {
               count++;
            }
         }

         if (count == 0 || count == piece.getPixelsWide())
         {
            return true;
         }

         count = 0;
         for (int y = 0; y < piece.getPixelsWide(); y++)
         {
            if (piece.getPixel(piece.getPixelsWide() - 1, y) == 1)
            {
               count++;
            }
         }

         if (count == 0 || count == piece.getPixelsWide())
         {
            return true;
         }
      }

      return false;
   }

   private boolean hasEqualPieces()
   {
      return hasEqualPieces(iPuzzelList);
   }

   private boolean hasEqualPieces(List<CubePuzzlePiece> puzzelList)
   {
      Map<CubePuzzlePiece, List<String>> turnMap = new HashMap<>();
      iVc.resetDifferentEdges();
      for (CubePuzzlePiece cubePuzzlePiece : puzzelList)
      {
  
         // Original
         iVc.resetAllTurns();
         iVc.add(cubePuzzlePiece);
         // 3 more turns
         CubePuzzlePiece piece = turn90Degrees(cubePuzzlePiece);
         iVc.add(piece);

         piece = turn90Degrees(piece);
         iVc.add(piece);

         piece = turn90Degrees(piece);
         iVc.add(piece);

         // Flip
         piece = flipPiece(piece);
         iVc.add(piece);

         // 3 more turns
         piece = turn90Degrees(piece);
         iVc.add(piece);

         piece = turn90Degrees(piece);
         iVc.add(piece);

         piece = turn90Degrees(piece);
         iVc.add(piece);

         turnMap.put(cubePuzzlePiece, new ArrayList<String>(iVc.getAllTurns()));
      }

      for (CubePuzzlePiece piece : puzzelList)
      {
         for (CubePuzzlePiece piece2 : puzzelList)
         {
            if (piece != piece2)
            {
               List<String> list = turnMap.get(piece2);
               if (list.contains(piece.toString()))
               {
                  return true;
               }
            }
         }
      }
      return false;
   }

   private static CubePuzzlePiece flipPiece(CubePuzzlePiece piece)
   {
      int last = piece.getPixelsWide() - 1;
      CubePuzzlePiece piece2 = new CubePuzzlePiece(piece.getPixelsWide(), piece.getIdNumber(), true);
      for (int i = 0; i <= last; i++)
      {
         for (int j = 0; j <= last; j++)
         {
            piece2.setPixel(j, i, piece.getPixel(last - j, i));
         }
      }

      return piece2;
   }

   private static CubePuzzlePiece turn90Degrees(CubePuzzlePiece piece)
   {

      int last = piece.getPixelsWide() - 1;
      CubePuzzlePiece piece2 = new CubePuzzlePiece(piece.getPixelsWide(), piece.getIdNumber(), true);
      for (int i = 0; i <= last; i++)
      {
         for (int j = 0; j <= last; j++)
         {
            piece2.setPixel(j, i, piece.getPixel(last - i, j));
         }
      }

      return piece2;
   }

   private static PuzzelCord getCord(List<PuzzelCord> list, int selected)
   {
      int count = 0;
      for (PuzzelCord cord : list)
      {
         if (cord.getValue() != 2)
         {
            if (count == selected)
            {
               return cord;
            }
            count++;
         }
      }

      return null;
   }

   private static int getFreeCount(List<PuzzelCord> list)
   {
      int count = 0;
      for (PuzzelCord cord : list)
      {
         // System.out.println(cord.getKeyString() + " " + cord.getValue());
         if (cord.getValue() != 2)
         {
            count++;
         }
      }
      return count;
   }

   private void setPixelDisallowOthers(PuzzelCord p2)
   {
      setCordPixel(p2, 1);
      List<PuzzelCord> cordList = iCordMap.get(p2.getKeyString());
      if (cordList == null)
      {
         // System.out.println(">>> " + p2.getKeyString());
      }

      for (PuzzelCord cord : cordList)
      {
         if (!cord.getKeyString().equals(p2.getKeyString()))
         {
            setCordPixel(cord, 2);
         }
      }
   }

   private static PuzzelCord getCordNextToThis(PuzzelCord cord)
   {
      CubePuzzlePiece piece = cord.getPuzzlePiece();
      int last = piece.getPixelsWide() - 1;
      List<PuzzelCord> candidates = new ArrayList<>();

      if (cord.getX() != 0)
      {
         candidates.add(new PuzzelCord(piece, cord.getX() - 1, cord.getY()));
      }

      if (cord.getX() != last)
      {
         candidates.add(new PuzzelCord(piece, cord.getX() + 1, cord.getY()));
      }

      if (cord.getY() != 0)
      {
         candidates.add(new PuzzelCord(piece, cord.getX(), cord.getY() - 1));
      }

      if (cord.getY() != last)
      {
         candidates.add(new PuzzelCord(piece, cord.getX(), cord.getY() + 1));
      }
      
      Random rand = new Random();
      int selected = rand.nextInt(candidates.size());
      return candidates.get(selected);      
   }

   public static void setCordPixel(PuzzelCord cord, int value)
   {
      CubePuzzlePiece piece = cord.getPuzzlePiece();
      piece.setPixel(cord.getX(), cord.getY(), value);
      //System.out.println(piece.getIdNumber() + " gets " + value + " at (" + cord.getX() + ", " + cord.getY() + ")");
   }

   public void listAll()
   {
      List<String> mainList = new ArrayList<>();
      mainList.addAll(iCordMap.keySet());

      Collections.sort(mainList);
      for (String key : mainList)
      {
         System.out.println("Sorted " + key);
      }

   }

   public int getDifferentEdgesCount()
   {
      return iVc.getDifferentEdgesCount();
   }

}
