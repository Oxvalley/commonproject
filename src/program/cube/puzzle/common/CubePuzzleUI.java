package program.cube.puzzle.common;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import common.ui.components.ext.CompExtEnum;
import common.ui.components.ext.JPanelExt;
import program.cube.puzzle.version1.CubePuzzler;

public class CubePuzzleUI extends JFrame implements KeyListener
{
   private static final long serialVersionUID = 1L;

   // TODO Change to name of your icon
   private String ICON_NAME = null;

   // TODO init fields here

   /////////// Java GUI Designer version 1.1 Starts here. ////
   // Text below will be regenerated automatically.
   // Generated 2022-05-22 22:36:21

   private JPanelExt iPanelExt;
   private JLabel lbl1;
   private JTextField txtNumberOfCubes;
   private JLabel lbl2;
   private JTextField txtSizeInMM;
   private JLabel lbl3;
   private JTextField txtNumberOfTries;
   private JLabel lblTime;
   private JButton btnCancel;
   private JButton btnOK;

   private int width = 319;
   private int height = 334;

   private CubePuzzler iCp;

   public static void main(String[] args)
   {
      CubePuzzleUI cubePuzzleUI = new CubePuzzleUI(1, 1, 1000, null);
      cubePuzzleUI.setVisible(true);
   }

   public CubePuzzleUI(int puzzlePixels, int sizeinMM, int maxTurns, CubePuzzler cp)
   {
      super();
      iCp = cp;
      setTitle("Cube Puzzler v. 1.0");
      setSize(width, height);
      if (ICON_NAME != null)
      {
         setIconImage(getToolkit().getImage(ICON_NAME));
      }

      getContentPane().setLayout(new BorderLayout());
      iPanelExt = new JPanelExt(width, height);
      iPanelExt.setLayout(null);
      getContentPane().add(iPanelExt, BorderLayout.CENTER);

      lbl1 = new JLabel("Number of Cubes:");
      iPanelExt.addComponentExt(lbl1, 13, 12, 123, 38, CompExtEnum.Normal);

      txtNumberOfCubes = new JTextField();
      txtNumberOfCubes.setText(String.valueOf(puzzlePixels));
      txtNumberOfCubes.addKeyListener(this);
      iPanelExt.addComponentExt(txtNumberOfCubes, 139, 16, 137, 37, CompExtEnum.Normal);

      lbl2 = new JLabel("Size in mm:");
      iPanelExt.addComponentExt(lbl2, 13, 62, 123, 38, CompExtEnum.Normal);

      txtSizeInMM = new JTextField();
      txtSizeInMM.setText(String.valueOf(sizeinMM));
      txtSizeInMM.addKeyListener(this);
      iPanelExt.addComponentExt(txtSizeInMM, 139, 66, 137, 37, CompExtEnum.Normal);

      lbl3 = new JLabel("Number of Tries:");
      iPanelExt.addComponentExt(lbl3, 10, 112, 123, 38, CompExtEnum.Normal);

      txtNumberOfTries = new JTextField(maxTurns);
      txtNumberOfTries.setText(String.valueOf(maxTurns));
      txtNumberOfTries.addKeyListener(this);
      iPanelExt.addComponentExt(txtNumberOfTries, 139, 116, 137, 37, CompExtEnum.Normal);

      lblTime = new JLabel("This will take about 9 seconds");
      iPanelExt.addComponentExt(lblTime, 10, 168, 264, 31, CompExtEnum.Normal);

      setStatus();

      btnCancel = new JButton("Cancel");
      iPanelExt.addComponentExt(btnCancel, 101, 219, 84, 42, CompExtEnum.Normal);
      btnCancel.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnCancelClicked(e);
         }
      });

      btnOK = new JButton("OK");
      iPanelExt.addComponentExt(btnOK, 205, 221, 68, 40, CompExtEnum.Normal);
      btnOK.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnOKClicked(e);
         }
      });

      /////////// Java GUI Designer version 1.1 Generation ends here. ///

      // TODO Add init declarations here
      
      Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
      this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

   }

   private void setStatus()
   {
      try
      {
         int maxTurns = Integer.parseInt(txtNumberOfTries.getText());
         int cubes = Integer.parseInt(txtNumberOfCubes.getText());
         double seconds = maxTurns / 120000.0 * cubes * cubes - 3;
         seconds = Math.round(seconds * 1.0) / 1.0;

         lblTime.setText("This will take about " + seconds + " seconds");
      }
      catch (NumberFormatException e)
      {
         e.printStackTrace();
      }

   }

   private void btnCancelClicked(ActionEvent e)
   {
      // TODO Implement

   }

   private void btnOKClicked(ActionEvent e)
   {
      if (iCp != null)
      {
         int cubes = -1;
         int size = -1;
         int turns = -1;

         cubes = Integer.parseInt(txtNumberOfCubes.getText());
         size = Integer.parseInt(txtSizeInMM.getText());
         turns = Integer.parseInt(txtNumberOfTries.getText());

         double time = iCp.createCube(cubes, size, turns);
         lblTime.setText("Time for executing: " + time + " seconds");
      }

   }

   @Override
   public void keyTyped(KeyEvent e)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void keyPressed(KeyEvent e)
   {
      // TODO Auto-generated method stub

   }

   @Override
   public void keyReleased(KeyEvent e)
   {
      setStatus();
   }

}
