package program.cube.puzzle.common;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Store the content of one Cube Puzzle Piece
 * 
 * @author pc
 *
 */
public class CubePuzzlePiece
{

   private int iPixelsWide;
   private Integer[] iPixelArr;
   private int iIdNumber;
   private boolean iNoErrors;

   public CubePuzzlePiece(int pixelsWide, int idNumber)
   {
      this(pixelsWide, idNumber, false);
   }

   public CubePuzzlePiece clone()
   {
      CubePuzzlePiece retVal = new CubePuzzlePiece(iPixelsWide, iIdNumber);
      for (int y = 0; y < iPixelsWide; y++)
      {
         for (int x = 0; x < iPixelsWide; x++)
         {
            retVal.setPixel(x, y, getPixel(x, y));
         }
      }

      return retVal;
   }

   public static void browseURL(String fileName)
   {
      File file = new File(fileName);

      File file2 = new File(file.getAbsolutePath());

      browseURL(file2.getParent(), file2.getName());
   }

   public static void browseURL(String folder, String fileName)
   {
      if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE))
      {
         try
         {
            String url;

            if (folder == null)
            {
               url = "file:///" + fileName;
            }
            else
            {
               url = "file:///" + new File(folder).getAbsolutePath() + "/" + fileName;
            }

            url = url.replace("\\", "/").replace(" ", "%20");
            Desktop.getDesktop().browse(new URI(url));
         }
         catch (IOException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         catch (URISyntaxException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }
   }

   public CubePuzzlePiece(int pixelsWide, int idNumber, boolean noErrors)
   {
      iPixelsWide = pixelsWide;
      iIdNumber = idNumber;
      iNoErrors = noErrors;

      int size = pixelsWide * 4 - 4;
      iPixelArr = new Integer[size];
      for (int i = 0; i < size; i++)
      {
         iPixelArr[i] = 0;
      }
   }

   public int getPixelsWide()
   {
      return iPixelsWide;
   }

   public int getIdNumber()
   {
      return iIdNumber;
   }

   public static void main(String[] args)
   {
      CubePuzzlePiece ss = new CubePuzzlePiece(5, 1);
      for (int x = 0; x < 5; x++)
      {
         for (int y = 0; y < 5; y++)
         {
            ss.setPixel(x, y);
         }
      }

      System.out.println(ss);

      CubePuzzlePiece ss2 = new CubePuzzlePiece(5, 2);
      ss2.setPixel(0, 0);
      ss2.setPixel(0, 3);
      ss2.setPixel(0, 2);
      ss2.setPixel(4, 3);
      ss2.setPixel(1, 4);
      System.out.println("\n" + ss2);

      CubePuzzlePiece ss3 = new CubePuzzlePiece(7, 3);
      ss3.setPixel(6, 0);
      ss3.setPixel(0, 3);
      ss3.setPixel(0, 2);
      ss3.setPixel(6, 3);
      ss3.setPixel(5, 6);
      System.out.println("\n" + ss3);

   }

   public void setPixel(int x, int y)
   {
      setPixel(x, y, 1);
   }

   public void setPixel(int x, int y, int value)
   {
      if (x > 0 && x < iPixelsWide - 1)
      {
         // In the middle, just 0 and iPixelWide allowed
         if (y > 0 && y < iPixelsWide - 1 && !iNoErrors)
         {
            System.out.println("Bad pixel position: x=" + x + ", y=" + y);
         }
      }

      int index = getIndex(x, y);
      iPixelArr[index] = value;
   }

   private int getIndex(int x, int y)
   {
      int index = -1;
      if (y == 0)
      {
         index = x;
      }
      else if (y == iPixelsWide - 1)
      {
         index = x + (iPixelsWide * 3) - 4;
      }
      else
      {
         index = iPixelsWide + 2 * y - 1;
         if (x == 0)
         {
            index--;
         }
      }
      return index;
   }

   @Override
   public String toString()
   {
      String retVal = "";
      for (int y = 0; y < iPixelsWide; y++)
      {
         String output = "";
         for (int x = 0; x < iPixelsWide; x++)
         {
            int value = getPixel(x, y);
            // System.out.println("Value=" + value);
            if (value == 1)
            {
               output += "X";
            }
            else if (value == 0)
            {
               output += " ";
            }
            else
            {
               // 2 = Forbidden to set to 1
               output += ".";
            }
         }
         retVal += output + "\n";
      }

      return retVal;
   }

   public int getPixel(int x, int y)
   {
      if (x > 0 && x < iPixelsWide - 1)
      {
         // In the middle, just 0 and iPixelWide allowed
         if (y > 0 && y < iPixelsWide - 1)
         {
            return 1;
         }
      }

      int index = getIndex(x, y);
      // System.out.println(x + ", " + y + ", index=" + index);
      return iPixelArr[index];
   }

}
