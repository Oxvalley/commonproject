package program;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import enums.ComponentType;
import enums.FrameType;
import interfaces.IBOList;
import interfaces.IBusinessClass;
import interfaces.IButton;
import interfaces.IColumn;
import interfaces.IComponent;
import interfaces.IFrame;
import interfaces.IPanel;
import interfaces.ISQLUtil;
import interfaces.ISelected;
import program.components.Button;
import program.components.EditButtons;
import program.components.OKCancelButtons;
import program.components.TablePanel;
import program.components.extension.JFrameExtension;
import program.components.extension.JPanelExtension;
import program.components.extension.JScrollPaneExtension;
import program.components.implementors.OKCancelImpl;
import program.properties.Language;

public class ProgramCreator
{

   private ISQLUtil i_SQLObj = null;
   private IBOList i_IBO = null;
   private HashMap<IBusinessClass, IPanel> i_CachedIPanels = new HashMap<IBusinessClass, IPanel>();

   // Ugly fix
   // TODO prio 3 add reloadListener (and saveListener)
   public static JTable GlobalTable = null;
   public static IFrame GlobalFrame = null;

   public static IFrame MainFrame = null;
   public static IPanel MainPanel = null;

   // TODO prio 4 Make ProgramCreator static
   public ProgramCreator(ISQLUtil sqlobj)
   {
      i_SQLObj = sqlobj;
   }

   public IFrame getIFrame(String compGroupName, int frameType, IBOList ibo)
   {
      i_IBO = ibo;
      return getIFrameFromList(compGroupName, frameType, ibo.getBOList());
   }

   public IFrame getIFrameFromList(String compGroupName, int frameType,
         List<IBusinessClass> list)
   {
      switch (frameType)
      {
      case FrameType.Table:
         return getIFrame(compGroupName, list, getTableIPanel(compGroupName,
               false, list), ".grid.title");

      case FrameType.EditableTable:
         return getIFrame(compGroupName, list, getTableIPanel(compGroupName,
               true, list), ".grid.title");

      case FrameType.FormEdit:
         if (list != null && list.size() > 0)
         {
            return getFormIFrame(compGroupName, list.get(0), false,
                  ".form.title");
         }
         else
         {
            // No BO object sent in
            return null;
         }

      case FrameType.FormNew:
         if (list != null && list.size() > 0)
         {
            return getFormIFrame(compGroupName, list.get(0), true,
                  ".form.title");
         }
         else
         {
            // No BO object sent in
            return null;
         }

      case FrameType.ScrollableTopTableMain:
         if (list != null && list.size() > 0)
         {
            return getScrollableTopTableMainIFrame(compGroupName, list);
         }
         else
         {
            // No BO object sent in
            return null;
         }

      default:
         System.out.println("Unknown frameType: " + frameType);
         return null;
      }
   }

   private IFrame getScrollableTopTableMainIFrame(String compGroupName,
         List<IBusinessClass> list)
   {
      IPanel panel = getScrollableTopTableMainIPanel(compGroupName, list);
      return getDefaultIFrame(compGroupName, panel,
            "program.mainprogram.title", "Manage database with MainProgram 1.0");
   }

   private IPanel getScrollableTopTableMainIPanel(String compGroupName,
         List<IBusinessClass> list)
   {
      IPanel mainpanel = new JPanelExtension(new BorderLayout());
      IPanel panelMain = new JPanelExtension(new FlowLayout());
      if (list.size() > 0)
      {
         IBusinessClass businessClass = list.get(0);
         IPanel cached = getCachedITablePanel(businessClass, compGroupName);
         panelMain.add(cached);
      }

      MainPanel = panelMain;
      IPanel panelTop = getScrollableTopIPanel(compGroupName, list);
      mainpanel.add(panelTop, BorderLayout.NORTH);
      mainpanel.add(panelMain, BorderLayout.SOUTH);
      return mainpanel;
   }

   private IPanel getCachedITablePanel(IBusinessClass businessClass,
         String compGroupName)
   {
      IPanel pan = i_CachedIPanels.get(businessClass);
      if (pan == null)
      {
         List<IBusinessClass> lst = businessClass.getBOList("top 100 *", "");
         pan = getTableIPanel(compGroupName, true, lst);
         i_CachedIPanels.put(businessClass, pan);
      }

      return pan;
   }

   private IPanel getScrollableTopIPanel(final String compGroupName,
         List<IBusinessClass> list)
   {
      IPanel panel = new JPanelExtension(new FlowLayout());
      IPanel innerPanel = new JPanelExtension(new GridLayout(1, 0));
      for (final IBusinessClass businessClass : list)
      {
         innerPanel.add(new Button(compGroupName, new IButton()
         {
            @Override
            public void init()
            {

            }

            @Override
            public void doAction()
            {
               for (Component comp : MainPanel.getComponents())
               {
                  comp.setVisible(false);
               }
               IPanel pan = getCachedITablePanel(businessClass, compGroupName);
               MainPanel.add(pan);
               pan.setVisible(true);
               MainFrame.validate();
            }
         }, businessClass.getLangText(compGroupName), "options.jpg"));

      }
      //innerPanel.setPreferredSize(new Dimension(800, 200));
      JScrollPaneExtension scroll = new JScrollPaneExtension(innerPanel);
      scroll
            .setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

      panel.add(scroll);
      return panel;
   }

   public IFrame getIFrameFromBO(String compGroupName, int frameType,
         IBusinessClass bo)
   {
      if (bo == null)
      {
         return null;
      }

      ArrayList<IBusinessClass> lst = new ArrayList<IBusinessClass>();
      lst.add(bo);
      return getIFrameFromList(compGroupName, frameType, lst);
   }

   public IFrame getFormIFrame(String compGroupName, IBusinessClass bo,
         boolean isNew, String key)
   {
      if (bo == null)
      {
         return null;
      }

      IFrame frame = new JFrameExtension(Language.getTextWithLangWord(compGroupName,
            "tbl." + bo.getTableName() + key, bo.getTableName() + "form", bo
                  .getLangText(compGroupName)));
      // TODO prio 3 Dimensions in properties
      frame.setPreferredSize(new Dimension(600, 90 + 45 * bo.getColumnList()
            .size()));
      // frame.setPreferredSize(new Dimension(600, 80*));
      frame.setLayout(new GridLayout(0, 1));
      IPanel mainpanel = getFormOKCancelIPanel(compGroupName, bo, isNew, frame);

      frame.add(mainpanel);
      frame.pack();
      return frame;
   }

   private IPanel getFormOKCancelIPanel(String compGroupName,
         IBusinessClass bo, boolean isNew, IFrame frame)
   {
      IPanel mainpanel = getFormIPanel(compGroupName, bo, isNew);

      OKCancelButtons okcancel = getOKCanelIPanel(compGroupName, bo, isNew,
            frame);

      mainpanel.add(okcancel);

      bo.resetDirtyFlag();
      return mainpanel;
   }

   private IPanel getFormIPanel(String compGroupName, IBusinessClass bo,
         boolean isNew)
   {
      IPanel mainpanel = new JPanelExtension(new FlowLayout());
      for (IColumn col : bo.getColumnList())
      {
         IPanel panel = new JPanelExtension(new FlowLayout());
         IComponent label = col.getIComponent(compGroupName,
               ComponentType.ColumnNameLabel);

         panel.add(label);
         IComponent field = col.getIComponent(compGroupName,
               ComponentType.FormInput);
         if (!isNew && col.isPrimaryKey())
         {
            field.setEnabled(false);
         }

         // TODO prio 3 Dimensions in properties
         label.setPreferredSize(new Dimension(100, 30));
         field.setPreferredSize(new Dimension(400, 30));
         panel.add(field);
         mainpanel.add(panel);
      }
      return mainpanel;
   }

   private OKCancelButtons getOKCanelIPanel(String compGroupName,
         IBusinessClass bo, boolean isNew, IFrame frame)
   {
      OKCancelButtons okcancel = new OKCancelButtons(compGroupName,
            new OKCancelImpl(bo, frame, isNew));
      return okcancel;
   }

   private IFrame getIFrame(String compGroupName, List<IBusinessClass> list,
         IPanel ipanel, String key)
   {
      String title = null;
      if (list.size() > 0)
      {
         IBusinessClass bo = list.get(0);
         title = Language.getTextWithLangWord(compGroupName, "tbl."
               + bo.getTableName() + key, bo.getTableName() + "grid", bo
               .getLangText(compGroupName));
      }
      IFrame frame = new JFrameExtension(title);
      GlobalFrame = frame;
      frame.add(ipanel);
      frame.pack();
      frame.setPreferredSize(new Dimension(600, 600));
      return frame;
   }

   private IFrame getDefaultIFrame(String compGroupName, IPanel ipanel,
         String langKey, String langText)
   {
      IFrame frame = new JFrameExtension(Language.getText(compGroupName, langKey,
            langText));
      MainFrame = frame;
      frame.add(ipanel);
      frame.pack();
      frame.setPreferredSize(new Dimension(600, 600));
      return frame;
   }

   private IPanel getTableIPanel(String compGroupName, boolean editable,
         List<IBusinessClass> list)
   {
      IPanel panel = new JPanelExtension(list, i_SQLObj);
      panel.setLayout(new BorderLayout());
      TablePanel tablepane = new TablePanel(compGroupName, list, i_SQLObj,
            i_IBO, this);
      ISelected selected = tablepane;
      panel.add(tablepane, BorderLayout.NORTH);
      if (editable)
      {
         panel.add(new EditButtons(compGroupName, list, i_SQLObj, selected,
               this), BorderLayout.SOUTH);
      }
      return panel;
   }

   public void showAsMainWindow(IFrame frame)
   {
      frame.setAsMainWindow(frame);
      frame.setVisible(true);
   }
}
