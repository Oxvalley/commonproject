package renderer;

import java.text.Format;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class FormatRenderer extends DefaultTableCellRenderer
{
   // private Format formatter;

   public FormatRenderer(Format formatter)
   {
      if (formatter == null)
         throw new NullPointerException();
      // this.formatter = formatter;
   }

   // protected void setValue(Object obj) {
   // setText(obj == null ? "" : formatter.format(obj));
   // }

   // demo
   public static void main(String[] args)
   {
      double a = 1.25, b = 3000.50;
      Object[][] rowData = { { a, a, a }, { b, b, b } };
      Object[] columnNames = { "default renderer", "left justified",
            "right justified" };
      TableModel model = new DefaultTableModel(rowData, columnNames);
      JTable table = new JTable(model);
      TableColumnModel tcm = table.getColumnModel();

      NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.UK);
      FormatRenderer r = new FormatRenderer(formatter);
      tcm.getColumn(1).setCellRenderer(r);

      r = new FormatRenderer(formatter);
      r.setHorizontalAlignment(SwingConstants.RIGHT);
      tcm.getColumn(2).setCellRenderer(r);

      JFrame f = new JFrame("FormatRenderer");
      f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      f.getContentPane().add(new JScrollPane(table));
      f.pack();
      f.setLocationRelativeTo(null);
      f.setVisible(true);
   }
}
