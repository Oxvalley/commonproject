OpenScad Formatter is a free tool.

It can indent any .scad file for you.

PREREQUISITES
=============
Java runtime 8 or above installed.

How to use
==========

1. Unzip this zip file
2. In Windows: Double-click run OpenScad_Formatter_v_1_0.cmd 
or
2. In Linux: run file run_OpenScad_Formatter_v_1_0.sh

IMPORTANT! The OpenScad file MUST be saved before formatting the file.

A Window with 3 (active) buttons will open.

- To format a single file, 
    use button "Format File" and browse to your file.
	
- To format last saved file, 
    use button "Settings" and browse to the top folder where you saved your .scad files. 
	Click close. 
	Click button "Format Last Saved File"
	Next time, just click "Format Last Saved File" again.
	
Get OpenScad to accept newly formatted file
==========================================

In 	OpenSCad: Use menu File->Reload (Or Ctrl+R)
or
In OpenScad menu: Design->Reload and Preview (Or F4)
or
Set OpenScad to refresh automatically by checking menu: Design->Automatic Reload and Preview
	
	
Settings to change
==================

Under button "Settings" you may:
- Change Source Path to top folder of .scad files
- Turn all Tabs to spaces. Or not.
- Change the number of spaces to indent. Default is 2.
- Turn of the question by unchecking "Ask before format last saved"
By default a copy of your file is saved in the same folder as the original. This backup can be turned off by unchecking "Backup File before formatting"

Saving settings
===============
Settings are saved when the button "Close" is clicked.

The program is used as-is and not warranties or support exists

(C) Oxvalley Software 2021


 	