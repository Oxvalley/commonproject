package common.utils.open.scad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.lang3.StringUtils;

import common.properties.SettingProperties;
import common.utils.FileFilter;
import common.utils.FileUtil;

public class OpenSCADFormatterOld
{

   private String iSourcePath;
   private SettingProperties iIProps;
   private boolean write = true;
   private Stack<Integer> iStack = new Stack<>();

   public OpenSCADFormatterOld(SettingProperties iProps)
   {
      iIProps = iProps;
      iSourcePath = iProps.getProperty(OpenSCADUI.SCAD_SOURCE_PATH);
   }

   public void formatAllFiles()
   {
      List<File> allFiles = getAllSources(getSourcePath(), ".scad");
      for (File file : allFiles)
      {
         System.out.println(file.getAbsolutePath());
      }
   }

   private static List<File> getAllSources(String sourcePath, String tag)
   {
      Collection<File> files = FileUtils.listFiles(new File(sourcePath),
            new FileFilter(tag), DirectoryFileFilter.DIRECTORY);

      return new ArrayList<>(files);
   }

   public void format(String path)
   {

      // TODO Remove more than one empty line

      BufferedReader reader = null;

      String backupFilename = path + "." + getRandomCharacters(7);
      System.out.println(backupFilename);
      if (!copyFile(path, backupFilename))
      {
         System.out.println("Could not copy file. No formatting done. Exiting");
         return;
      }

      File file = new File(path);
      Writer w = null;
      try
      {
         reader = new BufferedReader(new FileReader(backupFilename));

         if (write)
         {
            try
            {
               w = new FileWriter(file);
            }
            catch (IOException e)
            {
               System.out.println(
                     "Could not open file " + path + " for writing. Exiting.");
               return;
            }

         }

         String line = reader.readLine();
         int indent = 0;
         int oldindent = -1;
         int origIndent = -1;
         int spaces = iIProps.getIntProperty(OpenSCADUI.SCAD_TAB_SIZE, 2);
         boolean isInComment = false;
         String spaceString = StringUtils.repeat(" ", spaces);

         int spaceTabsChoice =
               iIProps.getIntProperty(OpenSCADUI.SCAD_SPACES_RADIO,
                     OpenSCADUI.RADIO_TURN_TABS_INTO_SPACES);

         while (line != null)
         {
            if (line.trim().isEmpty())
            {
               if (write)
               {
                  w.write(line.trim() + "\n");
               }
            }
            else
            {
               switch (spaceTabsChoice)
               {
                  case OpenSCADUI.RADIO_TURN_TABS_INTO_SPACES:
                     line = line.replace("\t", spaceString);
                     break;

                  case OpenSCADUI.RADIO_TURN_SPACES_INTO_TABS:
                     line = line.replace(spaceString, "\t");
                     break;

                  case OpenSCADUI.RADIO_DO_NOTHING:
                     // Nothing of course
                     break;
               }

               if (!isInComment)
               {
                  if ((StringUtils.countMatches(line, "/*")
                        - StringUtils.countMatches(line, "*/")) > 0)
                  {
                     isInComment = true;
                  }
               }

               if (!isInComment)
               {
                  line = line.trim();

                  if (!line.endsWith(";") && !line.equals("{")
                        && !line.endsWith(">"))
                  {
                     if (!line.startsWith("{") && !line.startsWith("//")
                           && !line.startsWith("/*"))
                     {
                        if (origIndent == -1)
                        {
                           origIndent = indent;
                        }
                        indent++;
                     }
                  }
                  else
                  {
                     if (origIndent > -1)
                     {
                        if (line.equals("{") && origIndent < indent - 1)
                        {
                           indent--;
                        }
                        else
                        {
                           indent = origIndent;
                        }

                        origIndent = -1;
                     }
                  }

                  String line2 = getNonCommentedLine(line);
                  indent += ((StringUtils.countMatches(line2, "{")
                        + StringUtils.countMatches(line2, "(")
                        + StringUtils.countMatches(line2, "/*")
                        + StringUtils.countMatches(line2, "["))
                        - (StringUtils.countMatches(line2, "}")
                              + StringUtils.countMatches(line2, ")")
                              + StringUtils.countMatches(line2, "*/")
                              + StringUtils.countMatches(line2, "]")));

                  if (line.equals("{"))
                  {
                     oldindent--;
                  }

                  if (line.startsWith("}"))
                  {
                     oldindent = (int) Math.floor(iStack.peek() / spaces);
                     indent = oldindent;
                     origIndent = -1;
                  }

               }
               line = StringUtils.repeat(" ", oldindent * spaces) + line;

               // System.out.println(
               // line + " " + oldindent + " " + indent + " " + origIndent+ "
               // "
               // + extra);
               // System.out.println(line);
               if (write)
               {
                  w.write(line + "\n");
               }

               if (!isInComment)
               {
                  iStack = addOpenBracketstoStack(iStack, line);
               }

               if (isInComment)
               {
                  if ((StringUtils.countMatches(line, "*/"))
                        - (StringUtils.countMatches(line, "/*")) > 0)
                  {
                     isInComment = false;
                  }
               }

               oldindent = indent;
            }

            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }

            if (write)
            {
               if (w != null)
               {
                  w.close();
               }
            }
         }
         catch (Exception e)
         {
         }
      }
   }

   private static Stack<Integer> addOpenBracketstoStack(Stack<Integer> iStack,
         String line)
   {
      String line2 = getNonCommentedLine(line);
      for (int i = 0; i < line2.length(); i++)
      {
         char c = line2.charAt(i);
         if (c == '{')
         {
            iStack.push(i);
         }

         if (c == '}')
         {
            iStack.pop();
         }
      }
      return iStack;
   }

   public static String getNonCommentedLine(String line)
   {
      String line2 = line;

      int j = line2.indexOf("//");
      if (j > -1)
      {
         line2 = line2.substring(0, j);
      }
      return line2;
   }

   private static boolean copyFile(String sourceName, String copyName)
   {
      try
      {
         FileUtil.copy(sourceName, copyName, true);
         return true;
      }
      catch (IOException e)
      {
         e.printStackTrace();
         return false;
      }

   }

   private static String getRandomCharacters(int count)
   {
      Random rand = new Random();

      System.out.println((int) '1');

      String retVal = "";
      for (int j = 0; j < count; j++)
      {
         int n = rand.nextInt(36);
         if (n > 26)
         {
            retVal += Character.toString((char) (n + 21));
         }
         else
         {
            retVal += Character.toString((char) (n + 65));
         }

      }
      return retVal;
   }

   public void formatLastSavedFile()
   {
      if (iSourcePath == null)
      {
         iSourcePath = iIProps.getProperty(OpenSCADUI.SCAD_SOURCE_PATH);
      }

      File file = getLastSavedFile();
      if (iIProps.getBooleanProperty(OpenSCADUI.SCAD_ASK_FIRST, true))
      {
         int answer = JOptionPane.showConfirmDialog(null,
               "Do you want to format " + file.getAbsolutePath() + " ?");
         if (answer == JOptionPane.OK_OPTION)
         {
            format(file.getAbsolutePath());
         }
      }
      else
      {
         format(file.getAbsolutePath());
      }

   }

   private File getLastSavedFile()
   {
      List<File> allFiles = getAllSources(getSourcePath(), ".scad");

      long lastTime = -1;
      File lastFile = null;

      for (File file : allFiles)
      {
         long myTime = file.lastModified();
         if (myTime > lastTime)
         {
            lastTime = myTime;
            lastFile = file;
         }

      }

      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      System.out.println(lastFile.getAbsolutePath() + "  "
            + sdf.format(lastFile.lastModified()));
      return lastFile;
   }

   private String getSourcePath()
   {
      return iSourcePath;
   }

}
