package common.utils.open.scad;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import common.properties.SettingProperties;
import common.ui.components.ext.CompExtEnum;
import common.ui.components.ext.JPanelExt;
import common.utils.AppInfoInterface;

public class OpenSCADUI extends JFrame
{
   private static final long serialVersionUID = 1L;

   public static final String SCAD_SOURCE_PATH = "source.path";
   public static final String SCAD_ASK_FIRST = "ask.first";
   public static final String SCAD_SPACES_RADIO = "spaces.radio";
   public static final String SCAD_TAB_SIZE = "tab.size";
   public static final String SCAD_USE_BACKUP = "use.backup";

   private static final String SCAD_FILE_TO_FORMAT = "fileToFormat";
   public static final int RADIO_DO_NOTHING = 0;
   public static final int RADIO_TURN_TABS_INTO_SPACES = 1;
   public static final int RADIO_TURN_SPACES_INTO_TABS = 2;

   private static SettingProperties iProps;

   // TODO Change to name of your icon
   private String ICON_NAME = null;

   // TODO init fields here

   /////////// Java GUI Designer version 1.1 Starts here. ////
   // Text below will be regenerated automatically.
   // Generated 2020-02-27 19:43:07

   private JPanelExt iPanelExt;
   private JButton btnFormatFile;
   private JButton btnFormatLast;
   private JButton btnFormatAll;
   private JButton btnSettings;

   private int width = 674;
   private int height = 145;

   private String mSourcePath;

   private OpenSCADFormatter iFormatter;

   private OpenSCADSettingsUI iSettingsUI;

   public static void main(String[] args)
   {
      OpenSCADUI openSCADUI = new OpenSCADUI();
      openSCADUI.setVisible(true);

   }

   public OpenSCADUI()
   {
      super();
      iProps = SettingProperties.getInstanceWithName("openscad_formatter");
      iFormatter = new OpenSCADFormatter(iProps);
      iSettingsUI = new OpenSCADSettingsUI(iProps, this);

      AppInfoInterface appInfo = AppInfoOpenScad.getInstance();
      String name = appInfo.getApplicationName();
      String version = appInfo.getVersion();
      setTitle(name + " v " + version);
      setSize(width, height);
      if (ICON_NAME != null)
      {
         setIconImage(getToolkit().getImage(ICON_NAME));
      }

      getContentPane().setLayout(new BorderLayout());
      iPanelExt = new JPanelExt(width, height);
      iPanelExt.setLayout(null);
      getContentPane().add(iPanelExt, BorderLayout.CENTER);

      btnFormatFile = new JButton("Format File");
      iPanelExt.addComponentExt(btnFormatFile, 8, 11, 149, 85,
            CompExtEnum.Normal);
      btnFormatFile.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnFormatFileClicked(e);
         }
      });

      btnFormatLast = new JButton("Format Last Saved File");
      iPanelExt.addComponentExt(btnFormatLast, 173, 11, 165, 85,
            CompExtEnum.Normal);
      btnFormatLast.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnFormatLastClicked(e);
         }
      });

      btnFormatAll = new JButton("Format  All Source Files");
      btnFormatAll.setEnabled(false);
      iPanelExt.addComponentExt(btnFormatAll, 354, 8, 182, 88,
            CompExtEnum.Normal);
      btnFormatAll.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnFormatAllClicked(e);
         }
      });

      btnSettings = new JButton("Settings");
      iPanelExt.addComponentExt(btnSettings, 552, 11, 86, 85,
            CompExtEnum.Normal);
      btnSettings.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnSettingsClicked(e);
         }
      });

      setDefaultCloseOperation(EXIT_ON_CLOSE);
      addWindowListener(new WindowAdapter()
      {
         @Override
         public void windowClosing(WindowEvent we)
         {
            iProps.save();
         }
      });

      /////////// Java GUI Designer version 1.1 Generation ends here. ///

      // TODO Add init declarations here

   }

   private void btnFormatLastClicked(ActionEvent e)
   {
      if (!hasSourcepath())
      {
         return;
      }

      iFormatter.formatLastSavedFile();
   }

   public boolean hasSourcepath()
   {
      if (!hasSource())
      {
         JOptionPane.showMessageDialog(null,
               "You must set a Source path first! Find the top folder with all your .scad files you would like to be able to format/indent");
         iSettingsUI.setVisible(true);
         return false;
      }

      return true;
   }

   private boolean hasSource()
   {
      mSourcePath = iProps.getProperty(SCAD_SOURCE_PATH);

      return (mSourcePath != null && !mSourcePath.trim().isEmpty());
   }

   private void btnFormatAllClicked(ActionEvent e)
   {
      // TODO Implement

   }

   private void btnFormatFileClicked(ActionEvent e)
   {
      JFileChooser fc = new JFileChooser();
      fc.setDialogTitle("Find file to format");
      fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      String fileToSave = iProps.getProperty(SCAD_FILE_TO_FORMAT);
      if (fileToSave != null)
      {
         fc.setSelectedFile(new File(fileToSave));
      }

      int retval = fc.showOpenDialog(null);
      if (retval == JFileChooser.APPROVE_OPTION)
      {
         File myFile = fc.getSelectedFile();
         String path = myFile.getAbsolutePath();
         iProps.setProperty(SCAD_FILE_TO_FORMAT, path);
         iFormatter.format(path);
      }
   }

   private void btnSettingsClicked(ActionEvent e)
   {
      iSettingsUI.setVisible(true);
   }

   public void setSource(String sourcePath)
   {
      mSourcePath = sourcePath;
   }

}
