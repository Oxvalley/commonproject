package common.utils.open.scad;

import java.util.List;

public class FindFirst implements Comparable<FindFirst>
{

   private InstructionType iType;
   private int iIndex;
   private String iLine;
   private String iUntrimmedLine;

   public InstructionType getType()
   {
      return iType;
   }

   public FindFirst(String line, List<FindFirst> lst, InstructionType type, String lookFor)
   {
      iType = type;
      iUntrimmedLine = line;
      line = line.trim();
      iIndex = line.indexOf(lookFor);
      if (iIndex > -1)
      {
         lst.add(this);

         switch (type)
         {
            case generic:
            case slash:
            case startComment:
               iLine = line;
               break;

            case semiColon:
            case endBracket:
            case startBracket:
            case endComment:
               iLine = line.substring(0, iIndex + lookFor.length());
               break;

            case top:
            case emptyLine:
               break;
         }
      }
   }

   public String getUntrimmedLine()
   {
      return iUntrimmedLine;
   }

   @Override
   public int compareTo(FindFirst o)
   {
      // TODO Auto-generated method stub
      return iIndex - o.iIndex;
   }

   public String getLine()
   {
      return iLine;
   }

   public int getIndex()
   {
      return iIndex;
   }

}
