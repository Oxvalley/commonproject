package common.utils.open.scad;

public class LineContainer
{

   private String iLine;
   private int iIndent;

   public LineContainer(String line, int indent)
   {
      iLine = line;
      iIndent = indent;
   }

   public boolean isStartBracket()
   {
      return iLine.trim().startsWith("{");
   }

   public boolean isEndBracket()
   {
      return iLine.trim().startsWith("}");
   }

   public boolean hasSemiColon()
   {
      return iLine.trim().endsWith(";");
   }

   public int getIndent()
   {
      return iIndent;
   }

   public void changeIndent(int value)
   {
      iIndent += value;
   }

}
