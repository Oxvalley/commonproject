package common.utils.open.scad;

import java.util.ArrayList;
import java.util.List;

public class InstructionContainer
{

   private InstructionType iType;
   private String iInstruction;
   private List<InstructionContainer> iChildren = new ArrayList<>();
   private String iComment;
   private int iIndex;
   private InstructionContainer iParent;
   private boolean iIsInComment;
   private int iBracketLevel;

   public InstructionContainer()
   {
      // TODO Auto-generated constructor stub
   }

   public InstructionContainer(String instruction, InstructionType type, String comment, InstructionContainer parent,
         int index)
   {
      iInstruction = instruction;
      iType = type;
      iComment = comment;
      iParent = parent;
      iIndex = index;
   }

   public int getIndex()
   {
      return iIndex;
   }

   public InstructionContainer getParent()
   {
      return iParent;
   }

   @Override
   public String toString()
   {
      String text = iInstruction + " - " + iType;
      if (iComment != null)
      {
         text += " " + iComment;
      }
      return text;
   }

   public InstructionType getType()
   {
      return iType;
   }

   public List<InstructionContainer> getChildren()
   {
      return iChildren;
   }

   public String getComment()
   {
      return iComment;
   }

   public void setIfInComment(boolean value)
   {
      iIsInComment = value;
   }

   public void setInstruction(String instruction)
   {
      iInstruction = instruction;
   }

   public String getInstruction()
   {
      return iInstruction;
   }

   public InstructionContainer addChild(String instruction, InstructionType type, String comment,
         InstructionContainer parent, int index, int bracketLevel)
   {
      return addChild(instruction, type, comment, parent, index, false, bracketLevel);
   }

   public int getBracketLevel()
   {
      return iBracketLevel;
   }

   public void setBracketLevel(int bracketLevel)
   {
      iBracketLevel = bracketLevel;
   }

   public InstructionContainer addChild(String instruction, InstructionType type, String comment,
         InstructionContainer parent, int index)
   {
      return addChild(instruction, type, comment, parent, index, false, -1);
   }

   public InstructionContainer addChild(String instruction, InstructionType type, String comment,
         InstructionContainer parent, int index, boolean isInComment)
   {
      return addChild(instruction, type, comment, parent, index, isInComment, -1);
   }
   
   public InstructionContainer addChild(String instruction, InstructionType type, String comment,
         InstructionContainer parent, int index, boolean isInComment, int bracketLevel)
   {
      InstructionContainer retVal = new InstructionContainer(instruction, type, comment, parent, index);
      retVal.setIfInComment(isInComment);

      if (type == InstructionType.startBracket)
      {
         retVal.setBracketLevel(bracketLevel);
      }

      iChildren.add(retVal);
      return retVal;
   }

   public boolean isInComment()
   {
      return iIsInComment;
   }

}
