package common.utils.open.scad;

import common.utils.AppInfoInterface;
import common.utils.PackInstallationUtil;

/**
 * The Class PackInstallationUtilCommon.
 */
public class PackInstallationUtilOpenScadFormatter extends PackInstallationUtil
{


   /**
    * Instantiates a new pack installation util common.
    *
    * @param appName the app name
    * @param version the version
    * @param jdkVersion the jdk version
    * @param appNameVersion the app name version
    * @param mainClassNAme the main class N ame
    * @param packSource the pack source
    */
   public PackInstallationUtilOpenScadFormatter(String appName, String version,
         String jdkVersion, String appNameVersion, String mainClassNAme,
         boolean packSource, boolean createJavadoc)
   {
      super(appName, version, jdkVersion, appNameVersion, mainClassNAme,
            packSource, createJavadoc);
   }

   /**
    * The main method.
    *
    * @param args the arguments
    */
   public static void main(String[] args)
   {
      boolean packSource = false;
      boolean createJavadoc = false;
      if (args.length == 1)
      {
         if (args[0].equalsIgnoreCase("src"))
         {
            packSource = true;
         }

         if (args[0].equalsIgnoreCase("javadoc"))
         {
            createJavadoc = true;
         }
      }
      if (args.length == 2)
      {
         if (args[1].equalsIgnoreCase("src"))
         {
            packSource = true;
         }

         if (args[1].equalsIgnoreCase("javadoc"))
         {
            createJavadoc = true;
         }
      }
      
      AppInfoInterface appInfo = AppInfoOpenScad.getInstance();
      String name = appInfo.getApplicationName();
      String version = appInfo.getVersion();
      String jdkVersion = appInfo.getJdkVersion();
      String appNameVersion = "OpenScad_Formatter_v_" + getVersionNoDots(version);
      String mainClass = "common.utils.open.scad.OpenSCADUI";
      PackInstallationUtil pi = new PackInstallationUtilOpenScadFormatter(name, version,
            jdkVersion, appNameVersion, mainClass, packSource, createJavadoc);
      pi.execute();
   }
   
   /* (non-Javadoc)
    * @see common.utils.PackInstallationUtil#copyFilesAndFolders()
    */
   @Override
   protected void copyFilesAndFolders()
   {
      copyFileAndLog("src\\common\\utils\\open\\scad\\readme.txt");
   }

}
