package common.utils.open.scad;

public enum InstructionType
{
  semiColon, startBracket, endBracket, slash, startComment, endComment, generic, emptyLine, top;
}
