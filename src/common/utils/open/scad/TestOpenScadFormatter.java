package common.utils.open.scad;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import common.properties.SettingProperties;
import junit.framework.TestCase;

public class TestOpenScadFormatter extends TestCase
{

   private static final String INPUT_FILE_PATH = "openscad_input.txt";
   private static final String OUTPUT_FILE_PATH = "openscad_output.txt";
   private static final String SEPARATOR_PREFIX = "===!!";
   private static final String SEPARATOR_SUFFIX = "!!===";
   private Map<String, List<String>> iInputs = null;
   private Map<String, List<String>> iOutputs = null;

   public void test1() throws IOException
   {
      List<String> lstFormat = new ArrayList<>();
      List<String> lstExpected = new ArrayList<>();

      lstFormat.add("module test()");
      lstFormat.add("{");
      lstFormat.add("echo \"hej\");");
      lstFormat.add("}");

      lstExpected.add("module test()");
      lstExpected.add("{");
      lstExpected.add("  echo \"hej\");");
      lstExpected.add("}");

      makeTest(lstFormat, lstExpected);
   }

   public void test2() throws IOException
   {
      List<String> lstFormat = new ArrayList<>();
      List<String> lstExpected = new ArrayList<>();

      lstFormat.add("module test()");
      lstFormat.add("{");
      lstFormat.add("echo \"hej\");");
      lstFormat.add("}");

      lstExpected.add("module test()");
      lstExpected.add("{");
      lstExpected.add("  echo \"hej\");");
      lstExpected.add("}");

      makeTest(lstFormat, lstExpected);
   }

   public void test3() throws IOException
   {
      List<String> lstFormat = new ArrayList<>();
      List<String> lstExpected = new ArrayList<>();

      lstFormat.add("module test3()");
      lstFormat.add("{");
      lstFormat.add("if(front_side)");
      lstFormat.add("{");
      lstFormat.add("translate([x_pos,y_pos-3,z_pos])");
      lstFormat.add("rotate([-90,0,0])");
      lstFormat.add("pin_distance();");
      lstFormat.add("");
      lstFormat.add("translate([x_pos,y_pos-8,z_pos])");
      lstFormat.add("rotate([-90,rotate_degrees,0])");
      lstFormat.add("wheel(cogs);");
      lstFormat.add("");
      lstFormat.add("   if(add_clips)");
      lstFormat.add("{");
      lstFormat.add("translate([x_pos,y_pos-8.2,z_pos])");
      lstFormat.add("rotate([-90,180,180])");
      lstFormat.add("hair_clip_open();");
      lstFormat.add("}");
      lstFormat.add("}");
      lstFormat.add("else");
      lstFormat.add("{");
      lstFormat.add("// Backside");
      lstFormat.add("translate([x_pos,y_pos+11,z_pos])");
      lstFormat.add("rotate([90,0,0])");
      lstFormat.add("pin_distance();");
      lstFormat.add("}");
      lstFormat.add("}");

      lstExpected.add("module test3()");
      lstExpected.add("{");
      lstExpected.add("  if(front_side)");
      lstExpected.add("  {");
      lstExpected.add("    translate([x_pos,y_pos-3,z_pos])");
      lstExpected.add("      rotate([-90,0,0])");
      lstExpected.add("        pin_distance();");
      lstExpected.add("");
      lstExpected.add("    translate([x_pos,y_pos-8,z_pos])");
      lstExpected.add("      rotate([-90,rotate_degrees,0])");
      lstExpected.add("        wheel(cogs);");
      lstExpected.add("");
      lstExpected.add("    if(add_clips)");
      lstExpected.add("    {");
      lstExpected.add("      translate([x_pos,y_pos-8.2,z_pos])");
      lstExpected.add("        rotate([-90,180,180])");
      lstExpected.add("          hair_clip_open();");
      lstExpected.add("    }");
      lstExpected.add("  }");
      lstExpected.add("  else");
      lstExpected.add("  {");
      lstExpected.add("    // Backside");
      lstExpected.add("    translate([x_pos,y_pos+11,z_pos])");
      lstExpected.add("      rotate([90,0,0])");
      lstExpected.add("        pin_distance();");
      lstExpected.add("  }");
      lstExpected.add("}");

      makeTest(lstFormat, lstExpected);
   }

   public void test4() throws IOException
   {
      initFiles();
      test("test4");
   }

   public void test5() throws IOException
   {
      initFiles();
      test("test5");
   }

   public void test6() throws IOException
   {
      initFiles();
//      OpenSCADFormatter.printTree(test("test6"));
      test("test6");
   }

   public void test7() throws IOException
   {
      initFiles();
//      OpenSCADFormatter.printTree(test("test7"));
      test("test7");
   }

   public void initFiles()
   {
      iInputs = initFiles(iInputs, INPUT_FILE_PATH);
      iOutputs = initFiles(iOutputs, OUTPUT_FILE_PATH);
   }

   private InstructionContainer test(String key) throws IOException
   {
      return test(key, true);
   }

   private InstructionContainer test(String key, boolean markLines) throws IOException
   {
      List<String> lstInput = iInputs.get(key);
      List<String> lstOutput = iOutputs.get(key);

      assertTrue("Input file has no key " + key, lstInput != null);
      assertTrue("Output file has no key " + key, lstOutput != null);

      System.out.println(SEPARATOR_PREFIX + key + SEPARATOR_SUFFIX);

      return makeTest(lstInput, lstOutput, markLines);
   }

   private static Map<String, List<String>> initFiles(Map<String, List<String>> collectionMap, String path)
   {
      if (collectionMap != null)
      {
         return collectionMap;
      }

      try
      {
         collectionMap = new HashMap<String, List<String>>();
         BufferedReader iReader = new BufferedReader(new FileReader(path));
         String line;

         String key = "";
         List<String> lst = new ArrayList<>();

         while ((line = iReader.readLine()) != null)
         {
            int i = line.indexOf(SEPARATOR_PREFIX);
            int j = line.indexOf(SEPARATOR_SUFFIX);

            if (i != -1 && j != -1 && j > i + SEPARATOR_PREFIX.length())
            {
               collectLine(collectionMap, key, lst);

               lst = new ArrayList<>();

               key = line.substring(i + SEPARATOR_PREFIX.length(), j);
            }
            else
            {
               lst.add(line);
            }
         }
         collectLine(collectionMap, key, lst);
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }

      return collectionMap;
   }

   public static void collectLine(Map<String, List<String>> collectionMap, String key, List<String> lst)
   {
      if (!key.isEmpty())
      {
         collectionMap.put(key, lst);
      }
   }

   private static InstructionContainer makeTest(List<String> lstFormat, List<String> lstDone) throws IOException
   {
      return makeTest(lstFormat, lstDone, true);
   }

   private static InstructionContainer makeTest(List<String> lstFormat, List<String> lstDone, boolean markLines) throws IOException
   {


      List<String> lstOutput = new ArrayList<>();
      LinesHolder lh = new LinesHolder(lstFormat, false, SettingProperties.getInstance());
      InstructionContainer topParent = new InstructionContainer(null, InstructionType.top, null, null, -1);
      lh.getInstructionFromArray(topParent);
      OpenSCADFormatter.printInstructionInner(topParent, null, false, 2, lstOutput);
      
      
      assertTrue("Lists are not the same size (" + lstOutput.size() + ") and (" + lstDone.size() + ")",
            lstOutput.size() == lstDone.size());

      int errors = 0;

      int length = lstOutput.size();
      System.out.println();

      for (int i = 0; i < length; i++)
      {
         String line = lstOutput.get(i);
         if (!line.equals(lstDone.get(i)))
         {
            if (markLines)
            {
               line += " // << Line " + i + " >"+lstDone.get(i);
            }
            
            errors++;
         }

         System.out.println(line);

      }

      assertTrue("There are errors", errors == 0);
      return topParent;
   }
}
