package common.utils.open.scad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.lang3.StringUtils;

import common.properties.SettingProperties;
import common.utils.FileFilter;
import common.utils.FileUtil;

public class OpenSCADFormatter
{

   private String iSourcePath;
   private SettingProperties iProps;
   private boolean write = true;
   private boolean backup = true;

   public OpenSCADFormatter(SettingProperties props)
   {
      iProps = props;
      iSourcePath = iProps.getProperty(OpenSCADUI.SCAD_SOURCE_PATH);
   }

   public void formatAllFiles()
   {
      List<File> allFiles = getAllSources(getSourcePath(), ".scad");
      for (File file : allFiles)
      {
         System.out.println(file.getAbsolutePath());
      }
   }

   private static List<File> getAllSources(String sourcePath, String tag)
   {
      Collection<File> files =
            FileUtils.listFiles(new File(sourcePath), new FileFilter(tag), DirectoryFileFilter.DIRECTORY);

      return new ArrayList<>(files);
   }

   public static void main(String[] args)
   {
      OpenSCADFormatter of = new OpenSCADFormatter(SettingProperties.getInstance());
      of.format("cogwheels_copy.scad", "cogwheels_copy_test.scad");
   }

   public void format(String path)
   {
      format(path, path);
   }

   public void format(String path, String savePath)
   {

      // TODO Remove more than one empty line

      BufferedReader reader = null;

      String backupFilename;

      if (backup)
      {
         backupFilename = path + "." + getRandomCharacters(7);
         if (!copyFile(path, backupFilename))
         {
            System.out.println("Could not copy file " + path + ". No formatting done. Exiting");
            return;
         }
      }
      else
      {
         backupFilename = path;
      }

      LinesHolder lh = new LinesHolder(backupFilename, false, iProps);
      InstructionContainer topParent = new InstructionContainer(null, InstructionType.top, null, null, -1);
      lh.getInstruction(topParent);

      File file = new File(savePath);
      FileWriter w = null;
      if (write)
      {
         try
         {
            w = new FileWriter(file);
         }
         catch (IOException e)
         {
            System.out.println("Could not open file " + path + " for writing. Exiting.");
            closeWriter(w);
            return;
         }

      }

      try
      {
         printInstruction(topParent, w);

      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }

            closeWriter(w);
         }
         catch (Exception e)
         {
         }
      }
   }

   public void closeWriter(FileWriter w)
   {
      if (write)
      {
         if (w != null)
         {
            try
            {
               w.close();
            }
            catch (IOException e)
            {
               e.printStackTrace();
            }
         }
      }
   }

   private void printInstruction(InstructionContainer ic, FileWriter w) throws IOException
   {
      printInstructionInner(ic, w, true, iProps.getIntProperty(OpenSCADUI.SCAD_TAB_SIZE, 2), null);
   }

   public static void printInstructionInner(InstructionContainer ic, FileWriter w, boolean writeNow, int spaces,
         List outputArray)
      throws IOException
   {
      String line = ic.getInstruction();
      if (line == null)
      {
         line = "";
      }

      if (!ic.isInComment())
      {
         line = line.trim();
      }

      if (ic.getComment() != null)
      {
         line += " " + ic.getComment().trim();
      }

      String prefix = StringUtils.repeat(" ", ic.getIndex() * spaces);
      if (line.trim().isEmpty())
      {
         prefix = "";
      }

      String output = prefix + line;

      if (ic.getParent() != null)
      {
         if (writeNow)
         {
            System.out.println(output);
            w.write(output + "\n");
         }

         if (outputArray != null)
         {
            outputArray.add(output);
         }
      }

      for (InstructionContainer instr : ic.getChildren())
      {
         printInstructionInner(instr, w, writeNow, spaces, outputArray);
      }

   }

   public static String getNonCommentedLine(String line)
   {
      String line2 = line;

      int j = line2.indexOf("//");
      if (j > -1)
      {
         line2 = line2.substring(0, j);
      }
      return line2;
   }

   private static boolean copyFile(String sourceName, String copyName)
   {
      try
      {
         FileUtil.copy(sourceName, copyName, true);
         return true;
      }
      catch (IOException e)
      {
         e.printStackTrace();
         return false;
      }

   }

   private static String getRandomCharacters(int count)
   {
      Random rand = new Random();

      // System.out.println((int) '1');

      String retVal = "";
      for (int j = 0; j < count; j++)
      {
         int n = rand.nextInt(36);
         if (n > 26)
         {
            retVal += Character.toString((char) (n + 21));
         }
         else
         {
            retVal += Character.toString((char) (n + 65));
         }

      }
      return retVal;
   }

   public void formatLastSavedFile()
   {
      if (iSourcePath == null)
      {
         iSourcePath = iProps.getProperty(OpenSCADUI.SCAD_SOURCE_PATH);
      }

      File file = getLastSavedFile();
      if (iProps.getBooleanProperty(OpenSCADUI.SCAD_ASK_FIRST, true))
      {
         int answer = JOptionPane.showConfirmDialog(null, "Do you want to format " + file.getAbsolutePath() + " ?");
         if (answer == JOptionPane.OK_OPTION)
         {
            format(file.getAbsolutePath());
         }
      }
      else
      {
         format(file.getAbsolutePath());
      }

   }

   private File getLastSavedFile()
   {
      List<File> allFiles = getAllSources(getSourcePath(), ".scad");

      long lastTime = -1;
      File lastFile = null;

      for (File file : allFiles)
      {
         long myTime = file.lastModified();
         if (myTime > lastTime)
         {
            lastTime = myTime;
            lastFile = file;
         }

      }

      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      if (lastFile != null)
      {
         System.out.println(lastFile.getAbsolutePath() + "  " + sdf.format(lastFile.lastModified()));
      }
      return lastFile;
   }

   private String getSourcePath()
   {
      return iSourcePath;
   }

   public static void printTree(InstructionContainer parent)
   {
      printTreeInner(parent, "");

   }

   private static void printTreeInner(InstructionContainer parent, String prefix)
   {
      System.out.println(prefix + parent.toString()+" | indent: " + parent.getIndex()+" Bracket Level: " + parent.getBracketLevel());
      for (InstructionContainer child : parent.getChildren())
      {
         printTreeInner(child, prefix + " ");
      }
   }

}
