package common.utils.open.scad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import common.properties.SettingProperties;

public class LinesHolder
{

   private String iBackupFilename;
   private BufferedReader iReader = null;
   private boolean iWrite;
   private SettingProperties iProp;
   private String iLineRest = "";
   private boolean isComment = false;
   private List<String> iSourceArray;
   private int iIndex;

   public LinesHolder(String backupFilename, SettingProperties prop)
   {
      this(backupFilename, true, prop);
   }

   public LinesHolder(String backupFilename, boolean write, SettingProperties prop)
   {
      iBackupFilename = backupFilename;
      iWrite = write;
      iProp = prop;
   }

   public LinesHolder(List<String> sourceArray, boolean write, SettingProperties prop)
   {
      this((String) null, write, prop);
      iSourceArray = sourceArray;
      iIndex = 0;
   }

   public InstructionContainer getInstructionFromArray(InstructionContainer topParent)
   {
      String line = null;

      InstructionContainer ic = topParent;
      while (iIndex < iSourceArray.size())
      {
         if (iLineRest.trim().isEmpty())
         {
            line = iSourceArray.get(iIndex++);
            if (line == null)
            {
               return null;
            }

            iLineRest = line;
         }

         ic = getNextInstruction(ic, iLineRest);
      }

      return topParent;
   }

   public InstructionContainer getInstruction(InstructionContainer topParent)
   {
      String line = null;

      try
      {
         if (iReader == null)
         {
            File file = new File(iBackupFilename);
            Writer w = null;
            iReader = new BufferedReader(new FileReader(iBackupFilename));

            if (iWrite)
            {
               try
               {
                  w = new FileWriter(file);
               }
               catch (IOException e)
               {
                  System.out.println("Could not open file " + iBackupFilename + " for writing. Exiting.");
                  return null;
               }

            }
         }

         InstructionContainer ic = topParent;
         while (true)
         {
            if (iLineRest.trim().isEmpty())
            {
               line = iReader.readLine();
               if (line == null)
               {
                  return null;
               }

               iLineRest = line;
            }

            ic = getNextInstruction(ic, iLineRest);
         }

      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

      return topParent;
   }

   private InstructionContainer getNextInstruction(InstructionContainer parent, String line)
   {
      InstructionContainer retValParent = parent;

      // Are we in a comment?

      List<FindFirst> lst = new ArrayList<>();
      InstructionType type;
      String unTrimmedLine = line;
      line = line.trim();

      // TODO
      // int spaceTabsChoice =
      // iProp.getIntProperty(OpenSCADUI.SCAD_SPACES_RADIO,
      // OpenSCADUI.RADIO_TURN_TABS_INTO_SPACES);

      if (line == null || line.isEmpty())
      {
         type = InstructionType.emptyLine;
      }
      else
      {
         type = InstructionType.generic;
      }

      // Look for semicolon
      FindFirst ff = new FindFirst(line, lst, InstructionType.semiColon, ";");

      // Look for start Bracket
      ff = new FindFirst(line, lst, InstructionType.startBracket, "{");

      // Look for end Bracket
      ff = new FindFirst(line, lst, InstructionType.endBracket, "}");

      // Look for //
      ff = new FindFirst(line, lst, InstructionType.slash, "//");

      // Look for /*
      ff = new FindFirst(line, lst, InstructionType.startComment, "/*");

      // Look for /*
      ff = new FindFirst(line, lst, InstructionType.endComment, "*/");

      int index = -1;
      FindFirst fastest = null;

      if (lst.size() > 0)
      {
         // Which came first?
         Collections.sort(lst);
         fastest = lst.get(0);
         type = fastest.getType();
         index = fastest.getIndex();
      }

      if (isComment && !type.equals(InstructionType.endComment))
      {
         String op = line;
         int i = op.indexOf("*/");
         boolean isInComment = false;
         if (i != -1)
         {
            op = op.substring(0, i + 2);
            isComment = false;
         }
         else
         {
            op = unTrimmedLine;
            isInComment = true;
         }

         parent.addChild(op, InstructionType.startComment, null, parent, parent.getIndex() + 1, isInComment);
         removeInstruction(op.trim());
         return retValParent;
      }

      String comment = null;

      switch (type)
      {
         case emptyLine:
            // An instruction without semi colon. new parent
            parent.addChild(line, type, null, parent, parent.getIndex() + 1);
            break;
         case generic:
            // An instruction without semi colon. new parent
            comment = getComment(line);

            retValParent = parent.addChild(line, type, comment, parent, parent.getIndex() + 1);
            if (comment == null)
            {
               removeInstruction(line);
            }
            break;

         case semiColon:
            comment = getComment(fastest.getLine());
            parent.addChild(fastest.getLine(), type, comment, parent, parent.getIndex() + 1);
            if (comment == null)
            {
               removeInstruction(fastest.getLine());
            }
            retValParent = getNewParent(parent);
            break;

         case endBracket:
            if (index == 0)
            {
               // } is first sign on line
               comment = getComment(fastest.getLine());
               int bracketLevel = BracketLevel.getBracketLevel();
               retValParent = getNewParent(getStartBracketParent(parent, bracketLevel - 1));
               BracketLevel.setBracketLevel(BracketLevel.getBracketLevel() - 1);

               parent.addChild(fastest.getLine(), type, comment, parent, parent.getIndex());

               if (comment == null)
               {
                  removeInstruction(fastest.getLine());
               }
            }
            else
            {
               // An instruction without semi colon. new parent
               String op = line.substring(0, index);
               comment = getComment(op);
               retValParent = parent.addChild(op, InstructionType.generic, comment, parent, parent.getIndex() + 1);
               if (comment == null)
               {
                  removeInstruction(op);
               }
            }
            break;

         case startBracket:
            if (index == 0)
            {
               int bracketLevel = BracketLevel.getBracketLevelAndIncrease();
               line = fastest.getLine().substring(0, 1);
               comment = getComment(line);
               retValParent = parent.addChild(line, type, comment, parent, parent.getIndex(), bracketLevel);
               if (comment == null)
               {
                  removeInstruction(line);
               }
            }
            else
            {
               // An instruction without semi colon. new parent
               String op = line.substring(0, index);
               comment = getComment(op);
               retValParent =
                     parent.addChild(op, InstructionType.generic, comment, parent, parent.getIndex() + 1, 0);
               if (comment == null)
               {
                  removeInstruction(op);
               }
            }

            break;

         case slash:
            if (index == 0)
            {
               parent.addChild(fastest.getLine(), type, null, parent, parent.getIndex() + 1);
            }
            else
            {
               // An instruction without semi colon. new parent
               String op = line.substring(0, index);
               retValParent =
                     parent.addChild(op, InstructionType.generic, line.substring(index), parent, parent.getIndex() + 1);
            }
            iLineRest = "";
            break;
         case startComment:
            if (index == 0)
            {
               isComment = true;
               parent.addChild(fastest.getUntrimmedLine(), type, null, parent, parent.getIndex() + 1);
               iLineRest = "";
            }
            else
            {
               // An instruction without semi colon. new parent
               String op = line.substring(0, index);
               retValParent =
                     parent.addChild(op, InstructionType.generic, line.substring(index), parent, parent.getIndex() + 1);
               removeInstruction(op);
            }
            break;

         case endComment:
            // if (index == 0)
            // {
            comment = getComment(fastest.getLine());
            parent.addChild(fastest.getLine(), type, comment, parent, parent.getIndex() + 1);
            if (comment == null)
            {
               removeInstruction(fastest.getLine());
            }
            isComment = false;
            // }
            // else
            // {
            // // part of comment is forst
            // String com = line.substring(0, index);
            // parent.addChild(com, InstructionType.startComment, null, parent,
            // parent.getIndex() + 1);
            // removeInstruction(com);
            // }
            break;

         case top:
            break;
      }

      return retValParent;
   }

   private InstructionContainer getStartBracketParent(InstructionContainer parent, int bracketLevel)
   {
      if (parent == null)
      {
         return null;
      }

      if (parent.getParent() == null)
      {
         return parent;
      }

      if (parent.getBracketLevel() == bracketLevel)
      {
         return parent;
      }
      else
      {
         return getStartBracketParent(parent.getParent(), bracketLevel);
      }
   }

   private String getComment(String linePart)
   {
      String rest = iLineRest.trim().substring(linePart.length()).trim();
      if (rest.startsWith("//"))
      {
         // End comment after line, keep on same line
         iLineRest = "";
         return rest;
      }

      return null;
   }

   private InstructionContainer getNewParent(InstructionContainer parent)
   {
      if (parent.getType().equals(InstructionType.startBracket))
      {
         // No change of parent
         return parent;
      }

      if (parent.getType().equals(InstructionType.generic))
      {
         return getNewParent(parent.getParent());
      }

      return parent;
   }

   private InstructionContainer getNewParentForEndBracket(InstructionContainer parent)
   {
      if (parent.getType().equals(InstructionType.startBracket))
      {
         return parent.getParent();
      }

      return parent;
   }

   public void removeInstruction(String line)
   {
      iLineRest = iLineRest.trim().substring(line.length()).trim();
   }

}
