package common.utils.open.scad;

/**
 * A class to remember level of nested brackets
 * 
 */
public class BracketLevel
{

   private static int bracketLevel = 0;

   public static int getBracketLevelAndIncrease()
   {
      return ++bracketLevel;
   }

   public static int getBracketLevel()
   {
      return bracketLevel;
   }

   public static void setBracketLevel(int level)
   {
      bracketLevel = level;
   }

}
