package common.utils.open.scad;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import common.properties.SettingProperties;
import common.ui.components.ext.CompExtEnum;
import common.ui.components.ext.JPanelExt;
import common.utils.AppInfoInterface;

public class OpenSCADSettingsUI extends JFrame
{
   private static final long serialVersionUID = 1L;

   // TODO Change to name of your icon
   private String ICON_NAME = null;

   // TODO init fields here

   /////////// Java GUI Designer version 1.1 Starts here. ////
   // Text below will be regenerated automatically.
   // Generated 2020-02-27 19:40:19

   private JPanelExt iPanelExt;
   private JButton btnBrowse;
   private JLabel lbl3;
   private JTextField txtPath;
   private JRadioButton radioSpacesToTabs;
   private JCheckBox chkAskBeforeFormat;
   private JRadioButton radioTabsToSpaces;
   private JCheckBox chkBacupFile;
   private JRadioButton radioDoNothing;
   private JCheckBox chkSpacesAroundEquals;
   private JCheckBox chkSpaceAfterComma;
   private JLabel lbl2;
   private JTextField txtTabSize;
   private JButton btnClose;

   private int width = 645;
   private int height = 315;

   private OpenSCADUI iOpenSCADUI;

   private SettingProperties iProps;

   private javax.swing.ButtonGroup iSpacesTabs;

   public OpenSCADSettingsUI(SettingProperties props, OpenSCADUI openSCADUI)
   {
      super();
      iProps = props;
      iOpenSCADUI = openSCADUI;
      AppInfoInterface appInfo = AppInfoOpenScad.getInstance();
      String name = appInfo.getApplicationName();
      String version = appInfo.getVersion();
      setTitle(name + " Settings v " + version);
      setTitle("OpenSCAD Formatter Settings");
      setSize(width, height);
      if (ICON_NAME != null)
      {
         setIconImage(getToolkit().getImage(ICON_NAME));
      }

      getContentPane().setLayout(new BorderLayout());
      iPanelExt = new JPanelExt(width, height);
      iPanelExt.setLayout(null);
      getContentPane().add(iPanelExt, BorderLayout.CENTER);

      iSpacesTabs = new ButtonGroup();
      btnBrowse = new JButton("Browse");
      iPanelExt.addComponentExt(btnBrowse, 512, 12, 90, 43,
            CompExtEnum.MoveWithWidth);
      btnBrowse.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnBrowseClicked(e);
         }
      });

      lbl3 = new JLabel("Source Path:");
      iPanelExt.addComponentExt(lbl3, 17, 25, 75, 30, CompExtEnum.Normal);

      txtPath = new JTextField();
      String path = iProps.getProperty(OpenSCADUI.SCAD_SOURCE_PATH);
      if (path != null)
      {
         txtPath.setText(path);
      }

      iPanelExt.addComponentExt(txtPath, 102, 22, 399, 33,
            CompExtEnum.ResizeWithWidth);
      int spacesRadio = iProps.getIntProperty(OpenSCADUI.SCAD_SPACES_RADIO,
            OpenSCADUI.RADIO_TURN_TABS_INTO_SPACES);

      radioSpacesToTabs = new JRadioButton("Turn spaces into Tabs",
            (spacesRadio == OpenSCADUI.RADIO_TURN_SPACES_INTO_TABS));
      iSpacesTabs.add(radioSpacesToTabs);
      radioSpacesToTabs.setActionCommand("2");
      radioSpacesToTabs.setEnabled(false);
      iPanelExt.addComponentExt(radioSpacesToTabs, 17, 72, 164, 27,
            CompExtEnum.Normal);

      chkAskBeforeFormat = new JCheckBox("Ask before format last saved",
            iProps.getBooleanProperty(OpenSCADUI.SCAD_ASK_FIRST, true));
      iPanelExt.addComponentExt(chkAskBeforeFormat, 250, 72, 213, 28,
            CompExtEnum.Normal);

      radioTabsToSpaces = new JRadioButton("Turn Tabs into Spaces",
            (spacesRadio == OpenSCADUI.RADIO_TURN_TABS_INTO_SPACES));
      iSpacesTabs.add(radioTabsToSpaces);
      radioTabsToSpaces.setActionCommand("1");
      iPanelExt.addComponentExt(radioTabsToSpaces, 17, 105, 164, 27,
            CompExtEnum.Normal);

      chkBacupFile = new JCheckBox("Backup File before formatting",
            iProps.getBooleanProperty(OpenSCADUI.SCAD_USE_BACKUP, true));
      iPanelExt.addComponentExt(chkBacupFile, 250, 109, 213, 28,
            CompExtEnum.Normal);

      radioDoNothing = new JRadioButton("Do Nothing",
            (spacesRadio == OpenSCADUI.RADIO_DO_NOTHING));
      iSpacesTabs.add(radioDoNothing);
      radioDoNothing.setActionCommand("0");
      iPanelExt.addComponentExt(radioDoNothing, 17, 138, 164, 27,
            CompExtEnum.Normal);

      chkSpacesAroundEquals =
            new JCheckBox("Add spaces around equals when defining");
      iPanelExt.addComponentExt(chkSpacesAroundEquals, 250, 146, 272, 28,
            CompExtEnum.Normal);
      chkSpacesAroundEquals.setEnabled(false);

      chkSpaceAfterComma = new JCheckBox("Add space after comma");
      iPanelExt.addComponentExt(chkSpaceAfterComma, 250, 183, 212, 28,
            CompExtEnum.Normal);
      chkSpaceAfterComma.setEnabled(false);

      lbl2 = new JLabel("Tab size:");
      iPanelExt.addComponentExt(lbl2, 17, 190, 75, 30, CompExtEnum.Normal);

      txtTabSize =
            new JTextField(iProps.getProperty(OpenSCADUI.SCAD_TAB_SIZE, "2"));
      iPanelExt.addComponentExt(txtTabSize, 83, 188, 65, 33,
            CompExtEnum.Normal);

      btnClose = new JButton("Close");
      iPanelExt.addComponentExt(btnClose, 528, 208, 74, 43,
            CompExtEnum.MoveWithWidthAndHeight);
      btnClose.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnCloseClicked(e);
         }
      });

      setDefaultCloseOperation(HIDE_ON_CLOSE);
      addWindowListener(new WindowAdapter()
      {
         @Override
         public void windowClosing(WindowEvent we)
         {
            saveValues();
         }
      });

      /////////// Java GUI Designer version 1.1 Generation ends here. ///

      // TODO Add init declarations here

   }

   private void btnBrowseClicked(ActionEvent e)
   {
      JFileChooser fc = new JFileChooser(txtPath.getText());
      fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      int retval = fc.showOpenDialog(null);
      if (retval == JFileChooser.APPROVE_OPTION)
      {
         File myFile = fc.getSelectedFile();
         txtPath.setText(myFile.getAbsolutePath());
      }
   }

   private void btnCloseClicked(ActionEvent e)
   {
      saveValues();
      setVisible(false);
   }

   public void saveValues()
   {
      iProps.setProperty(OpenSCADUI.SCAD_ASK_FIRST,
            chkAskBeforeFormat.isSelected());
      iProps.setProperty(OpenSCADUI.SCAD_SOURCE_PATH, txtPath.getText());
      iProps.setProperty(OpenSCADUI.SCAD_SPACES_RADIO,
            iSpacesTabs.getSelection().getActionCommand());
      iProps.setProperty(OpenSCADUI.SCAD_TAB_SIZE, txtTabSize.getText());
      iProps.setProperty(OpenSCADUI.SCAD_USE_BACKUP, chkBacupFile.isSelected());
      iOpenSCADUI.setSource(txtPath.getText());
   }
}
