package common.utils.house;


public class Shape
{

   private String iType;
   private String iName;
   private int iHeight;
   private int iWidth;
   private int iX;
   private int iY;
   private int iZ;
   private boolean iIsInvisible;

   public Shape(String type, String name, int height, int width, int x, int y,
         int z, boolean isInvisible)
   {
      iType = type;
      iName = name;
      iHeight = height;
      iWidth = width;
      iX = x;
      iY = y;
      iZ = z;
      iIsInvisible = isInvisible;
      // TODO Auto-generated constructor stub
   }

   public String getType()
   {
      return iType;
   }

   public void setType(String type)
   {
      iType = type;
   }

   public String getName()
   {
      return iName;
   }

   public void setName(String name)
   {
      iName = name;
   }

   public int getHeight()
   {
      return iHeight;
   }

   public void setHeight(int height)
   {
      iHeight = height;
   }

   public int getWidth()
   {
      return iWidth;
   }

   public void setWidth(int width)
   {
      iWidth = width;
   }

   public int getX()
   {
      return iX;
   }

   public void setX(int x)
   {
      iX = x;
   }

   public int getY()
   {
      return iY;
   }

   public void setY(int y)
   {
      iY = y;
   }

   public int getZ()
   {
      return iZ;
   }

   public void setZ(int z)
   {
      iZ = z;
   }

   public boolean isInvisible()
   {
      return iIsInvisible;
   }

   public void setIsInvisible(boolean isInvisible)
   {
      iIsInvisible = isInvisible;
   }

}
