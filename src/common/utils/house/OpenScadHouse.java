package common.utils.house;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import common.utils.LogUtil;
import house.Cupboard;
import house.Door;
import house.Floor;
import house.House;
import house.Room;
import house.Stl;
import house.Wall;
import house.Window;

public class OpenScadHouse
{

   public static void main(String[] args)
   {
      // debugOutputObjects("house/Höglyckegatan 4.xml");
      House root = loadXML("house/exampleHouse1.xml");
      PaintHouse ph = new PaintHouse(root);

   }

   

   public static void debugOutputObjects(String path)
   {
      House root = loadXML(path);

      for (Floor floor : root.getFloor())
      {
         System.out.println("Floor: " + floor.getName() + ": " + floor.getHeight() + ", " + floor.getWidth());
         for (Object obj : floor.getStlOrRoom())
         {
            if (obj instanceof Room)
            {
               Room room = (Room) obj;
               System.out.println(room.getName());
               for (Object furniture : room.getStlOrCupboardOrDoor())
               {
                  if (furniture instanceof Stl)
                  {
                     Stl stl = (Stl) furniture;
                     System.out.println(stl.getName());
                  }

                  if (furniture instanceof Cupboard)
                  {
                     Cupboard cupb = (Cupboard) furniture;
                     System.out.println("Cupboard: " + cupb.getDoorCount() + cupb.getHeight() + ", " + cupb.getWidth());
                  }

                  if (furniture instanceof Door)
                  {
                     Door door = (Door) furniture;
                     System.out.println("Door: " + door.getAngle() + door.getHeight() + ", " + door.getWidth());
                  }

                  if (furniture instanceof Window)
                  {
                     Window window = (Window) furniture;
                     System.out.println("Window: " + window.getHeight() + ", " + window.getWidth());
                  }

                  if (furniture instanceof Wall)
                  {
                     Wall wall = (Wall) furniture;
                     System.out.println("Wall: " + wall.getHeight());
                  }
               }

            }

            if (obj instanceof Stl)
            {
               Stl stl = (Stl) obj;
               System.out.println(stl.getName());
            }

         }

      }
   }

   private static House loadXML(String fileName) throws ClassCastException
   {
      String schemaFileName = "house/house.xsd";

      House root = loadXML("house", fileName, schemaFileName);
      return root;
   }

   @SuppressWarnings("unchecked")
   public static <T extends Object> T loadXML(String packagePath, String fileName, String schemaFileName)
   {
      try
      {
         JAXBContext jContext2 = JAXBContext.newInstance(packagePath);
         Unmarshaller unMarsh = jContext2.createUnmarshaller();
         SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
         Schema schema = factory.newSchema(new File(schemaFileName));
         unMarsh.setSchema(schema);
         return (T) unMarsh.unmarshal(new File(fileName));
      }
      catch (JAXBException e)
      {
         LogUtil.printDialog(e.getLinkedException().getMessage());
      }
      catch (SAXException e)
      {
         LogUtil.printDialog(e.getMessage());
      }
      return null;
   }

}
