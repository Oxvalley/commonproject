package common.utils.house;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import house.Cupboard;
import house.Door;
import house.Floor;
import house.House;
import house.Room;
import house.Stl;
import house.Wall;
import house.Window;

public class PaintHouse extends JFrame
{
   private int iWidth;
   private int iHeight;
   private int iX = 15;
   private int iY = 40;
   private int screenWidth;
   private int screenHeight;
   private List<Shape> iShapes;
   private double ratio;
   private House iRoot;
   private Map<String, Object> mIndexMap = new HashMap<>();

   public PaintHouse(int width, int height, House root) throws HeadlessException
   {
      iRoot = root;
      iShapes = getShapes(root);
      iWidth = width;
      iHeight = height;

      getRootPane().addComponentListener(new ComponentAdapter()
      {
         @Override
         public void componentResized(ComponentEvent evt)
         {
            recalcSizes();
         }

      });

      recalcSizes();
      setSize(iWidth, iHeight);
      setVisible(true);
   }

   public PaintHouse(House root) throws HeadlessException
   {
      this(1800, 700, root);
   }

   private List<Shape> getShapes(House root)
   {
      List<Shape> lst = new ArrayList<>();

      for (Floor floor : root.getFloor())
      {
         System.out.println("Floor: " + floor.getName() + ", " + floor.getHeight() + ", " + floor.getWidth() + " ["
               + floor.getX() + ", " + floor.getY() + ", " + floor.getZ() + "]");
         lst.add(new Shape("Floor", floor.getName(), floor.getHeight(), floor.getWidth(), floor.getX(), floor.getY(),
               floor.getZ(), false));
         addToIndex(floor);

         for (Object obj : floor.getStlOrRoom())
         {
            if (obj instanceof Room)
            {
               Room room = (Room) obj;
               System.out.println("Room: " + room.getName() + ", " + room.getHeight() + ", " + room.getWidth() + " ["
                     + room.getX() + ", " + room.getY() + ", " + room.getZ() + "]");
               lst.add(new Shape("Room", room.getName(), room.getHeight(), room.getWidth(), floor.getX() + room.getX(),
                     floor.getY() + room.getY(), floor.getZ() + room.getZ(), false));
               for (Object furniture : room.getStlOrCupboardOrDoor())
               {
                  if (furniture instanceof Stl)
                  {
                     Stl stl = (Stl) furniture;
                     System.out.println(stl.getName());
                  }

                  if (furniture instanceof Cupboard)
                  {
                     Cupboard cupb = (Cupboard) furniture;
                     System.out.println("Cupboard: " + cupb.getDoorCount() + cupb.getHeight() + ", " + cupb.getWidth());
                  }

                  if (furniture instanceof Door)
                  {
                     Door door = (Door) furniture;
                     System.out.println("Door: " + door.getName() + ", " + door.getAngle() + door.getHeight() + ", "
                           + door.getWidth());
                     lst.add(new Shape("Door", door.getName(), door.getHeight(), door.getWidth(),
                           floor.getX() + room.getX() + door.getX(), floor.getY() + room.getY() + door.getY(),
                           floor.getZ() + room.getZ() + door.getZ(), true));
                  }

                  if (furniture instanceof Window)
                  {
                     Window window = (Window) furniture;
                     System.out
                           .println("Window: " + window.getName() + ", " + window.getHeight() + ", " + window.getWidth()
                                 + " [" + window.getX() + ", " + window.getY() + ", " + window.getZ() + "]");
                     lst.add(new Shape("Window", window.getName(), window.getHeight(), window.getWidth(),
                           floor.getX() + room.getX() + window.getX(), floor.getY() + room.getY() + window.getY(),
                           floor.getZ() + room.getZ() + window.getZ(), true));
                  }

                  if (furniture instanceof Wall)
                  {
                     Wall wall = (Wall) furniture;
                     System.out.println("Wall: " + wall.getHeight());
                  }
               }

            }

            if (obj instanceof Stl)
            {
               Stl stl = (Stl) obj;
               System.out.println(stl.getName());
            }

         }

      }
      return lst;
   }

   private <T> void addToIndex(Floor item)
   {
      innerAddToIndex(item.getName(), item);
   }

   private <T> void addToIndex(Room item)
   {
      innerAddToIndex(item.getName(), item);
   }

   private <T> void addToIndex(Window item)
   {
      innerAddToIndex(item.getName(), item);
   }

   private <T> void addToIndex(Door item)
   {
      innerAddToIndex(item.getName(), item);
   }

   private <T> void innerAddToIndex(String name, T floor)
   {
      Object value = mIndexMap.get(name);
      if (value != null)
      {
         System.out.println("There can not be two objects called " + value);
         System.exit(1);
      }
      mIndexMap.put(name, floor);
   }

   private void recalcSizes()
   {
      screenWidth = getWidth() - 35;
      screenHeight = getHeight() - 60;
   }

   @Override
   public void paint(Graphics g)
   {
      super.paint(g);
      g.setColor(Color.CYAN);
      g.fillRect(iX, iY, screenWidth, screenHeight);

      int minX = Integer.MAX_VALUE;
      int minY = Integer.MAX_VALUE;
      int maxX = Integer.MIN_VALUE;
      int maxY = Integer.MIN_VALUE;

      for (Shape shape : iShapes)
      {
         if (minX > shape.getX())
         {
            minX = shape.getX();
         }

         if (minY > shape.getY())
         {
            minY = shape.getY();
         }

         if (maxX < shape.getX() + shape.getWidth())
         {
            maxX = shape.getX() + shape.getWidth();
         }

         if (maxY < shape.getY() + shape.getHeight())
         {
            maxY = shape.getY() + shape.getHeight();
         }
      }

      int wall_thickness = 50;
      // System.out.println(minX + ", " + minY + ", " + maxX + ", " + maxY);
      // System.out.println(screenWidth + " and " + screenHeight);
      double ratio1 = (maxX - minX + 2 * wall_thickness) * 1.0 / screenWidth;
      double ratio2 = (maxY - minY + 2 * wall_thickness) * 1.0 / screenHeight;
      // System.out.println("ratio1: " + ratio1);
      // System.out.println("ratio2: " + ratio2);

      ratio = ratio1;
      if (ratio2 > ratio)
      {
         ratio = ratio2;
      }

      for (Shape shape : iShapes)
      {
         if (!shape.isInvisible())
         {
            drawSquares(g, wall_thickness, shape);
         }
      }

      for (Shape shape : iShapes)
      {
         if (shape.isInvisible())
         {
            drawSquares(g, wall_thickness, shape);
         }
      }

   }

   public void drawSquares(Graphics g, int wall_thickness, Shape shape)
   {
      Color color = Color.RED;
      if (useOuterWall(shape.getType()))
      {
         g.setColor(color);
         g.fillRect(iX + getScale(shape.getX()), iY + getScale(shape.getY()),
               getScale(shape.getWidth() + 2 * wall_thickness), getScale(shape.getHeight() + 2 * wall_thickness));
         color = Color.CYAN;
      }

      g.setColor(color);
      g.fillRect(iX + getScale(shape.getX() + wall_thickness), iY + getScale(shape.getY() + wall_thickness),
            getScale(shape.getWidth()), getScale(shape.getHeight()));
   }

   private static boolean useOuterWall(String type)
   {
      return (type.equals("Floor") || type.equals("Room"));
   }

   private int getScale(int value)
   {
      return (int) Math.round((value * 1.0) / ratio);
   }

}
