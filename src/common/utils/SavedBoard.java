package common.utils;

import java.util.List;

public class SavedBoard
{

   private String[] iBoard;
   private List<Wall> iKw;

   public SavedBoard(String[] board, List<Wall> kw)
   {
      iBoard = board;
      iKw = kw;
   }

   public String[] getBoard()
   {
      return iBoard;
   }

   public List<Wall> getKw()
   {
      return iKw;
   }

}
