package common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.sikuli.script.Screen;

import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.Decoder;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.SampleBuffer;

public class MP3SplitBySilence
{
   private static int samplesPerFrame;

   public static void main(String[] args)
   {
      // Specify the input MP3 file and output directory
      String inputFilePath = "Z:\\video\\JVC videokamera\\IipaxInstaller\\speaker 2.mp3";
      String outputDirectory = "output/";

      samplesPerFrame = MP3HeaderParser.getSamplesPerFrame(inputFilePath);
      if (samplesPerFrame > 0)
      {
         System.out.println("Samples per frame: " + samplesPerFrame);
      }
      ArrayList<Integer> totalBuffer = new ArrayList<>();
      ;
      // samplesPerFrame = 144;

      // Specify the minimum silence duration in milliseconds
      int minSilenceDuration = 500;

      try
      {
         // Decode the MP3 file using JLayer
         FileInputStream mp3Stream = new FileInputStream(inputFilePath);
         Bitstream bitstream = new Bitstream(mp3Stream);
         Decoder decoder = new Decoder();
         Header header;

         // Create the output directory if it doesn't exist
         File outputPath = new File(outputDirectory);
         outputPath.mkdirs();

         // Initialize SikuliX
         Screen screen = new Screen();
         int silenceStart = -1;
         float silenceDuration = 0;

         // Loop through MP3 frames
         int frameCount = 0;
         while ((header = bitstream.readFrame()) != null)
         {
            SampleBuffer output = (SampleBuffer) decoder.decodeFrame(header, bitstream);

            short[] pcmData = output.getBuffer();
            for (short s : pcmData)
            {
               totalBuffer.add((int) s);
            }

            // frameCount++;
            // System.out.println("Count of frames: " + frameCount);
            // System.out.println("Frame size: " + pcmData.length);
            //
            // Detect silence using a simple threshold (adjust as needed)
            // boolean isSilence = isSilence(pcmData);

            // if (isSilence)
            // {
            // // Record the silence start position
            // // If silence just started, record the silence start position
            // if (silenceStart == -1)
            // {
            // silenceStart = header.max_number_of_frames(1);
            // }
            // // Increase silence duration
            // silenceDuration += header.ms_per_frame();
            // }
            // else
            // {
            // // If silence has been detected and exceeds the minimum duration,
            // // split the MP3
            // System.out.println(silenceDuration);
            // if (silenceStart != -1 && silenceDuration >= minSilenceDuration)
            // {
            // // Implement logic to split MP3 based on silence sections
            // String outputFilePath = outputDirectory + "output_" +
            // silenceStart + ".mp3";
            // saveSplitSection(mp3Stream, outputFilePath, silenceStart,
            // header.max_number_of_frames(1));
            // }
            // // Reset silence tracking
            // silenceStart = -1;
            // silenceDuration = 0;
            // }
            bitstream.closeFrame();
         }

         // Close the input stream and bitstream
         bitstream.close();
         mp3Stream.close();

      }
      catch (Exception e)
      {
         e.printStackTrace();
      }

      int count = 0;
      int lastPosition = -1;
      int bufferSize = 1000;
      int silenceCount = 0;
      int talkCount = 0;
      for (int i = 0; i < totalBuffer.size(); i += bufferSize)
      {
         lastPosition = i + bufferSize;
         if (lastPosition > totalBuffer.size())
         {
            lastPosition = totalBuffer.size();
         }

         boolean isSilenceNow = isThisSilence(totalBuffer.subList(i, lastPosition));
         //System.out.println(isSilence);
         if (isSilenceNow)
         {
            if (talkCount > 3)
            {
               System.out.println("talkCount=" + talkCount);
               talkCount = 0;
            }
            silenceCount++;
         }
         else
         {
            if (silenceCount > 3)
            {
               System.out.println("silenceCount=" + silenceCount);
               silenceCount = 0;
            }
            talkCount++;
         }

      }

      System.exit(0);
   }

   private static boolean isThisSilence(List<Integer> subList)
   {
      long sum = 0;
      for (Integer value : subList)
      {
         // System.out.println(value);
         sum += Math.abs(value - 128);
      }

      double average = sum * 1.0 / subList.size();
      //System.out.println("Sum is " + average);

      return (average < 100.0);
   }

   // Placeholder method to detect silence in PCM data
   private static boolean isSilence(short[] pcmData)
   {
      // Adjust this threshold based on your audio characteristics
      int energyThreshold = 5000; // You may need to experiment with this value

      // Compute the energy of the PCM data
      long sumSquared = 0;
      for (short sample : pcmData)
      {
         sumSquared += sample * sample;
      }
      double energy = sumSquared / (double) pcmData.length;

      // Check if the energy is below the threshold
      return energy < energyThreshold;
   }

   private static void saveSplitSection(FileInputStream input, String outputFilePath, long startFrame,
         float silenceDurationInSeconds)
   {
      try
      {
         // Calculate the number of frames to split based on the silence
         // duration and samples per frame
         int framesToSplit = Math.round((silenceDurationInSeconds * samplesPerFrame) / 1000);

         // Seek to the start frame in the input stream
         input.getChannel().position(startFrame * samplesPerFrame * 4);

         // Read and save the split section to the output file
         try (FileOutputStream output = new FileOutputStream(outputFilePath))
         {
            byte[] buffer = new byte[1024];
            int bytesRead;
            int totalFramesRead = 0;
            while (totalFramesRead < framesToSplit && (bytesRead = input.read(buffer)) != -1)
            {
               output.write(buffer, 0, bytesRead);
               int framesRead = bytesRead / (samplesPerFrame * 4); // Assuming
                                                                   // 16-bit
                                                                   // data (2
                                                                   // bytes per
                                                                   // sample)
                                                                   // for stereo
               totalFramesRead += framesRead;
            }
         }
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

}
