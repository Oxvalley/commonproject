package common.utils;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;


/**
 * The Class MyStopWatch.
 */
public class MyStopWatch
{

   /** The My stop watch. */
   private static MyStopWatch iMyStopWatch;

   /**
    * Gets the single instance of MyStopWatch.
    *
    * @return single instance of MyStopWatch
    */
   public static MyStopWatch getInstance()
   {
      if (iMyStopWatch ==null)
      {
         iMyStopWatch = new MyStopWatch();
      }
      return iMyStopWatch;
   }

   /** The Stop watch. */
   private StopWatch iStopWatch;

   /**
    * Instantiates a new my stop watch.
    */
   private MyStopWatch()
   {
   }

   /**
    * Inits the.
    */
   public void init()
   {
      iStopWatch = StopWatch.createStarted();
   }

   /**
    * Prints the time.
    *
    * @param text the text
    */
   public void printTime(String text)
   {
      System.out.println(iStopWatch.getTime(TimeUnit.MILLISECONDS) + " " + text );      
   }

}
