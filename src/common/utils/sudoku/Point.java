package common.utils.sudoku;

public class Point
{

    private int mCol;
    private int mRow;

    public Point(int col, int row)
    {
        mCol = col;
        mRow = row;
    }

    public int getCol()
    {
        return mCol;
    }

    public int getRow()
    {
        return mRow;
    }

}
