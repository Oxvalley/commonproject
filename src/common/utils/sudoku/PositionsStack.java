package common.utils.sudoku;

import java.util.Stack;

public class PositionsStack {
    private Stack<Point> valuesList = new Stack<>();

    public void setPos(int col, int row) {
        valuesList.push(new Point(col,row));
    }

    public Point getLastPos() {
        return valuesList.pop();
    }
}

