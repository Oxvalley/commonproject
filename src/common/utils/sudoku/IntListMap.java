package common.utils.sudoku;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IntListMap
{
   private Map<String, List<Integer>> map = new HashMap<>();
   private int iSize;

   public IntListMap(int iSize)
   {
      this.iSize = iSize;
   }

   public void put(int r, int c, List<Integer> intList)
   {
      String key = getKey(r, c);
      map.put(key, intList);
   }

   public int getNextInt(int r, int c)
   {
      String key = getKey(r, c);
      List<Integer> intList = map.get(key);
      if (intList == null)
      {
         intList = generateRandomIntList(iSize);
         put(r, c, intList);
         return getNextInt(r, c);
      }

      if (intList.isEmpty())
      {
         return -1;
      }
      else
      {
         return intList.remove(0);
      }
   }

   public static String getKey(int r, int c)
   {
      return r + ", " + c;
   }

   public static List<Integer> generateRandomIntList(int n)
   {
      List<Integer> intList = new ArrayList<>();
      for (int i = 1; i < n+1; i++)
      {
         intList.add(i);
      }

      Collections.shuffle(intList);
      return intList;
   }

   public void removeList(int r, int c)
   {
      String key = getKey(r, c);
      map.remove(key);
   }
}