package common.utils.sudoku;

public class Sudoku
{
    private int[][] board;
    private int size;
    private IntListMap boardManager = null;
    private PositionsStack lastPositions = new PositionsStack();

    public Sudoku(int size)
    {
        this.size = size;
        boardManager = new IntListMap(size);
        board = new int[size][size];
        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                board[x][y] = -1;
            }
        }
    }

    public void printBoard()
    {
        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                System.out.print(board[x][y]);
                if (x < size - 1)
                {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void main(String[] args)
    {

        Sudoku sudoku = new Sudoku(9);
        Point point = null;
        while (!sudoku.isComplete())
        {
            if (point == null)
            {
                point = sudoku.getNextFreeCell();
            }
            point = sudoku.solve(point);
        }

        sudoku.printBoard();
    }

    public boolean isValidMove(int col, int row, int value)
    {
        // check row
        for (int x = 0; x < size; x++)
        {
            if (x != col && board[x][row] == value)
            {
                return false;
            }
        }

        // check column
        for (int y = 0; y < size; y++)
        {
            if (y != row && board[col][y] == value)
            {
                return false;
            }
        }

        // check sub-grid
        int subSize = (int) Math.sqrt(size);
        int subRow = (row / subSize) * subSize;
        int subCol = (col / subSize) * subSize;
        for (int y = subRow; y < subRow + subSize; y++)
        {
            for (int x = subCol; x < subCol + subSize; x++)
            {
                if (board[x][y] == value)
                {
                    return false;
                }
            }
        }

        // move is valid
        return true;

    }

    public void makeMove(int col, int row, int value)
    {
        if (isValidMove(col, row, value))
        {
            board[col][row] = value;
        }
    }

    public boolean isComplete()
    {
        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                if (board[x][y] == -1)
                {
                    return false; // there is an empty cell
                }
            }
        }
        return true; // all cells are filled
    }

    private Point getNextFreeCell()
    {
        // find the next empty cell
        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                if (board[x][y] == -1)
                {
                    return new Point(x, y);
                }
            }
        }

        // if there are no empty cells
        return null;
    }

    public Point solve(Point point)
    {
        int col = point.getCol();
        int row = point.getRow();

        // try all possible values for the empty cell
        int value = -1;
        while ((value = boardManager.getNextInt(col, row)) != -1)
        {
            if (isValidMove(col, row, value))
            {
                // try this value
                markPosition(col, row, value);
                lastPositions.setPos(col, row);
                return null;
            }
        }

        // if we've tried all possible values for this cell and none led to a
        // solution,
        // backtrack to the previous cell and try a different value for it
        boardManager.removeList(col, row);
        Point last = lastPositions.getLastPos();
        markPosition(last.getCol(), last.getRow(), -1);
        return last;
    }

    
    
    public void markPosition(int col, int row, int value)
    {
        board[col][row] = value;
    }

}
