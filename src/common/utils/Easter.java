package common.utils;

import java.util.Calendar;
import java.util.Locale;
import java.util.Scanner;

class Easter
{
   public static void main(String[] args)
   {
      while (true)
      {
         try
         {
            System.out.print("Ange ett �r att r�kna fram datum f�r p�skdagen fr�n (eller exit)\n>");
            Scanner s = new Scanner(System.in);
            System.out.println(getEasterSundayDate(getResult(s)));
         }
         catch (RuntimeException e)
         {
            System.out.print("Ett positivt heltal m�ste anges.\n>");
         }
      }
   }

   private static int getResult(Scanner s) throws RuntimeException
   {
      int year = -1;

      if (s.hasNextInt())
      {
         year = s.nextInt();
      }

      if (year < 0)
      {
         if (s.hasNextLine() && s.next().toLowerCase().equals("exit"))
         {
            System.out.println("Avslutar programmet!");
            System.exit(0);
         }

         throw new RuntimeException();
      }

      return year;
   }

   public static String getEasterSundayDate(int year)
   {
      int a = year % 19;
      int b = year / 100;
      int c = year % 100;
      int d = b / 4;
      int e = b % 4;
      int g = (8 * b + 13) / 25;
      int h = (19 * a + b - d - g + 15) % 30;
      int j = c / 4;
      int k = c % 4;
      int m = (a + 11 * h) / 319;
      int r = (2 * e + 2 * j - k - h + m + 32) % 7;
      int n = (h - m + r + 90) / 25;
      int p = (h - m + r + n + 19) % 32;

      Calendar cal = Calendar.getInstance();
      cal.set(Calendar.MONTH, n - 1);
      cal.set(Calendar.DAY_OF_MONTH, 1);
      String monthName = cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

      return "P�skdagen infaller �r " + year + " den " + p + " " + monthName;
   }
}
