package common.utils;

public class ToolTipUtil
{
/**
 * Make multiline tooltip possible. Converts text to html and replace all \n with &lt;br/>
 * @param text
 * @return
 */
   public static String createHtmlTooltip(String text)
   {
      String retVal = "<html>" + text.replace("\n", "<br/>") + "</html>";
      return retVal;
   }

}
