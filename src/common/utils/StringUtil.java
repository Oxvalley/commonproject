package common.utils;


/**
 * The Class StringUtil.
 */
public class StringUtil
{

   /**
    * Capitalize.
    *
    * @param text the text
    * @return the string
    */
   public static String capitalize(String text)
   {
      String retVal = "";
      String[] words = text.split("\\s+");
      for (String word : words)
      {
         String capitalized = "";
         if (word.length() >= 1)
         {
            capitalized = word.substring(0, 1).toUpperCase();
         }
         if (word.length() >= 2)
         {
            capitalized += word.substring(1).toLowerCase();
         }

         retVal += capitalized + " ";
      }

      return retVal.trim();
   }



   /**
    * The main method.
    *
    * @param args the arguments
    */
   public static void main(String[] args)
   {
      System.out.println(capitalize("olle"));
      System.out.println(capitalize("Olle"));
      System.out.println(capitalize("OLLE"));
      System.out.println(capitalize("ollE"));
      System.out.println(capitalize("olle johansson bramby"));
   }

   /**
    * Space.
    *
    * @param length the length
    * @return the string
    */
   public static String space(int length)
   {
      if (length < 0)
      {
         return "";
      }
      else
      {
         return "                                                      "
               .substring(0, length);
      }
   }

   /**
    * Adds spaces after text to fill up a column of width .
    *
    * @param text the text
    * @param width the width
    * @return the string
    */
   
   public static String padWithSpaces(String text, int width)
   {
      return text
            + "                                                           "
                  .substring(0, width - text.length());
   }
   
   public static String getVarName(String word)
   {
      if (word.equals(word.toLowerCase()))
      {
         // is Lower Case
         return getCapitalized(word, true);
      }

      if (word.equals(word.toUpperCase()))
      {
         // is Upper Case
         return getCapitalized(word, true);
      }

      // Is a mix of Upper and Lower Case. Keep it but make first Upper Case
      return getCapitalized(word, false);
   }

   private static String getCapitalized(String word, boolean makeRestLowerCase)
   {
      if (word == null || word.length() == 0)
      {
         return word;
      }

      String firstChar = word.substring(0, 1).toString();
      if (word.length() == 1)
      {
         return firstChar.toUpperCase();
      }
      else
      {
         String theRest = word.substring(1);
         if (makeRestLowerCase)
         {
            theRest = theRest.toLowerCase();
         }
         return firstChar.toUpperCase() + theRest;
      }
   }

   public static String getVarInstanceName(String columnName1, String varName)
   {
      // Do the same as getVarname, but make first char Lower Case
      if (columnName1 == null || columnName1.length() == 0)
      {
         return columnName1;
      }

      String columnName = varName;

      if (columnName == null || columnName.length() == 0)
      {
         return columnName;
      }

      String firstChar = columnName.substring(0, 1).toString().toLowerCase();
      String theRest;

      if (columnName.length() == 1)
      {
         theRest = "";
      }
      else
      {
         theRest = columnName.substring(1);
      }
      String retVal = firstChar + theRest;

      // Finally, check if it is the same as the varName (could start with a
      // number)
      if (retVal.equals(varName))
      {
         // If so, add '1'
         retVal += "1";
      }

      return retVal;
   }


}
