/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;


/**
 * The Class RunApacheUtil.
 *
 * @author Lars Svensson
 */
public class RunApacheUtil extends Thread
{
   
   /** The Command line. */
   private final String iCommandLine;
   
   /** The Working directory. */
   private String iWorkingDirectory;

   /**
    * Instantiates a new run apache util.
    *
    * @param commandLine the command line
    * @param workingDirectory the working directory
    */
   private RunApacheUtil(String commandLine, String workingDirectory)
   {
      iCommandLine = commandLine;
      iWorkingDirectory = workingDirectory;
   }

   /* (non-Javadoc)
    * @see java.lang.Thread#run()
    */
   @Override
   public void run()
   {
      iRetVal = exec(iCommandLine, iWorkingDirectory);
   }

   /**
    * Exec.
    *
    * @param commandLine the command line
    * @param workingDirectory the working directory
    * @return the int
    */
   public static int exec(String commandLine, String workingDirectory)
   {
      int exitValue = -1;
      CommandLine commandLine2 = CommandLine.parse(commandLine);
      DefaultExecutor executor = new DefaultExecutor();
      executor.setExitValue(0);
      try
      {
         if (workingDirectory != null)
         {
            executor.setWorkingDirectory(new File(workingDirectory));
         }
         
         exitValue = executor.execute(commandLine2);
      }
      catch (ExecuteException e)
      {
         System.out.println(e.getMessage());
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      
      return exitValue;
   }

   /**
    * Gets the splitted commands.
    *
    * @param commandLine the command line
    * @return the splitted commands
    */
   public static List<String> getSplittedCommands(String commandLine)
   {
      boolean outsideQuote = true;
      List<String> retList = new ArrayList<String>();
      String command = "";
      for (int i = 0; i < commandLine.length(); i++)
      {
         char c = commandLine.charAt(i);
         if (outsideQuote)
         {
            if (c == '"')
            {
               outsideQuote = false;
            }
            else if (c != ' ')
            {
               command += c;
            }
            else
            {
               // space outside quotes, new command
               retList.add(command);
               command = "";
            }
         }
         else
         {
            if (c == '"')
            {
               outsideQuote = true;
               retList.add(command);
               command = "";
            }
            else
            {
               command += c;
            }
         }
      }
      return retList;
   }

   /**
    * Exec.
    *
    * @param commandLine the command line
    * @param workingDirectry the working directry
    * @param asOwnThread the as own thread
    * @return the int
    */
   public static int exec(String commandLine, String workingDirectry, boolean asOwnThread)
   {
      System.out.println(commandLine);
      if (asOwnThread)
      {

         RunApacheUtil run = new RunApacheUtil(commandLine, workingDirectry);
         run.start();
         return run.getRetVal();
      }
      else
      {
         return exec(commandLine, workingDirectry);
      }
   }

   /** The Ret val. */
   private int iRetVal;

   /**
    * Gets the ret val.
    *
    * @return the ret val
    */
   private int getRetVal()
   {
      return iRetVal;
   }

}
