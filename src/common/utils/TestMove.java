package common.utils;

import java.awt.Color;

public class TestMove
{

   private Color[][][] iSavedStateColors;
   private TestDot iTestDot;
   private int iIndexToTest;

   public TestMove(Color[][][] colors)
   {
      this(null, colors);
   }
   
   
   public TestMove(TestDot testDot, Color[][][] colors)
   {
      iTestDot = testDot;
      iSavedStateColors = copyColors(colors);
      
      if (testDot == null)
      {
         iIndexToTest = 0;
      }
      else
      {
         iIndexToTest = iTestDot.getUnsolved().size() - 1;
      }
   }

   public int getIndexToTest()
   {
      return iIndexToTest;
   }

   public static Color[][][] copyColors(Color[][][] original)
   {
      if (original == null)
      {
         return null;
      }

      int depth1 = original.length;
      int depth2 = original[0].length;
      int depth3 = original[0][0].length;

      Color[][][] copy = new Color[depth1][depth2][depth3];

      for (int i = 0; i < depth1; i++)
      {
         for (int j = 0; j < depth2; j++)
         {
            for (int k = 0; k < depth3; k++)
            {
               copy[i][j][k] = new Color(original[i][j][k].getRGB());
            }
         }
      }
      
      if (WallBuilderBoard.slowDown)
      {
        WallBuilderBoard.sleep(2000);
      }

      return copy;
   }

   public Wall getNextMove()
   {
      if (iIndexToTest < 0)
      {
         return null;
      }
      
      return iTestDot.getUnsolved().get(iIndexToTest--);
   }
   
   public Color[][][] getSavedStateColors()
   {
      return iSavedStateColors;
   }

   public TestDot getTestDot()
   {
      return iTestDot;
   }

}
