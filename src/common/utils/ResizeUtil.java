package common.utils;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;


/**
 * The Class ResizeUtil.
 */
public class ResizeUtil
{
    
    /**
     * Scale.
     *
     * @param src the src
     * @param scale the scale
     * @return the image icon
     */
    public static ImageIcon scale(Image src, double scale)
    {
        int w = (int) (scale * src.getWidth(null));
        int h = (int) (scale * src.getHeight(null));
        int type = BufferedImage.TYPE_INT_RGB;
        BufferedImage dst = new BufferedImage(w, h, type);
        Graphics2D g2 = dst.createGraphics();
        g2.drawImage(src, 0, 0, w, h, null);
        g2.dispose();
        return new ImageIcon(dst);
    }

    /**
     * Scale.
     *
     * @param path the path
     * @param dim the dim
     * @return the image icon
     */
    public static ImageIcon scale(String path, Dimension dim)
    {
        int width = (int) dim.getWidth();
        int height = (int) dim.getHeight();
        ImageIcon vv = new ImageIcon(path);
        return scale(vv, width, height);
    }

    /**
     * Scale.
     *
     * @param vv the vv
     * @param width the width
     * @param height the height
     * @return the image icon
     */
    public static ImageIcon scale(ImageIcon vv, int width, int height)
    {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = image.createGraphics();
        g2.drawImage(vv.getImage(), 0, 0, width, height, null);
        g2.dispose();
        return new ImageIcon(image);
    }
}