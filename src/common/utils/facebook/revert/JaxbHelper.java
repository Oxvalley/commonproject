package common.utils.facebook.revert;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import common.utils.LogUtil;
import fb.Html;

public class JaxbHelper
{
   public static void saveXML(String packagePath, Html basics,
         String newFileName)
   {
      try
      {
         File file = new File(newFileName);
         JAXBContext jContext3 = JAXBContext.newInstance(packagePath);
         Marshaller marshaller = jContext3.createMarshaller();
         marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
         marshaller.marshal(basics, file);
         LogUtil.print(file.getAbsolutePath() + " saved!");
      }
      catch (PropertyException e)
      {
         e.printStackTrace();
      }
      catch (JAXBException e)
      {
         e.printStackTrace();
      }
   }

   public static String saveJaxBToXmlString(String packagePath, Html basics)
   {
      try
      {
         JAXBContext jContext3 = JAXBContext.newInstance(packagePath);
         Marshaller marshaller = jContext3.createMarshaller();
         marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
         StringWriter sw = new StringWriter();
         marshaller.marshal(basics, sw);
         return sw.toString();
      }
      catch (PropertyException e)
      {
         e.printStackTrace();
         return null;
      }
      catch (JAXBException e)
      {
         e.printStackTrace();
         return null;
      }
   }

   @SuppressWarnings("unchecked")
   public static <T extends Object> T loadXML(String packagePath,
         String fileName, String schemaFileName)
   {
      try
      {
         JAXBContext jContext2 = JAXBContext.newInstance(packagePath);
         Unmarshaller unMarsh = jContext2.createUnmarshaller();
         SchemaFactory factory =
               SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
         Schema schema = factory.newSchema(new File(schemaFileName));
         unMarsh.setSchema(schema);
         return (T) unMarsh.unmarshal(new File(fileName));
      }
      catch (JAXBException e)
      {
         LogUtil.printDialog(e.getLinkedException().getMessage());
      }
      catch (SAXException e)
      {
         LogUtil.printDialog(e.getMessage());
      }
      return null;
   }

   @SuppressWarnings("unchecked")
   public static <T extends Object> T loadXMLFromString(String packagePath,
         String xmlString, String schemaFileName)
   {
      try
      {
         JAXBContext jContext2 = JAXBContext.newInstance(packagePath);
         Unmarshaller unMarsh = jContext2.createUnmarshaller();
         SchemaFactory factory =
               SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
         Schema schema = factory.newSchema(new File(schemaFileName));
         unMarsh.setSchema(schema);
         StringReader reader = new StringReader(xmlString);
         return (T) unMarsh.unmarshal(reader);
      }
      catch (JAXBException e)
      {
         LogUtil.printDialog(e.getLinkedException().getMessage());
      }
      catch (SAXException e)
      {
         LogUtil.printDialog(e.getMessage());
      }
      return null;
   }
   
}
