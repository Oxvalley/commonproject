package common.utils.facebook.revert;

import java.util.ArrayList;
import java.util.List;

import fb.Div;

public class ParentHolder
{

   private Div iParent;
   private List<ParentChild> iParentChildList = new ArrayList<>();

   public List<ParentChild> getParentChildList()
   {
      return iParentChildList;
   }

   public Div getParent()
   {
      return iParent;
   }

   public void setParent(Div parent)
   {
      iParent = parent;
   }

   public void removeChildLater(Div childDiv, Div parentDiv)
   {
      iParentChildList.add(new ParentChild(parentDiv, childDiv));      
   }

}
