package common.utils.facebook.revert;

import java.io.File;
import java.util.Stack;

import fb.A;
import fb.Base;
import fb.Div;
import fb.Head;
import fb.Html;
import fb.Img;

public class FaceBookReverter
{
    public static void main(String[] args)
    {
        if (args.length == 0)
        {
            revertDir("facebook revert");
        }
        else
        {
            revertDir(args[0]);
        }
    }

    public static void revertDir(String directory)
    {
        File dir = new File(directory);

        if (!dir.isDirectory())
        {
            System.out.println(dir.getAbsolutePath() + " is not a directory");
            return;
        }

        for (File file : dir.listFiles())
        {
            if (file.isDirectory())
            {
                revertDir(file.getAbsolutePath());
            }
            else
            {
                if (file.getName().toLowerCase().endsWith(".html") ||
                    file.getName().toLowerCase().endsWith(".htm"))
                {

                    if (file.getName().toLowerCase().startsWith("message_") &&
                        !file.getName().toLowerCase().contains("-reverted.html"))
                    {
                        revertFile(file.getAbsolutePath());
                    }
                }
            }
        }
    }

    public static void revertFile(String filename)
    {
        System.out.println(">> " + filename);
        Html html = JaxbHelper.loadXML("fb", filename, "facebook revert/Facebook.xsd");

        if (html == null)
        {
            System.out.println(">> " + filename);
        }
        else
        {
            Base base = getBase(html.getHead());
            if (base != null && base.getHref().equals("../../../"))
            {
                base.setHref(null);
            }

            Stack<Div> stack = new Stack<>();
            ParentHolder ph = new ParentHolder();

            // Examine all recursive
            examineDiv(html.getBody().getDiv(), stack, null, ph);

            if (ph.getParent() != null)
            {
                // Empty parent div first
                ph.getParent().getContent().clear();

                // Add all divs in reverse order
                while (!stack.isEmpty())
                {
                    ph.getParent().getContent().add(stack.pop());
                }
            }

            // remove all divs without content <div/>
            for (ParentChild pc : ph.getParentChildList())
            {
                pc.getParentDiv().getContent().remove(pc.getChildDiv());
            }

            // Find filename to save to
            File file = new File(filename);
            String name = file.getName();
            int i = name.lastIndexOf(".");
            if (i > -1)
            {
                name = name.substring(0, i);
            }

            JaxbHelper.saveXML("fb", html, file.getParent() + "/" + name + "-reverted.html");
        }
    }

    private static Base getBase(Head head)
    {
        if (head != null)
        {

            for (Object obj : head.getContent())
            {
                if (obj instanceof Base)
                {
                    return (Base) obj;
                }

            }
        }

        return null;
    }

    private static void examineDiv(Div div, Stack<Div> stack, Div parent, ParentHolder parentHolder)
    {

        if (div != null)
        {
            if (div.getClazz() != null && div.getClazz().contains("pam _3-95"))
            {
                // add all messages to the Stack
                stack.push(div);
                if (parent != null && parent.getContent() != null)
                {
                    if (parentHolder.getParent() == null)
                    {
                        parentHolder.setParent(parent);
                    }
                }
            }

            for (Object childObject : div.getContent())
            {
                if (childObject instanceof Div)
                {
                    Div childDiv = (Div) childObject;

                    if (childDiv.getContent() == null || childDiv.getContent().isEmpty())
                    {
                        // Add for empty divs for removal laer <div/>
                        parentHolder.removeChildLater(childDiv, div);
                    }

                    // Test all divs recursive
                    examineDiv(childDiv, stack, div, parentHolder);
                }
                else
                {
                    if (childObject instanceof A)
                    {
                        // Correct image paths
                        A childA = (A) childObject;
                        String href = childA.getHref();
                        int i = href.indexOf("/photos/");

                        if (i > -1)
                        {
                            childA.setHref(href.substring(i + 1));
                        }

                        for (Object imgObj : childA.getContent())
                        {
                            if (imgObj instanceof Img)
                            {
                                Img childImg = (Img) imgObj;

                                changeURLs(href, childImg);

                            }
                        }
                    }
                }
            }
        }
    }

    private static void changeURLs(String href, Img childImg)
    {
        final String[] keys =
        { "photos", "stickers_used", "archived_threads", "gifs", "aaa" };

        boolean isThis = false;
        boolean found = false;

        for (String key : keys)
        {
            isThis = changeURL(href, key, childImg);

            if (isThis)
            {
                found = true;
            }
        }

        if (!found && !href.contains("http"))
        {
            System.out.println(href);
            System.out.println("Now!!");
        }
    }

    private static boolean changeURL(String href, String subPath, Img childImg)
    {
        String src = childImg.getSrc();
        int j = src.indexOf("/" + subPath + "/");
        if (j > -1)
        {
            childImg.setSrc(href.substring(j + 1));
            return true;
        }
        else
        {
            return false;
        }
    }

}
