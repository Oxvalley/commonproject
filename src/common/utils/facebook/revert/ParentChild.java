package common.utils.facebook.revert;

import fb.Div;

public class ParentChild
{

   private Div iParentDiv;
   private Div iChildDiv;

   public Div getParentDiv()
   {
      return iParentDiv;
   }

   public Div getChildDiv()
   {
      return iChildDiv;
   }

   public ParentChild(Div parentDiv, Div childDiv)
   {
      iParentDiv = parentDiv;
      iChildDiv = childDiv;
      // TODO Auto-generated constructor stub
   }

}
