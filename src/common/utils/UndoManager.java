package common.utils;

import java.util.ArrayList;
import java.util.List;

public class UndoManager
{

   private static List<UndoInterface> iUndoers = new ArrayList<>();
   private static UndoManager iUndoManagerInstance;
   private int iChangesMax = 10;
   private int iChangeCount = 0;
   private int iUndoMax = 10;
   private int iUndoCount = 0;
   private int iRedoCount = 0;

   public void saveSnapshot()
   {
      if (iUndoCount >= iUndoMax)
      {
         for (UndoInterface undoer : iUndoers)
         {
            undoer.removeOldestSave();
         }
         iUndoCount--;
      }

      for (UndoInterface undoer : iUndoers)
      {
         undoer.saveSnapshot();
      }
      
      iChangeCount = 0;
      iUndoCount++;
   }

   public static UndoManager getInstance()
   {
      if (iUndoManagerInstance == null)
      {
         iUndoManagerInstance = new UndoManager();
      }
      return iUndoManagerInstance;
   }

   public void addUndoer(UndoInterface undoer)
   {
      iUndoers.add(undoer);
   }

   public void init(int undoMax, int changesMax)
   {
      iUndoMax = undoMax;
      iChangesMax = changesMax;
   }

   public void saveChange()
   {
      for (UndoInterface undoer : iUndoers)
      {
         undoer.saveChange();
      }
      iChangeCount++;
      if (iChangeCount >= iChangesMax)
      {
         saveSnapshot();
      }
   }

   public void undo()
   {
      if (iUndoCount > 0)
      {
         iUndoCount--;
         iRedoCount++;
         iChangeCount = 0;
         for (UndoInterface undoer : iUndoers)
         {
            undoer.undo();
         }
      }
   }

   public void redo()
   {
      if (iRedoCount > 0)
      {
         iUndoCount++;
         iRedoCount--;
         iChangeCount = 0;
         for (UndoInterface undoer : iUndoers)
         {
            undoer.redo();
         }
      }
   }

   public void reInit()
   {
      iChangeCount = 0;
      iUndoCount = 0;
      iRedoCount = 0;
   }

}
