package common.utils;

import java.util.Calendar;

import javax.swing.ImageIcon;

import common.utils.AppInfoInterface;


/**
 * The Class AppInfo.
 */
public class AppInfoOpenScad implements AppInfoInterface
{
   
   /** The m instance. */
   static AppInfoInterface mInstance = null;

   /** The application name. */
   private final String APPLICATION_NAME = "OpenScad Formatter";
   
   /** The application version. */
   private final String APPLICATION_VERSION = "1.0";
   
   /** The application copywrite text. */
   private final String APPLICATION_COPYWRITE_TEXT =
         "(C) Oxvalley Software " + Calendar.getInstance().get(Calendar.YEAR);
   
   /** The application icon path. */
   private final String APPLICATION_ICON_PATH = "example.jpg";
   
   /** The jdk version. */
   private final String JDK_VERSION = "1.8";

   /* (non-Javadoc)
    * @see common.utils.AppInfoInterface#getJdkVersion()
    */
   @Override
   public String getJdkVersion()
   {
      return JDK_VERSION;
   }

   /* (non-Javadoc)
    * @see common.utils.AppInfoInterface#getApplicationName()
    */
   @Override
   public String getApplicationName()
   {
      return APPLICATION_NAME;
   }

   /* (non-Javadoc)
    * @see common.utils.AppInfoInterface#getVersion()
    */
   @Override
   public String getVersion()
   {
      return APPLICATION_VERSION;
   }

   /* (non-Javadoc)
    * @see common.utils.AppInfoInterface#getIcon()
    */
   @Override
   public ImageIcon getIcon()
   {
      return new ImageIcon(APPLICATION_ICON_PATH);
   }

   /* (non-Javadoc)
    * @see common.utils.AppInfoInterface#getCopywrightText()
    */
   @Override
   public String getCopywrightText()
   {
      return APPLICATION_COPYWRITE_TEXT;
   }

   /**
    * Gets the single instance of AppInfo.
    *
    * @return single instance of AppInfo
    */
   public static AppInfoInterface getInstance()
   {
      if (mInstance == null)
      {
         mInstance = new AppInfoOpenScad();
      }
      return mInstance;
   }

   /**
    * Instantiates a new app info.
    */
   private AppInfoOpenScad()
   {

   }

}
