package common.utils;

import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JComponent;

import org.apache.commons.lang3.SystemUtils;


/**
 * The Class GraphicsUtil.
 */
public class GraphicsUtil
{

    /**
     * Translate log.
     *
     * @param id the id
     * @param g the g
     * @param x the x
     * @param y the y
     */
    public static void translateLog(String id, Graphics g, int x, int y)
    {
        boolean debug = true;
        if (debug)
        {
            System.out.println("Translating " + id + ": x=" + x + ", y=" + y);
        }
        g.translate(x, y);
    }

    /**
     * Adjust font for OS.
     *
     * @param comp the comp
     */
    public static void adjustFontForOS(JComponent comp)
    {
        if (!SystemUtils.IS_OS_WINDOWS)
        {
            Font font = comp.getFont();
            comp.setFont(new Font(font.getFontName(), Font.PLAIN,
                (int) Math.round(font.getSize() * .85)));
        }

    }
}
