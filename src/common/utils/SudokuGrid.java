package common.utils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class SudokuGrid extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final int SIZE = 9;  // Size of the Sudoku grid
    private static final int CELL_SIZE = 100;  // Size of each cell
    private static final int GRID_SIZE = SIZE * CELL_SIZE;  // Size of the whole grid
    private int[][] board;
    private String[][] candidates;
    private String[][] greenCandidates;

    public SudokuGrid(int[][] board) {
        this.board = board;
        this.candidates = new String[SIZE][SIZE];
        this.greenCandidates = new String[SIZE][SIZE];
        setPreferredSize(new Dimension(GRID_SIZE, GRID_SIZE));
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                handleClick(e.getX(), e.getY());
            }
        });
    }

    private void handleClick(int x, int y) {
        int col = x / CELL_SIZE;
        int row = y / CELL_SIZE;

        if (board[row][col] == 0 && candidates[row][col].length() == 1) {
            board[row][col] = Integer.parseInt(candidates[row][col]);
            repaint();
        }

        if (board[row][col] == 0 &&  greenCandidates[row][col].length() == 1) {
           board[row][col] = Integer.parseInt(greenCandidates[row][col]);
           repaint();
       }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawGrid(g);
        drawNumbers(g);
        drawCandidates(g);
    }

    private static void drawGrid(Graphics g) {
        g.setColor(Color.BLACK);
        for (int i = 0; i <= SIZE; i++) {
            int thickness = (i % 3 == 0) ? 2 : 1;
            ((Graphics2D) g).setStroke(new BasicStroke(thickness));
            g.drawLine(i * CELL_SIZE, 0, i * CELL_SIZE, GRID_SIZE);
            g.drawLine(0, i * CELL_SIZE, GRID_SIZE, i * CELL_SIZE);
        }
    }

    private void drawNumbers(Graphics g) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.PLAIN, 20));
        FontMetrics fm = g.getFontMetrics();
        for (int row = 0; row < SIZE; row++) {
            for (int col = 0; col < SIZE; col++) {
                int number = board[row][col];
                if (number != 0) {
                    String text = String.valueOf(number);
                    int textWidth = fm.stringWidth(text);
                    int textHeight = fm.getHeight();
                    int x = col * CELL_SIZE + (CELL_SIZE - textWidth) / 2;
                    int y = row * CELL_SIZE + CELL_SIZE - (CELL_SIZE - textHeight) / 2 - 5 + CELL_SIZE / 3;
                    g.drawString(text, x, y);
                }
            }
        }
    }

    private void drawCandidates(Graphics g) {
        candidates = new String[SIZE][SIZE];  // Clear old candidates
        g.setColor(Color.GRAY);
        g.setFont(new Font("Arial", Font.PLAIN, 12));
        FontMetrics fm = g.getFontMetrics();
        for (int row = 0; row < SIZE; row++) {
            for (int col = 0; col < SIZE; col++) {
                if (board[row][col] == 0) {  // Only consider empty cells
                    StringBuilder candidatesBuilder = new StringBuilder();
                    int candidateCount = 0;
                    int lastCandidate = 0;

                    for (int number = 1; number <= 9; number++) {
                        if (usedNumber(row, col, number)) {
                            candidatesBuilder.append(number).append(" ");
                            candidateCount++;
                            lastCandidate = number;
                        }
                    }

                    String candidateText = candidatesBuilder.toString().trim();
                    candidates[row][col] = candidateText;  // Store candidates

                    int x = col * CELL_SIZE + 5;  // Slightly offset to upper part
                    int y = row * CELL_SIZE + fm.getHeight();
                    g.drawString(candidateText, x, y);

                    if (candidateCount == 1) {
                        g.setColor(Color.PINK);
                        g.setFont(new Font("Arial", Font.BOLD, 20));
                        FontMetrics fmLarge = g.getFontMetrics();
                        String text = String.valueOf(lastCandidate);
                        int textWidth = fmLarge.stringWidth(text);
                        int textHeight = fmLarge.getHeight();
                        int xLarge = col * CELL_SIZE + (CELL_SIZE - textWidth) / 2-(CELL_SIZE/3);
                        int yLarge = row * CELL_SIZE + CELL_SIZE - (CELL_SIZE - textHeight) / 2 - 5;
                        g.drawString(text, xLarge, yLarge);
                        g.setColor(Color.GRAY);  // Reset color back to gray for candidates
                        g.setFont(new Font("Arial", Font.PLAIN, 12));  // Reset font back to small font
                    }
                }
            }
        }

        drawUniqueCandidates(g);
    }

    private void drawUniqueCandidates(Graphics g) {
       greenCandidates = new String[SIZE][SIZE];  // Clear old green candidates
        g.setColor(Color.GREEN);
        g.setFont(new Font("Arial", Font.BOLD, 18));
        FontMetrics fm = g.getFontMetrics();
        
        for (int number = 1; number <= 9; number++) {
            checkUniqueInRows(g, number, fm);
            checkUniqueInColumns(g, number, fm);
            checkUniqueInSquares(g, number, fm);
        }
    }

   private void checkUniqueInRows(Graphics g, int number, FontMetrics fm) {
        for (int row = 0; row < SIZE; row++) {
            int uniqueCol = -1;
            for (int col = 0; col < SIZE; col++) {
                if (board[row][col] == 0 && candidates[row][col].contains(String.valueOf(number))) {
                    if (uniqueCol == -1) {
                        uniqueCol = col;
                    } else {
                        uniqueCol = -2;
                        break;
                    }
                }
            }
            if (uniqueCol > -1) {
               System.out.println(uniqueCol + ", " + row + " Green C " + number);
                drawGreenNumber(g, number, fm, row, uniqueCol);
            }
        }
    }

   public void drawGreenNumber(Graphics g, int number, FontMetrics fm, int row, int uniqueCol)
   {
      int textWidth = fm.stringWidth(String.valueOf(number));
       int x = uniqueCol * CELL_SIZE + (CELL_SIZE - textWidth) / 2;
       int y = row * CELL_SIZE + (CELL_SIZE + fm.getAscent()) / 2;
       g.drawString(String.valueOf(number), x, y);
       greenCandidates[row][uniqueCol] = String.valueOf(number);  // Store candidates
   }

    private void checkUniqueInColumns(Graphics g, int number, FontMetrics fm) {
        for (int col = 0; col < SIZE; col++) {
            int uniqueRow = -1;
            for (int row = 0; row < SIZE; row++) {
                if (board[row][col] == 0 && candidates[row][col].contains(String.valueOf(number))) {
                    if (uniqueRow == -1) {
                        uniqueRow = row;
                    } else {
                        uniqueRow = -2;
                        break;
                    }
                }
            }
            if (uniqueRow > -1) {
               System.out.println(col + ", " + uniqueRow + " Green B " + number);
                drawGreenNumber(g, number, fm, uniqueRow, col);
            }
        }
    }

    private void checkUniqueInSquares(Graphics g, int number, FontMetrics fm) {
        for (int boxRow = 0; boxRow < SIZE; boxRow += 3) {
            for (int boxCol = 0; boxCol < SIZE; boxCol += 3) {
                int uniqueRow = -1;
                int uniqueCol = -1;
System.out.println("Testing " + number + " in a box");
                for (int row = 0; row < 3; row++) {
                    for (int col = 0; col < 3; col++) {
                        int currentRow = boxRow + row;
                        int currentCol = boxCol + col;

                        if (board[currentRow][currentCol] == 0 && candidates[currentRow][currentCol].contains(String.valueOf(number))) {
                            if (uniqueRow == -1 && uniqueCol == -1) {
                               System.out.println(currentCol + ", " +  currentRow + " has one candidate");
                                uniqueRow = currentRow;
                                uniqueCol = currentCol;
                            } else {
                               System.out.println(currentCol + ", " +  currentRow + " Too many candidates");
                                uniqueRow = -2;
                                uniqueCol = -2;
                                break;
                            }
                        }
                    }
                }

                if (uniqueRow > -1 && uniqueCol > -1) {
                   System.out.println(uniqueCol + ", " + uniqueRow + " Green A " + number);
                    drawGreenNumber(g, number, fm, uniqueRow, uniqueCol);
                }
            }
        }
    }

    public boolean inColumn(int col, int number) {
        for (int row = 0; row < board.length; row++) {
            if (board[row][col] == number) {
                return true;
            }
        }
        return false;
    }

    public boolean inRow(int row, int number) {
        for (int col = 0; col < board[row].length; col++) {
            if (board[row][col] == number) {
                return true;
            }
        }
        return false;
    }

    public boolean usedNumber(int row, int col, int number) {
        return !inRow(row, number) && !inColumn(col, number) && !inSquare(row, col, number);
    }

    public boolean inSquare(int row, int col, int number) {
        // Determine the starting row and column of the 3x3 square
        int squareRowStart = (row / 3) * 3;
        int squareColStart = (col / 3) * 3;

        // Check within the 3x3 square
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (board[squareRowStart + r][squareColStart + c] == number) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void createAndShowGUI(int[][] board) {
        JFrame frame = new JFrame("Sudoku Grid");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new SudokuGrid(board));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Example initialization of the Sudoku board
        int[][] initialBoard = {
            {5, 3, 0, 0, 7, 0, 0, 0, 0},
            {6, 0, 0, 1, 9, 5, 0, 0, 0},
            {0, 9, 8, 0, 0, 0, 0, 6, 0},
            {8, 0, 0, 0, 6, 0, 0, 0, 3},
            {4, 0, 0, 8, 0, 3, 0, 0, 1},
            {7, 0, 0, 0, 2, 0, 0, 0, 6},
            {0, 6, 0, 0, 0, 0, 2, 8, 0},
            {0, 0, 0, 4, 1, 9, 0, 0, 5},
            {0, 0, 0, 0, 8, 0, 0, 7, 9}
        };

        showGrid(initialBoard);
    }

    public static void showGrid(int[][] initialBoard) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        SwingUtilities.invokeLater(() -> createAndShowGUI(initialBoard));
    }
}
