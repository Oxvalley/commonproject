package common.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;

import common.properties.SettingProperties;

public class Language
{

   private static Language iInstance = null;
   private String iLanguageUsed = "English";
   private Map<String, String> iLangPairs = new TreeMap<>();
   private boolean isDirty;
   private String iLangFilesFolder = "languages";
   private String iFilename;
   private String iCurrentDirectory;

   public String getText(String id)
   {
      String key = getKey(id);
      String text = iLangPairs.get(key);
      if (text == null)
      {
         text = "TRANSLATE: " + id;
         iLangPairs.put(key, text);
         isDirty = true;
      }

      return text;
   }

   public static String getKey(String id)
   {
      return id.replace(" ", ".").replace("=", ".");
   }

   public static Language getInstance()
   {
      if (iInstance == null)
      {
         iInstance = new Language(null);
      }

      return iInstance;
   }

   public static Language getInstance(String currentDirectory)
   {
      if (iInstance == null)
      {
         iInstance = new Language(currentDirectory);
      }

      return iInstance;
   }

   /**
    * When this method is used, do not trust variable iInstance
    * 
    * @return
    */
   public static Language getNewInstance()
   {
      return new Language(null);
   }

   public static Language getNewInstance(String currentDirectory)
   {
      return new Language(currentDirectory);
   }

   private Language(String currentDirectory)
   {
      iCurrentDirectory = currentDirectory;
      System.out.println("Init Language with " + iCurrentDirectory);

      if (iCurrentDirectory != null)
      {
         iLangFilesFolder = iCurrentDirectory + "/" + iLangFilesFolder;
      }

      SettingProperties prop = SettingProperties.getInstance();
      iLanguageUsed = prop.getProperty(BasicConstants.PROP_USED_LANGUAGE, "Swedish");
      init();
   }

   public void setLanguage(String language, String langFilesFolder)
   {
      if (language != null)
      {
         iLanguageUsed = language;
      }

      if (langFilesFolder != null)
      {
         iLangFilesFolder = langFilesFolder;
      }

      init();
   }

   private void init()
   {
      iFilename = iLangFilesFolder + "/" + iLanguageUsed + ".txt";

      System.out.println(iFilename);

      iLangPairs.clear();

      if (new File(iFilename).exists())
      {
         System.out.println("Exists");
         BufferedReader reader = null;
         try
         {
            reader = Files.newBufferedReader(Paths.get(iFilename), StandardCharsets.UTF_8);
            String line = reader.readLine();

            while (line != null)
            {
               String[] arr = line.split("=");
               iLangPairs.put(arr[0], arr[1]);

               // Read next line for while condition
               line = reader.readLine();
            }
         }
         catch (IOException ioe)
         {
            System.out.println(ioe.getMessage());
         }
         finally
         {
            try
            {
               if (reader != null)
               {
                  reader.close();
               }
            }
            catch (Exception e)
            {
            }
         }
      }
      else
      {
         System.out.println("Do not exist");
      }

   }

   public void save()
   {
      if (isDirty)
      {
         try
         {
            new File(iLangFilesFolder).mkdirs();

            Writer fw = null;
            BufferedWriter out = null;
            fw = new OutputStreamWriter(new FileOutputStream(iFilename), StandardCharsets.UTF_8);

            for (String key : iLangPairs.keySet())
            {
               fw.write(key + "=" + iLangPairs.get(key) + "\n");
            }
            fw.close();

         }
         catch (IOException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }

      }

   }

}
