package common.utils;

public class Coffe
{

   public static void main(String[] args)
   {
      double standard = getVolume(2.7);
      System.out.println(standard);

      double NewCl = standard * 7.0 / 6.0;
      System.out.println(NewCl);

      int tries = 100000;
      double maxTest = 3.0;
      double bestDiff = Double.MAX_VALUE;
      double bestD = Double.MAX_VALUE;
      double bestVolume = Double.MAX_VALUE;

      for (int d = 0; d < tries; d++)
      {
         double newTest = standard + (maxTest - standard) * 1.0 / tries * d * 1.0;
         double newValue = getVolume(newTest);
         double diff = Math.abs(NewCl - newValue);
         if (diff < bestDiff)
         {
            bestD = newTest;
            bestDiff = diff;
            bestVolume = newValue;
            System.out.println(newValue);
         }
      }

      System.out.println(bestD + " as diameter is closest with volume " + bestVolume + " and diff " + bestDiff);

   }

   private static double getVolume(double d)
   {
      double r = d / 2;

      double s = r * 1.1;
      double h = 0.58148 * d;
      double pi = Math.PI;

      double v1 = 2 * pi * r * r * r / 3;
      double v2 = s * s * pi * h;

      return (v1 + v2) / 10.0;
   }

}
