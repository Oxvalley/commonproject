package common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileHelper
{

   public static File getFolderFileFromPath(String destinationPath)
   {
      File destinationFile;
      if (destinationPath != null)
      {
         destinationFile = new File(destinationPath);
         if (!destinationFile.exists())
         {
            System.err.println("Destination folder " + destinationPath
                  + " does not exist. Exiting...");
            System.exit(1);
         }

         if (!destinationFile.isDirectory())
         {
            System.err.println("Destination " + destinationPath
                  + " is not a folder, but a file. Exiting...");
            System.exit(1);
         }
      }
      else
      {
         destinationFile = null;
      }
      return destinationFile;
   }

   public static void copy(String fromFileName, String toFileName)
      throws IOException
   {
      copy(new File(fromFileName), new File(toFileName));
   }

   public static void copy(File fromFile, File toFile) throws IOException
   {

      if (!fromFile.exists())
         throw new IOException(
               "FileCopy: " + "no such source file: " + fromFile.getName());
      if (!fromFile.isFile())
         throw new IOException(
               "FileCopy: " + "can't copy directory: " + fromFile.getName());
      if (!fromFile.canRead())
         throw new IOException("FileCopy: " + "source file is unreadable: "
               + fromFile.getName());

      if (toFile.isDirectory())
         toFile = new File(toFile, fromFile.getName());

      if (toFile.exists())
      {
         if (!toFile.canWrite())
            throw new IOException("FileCopy: "
                  + "destination file is unwriteable: " + toFile.getName());
         System.out.print(
               "Overwrite existing file " + toFile.getName() + "? (Y/N): ");
         System.out.flush();
         BufferedReader in =
               new BufferedReader(new InputStreamReader(System.in));
         String response = in.readLine();
         if (!response.equals("Y") && !response.equals("y"))
            throw new IOException(
                  "FileCopy: " + "existing file was not overwritten.");
      }
      else
      {
         String parent = toFile.getParent();
         if (parent == null)
            parent = System.getProperty("user.dir");
         File dir = new File(parent);
         if (!dir.exists())
            throw new IOException("FileCopy: "
                  + "destination directory doesn't exist: " + parent);
         if (dir.isFile())
            throw new IOException(
                  "FileCopy: " + "destination is not a directory: " + parent);
         if (!dir.canWrite())
            throw new IOException("FileCopy: "
                  + "destination directory is unwriteable: " + parent);
      }

      FileInputStream from = null;
      FileOutputStream to = null;
      try
      {
         from = new FileInputStream(fromFile);
         to = new FileOutputStream(toFile);
         byte[] buffer = new byte[4096];
         int bytesRead;

         while ((bytesRead = from.read(buffer)) != -1)
            to.write(buffer, 0, bytesRead); // write
      }
      finally
      {
         if (from != null)
            try
            {
               from.close();
            }
            catch (IOException e)
            {
               ;
            }
         if (to != null)
            try
            {
               to.close();
            }
            catch (IOException e)
            {
               ;
            }
      }
   }

   public static String getFileTag(String filename)
   {
      int i = filename.lastIndexOf(".");
      if (i == -1 || i == filename.length() - 1)
      {
         return "";
      }
      else
      {
         return filename.substring(i + 1);
      }
   }

   public static String getLastFolder(String parentPath)
   {
      String path = parentPath.replace("\\", "/").trim();
      if (path.endsWith("/"))
      {
         path = path.substring(0, path.length() - 1);
      }

      int i = path.lastIndexOf("/");
      if (i == -1)
      {
         return path;
      }

      return path.substring(i + 1);
   }

}
