package common.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class HouseReader
{

   private static final String[] iOperators =
         { "code", "cube", "cylinder", "door", "end", "floor", "pos", "rem",
            "remove", "room", "rotate", "stl","translate", "window" };
   private static final String[] iEndOperators = { "pos", "remove", "rotate", "translate" };
   private static List<ModuleReader> iModuleList = new ArrayList<>();
   private static List<String> iOperatorsList = new ArrayList<>();
   private static List<String> iEndOperatorsList = new ArrayList<>();
   private static int lineCount = 0;
   private static boolean iHasErrors = false;

   public static void main(String[] args)
   {
      for (String operator : iOperators)
      {
         iOperatorsList.add(operator);
      }

      for (String operator : iEndOperators)
      {
         iEndOperatorsList.add(operator);
      }

      readFile("höglyckegatan_4.house");
      printCode();

   }

   private static void printCode()
   {
      for (ModuleReader module : iModuleList)
      {
         System.out.println(module);
      }
   }

   public static void readFile(String filename)
   {

      BufferedReader reader = null;
      ModuleReader iModuleNow = null;
      boolean inCodeComments = false;

      Map<String, String> mainVariables = new HashMap<String, String>();

      try
      {
         reader = new BufferedReader(new FileReader(filename));
         String line = reader.readLine();
         while (line != null)
         {
            lineCount++;
            line = line.trim();
            // if (!line.isEmpty())
            {

               if (line.endsWith(":"))
               {
                  iModuleNow = new ModuleReader(mainVariables,
                        line.substring(0, line.length() - 1));
                  iModuleList.add(iModuleNow);
               }
               else if (inCodeComments)
               {
                  if (line.startsWith("end code"))
                  {
                     iModuleNow.createLines("code_end", line);
                     inCodeComments = false;
                  }
                  else
                  {
                     iModuleNow.createLines("inCode", line);
                  }
               }
               else if (line.startsWith("code"))
               {
                  iModuleNow.createLines("code", line);
                  inCodeComments = true;
               }
               else if (line.contains("=") && iModuleNow == null
                     && line.indexOf("=") > -1)
               {

                  String key = line.substring(0, line.indexOf("=")).trim();
                  String value = line
                        .substring(line.indexOf("=") + 1, line.length()).trim();

                  mainVariables.put(key, value);

               }
               else if (iModuleNow != null)
               {
                  iModuleNow.addLine(line);
                  String operator = getFirstWord(line);
                  if (isCorrectWord(operator))
                  {
                     iModuleNow.createLines(operator, line);

                     if (isEndOperator(operator))
                     {
                        iModuleNow.pushOperator(operator);
                     }

                     if (operator.equals("end"))
                     {
                        String errorText = iModuleNow
                              .pop(line.substring(operator.length()).trim());
                        if (errorText != null)
                        {
                           printError(iModuleNow, errorText);
                        }
                      }

                  }
                  else if (line.isEmpty())
                  {
                     iModuleNow.createLines("empty", line);
                  }
                  else
                  {
                     printError(iModuleNow,
                           "Syntax error: Unknown operator: " + operator);
                  }
               }
            }
            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }

   }

   private static boolean isEndOperator(String operator)
   {
      return iEndOperatorsList.contains(operator);
   }

   public static void printError(ModuleReader iModuleNow, String text)
   {
      System.out
            .println(iModuleNow.getName() + " Line " + lineCount + ": " + text);
      iHasErrors = true;
   }

   private static boolean isCorrectWord(String operator)
   {
      return iOperatorsList.contains(operator);
   }

   private static String getFirstWord(String line)
   {
      int i = line.indexOf(" ");
      if (i == -1)
      {
         return line;
      }

      return line.substring(0, i);
   }

}
