package common.utils;


/**
 * The Class ProcMon.
 */
public class ProcMon implements Runnable
{

   /** The Process. */
   private Process iProcess = null;
   
   /** The complete. */
   private volatile boolean _complete;

   /**
    * Instantiates a new proc mon.
    *
    * @param proc the proc
    */
   public ProcMon(Process proc)
   {
      iProcess = proc;
   }

   /**
    * Checks if is complete.
    *
    * @return true, if is complete
    */
   public boolean isComplete()
   {
      return _complete;
   }

   /* (non-Javadoc)
    * @see java.lang.Runnable#run()
    */
   @Override
   public void run()
   {
      try
      {
         iProcess.waitFor();
      }
      catch (InterruptedException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      _complete = true;
   }

   /**
    * Creates the.
    *
    * @param proc the proc
    * @return the proc mon
    */
   public static ProcMon create(Process proc)
   {
      ProcMon procMon = new ProcMon(proc);
      Thread t = new Thread(procMon);
      t.start();
      return procMon;
   }
}
