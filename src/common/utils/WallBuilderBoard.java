package common.utils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class WallBuilderBoard extends JPanel
{
   @Override
   public void setVisible(boolean aFlag)
   {
      iFrame.setVisible(aFlag);
   }

   private static final long serialVersionUID = 1L;
   static int SIZE; // Size of the Sudoku grid
   private static final int CELL_SIZE = 100; // Size of each cell
   public static final int ABOVE = 0;
   public static final int BELOW = 1;
   public static final int LEFT = 2;
   public static final int RIGHT = 3;
   private static int GRID_SIZE; // Size of the whole grid
   private static int clickCount = 0;
   private static int drawCount = 0;
   protected String[] iBoard;
   protected Color colors[][][];
   private Color brickColors[][];
   protected static final Color UNSOLVED_COLOR = Color.ORANGE;
   protected static final Color TEST_CHILD_COLOR = Color.PINK;
   protected static final Color OK_COLOR = Color.GREEN;
   protected static final Color FORBIDDEN_COLOR = Color.RED;
   protected static final Color TESTING_COLOR = Color.BLUE;
   protected static final Color PERMANENT_COLOR = Color.BLACK;
   private static final int DOT_SIZE = 12;
   private static final int SLEEP_TIME = 10;
   private List<LineElement> drawLineList = new ArrayList<>();
   private Stack<TestMove> iTestMoveList = new Stack<>();
   private boolean hasErrors;
   private boolean foundOne;
   private boolean allIsDone;
   private int speed = 0;
   private int incorrectCount = 0;
   private int iLeastErrors = Integer.MAX_VALUE;
   private Color[][][] iBestBoardColors;
   private JFrame iFrame;
   public static boolean slowDown = false;
   private List<String> iLogList = new ArrayList<>();
   private int loops = 0;
   private boolean iOutOfOptions = false;

   public WallBuilderBoard(String[] board, List<Wall> kw, String title)
   {
      iFrame = new JFrame(title);
      iBoard = board;
      SIZE = board.length;
      this.colors = new Color[SIZE + 1][SIZE + 1][2];
      for (int x = 0; x < SIZE + 1; x++)
      {
         for (int y = 0; y < SIZE + 1; y++)
         {
            this.colors[x][y][0] = UNSOLVED_COLOR;
            this.colors[x][y][1] = UNSOLVED_COLOR;
         }
      }

      brickColors = new Color[SIZE][SIZE];
      for (int x = 0; x < SIZE; x++)
      {
         for (int y = 0; y < SIZE; y++)
         {
            brickColors[x][y] = Color.WHITE;
         }
      }

      for (Wall knownWall : kw)
      {
         int x = knownWall.getX();
         int y = knownWall.getY();
         int direction = 0;
         switch (knownWall.getDirection())
         {
            case 0:
               break;

            case 1:
               y++;
               break;

            case 2:
               direction++;
               break;

            case 3:
               x++;
               direction++;
               break;

            default:
               break;
         }

         colors[x][y][direction] = PERMANENT_COLOR;
      }

      setWindow();
   }

   public void setWindow()
   {
      GRID_SIZE = (int) ((SIZE + .4) * CELL_SIZE);
      setPreferredSize(new Dimension(GRID_SIZE, GRID_SIZE));
   }

   @Override
   protected void paintComponent(Graphics g)
   {
      super.paintComponent(g);
      g.setColor(Color.WHITE);
      g.fillRect(0, 0, GRID_SIZE, GRID_SIZE);

      drawGrid(g);

   }

   public void drawGrid(Graphics g)
   {
      drawBackgrounds(g);
      drawAllLines(g);
      drawBlackDots(g);

   }

   public void drawBlackDots(Graphics g)
   {
      int yCount;
      int x;
      int y;
      int dotSize = DOT_SIZE;
      yCount = 0;
      g.setColor(Color.BLACK);
      for (String line : iBoard)
      {
         for (int xCount = 0; xCount < line.length(); xCount++)
         {
            x = getPos(xCount);
            y = getPos(yCount);
            g.fillOval(x, y, dotSize, dotSize);

            if (xCount == line.length() - 1)
            {
               g.fillOval(getPos(xCount + 1), getPos(yCount + 1), dotSize, dotSize);
            }

            if (yCount == iBoard.length - 2)
            {
               g.fillOval(getPos(xCount), getPos(yCount + 2), dotSize, dotSize);
            }
         }

         yCount++;

      }
   }

   public void drawBackgrounds(Graphics g)
   {
      for (int y = 0; y < SIZE; y++)
      {
         for (int x = 0; x < SIZE; x++)
         {
            g.setColor(brickColors[x][y]);
            g.fillRect(getPos(x), getPos(y), getPos(x + 1), getPos(y + 1));
         }
      }
   }

   public void drawAllLines(Graphics g)
   {
      g.setColor(Color.BLACK);
      g.setFont(new Font("Serif", Font.PLAIN, 48));

      int yCount = 0;
      int x = 0;
      int y = 0;

      // Draw Unsolved lines and the numbers
      g.setColor(Color.BLACK);
      for (String line : iBoard)
      {
         for (int xCount = 0; xCount < line.length(); xCount++)
         {
            x = getPos(xCount);
            y = getPos(yCount);

            // Draw lines in all colors around each cell
            drawLine(g, yCount, xCount, ABOVE);
            drawLine(g, yCount, xCount, BELOW);
            drawLine(g, yCount, xCount, LEFT);
            drawLine(g, yCount, xCount, RIGHT);

            g.setColor(Color.BLACK);
            String c = line.substring(xCount, xCount + 1);
            if (!c.equals(" "))
            {
               g.drawString(c, (int) (x + .4 * CELL_SIZE), (int) (y + .7 * CELL_SIZE));
            }
         }

         yCount++;
      }
   }

   private void drawLine(Graphics g, int x, int y, int direction)
   {
      int x1 = 0;
      int y1 = 0;
      int x2 = 0;
      int y2 = 0;
      int rightMove = 7;
      Color color;

      switch (direction)
      {
         case ABOVE:
            x1 = getPos(x);
            y1 = getPos(y);
            x2 = getPos(x + 1);
            y2 = getPos(y);
            break;

         case BELOW:
            x1 = getPos(x);
            y1 = getPos(y + 1);
            x2 = getPos(x + 1);
            y2 = getPos(y + 1);
            break;

         case LEFT:
            x1 = getPos(x);
            y1 = getPos(y);
            x2 = getPos(x);
            y2 = getPos(y + 1);
            break;

         case RIGHT:
            x1 = getPos(x + 1);
            y1 = getPos(y);
            x2 = getPos(x + 1);
            y2 = getPos(y + 1);
            break;

         default:
            break;
      }

      color = getColor(x, y, direction);
      Graphics2D g2 = (Graphics2D) g;
      double thickness = 2;
      if (color.equals(OK_COLOR) || color.equals(TESTING_COLOR) || color.equals(TEST_CHILD_COLOR)
            || color.equals(PERMANENT_COLOR))
      {
         thickness = 10;
      }

      Stroke oldStroke = g2.getStroke();
      g2.setStroke(new BasicStroke((float) thickness));
      g2.setColor(color);
      g.drawLine(x1 + rightMove, y1 + rightMove, x2 + rightMove, y2 + rightMove);
      g2.setStroke(oldStroke);

   }

   // private void saveLine(int x, int y, int direction, Color color)
   // {
   // drawLineList.add(new LineElement(x, y, direction, color));
   // }

   // private void drawSavedLine()
   // {
   // clickCount++;
   // if (drawCount < clickCount && drawLineList.size() > 0)
   // {
   // setColor(drawLineList.remove(0));
   // drawCount++;
   // repaint();
   // }
   //
   // }

   public void setColor(LineElement element)
   {
      setColor(element.getX(), element.getY(), element.getDirection(), element.getColor());
      // WallBuilder.print("draw a saved dot " + element);
   }

   private Color getColor(int x, int y, int direction)
   {
      if (x < 0 || x > SIZE || y < 0 || y > SIZE)
      {
         return null;
      }

      switch (direction)
      {
         case ABOVE:
            return this.colors[x][y][0];

         case BELOW:
            return this.colors[x][y + 1][0];

         case LEFT:
            return this.colors[x][y][1];

         case RIGHT:
            return this.colors[x + 1][y][1];
      }

      return null;
   }

   private static int getPos(int index)
   {
      return (int) (index * CELL_SIZE + 0.2 * CELL_SIZE);
   }

   public void createAndShowGUI(WallBuilderBoard wwb)
   {
      iFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      iFrame.add(wwb);
      iFrame.pack();
      iFrame.setLocationRelativeTo(null);

      wwb.setFocusable(true);
      wwb.requestFocusInWindow();

      iFrame.setVisible(true);
   }

   // Method to get the number from the board array based on x, y coordinates
   public int getNumber(int x, int y)
   {
      // Check if the coordinates are within bounds
      if (x >= 0 && x < SIZE && y >= 0 && y < iBoard[y].length())
      {
         char c = iBoard[y].charAt(x);
         if (Character.isDigit(c))
         {
            return Character.getNumericValue(c); // Return the digit as an
                                                 // integer
         }
      }
      return -1; // Return -1 if the position is out of bounds or not a digit
   }

   public boolean solve()
   {
      loops++;
      if (iFrame != null)
      {
         iFrame.setTitle("Loops: " + loops);
      }
      log("******* Loop " + loops + " *********************************************");
      hasErrors = false;
      allIsDone = true;
      incorrectCount = 0;
      initLog();

      // Check all numbers
      for (int y = 0; y < SIZE; y++)
      {
         for (int x = 0; x < SIZE; x++)
         {
            WallList wl = getWallLists(x, y);
            int number = getNumber(x, y);
            List<Wall> unsolved = wl.getUnsolvedList();
            List<Wall> forbidden = wl.getForbiddenList();
            List<Wall> ok = wl.getOKList();
            log("===== New:         " + x + ", " + y + " || " + number + " || u=" + unsolved.size() + ", f="
                  + forbidden.size() + ", ok=" + ok.size());

            if (number > -1)
            {
               int points = Math.abs(ok.size() - number);
               log("B. Has " + ok.size() + " ok. Should have been " + number + "(-" + points + ")");
               incorrectCount += points;
            }

            // log("0. Has Error, not all Done");
            // hasErrors = true;
            allIsDone = false;

            if ((number > -1 && (ok.size() > number || ok.size() + unsolved.size() < number)) || ok.size() == 4)
            {
               log("1. Has Error, not all Done");
               hasErrors = true;
               allIsDone = false;
            }
            else
            {

               if (ok.size() == number && unsolved.size() != 0)
               {
                  log("2. Setting some forbidden");
                  sleep(speed);
                  setColors(unsolved, FORBIDDEN_COLOR);
                  repaint();
               }

               if (ok.size() != number && unsolved.size() + ok.size() == number)
               {
                  log("3. Setting some OK");
                  sleep(speed);
                  setColors(unsolved, OK_COLOR);
                  repaint();
               }

               if ((ok.size() != number || unsolved.size() != 0) && number != -1)
               {
                  log("4. All is not Done");
                  allIsDone = false;
               }

            }

            WallList w2 = getWallLists(x, y);
            unsolved = w2.getUnsolvedList();
            forbidden = w2.getForbiddenList();
            ok = wl.getOKList();
            log("===== count again: " + x + ", " + y + " || " + number + " || u=" + unsolved.size() + ", f="
                  + forbidden.size() + ", ok=" + ok.size());
            // incorrectCount += unsolved.size();
            if (number > -1 && ok.size() != number)
            {
               log("5. All is not Done");
               allIsDone = false;
            }

         }
      }

      // Check every dot
      List<TestDot> tdList = new ArrayList<>();
      foundOne = false;
      for (int y = 0; y < SIZE + 1; y++)
      {
         for (int x = 0; x < SIZE + 1; x++)
         {
            if (solveDots(x, y, tdList))
            {
               foundOne = true;
            }
         }
      }

      saveLeastErrors();

      return noMoveActions(tdList);
   }

   private void initLog()
   {
      iLogList.clear();
   }

   void printLastLog()
   {
      for (String line : iLogList)
      {
         WallBuilder.print(line);
      }
   }

   private static void log(String line)
   {
      // iLogList.add(line);
      WallBuilder.print(line);
   }

   private void saveLeastErrors()
   {
      WallBuilder.print("Total of numbers of Errors: " + incorrectCount);
      if (incorrectCount < iLeastErrors)// && hasErrors == false)
      {
         WallBuilder.print("Best result so far");
         iLeastErrors = incorrectCount;
         iBestBoardColors = TestMove.copyColors(colors);
      }
   }

   public boolean noMoveActions(List<TestDot> tdList)
   {
      if (tdList.size() == 0)
      {
         log("21. Nothing in tdList");
         // hasErrors = true;
         WallBuilder.print("Incorrect count: " + incorrectCount);
         if (!hasErrors && incorrectCount == 0)
         {
            WallBuilder.print("Incorrect count: " + incorrectCount);
            WallBuilder.print("All done!!!!");
            return true;
         }
      }

      if (hasErrors)
      {
         // WallBuilder.print("Has Errors!");

         // If all moves are done for this node, use next node in list
         log("22. Make next step");
         Wall wall = makeNextBlueMove(false);
         if (wall == null)
         {
            iOutOfOptions = true;
         }

      }
      else
      {
         if (!foundOne)
         {
            Collections.shuffle(tdList);
            sleep(speed);

            if (tdList.size() > 0)
            {
               TestMove nextTest = new TestMove(tdList.get(0), colors);
               iTestMoveList.push(nextTest);
               log("23, Set a blue");

               setBlueColor(nextTest.getNextMove());

            }
            else
            {
               makeNextBlueMove(false);
            }
         }
      }

      return false;
   }

   public Wall makeNextBlueMove(boolean foundSolutions)
   {
      System.out.println(iTestMoveList.size());
      if (iTestMoveList.size() == 0)
      {

         if (foundSolutions)
         {
            iOutOfOptions = true;
            return null;
         }
         return null;
      }

      TestMove prevMove = iTestMoveList.peek();
      // Go back to previous board
      log("--------------------------------------- Revert to old board!!!!");
      colors = TestMove.copyColors(prevMove.getSavedStateColors());
      Wall wall = prevMove.getNextMove();
      if (wall == null)
      {
         iTestMoveList.pop();
         return makeNextBlueMove(foundSolutions);
      }

      setBlueColor(wall);
      return wall;
   }

   public void setBlueColor(Wall nextMove)
   {
      // WallBuilder.print("Next Move: " + nextMove);
      setColor(nextMove, TESTING_COLOR);
   }

   public Wall getAMove()
   {
      TestMove tester = iTestMoveList.peek();
      if (tester == null)
      {
         return null;
      }

      Wall nextMove = tester.getNextMove();
      if (nextMove == null)
      {
         iTestMoveList.pop();
         return getAMove();
      }
      return nextMove;
   }

   public boolean solveDots(int x, int y, List<TestDot> tdList)
   {
      WallList wl = geDotlLists(x, y);
      // int number = getNumber(x-1, y-1);
      List<Wall> unsolved = wl.getUnsolvedList();
      List<Wall> forbidden = wl.getForbiddenList();
      List<Wall> ok = wl.getOKList();

      log("===  New solveDots: " + x + ", " + y + " || u=" + unsolved.size() + ", f=" + forbidden.size() + ", ok="
            + ok.size());

      log("A. Minus points " + wl.getUnsolvedList().size());
      // incorrectCount += wl.getUnsolvedList().size();

      if (ok.size() == 3)
      {
         log("C. 3 Lines to one dot. Very bad! (-20)");
         // incorrectCount += 20;
      }

      if (ok.size() == 1 && unsolved.size() == 1)
      {
         sleep(speed);
         log("6. Setting som OK and done");
         setColors(unsolved, OK_COLOR);
         repaint();
         return true;
      }

      if (unsolved.size() > 0 && ok.size() == 2)
      {
         log("7. Setting som forbidden and done");
         sleep(speed);
         setColors(unsolved, FORBIDDEN_COLOR);
         repaint();
         return true;
      }

      if (forbidden.size() == 3 && unsolved.size() == 1)
      {
         log("8. Setting som forbidden and done");
         sleep(speed);
         setColors(unsolved, FORBIDDEN_COLOR);
         repaint();
         return true;
      }

      if (ok.size() == 1 || ok.size() > 2)
      {
         log("9. All is not one");
         allIsDone = false;
      }

      if (ok.size() > 2 || (ok.size() == 1 && unsolved.size() == 0))
      {
         log("10. Has errors and return false");
         hasErrors = true;
         return false;
      }

      if (ok.size() == 1)
      {
         log("11. Making a testdot and return false");
         tdList.add(new TestDot(x, y, unsolved));
         return false;
      }

      log("12. Returning false");
      return false;
   }

   public static void sleep(int speedNow)
   {
      try
      {
         Thread.sleep(speedNow);
      }
      catch (InterruptedException e)
      {
         e.printStackTrace();
      }
   }

   private WallList geDotlLists(int x, int y)
   {
      WallList wl = new WallList();
      if (x < 9)
      {
         addToList(wl, x, y, ABOVE);
      }
      addToList(wl, x - 1, y, ABOVE);
      addToList(wl, x, y - 1, LEFT);

      if (y < 9)
      {
         addToList(wl, x, y, LEFT);
      }
      return wl;
   }

   private void setColors(List<Wall> list, Color color)
   {
      if (iTestMoveList.size() > 0 && color.equals(OK_COLOR))
      {
         color = TEST_CHILD_COLOR;
      }

      for (Wall knownWalls : list)
      {
         setColor(knownWalls, color);
      }

   }

   private void setColor(Wall list, Color color)
   {
      setOrSaveColor(list.getX(), list.getY(), list.getDirection(), color);

   }

   private WallList getWallLists(int x, int y)
   {
      WallList wl = new WallList();
      addToList(wl, x, y, ABOVE);
      addToList(wl, x, y, BELOW);
      addToList(wl, x, y, LEFT);
      addToList(wl, x, y, RIGHT);
      return wl;
   }

   public void addToList(WallList wl, int x, int y, int direction)
   {
      if (x > -1 && x <= SIZE && y > -1 && y <= SIZE)
      {
         Color col = getColor(x, y, direction);
         if (col == null)
         {
            return;
         }

         if (col.equals(UNSOLVED_COLOR))
         {
            wl.addUnsolved(x, y, direction);
         }

         if (col.equals(OK_COLOR) || col.equals(TESTING_COLOR) || col.equals(TEST_CHILD_COLOR)
               || col.equals(PERMANENT_COLOR))
         {
            wl.addOK(x, y, direction);
         }

         if (col.equals(FORBIDDEN_COLOR))
         {
            wl.addForbidden(x, y, direction);
         }
      }
   }

   private void setOrSaveColor(int x, int y, int direction, Color color)
   {
      setColor(x, y, direction, color);
      repaint();
   }

   private void setColor(int x, int y, int direction, Color color)
   {
      switch (direction)
      {
         case ABOVE:
            colors[x][y][0] = color;
            break;

         case BELOW:
            colors[x][y + 1][0] = color;
            break;

         case LEFT:
            colors[x][y][1] = color;
            break;

         case RIGHT:
            colors[x + 1][y][1] = color;
            break;
      }
   }

   public void showBestSoFar()
   {
      WallBuilder.print("Stopping now!");
      iFrame.setTitle("Number of errors " + iLeastErrors);
      colors = TestMove.copyColors(iBestBoardColors);
      // colorErrors();
      repaint();
   }

   private void colorErrors()
   {
      int x1 = Integer.MAX_VALUE;
      int y1 = Integer.MAX_VALUE;
      int x2 = Integer.MIN_VALUE;
      int y2 = Integer.MIN_VALUE;
      // Check all numbers
      for (int y = 0; y < SIZE; y++)
      {
         for (int x = 0; x < SIZE; x++)
         {
            WallList wl = getWallLists(x, y);
            int number = getNumber(x, y);
            List<Wall> unsolved = wl.getUnsolvedList();
            List<Wall> forbidden = wl.getForbiddenList();
            List<Wall> ok = wl.getOKList();

            if (ok.size() != number && number != -1)
            {
               brickColors[x][y] = Color.YELLOW;

               if (x < x1)
               {
                  x1 = x;
               }

               if (y < y1)
               {
                  y1 = y;
               }

               if (x > x2)
               {
                  x2 = x;
               }

               if (y > y2)
               {
                  y2 = y;
               }
            }
         }
      }
      repaint();

      sleep(SLEEP_TIME);

      x1--;
      y1--;
      x2 += 2;
      y2 += 2;

      for (int y = y1; y < y2; y++)
      {
         for (int x = x1; x < x2; x++)
         {
            if (x <= SIZE + 1 && y <= SIZE + 1 && x >= 0 && y >= 0)
            {
               colors[x][y][0] = UNSOLVED_COLOR;
               colors[x][y][1] = UNSOLVED_COLOR;

               if (x < SIZE && y < SIZE)
               {
                  brickColors[x][y] = Color.WHITE;
               }

            }
         }
      }

      repaint();
      sleep(SLEEP_TIME);

      for (int x = 0; x < SIZE + 1; x++)
      {
         for (int y = 0; y < SIZE + 1; y++)
         {
            if (colors[x][y][0].equals(TESTING_COLOR))
            {
               colors[x][y][0] = OK_COLOR;
            }

            if (colors[x][y][1].equals(TESTING_COLOR))
            {
               colors[x][y][1] = OK_COLOR;
            }

         }
      }

      iTestMoveList.clear();

      // iLeastErrors = Integer.MAX_VALUE;

      // slowDown = true;

      repaint();

      sleep(SLEEP_TIME);
   }

   public int getLeastCount()
   {
      return iLeastErrors;
   }

//   protected void startEditing()
//   {
//      // TODO Auto-generated method stub
//
//   }

   public boolean hasChoicesLeft()
   {
      System.out.println("Left " + iTestMoveList.size() + " and outofOptions = " + iOutOfOptions);

      return (iTestMoveList.size() > 0 || iOutOfOptions);
   }

   public TestMove saveSolution()
   {
      return new TestMove(colors);
   }

   public void showPage(Color[][][] colors2)
   {
      colors = TestMove.copyColors(colors2);
      repaint();
   }

   public void setTitle(String title)
   {
     iFrame.setTitle(title);      
   }

   public void setLocation(int i)
   {
      int xIndex = i;
      Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
      double numberOfWindowsShowing = Math.floor((dim.getWidth())/iFrame.getWidth());

      System.out.println(numberOfWindowsShowing);
      
      if (i > numberOfWindowsShowing)
      {
         xIndex = (int) numberOfWindowsShowing;
      }
      
      System.out.println(xIndex);
      
      iFrame.setLocation(0+(xIndex-1)*iFrame.getWidth(), 20);
   }

}
