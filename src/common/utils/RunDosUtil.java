/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package common.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;

import org.apache.commons.lang3.SystemUtils;

import common.properties.SettingProperties;


/**
 * The Class RunDosUtil.
 *
 * @author Lars Svensson
 */
public class RunDosUtil extends Thread
{
    
    /** The Command line. */
    private final String iCommandLine;

    /**
     * Instantiates a new run dos util.
     *
     * @param commandLine the command line
     */
    private RunDosUtil(String commandLine)
    {
        iCommandLine = commandLine;
    }

    /* (non-Javadoc)
     * @see java.lang.Thread#run()
     */
    @Override
    public void run()
    {
        iRetVal = exec(iCommandLine);
    }

    /**
     * Exec.
     *
     * @param commandLine the command line
     * @return the int
     */
    public static int exec(String commandLine)
    {
        Process p = null;
        try
        {
            // get runtime environment and execute child process
            System.out.println("Executing: " + commandLine);

            List<String> list = getSplittedCommands(commandLine);
            System.out.println(list);
            ProcessBuilder pb = new ProcessBuilder(list);
            Map<String, String> env = pb.environment();
            env.put("var1", "value1");
            try
            {
                pb.directory(new File("."));
                p = pb.start();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return 0;
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }
        return 0;
    }

    /**
     * Gets the splitted commands.
     *
     * @param commandLine the command line
     * @return the splitted commands
     */
    public static List<String> getSplittedCommands(String commandLine)
    {
        boolean outsideQuote = true;
        List<String> retList = new ArrayList<String>();
        String command = "";
        for (int i = 0; i < commandLine.length(); i++)
        {
            char c = commandLine.charAt(i);
            if (outsideQuote)
            {
                if (c == '"')
                {
                    outsideQuote = false;
                }
                else if (c != ' ')
                {
                    command += c;
                }
                else
                {
                    // space outside quotes, new command
                    retList.add(command);
                    command = "";
                }
            }
            else
            {
                if (c == '"')
                {
                    outsideQuote = true;
                    retList.add(command);
                    command = "";
                }
                else
                {
                    command += c;
                }
            }
        }
        return retList;
    }

    /**
     * Exec.
     *
     * @param commandLine the command line
     * @param asOwnThread the as own thread
     * @return the int
     */
    public static int exec(String commandLine, boolean asOwnThread)
    {
        if (asOwnThread)
        {

            RunDosUtil run = new RunDosUtil(commandLine);
            run.start();
            return run.getRetVal();
        }
        else
        {
            return exec(commandLine);
        }
    }

    /** The Ret val. */
    private int iRetVal;

    /**
     * Gets the ret val.
     *
     * @return the ret val
     */
    private int getRetVal()
    {
        return iRetVal;
    }

    /**
     * Run jar.
     *
     * @param propertyKeyJarPath the property key jar path
     * @param mainClassPath the main class path
     * @param args the args
     */
    public static void runJar(String propertyKeyJarPath, String mainClassPath, String args)
    {
        SettingProperties prop = SettingProperties.getInstance();
        String jarPath = prop.getProperty(propertyKeyJarPath);

        if (jarPath == null || !new File(jarPath).exists() || !new File(jarPath).isFile())
        {
            JFileChooser fc = new JFileChooser();
            fc.setDialogTitle("Find the jar file for " + propertyKeyJarPath);
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int retval = fc.showOpenDialog(null);
            if (retval == JFileChooser.APPROVE_OPTION)
            {
                File myFile = fc.getSelectedFile();
                jarPath = myFile.getAbsolutePath();
                prop.setProperty(propertyKeyJarPath, jarPath);
            }
        }

        String javaHome = System.getProperty("java.home");
        String javaBin = javaHome + File.separator + "bin" + File.separator + "java";
        File jarFile = new File(jarPath);
        String jarFileName = jarFile.getName();
        String jarFolder = jarFile.getParent();

        // String execLine = "\"" + javaBin + "\" -jar \"" + jarPath + "\" " +
        // args;

        String pathSeparator = getOSDependentPathSeparator();

        String libs = jarFileName + pathSeparator + "lib/*" + pathSeparator + ".";

        String execLine = "\"" + javaBin + "\" -cp \"" + libs + "\" " + mainClassPath + " " + args;

        RunApacheUtil.exec(execLine, jarFolder, true);

    }

   public static String getOSDependentPathSeparator()
   {
      String pathSeparator = ";";
        if (!SystemUtils.IS_OS_WINDOWS)
        {
            pathSeparator = ":";
        }
      return pathSeparator;
   }

}
