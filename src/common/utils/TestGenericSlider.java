package common.utils;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import common.ui.components.ext.JPanelExt;

public class TestGenericSlider extends JFrame implements ActionListener
{
   private static final long serialVersionUID = 1L;

   // TODO init fields here

   /////////// Java GUI Designer version 1.1 Starts here. ////
   // Text below will be regenerated automatically.
   // Generated 2018-11-06 22:24:04

   private JPanelExt panel1;
   
   private int width = 950;
   private int height = 268;

   public static void main(String[] args)
   {
      TestGenericSlider creteNewCrossStiches = new TestGenericSlider();
      creteNewCrossStiches.setVisible(true);
   }

   public TestGenericSlider()
   {
      super();
      setTitle("Test GeneriSlider");
      setSize(width, height);
      setIconImage(AppInfo.getInstance().getIcon().getImage());

      getContentPane().setLayout(new BorderLayout());
      panel1 = new JPanelExt(width, height);
      panel1.setLayout(null);

      int sliderWidth = width / 2;
      int sliderHeight = 60;

      GenericSlider gs = new GenericSlider(0, "Light:", sliderWidth, sliderHeight, -50, 50, 0, 1);
      gs.setBounds(10, 10, sliderWidth, sliderHeight);

      panel1.add(gs);

      GenericSlider gs2 = new GenericSlider(1, "Contrast:", sliderWidth * 2, sliderHeight * 2, -50, 50, 0, 1);
      gs2.setBounds(10, 80, sliderWidth * 2, sliderHeight * 2);
      System.out.println(gs2.getValue());

      gs2.addActionListener(this);

      panel1.add(gs2);

      getContentPane().add(panel1);

      /////////// Java GUI Designer version 1.1 Generation ends here. ///

      // TODO Add init declarations here

   }

   private static int getNumber(JTextField txt)
   {
      String value = txt.getText();
      int retVal = -1;

      try
      {
         retVal = Integer.parseInt(value);
      }
      catch (NumberFormatException e)
      {
         e.printStackTrace();
      }

      return retVal;
   }

   @Override
   public void actionPerformed(ActionEvent e)
   {
      if (e.getSource() instanceof GenericSlider)
      {
         GenericSlider gs = (GenericSlider) e.getSource();
         System.out.println("New value: " + gs.getValue());
      }
   }

}
