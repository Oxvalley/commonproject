package common.utils;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * A class to find the silent parts of a wave file.
 */
public class SilentPartsFinder
{

    /**
     * Finds the silent parts of a wave file.
     *
     * @param filePath
     *        The path to the wave file.
     * @param threshold
     *        The threshold below which the amplitude is considered silent.
     * @return An array of SilentPart objects representing the silent parts of the wave file.
     *
     * @throws IOException
     *         If an I/O error occurs while reading the wave file.
     * @throws UnsupportedAudioFileException
     *         If the wave file format is not supported.
     */
    public static SilentPart[] findSilentParts(String filePath, double threshold)
        throws IOException, UnsupportedAudioFileException
    {
        // Open the wave file
        File file = new File(filePath);
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);

        // Get the audio format and frame size
        AudioFormat audioFormat = audioInputStream.getFormat();
        int frameSize = audioFormat.getFrameSize();

        // Calculate the number of frames per buffer
        int bufferSize = (int) (audioFormat.getSampleRate() * frameSize);

        // Read the audio data in chunks
        byte[] buffer = new byte[bufferSize];
        int bytesRead;
        int totalBytesRead = 0;
        int silentStartFrame = -1;
        SilentPart[] silentParts = new SilentPart[0];

        while ((bytesRead = audioInputStream.read(buffer)) != -1)
        {
            // Process each frame in the buffer
            for (int i = 0; i < bytesRead; i += frameSize)
            {
                // Calculate the amplitude of the frame
                double amplitude = calculateAmplitude(buffer, i, frameSize);

                // Check if the amplitude is below the threshold
                if (amplitude < threshold)
                {
                    // If this is the start of a silent part, record the frame index
                    if (silentStartFrame == -1)
                    {
                        silentStartFrame = totalBytesRead / frameSize;
                    }
                }
                else
                {
                    // If this is the end of a silent part, record the silent part and reset the
                    // start frame
                    if (silentStartFrame != -1)
                    {
                        int silentEndFrame = totalBytesRead / frameSize - 1;
                        silentParts = addSilentPart(silentParts,
                            new SilentPart(silentStartFrame, silentEndFrame));
                        silentStartFrame = -1;
                    }
                }

                // Increment the total bytes read
                totalBytesRead += frameSize;
            }
        }

        // If there is a silent part at the end of the wave file, record it
        if (silentStartFrame != -1)
        {
            int silentEndFrame = totalBytesRead / frameSize - 1;
            silentParts = addSilentPart(silentParts,
                new SilentPart(silentStartFrame, silentEndFrame));
        }

        // Close the audio input stream
        audioInputStream.close();

        return silentParts;
    }

    /**
     * Calculates the amplitude of a frame in the audio data.
     *
     * @param buffer
     *        The buffer containing the audio data.
     * @param offset
     *        The offset of the frame in the buffer.
     * @param frameSize
     *        The size of the frame.
     * @return The amplitude of the frame.
     */
    private static double calculateAmplitude(byte[] buffer, int offset, int frameSize)
    {
        double amplitude = 0;

        // Convert the bytes to samples and calculate the amplitude
        for (int i = 0; i < frameSize; i += 2)
        {
            short sample = (short) ((buffer[offset + i] & 0xff) | (buffer[offset + i + 1] << 8));
            amplitude += Math.abs(sample) / 32768.0;
        }

        // Calculate the average amplitude
        amplitude /= frameSize / 2;

        return amplitude;
    }

    /**
     * Adds a silent part to the array of silent parts.
     *
     * @param silentParts
     *        The array of silent parts.
     * @param silentPart
     *        The silent part to add.
     * @return The updated array of silent parts.
     */
    private static SilentPart[] addSilentPart(SilentPart[] silentParts, SilentPart silentPart)
    {
        SilentPart[] newSilentParts = new SilentPart[silentParts.length + 1];
        System.arraycopy(silentParts, 0, newSilentParts, 0, silentParts.length);
        newSilentParts[silentParts.length] = silentPart;
        return newSilentParts;
    }

    /**
     * A class representing a silent part of a wave file.
     */
    public static class SilentPart
    {
        private final int startFrame;
        private final int endFrame;

        /**
         * Constructs a SilentPart object with the specified start and end frames.
         *
         * @param startFrame
         *        The start frame of the silent part.
         * @param endFrame
         *        The end frame of the silent part.
         */
        public SilentPart(int startFrame, int endFrame)
        {
            this.startFrame = startFrame;
            this.endFrame = endFrame;
        }

        /**
         * Gets the start frame of the silent part.
         *
         * @return The start frame.
         */
        public int getStartFrame()
        {
            return startFrame;
        }

        /**
         * Gets the end frame of the silent part.
         *
         * @return The end frame.
         */
        public int getEndFrame()
        {
            return endFrame;
        }
    }

    /**
     * The main method to demonstrate the usage of the findSilentParts method. The silent parts of a
     * wave file are printed to the console.
     *
     * @param args
     *        The command line arguments.
     *
     * @throws IOException
     *         If an I/O error occurs while reading the wave file.
     * @throws UnsupportedAudioFileException
     *         If the wave file format is not supported.
     *
     * @example // Console output: // Silent part: startFrame=0, endFrame=999 // Silent part:
     *          startFrame=2000, endFrame=2999
     */
    public static void main(String[] args) throws IOException, UnsupportedAudioFileException
    {
        String filePath = "path/to/wave/file.wav";
        double threshold = 0.1;

        SilentPart[] silentParts = findSilentParts(filePath, threshold);

        for (SilentPart silentPart : silentParts)
        {
            System.out.println("Silent part: startFrame=" + silentPart.getStartFrame() +
                ", endFrame=" + silentPart.getEndFrame());
        }
    }
}