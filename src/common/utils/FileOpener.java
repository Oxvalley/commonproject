package common.utils;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;


/**
 * The Class FileOpener.
 */
public class FileOpener
{
   
   /**
    * The main method.
    *
    * @param a the arguments
    */
   public static void main(String[] a)
   {

      String fileName = "c:\\a.doc";
      openFile(fileName);
   }

   /**
    * Open file.
    *
    * @param fileName the file name
    */
   public static void openFile(String fileName)
   {
      try
      {
         if (Desktop.isDesktopSupported())
         {
            Desktop.getDesktop().open(new File(fileName));
         }
      }
      catch (IOException ioe)
      {
         ioe.printStackTrace();
      }
   }
}