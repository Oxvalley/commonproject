package common.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Iterator;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.TagField;

import common.utils.xjc.Error;
import common.utils.xjc.Errors;
import common.utils.xjc.Extra;
import common.utils.xjc.Extras;
import common.utils.xjc.Root;
import common.utils.xjc.Song;

public class MP3MetaData
{

   public static void getMetaData(String fileName, Root root, int count)
   {

      AudioFile f;
      Song song = new Song();
      root.getSong().add(song);
      try
      {
         String escapedFileName = escape(fileName);
         System.out.println("Filname: " + escapedFileName);
         String type = FileHelper.getFileTag(fileName);
         song.setId(BigInteger.valueOf(count));
         song.setType(type);
         song.setPath(escapedFileName);

         File file = new File(fileName);
         String parentPath = file.getParent();
         String parent = FileHelper.getLastFolder(parentPath);
         song.setParent(parent);

         File parentFile = new File(parentPath);
         String grandParentPath = parentFile.getParent();
         String grandParent = FileHelper.getLastFolder(grandParentPath);
         song.setGrandparent(grandParent);

         f = AudioFileIO.read(file);
         Tag tag = f.getTag();
         AudioHeader ah = f.getAudioHeader();
         String usedValues = "";
         if (tag != null)
         {
            String value = escape(tag.getFirst(FieldKey.ARTIST));
            System.out.println("Artist: " + value);
            usedValues = addToUsedValues(usedValues, value);
            song.setArtist(escape(value));

            value = escape(tag.getFirst(FieldKey.ALBUM));
            System.out.println("Album: " + value);
            usedValues = addToUsedValues(usedValues, value);
            song.setAlbum(escape(value));

            value = escape(tag.getFirst(FieldKey.TITLE));
            System.out.println("Title: " + value);
            usedValues = addToUsedValues(usedValues, value);
            song.setTitle(escape(value));

            value = escape(tag.getFirst(FieldKey.YEAR));
            System.out.println("Year: " + value);
            usedValues = addToUsedValues(usedValues, value);
            song.setYear(escape(value));

            value = escape(tag.getFirst(FieldKey.COMMENT));
            System.out.println("Comment: " + value);
            usedValues = addToUsedValues(usedValues, value);
            song.setComment(escape(value));

            String time = getTimeFromSeconds(ah.getTrackLength());
            System.out.println("Length: " + time);
            song.setLength(time);

            song.setSamplerate(BigInteger.valueOf(ah.getSampleRateAsNumber()));
            System.out.println("Sample Rate: " + ah.getSampleRateAsNumber());

            Iterator<TagField> iterator = tag.getFields();
            while (iterator.hasNext())
            {
               TagField field = iterator.next();
               String key = field.getId();
               try
               {
                  value = escape(tag.getFirst(key));
                  System.out.println(key + " = " + value);
                  if (!usedValues.contains(value))
                  {
                     Extras extras = song.getExtras();

                     if (extras == null)
                     {
                        extras = new Extras();
                        song.setExtras(extras);
                     }

                     Extra extra = new Extra();
                     extra.setKey(key);
                     extra.setContent(value);
                     extras.getExtra().add(extra);
                  }
               }
               catch (Exception e)
               {
                  addToErrors(song, e, key);
               }
            }
         }

      }
      catch (CannotReadException e)
      {
         addToErrors(song, e);
      }
      catch (IOException e)
      {
         addToErrors(song, e);
      }
      catch (TagException e)
      {
         addToErrors(song, e);
      }
      catch (ReadOnlyFileException e)
      {
         addToErrors(song, e);
      }
      catch (InvalidAudioFrameException e)
      {
         addToErrors(song, e);
      }
      catch (Exception e)
      {
         addToErrors(song, e);
      }
      //
      //// The examples above assume textual data, but what about binary data.
      // You can retrieve the complete tag field instead of just a String
      // representation of its value by using getFirstField()
      //
      //
      // TagField binaryField = tag.getFirstField(FieldKey.COVER_ART)); }
   }

   /**
    * Get mp3 length in seconds
    *
    * @param fileName
    * @return
    */
   public static int getDuration(String fileName)
   {

      AudioFile f;
      try
      {
         f = AudioFileIO.read(new File(fileName));
         Tag tag = f.getTag();
         AudioHeader ah = f.getAudioHeader();
         if (tag != null)
         {
            return ah.getTrackLength();
         }
      }
      catch (CannotReadException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      catch (TagException e)
      {
         e.printStackTrace();
      }
      catch (ReadOnlyFileException e)
      {
         e.printStackTrace();
      }
      catch (InvalidAudioFrameException e)
      {
         e.printStackTrace();
      }
      return -1;
   }


   private static String escape(String value)
   {
      // The list of valid characters is in the XML specification:
      // Char ::= #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] |
      // [#x10000-#x10FFFF]

      int[] unallowed = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 14, 15, 16, 17, 18,
         19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 239 };
      value = removeChars(value, unallowed);
      return StringEscapeUtils.escapeHtml4(value);
   }

   private static String removeChars(String value, int[] unallowed)
   {
       if(value == null)
       {
           return null;
       }

      for (int charvalue : unallowed)
      {
         value = value.replace((char) charvalue, ' ');
      }

      return value;
   }

   private static void addToErrors(Song song, Exception e)
   {
      addToErrors(song, e, null);
   }

   private static void addToErrors(Song song, Exception e, String key)
   {
      Errors errors = song.getErrors();

      if (errors == null)
      {
         errors = new Errors();
         song.setErrors(errors);
      }

      Error error = new Error();

      String message = escape(e.getMessage());
      if (key != null)
      {
         error.setTitle(key);
         System.out.println(key + " = " + message);
      }
      else
      {
         error.setTitle("");
         System.out.println(message);
      }
      error.setContent(message);
      errors.getError().add(error);
   }

   public static String addToUsedValues(String usedValues, String value)
   {
      return usedValues + value + "-|-";
   }

   private static String getTimeFromSeconds(int totalSecs)
   {
      int minutes = (totalSecs % 3600) / 60;
      int seconds = totalSecs % 60;

      return String.format("%02d:%02d", minutes, seconds);
   }
}
