package common.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

public class ReplaceUtil
{

    public static void main(String[] args)
    {
        if (args.length < 3)
        {
            System.out
                .println("ReplaceUtil \"replaceThis\" \"withThis\" \"fileName\" [\"newFilename\"] [\"encoding\"(default utf-8)]");
            System.exit(1);
        }

        String newFilename = args[2];
        if (args.length >= 4)
        {
            newFilename = args[3];
        }

        String encoding = "utf-8";
        if (args.length >= 5)
        {
            encoding = args[4];
        }

        String content;
        try
        {
            content = IOUtils.toString(new FileInputStream(args[2]), encoding);
            content = content.replaceAll(args[0], args[1]);
            IOUtils.write(content, new FileOutputStream(newFilename), encoding);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
