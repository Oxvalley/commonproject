package common.utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

public class WallBuilder
{

   private static final int SIZE = 9;

   private static final int ABOVE = 0;
   private static final int BELOW = 1;
   private static final int LEFT = 2;
   private static final int RIGHT = 3;

   private static FileWriter fw;

   public static void main(String[] args)
   {
      List<SavedBoard> iBoardList = new ArrayList<>();
      // OK = 1, 2, 3, 4, 6
      // Not OK 5, 7
      int useBoard = 6;

      // Number 1
      String[] board = new String[SIZE];
      board[0] = " 2  0   2";
      board[0] = " 2  0   2";
      board[1] = "23 23  3 ";
      board[2] = " 2 22    ";
      board[3] = "1      2 ";
      board[4] = "11   3  2";
      board[5] = "   23 2 1";
      board[6] = "1     222";
      board[7] = " 1   311 ";
      board[8] = "2121 322 ";
      List<Wall> kw = new ArrayList<Wall>();
      iBoardList.add(new SavedBoard(board, kw));

      // Number 2
      board = new String[SIZE];
      board[0] = "   0 0   ";
      board[1] = "   1 20  ";
      board[2] = " 112  11 ";
      board[3] = "   1 2  2";
      board[4] = "031112032";
      board[5] = "  1 1   1";
      board[6] = "03221   1";
      board[7] = "  1211  1";
      board[8] = " 2   1 12";
      kw = new ArrayList<Wall>();
      // kw.add(new Wall(1, 2, 1));
      // kw.add(new Wall(1, 6, 1));
      kw.add(new Wall(1, 8, RIGHT)); // This gives two solutions
      // kw.add(new Wall(5, 3, ABOVE));
      // kw.add(new Wall(5, 3, 2));
      // kw.add(new Wall(7, 4, 1));
      // kw.add(new Wall(6, 6, RIGHT));
      kw.add(new Wall(6, 7, RIGHT));
      // kw.add(new Wall(8, 3, 3));
      // kw.add(new Wall(5, 8, 1));
      iBoardList.add(new SavedBoard(board, kw));

      // Number 3
      // 06.19.21
      board = new String[SIZE];
      board[0] = " 1112    ";
      board[1] = " 1  12   ";
      board[2] = "22  13   ";
      board[3] = "23012    ";
      board[4] = " 2 1 10  ";
      board[5] = "   0 010 ";
      board[6] = " 2 3 2 2 ";
      board[7] = "03   311 ";
      board[8] = "  11 211 ";
      kw = new ArrayList<>();
      kw.add(new Wall(4, 0, ABOVE));
      kw.add(new Wall(5, 2, RIGHT));
      kw.add(new Wall(5, 2, BELOW));
      kw.add(new Wall(0, 3, LEFT));
      kw.add(new Wall(5, 4, LEFT));
      kw.add(new Wall(0, 5, RIGHT));
      kw.add(new Wall(3, 6, BELOW));
      kw.add(new Wall(8, 7, RIGHT));
      kw.add(new Wall(3, 8, BELOW));
      kw.add(new Wall(5, 8, BELOW));
      iBoardList.add(new SavedBoard(board, kw));

      // Number 4 06.17.40 no 1
      board = new String[SIZE];
      board[0] = "2   0 0  ";
      board[1] = "230 1 20 ";
      board[2] = "1   1  1 ";
      board[3] = " 30021 10";
      board[4] = " 2 131 01";
      board[5] = "11222  3 ";
      board[6] = "2  2 11  ";
      board[7] = "3  2 2   ";
      board[8] = "  1221223";
      kw = new ArrayList<Wall>();
      kw.add(new Wall(0, 1, LEFT));
      kw.add(new Wall(1, 2, ABOVE));
      kw.add(new Wall(0, 5, LEFT));
      kw.add(new Wall(2, 5, ABOVE));
      kw.add(new Wall(7, 6, ABOVE));
      kw.add(new Wall(0, 7, LEFT));
      kw.add(new Wall(6, 7, LEFT));
      kw.add(new Wall(8, 7, ABOVE));
      iBoardList.add(new SavedBoard(board, kw));

      // Number 5 06.19.21 No 2
      board = new String[SIZE];
      board[0] = "2   2  1 ";
      board[1] = " 2 3  112";
      board[2] = "12  221 1";
      board[3] = "222  21 1";
      board[4] = "   0 31 2";
      board[5] = " 30  2  2";
      board[6] = "2    1  2";
      board[7] = "  3    32";
      board[8] = " 2 22 12 ";
      kw = new ArrayList<Wall>();
      iBoardList.add(new SavedBoard(board, kw));

      // Number 6 06.18.12 no 1
      board = new String[SIZE];
      board[0] = "0 0 01 12";
      board[1] = "  3  02  ";
      board[2] = " 21   21 ";
      board[3] = " 2 30 21 ";
      board[4] = " 3131 112";
      board[5] = "  122 23 ";
      board[6] = " 11    01";
      board[7] = "  11  0  ";
      board[8] = " 212 0   ";
      kw = new ArrayList<Wall>();
      kw.add(new Wall(1, 3, ABOVE));
      kw.add(new Wall(6, 3, ABOVE));
      kw.add(new Wall(0, 4, LEFT));
      kw.add(new Wall(8, 4, RIGHT));
      kw.add(new Wall(5, 5, LEFT));
      kw.add(new Wall(0, 6, LEFT));
      kw.add(new Wall(6, 6, LEFT));
      kw.add(new Wall(8, 6, ABOVE));
      kw.add(new Wall(1, 8, ABOVE));
      kw.add(new Wall(2, 8, BELOW));
      iBoardList.add(new SavedBoard(board, kw));

      // Number 7 06.18.12 no 2
      board = new String[SIZE];
      board[0] = "2111 11 2";
      board[1] = " 1 11 1 1";
      board[2] = "32 2  12 ";
      board[3] = " 2 2  1  ";
      board[4] = "  23     ";
      board[5] = "     22  ";
      board[6] = " 1112 221";
      board[7] = "  2   220";
      board[8] = "   3   10";
      kw = new ArrayList<>();
      iBoardList.add(new SavedBoard(board, kw));

      SavedBoard myBoard = iBoardList.get(useBoard - 1);
      solve(myBoard.getBoard(), myBoard.getKw());

   }

   private static void solve(String[] board, List<Wall> kw)
   {
      int LOOP_COUNT = 6000;
      checkBoard(board);
      WallBuilderBoard wbb = createPage(board, kw, "");
      List<TestMove> solutions = new ArrayList<>();

      boolean isDone = false;
      int count = 0;
      while (!isDone)
      {
         if (count < LOOP_COUNT && !isDone)
         {
            isDone = wbb.solve();
            if (isDone)
            {
               print("Done now!");
               // wbb.printLastLog();

               solutions.add(wbb.saveSolution());

               if (wbb.hasChoicesLeft())
               {
                  System.out.println("More to test!");
                  Wall nextMove = wbb.makeNextBlueMove(true);
                  isDone = (nextMove == null);
               }
               else
               {
                  showAllSolutions(board, kw, solutions, wbb);

                  return;
               }
            }
            count++;
            if (count % 100 == 0)
            {
               print(count + " " + wbb.getLeastCount());
            }
         }

         if (count == LOOP_COUNT)
         {
            wbb.printLastLog();
            wbb.showBestSoFar();
            count = 0;
            // while (true)
            // {
            // WallBuilderBoard.sleep(10000);
            // }
            print("One last solve()");
            isDone = wbb.solve();
            wbb.printLastLog();
            print("Reached " + LOOP_COUNT);
            // WallBuilderBoard.sleep(20000);
            // System.exit(1);
            isDone = true;
         }

         if (isDone)
         {
            showAllSolutions(board, kw, solutions, wbb);
         }
      }

   }

   public static void showAllSolutions(String[] board, List<Wall> kw, List<TestMove> solutions, WallBuilderBoard wbb)
   {
      Collection<Color[][][]> distincSolutions = testSolutions(solutions);
      if (distincSolutions.size() > 0)
      {
         wbb.setVisible(false);
         int i = 0;
         for (Color[][][] colors : distincSolutions)
         {
            i++;
            WallBuilderBoard wbb2 = createPage(board, kw, "Solution nr " + i);
            wbb2.showPage(colors);
            WallBuilderBoard.sleep(300);
            wbb2.setLocation(i);
         }
      }
   }

   public static WallBuilderBoard createPage(String[] board, List<Wall> kw, String title)
   {
      WallBuilderBoard wbb = new WallBuilderBoard(board, kw, "WallBuilder Grid");
      wbb.setTitle(title);
      SwingUtilities.invokeLater(() -> wbb.createAndShowGUI(wbb));
      return wbb;
   }

   public static Collection<Color[][][]> testSolutions(List<TestMove> solutions)
   {
      System.out.println("All totally tested! Found " + solutions.size() + " solutions.");
      Map<String, Color[][][]> solutionsSet = new HashMap<>();
      for (TestMove testMove : solutions)
      {
         String sign = getSignature(testMove.getSavedStateColors());
         System.out.println(sign);
         solutionsSet.put(sign, testMove.getSavedStateColors());
      }

      System.out.println("Number of unique solutions: " + solutionsSet.size());

      return solutionsSet.values();
   }

   private static String getSignature(Color[][][] colors)
   {
      String retVal = "";

      for (int i = 0; i < colors.length; i++)
      {
         for (int j = 0; j < colors[0].length; j++)
         {
            for (int k = 0; k < colors[0][0].length; k++)
            {
               Color color = colors[i][j][k];
               if (color.equals(WallBuilderBoard.OK_COLOR) || color.equals(WallBuilderBoard.TESTING_COLOR)
                     || color.equals(WallBuilderBoard.TEST_CHILD_COLOR)
                     || color.equals(WallBuilderBoard.PERMANENT_COLOR))
               {
                  retVal += "1";
               }
               else
               {
                  retVal += "0";
               }

            }
            retVal += "|";

         }
         retVal += "@";
      }

      return retVal;
   }

   private static void checkBoard(String[] board)
   {
      int boardSize = board.length;
      print("Board size " + boardSize);
      boolean errors = false;
      for (String line : board)
      {
         if (line.length() != boardSize)
         {
            print("Error in line \"" + line + "\" Not size " + boardSize + " (" + line.length() + ")");
            errors = true;
         }
      }

      if (errors)
      {
         System.exit(1);
      }
   }

   public static void print(String text)
   {
      try
      {
         if (fw == null)
         {
            fw = new FileWriter("output.log");
         }

         System.out.println(text);

         fw.write(text + "\n");
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

   }

}
