package common.utils;

import java.io.FileInputStream;
import java.io.IOException;

public class MP3HeaderParser
{
   // Sample rate lookup table for MPEG 1 Layer III
   private static final int[] MPEG1_L3_SAMPLE_RATES = { 44100, 48000, 32000, 0 };

   // Bitrate lookup table for MPEG 1 Layer III (in kbps)
   private static final int[] MPEG1_L3_BITRATES =
         { 0, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 0 };

   // Returns the number of samples per frame for a stereo MP3 file
   public static int getSamplesPerFrame(String mp3FilePath)
   {
      try (FileInputStream inputStream = new FileInputStream(mp3FilePath))
      {
         byte[] headerBytes = new byte[4];
         // Read the first 4 bytes of the MP3 file
         if (inputStream.read(headerBytes) != 4)
         {
            // Failed to read header
            return -1;
         }

         // Check if the header starts with the MPEG 1 Layer III sync marker
         if ((headerBytes[0] & 0xFF) == 0xFF && (headerBytes[1] & 0xE0) == 0xE0)
         {
            int versionIndex = (headerBytes[1] >>> 3) & 0x03;
            int bitrateIndex = (headerBytes[2] >>> 4) & 0x0F;
            int sampleRateIndex = (headerBytes[2] >>> 2) & 0x03;

            int bitrate = MPEG1_L3_BITRATES[bitrateIndex] * 1000; // Convert to
                                                                  // bps
            int sampleRate = MPEG1_L3_SAMPLE_RATES[sampleRateIndex];
            int samplesPerFrame = 1152; // Default samples per frame for Layer
                                        // III

            if (bitrate > 0 && sampleRate > 0)
            {
               // Calculate samples per frame
               samplesPerFrame = (versionIndex == 3) ? 576 : 1152;
            }

            return samplesPerFrame;
         }
         else
         {
            // Not a valid MP3 file
            return -1;
         }
      }
      catch (IOException e)
      {
         e.printStackTrace();
         return -1;
      }
   }

   public static void main(String[] args)
   {
      String mp3FilePath = "input.mp3"; // Specify the path to the MP3 file
      int samplesPerFrame = getSamplesPerFrame(mp3FilePath);
      if (samplesPerFrame > 0)
      {
         System.out.println("Samples per frame: " + samplesPerFrame);
      }
      else
      {
         System.out.println("Unable to determine samples per frame.");
      }
   }
}
