package common.utils;

public class SudokuHelp {
   public static void main(String[] args) {
       // Example initialization of the Sudoku board
       // (0 represents an empty cell)
       int[][] initialBoard = {
           {2,0,4,0,6,0,0,0,0},
           {0,9,0,4,1,0,0,0,0},
           {0,6,0,0,0,0,0,0,0},
           {0,1,0,0,0,0,4,8,0},
           {0,5,0,0,0,0,2,6,0},
           {4,0,2,0,8,0,0,0,3},
           {3,8,0,0,0,0,6,0,0},
           {0,4,0,1,0,0,0,0,5},
           {0,2,0,0,0,0,9,3,1}
       };

       solve(initialBoard);
   }

   public static void solve(int[][] initialBoard)
   {
      // Define the size of the Sudoku board
      int SIZE = 9;

       // Create a 2D array to store the Sudoku board
       int[][] board = new int[SIZE][SIZE];

       // Copy the initial board to the main board
       for (int i = 0; i < SIZE; i++) {
           for (int j = 0; j < SIZE; j++) {
               board[i][j] = initialBoard[i][j];
           }
       }

       // Print the board to verify
       printBoard(board);
       SudokuGrid.showGrid(board);
   }

   // Method to print the Sudoku board
   public static void printBoard(int[][] board) {
       for (int row = 0; row < board.length; row++) {
           for (int col = 0; col < board[row].length; col++) {
              int num = board[row][col];
              if (num == 0)
            {
                 System.out.print("  ");
            }
            else
            {
               System.out.print(num + " ");
            }
               if (col % 3 == 2 && col < 8)
               {
                  System.out.print("| ");
               }
           }
           if (row % 3 == 2)
           {
              System.out.print("\n------+-------+------");
           }

           System.out.println();
       }
   }
}

