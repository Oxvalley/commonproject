package common.utils;


/**
 * The Class PackInstallationUtilCommon.
 */
public class PackInstallationUtilCommon extends PackInstallationUtil
{


   /**
    * Instantiates a new pack installation util common.
    *
    * @param appName the app name
    * @param version the version
    * @param jdkVersion the jdk version
    * @param appNameVersion the app name version
    * @param mainClassNAme the main class N ame
    * @param packSource the pack source
    */
   public PackInstallationUtilCommon(String appName, String version,
         String jdkVersion, String appNameVersion, String mainClassNAme,
         boolean packSource, boolean createJavadoc)
   {
      super(appName, version, jdkVersion, appNameVersion, mainClassNAme,
            packSource, createJavadoc);
   }

   /**
    * The main method.
    *
    * @param args the arguments
    */
   public static void main(String[] args)
   {
      boolean packSource = false;
      boolean createJavadoc = false;
      if (args.length == 1)
      {
         if (args[0].equalsIgnoreCase("src"))
         {
            packSource = true;
         }

         if (args[0].equalsIgnoreCase("javadoc"))
         {
            createJavadoc = true;
         }
      }
      if (args.length == 2)
      {
         if (args[1].equalsIgnoreCase("src"))
         {
            packSource = true;
         }

         if (args[1].equalsIgnoreCase("javadoc"))
         {
            createJavadoc = true;
         }
      }
      
      AppInfoInterface appInfo = AppInfo.getInstance();
      String name = appInfo.getApplicationName();
      String version = appInfo.getVersion();
      String jdkVersion = appInfo.getJdkVersion();
      String appNameVersion = "Oxvalley_Common_v_" + getVersionNoDots(version);
      String mainClass = "common.utils.AboutBox";
      PackInstallationUtil pi = new PackInstallationUtilCommon(name, version,
            jdkVersion, appNameVersion, mainClass, packSource, createJavadoc);
      pi.execute();
   }

}
