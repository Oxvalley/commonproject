package common.utils;

public class SudokuHelp2 extends SudokuHelp
{
   public static void main(String[] args) {
      // Example initialization of the Sudoku board
      // (0 represents an empty cell)
      int[][] initialBoard = {
         {0,5,0,8,7,0,1,0,3},
         {1,8,7,6,0,0,0,0,2},
         {0,0,3,0,9,0,0,6,0},
         {2,0,0,7,0,1,3,0,6},
         {0,7,0,3,0,0,2,0,0},
         {0,3,6,0,0,8,0,0,1},
         {0,0,0,0,8,0,6,1,0},
         {0,6,2,0,1,0,0,0,0},
         {9,0,8,0,3,0,7,0,0}
      };

      solve(initialBoard);
  }

   

}
