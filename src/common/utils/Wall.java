package common.utils;

public class Wall
{

   private int iX;
   private int iY;
   private int iDirection;

   public Wall(int x, int y, int direction)
   {
      iX = x;
      iY = y;
      iDirection = direction;
   }

   @Override
   public String toString()
   {
     return "X="+iX+", Y="+iY+", Direction="+iDirection;
   }

   public int getX()
   {
      return iX;
   }

   public int getY()
   {
      return iY;
   }

   public int getDirection()
   {
      return iDirection;
   }

}
