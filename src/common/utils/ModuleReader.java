package common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class ModuleReader
{

   private String iName;
   private Map<String, String> iMainVariables;
   private List<String> iLinesList = new ArrayList<>();
   private Stack<String> iStack = new Stack<>();
   private List<String> iOutputLines = new ArrayList<>();
   private List<String> iRemoveLines = new ArrayList<>();
   // private String iLines = "";
   boolean isRemoveNow = false;

   private int iFloor = 0;

   public ModuleReader(Map<String, String> mainVariables, String name)
   {
      iMainVariables = mainVariables;
      iName = name;
      createLines("module", name);
      iStack.push("module");
   }

   public String getName()
   {
      return iName;
   }

   public void addLine(String line)
   {
      iLinesList.add(line);

   }

   public void pushOperator(String operator)
   {
      iStack.push(operator);
   }

   public String pop(String operator)
   {
      String pop = iStack.pop();
      String retVal = null;
      if (!pop.equals(operator))
      {
         retVal = "Bad end. Found end " + operator + ". Expected end " + pop;
      }

      return retVal;
   }

   @Override
   public String toString()
   {
      while (iStack.size() > 0)
      {
         String pop = iStack.pop();
         createLines("end", "end " + pop + "// Extra");

      }

      if (iRemoveLines.size() > 0)
      {
         iOutputLines.add(2, "difference()");
         iOutputLines.add(3, "{");
         iOutputLines.add(4, "union()");
         iOutputLines.add(5, "{");
         iOutputLines.add("} // end union");
         iOutputLines.add("");
         iOutputLines.add("// Remove items below");
         for (String line : iRemoveLines)
         {
            iOutputLines.add(line);
         }
         iOutputLines.add("} // end difference");
      }

      String retVal = "";
      int curlCount = 0;

      for (String line : iOutputLines)
      {

         curlCount -= StringUtils.countMatches(line, "}");
         int curlCopy = curlCount;
         if (curlCopy < 0)
         {
            curlCopy = 0;
         }

         retVal +=
               "                                                            "
                     .substring(0, 2 * curlCopy) + line + "\n";

         curlCount += StringUtils.countMatches(line, "{");
         // System.out.println(curlCount + " " + line);

      }

      return retVal;
   }

   public void createLines(String operator, String line)
   {
      String[] params;
      String paramLine;

      switch (operator)
      {
         case "module":
            addCodeLine("module " + iName + "()");
            addCodeLine("{");
            break;

         case "empty":
            addCodeLine(line);
            break;

         case "inCode":
            addCodeLine(line);
            break;

         case "code":
            addCodeLine("// Inline code start");
            break;

         case "code_end":
            addCodeLine("// Inline code end");
            break;

         case "end":
            String comment = line;
            if (line.equals("end pos"))
            {
               comment = "end translate";
            }

            if (!operator.equals("remove"))
            {
               addCodeLine("} // " + comment);
            }

            if (line.equals("end remove"))
            {
               isRemoveNow = false;
            }

            break;

         case "rem":
            addCodeLine("// " + line.substring(operator.length() + 1));
            break;

         case "stl":
            params = getParameters(operator, line);
            paramLine = String.join(",", params);
            addCodeLine("add_stl_scaled(" + paramLine.trim() + ");");
            break;

         case "remove":
            addRemoveCodeLine("// TODO Special for " + line);
            isRemoveNow = true;
            break;

         case "room":
            iOutputLines
                  .add("room(" + line.substring(operator.length() + 1) + ");");
            break;

         case "floor":
            params = getParameters(operator, line);
            String floorString = params[0].trim();

            if (!NumberUtils.isCreatable(floorString))
            {
               HouseReader.printError(this,
                     "Not a number for operator floor: " + floorString);
            }
            else
            {
               iFloor = Integer.valueOf(floorString);
            }
            break;

         case "cube":
         case "cylinder":

            params = getParameters(operator, line);

            createCode(operator, params, true, false);
            break;

         case "rotate":

            params = getParameters(operator, line);

            createCode(operator, params, false, true);
            break;

         case "pos":
         case "translate":

            params = getParameters(operator, line);

            if (params.length < 3)
            {
               params = increaseArray(params, 3);
               params[2] = iMainVariables.get("floor.height");
            }

            createCode("translate", params, false, true);
            break;

         case "window":

            params = getParameters(operator, line);

            params[0] = "\"" + getWordDirection(params[0]) + "\"";

            int paramsLength = params.length;

            if (paramsLength < 5)
            {
               params = increaseArray(params, 5);
               params[4] = iMainVariables.get("window.height");
            }

            if (paramsLength < 4)
            {
               params[3] = iMainVariables.get("window.width");
            }

            if (paramsLength < 3)
            {
               params[2] = iMainVariables.get("window.above.floor");
            }

            paramLine = String.join(",", params);
            addCodeLine(operator + "(" + paramLine.trim() + ");");
            break;

         case "door":

            params = getParameters(operator, line);

            params[0] = "\"" + getWordDirection(params[0]) + "\"";

            params[2] = "\"" + getWordInOut(params[2]) + "\"";

            paramsLength = params.length;

            if (params.length < 7)
            {
               params = increaseArray(params, 7);
               params[6] = iMainVariables.get("door.angle");
            }

            if (paramsLength < 6)
            {
               params[5] = iMainVariables.get("door.height");
            }

            if (paramsLength < 5)
            {
               params[4] = iMainVariables.get("door.width");
            }

            if (paramsLength < 4)
            {
               params[3] = "NO";
            }

            if (params[3].equals("L"))
            {
               params[3] = "\"Left\"";
            }

            if (params[3].equals("R"))
            {
               params[3] = "\"Right\"";
            }

            if (params[3].equals("N"))
            {
               params[3] = "\"NoDoor\"";
            }

            String saveIn = params[3].trim();
            params[3] = "RemoveMe";

            paramLine = String.join(",", params);
            paramLine = paramLine.replace(",RemoveMe", "");
            addCodeLine(operator + "(" + paramLine + ");");

            if (saveIn.equals("IN"))
            {
               addCodeLine("// TODO add door in\n");
            }

            if (saveIn.equals("OUT"))
            {
               addCodeLine("// TODO add door out");
            }
            break;

         default:
            HouseReader.printError(this, "No action for operator " + operator);
            break;
      }

   }

   private static String getWordDirection(String abbr)
   {
      switch (abbr.trim())
      {
         case "N":
            return "North";

         case "S":
            return "South";

         case "W":
            return "West";

         case "E":
            return "East";
      }

      return abbr;
   }

   private static String getWordInOut(String abbr)
   {
      switch (abbr.trim())
      {
         case "L":
            return "Left";

         case "R":
            return "Right";

         case "N":
            return "NoDoor";
      }

      return abbr;
   }

   public void createCode(String operator, String[] params, boolean semi,
         boolean startBracket)
   {
      String paramLine;
      paramLine = String.join(",", params);

      String semiSign = "";
      if (semi)
      {
         semiSign = ";";
      }

      addCodeLine(operator + "([" + paramLine.trim() + "])" + semiSign);

      if (startBracket)
      {
         addCodeLine("{");
      }
   }

   private void addCodeLine(String line)
   {
      if (line.contains("pos3"))
      {
         // System.out.println("Nu!!");
      }

      if (isRemoveNow)
      {
         iRemoveLines.add(line);
      }
      else
      {
         iOutputLines.add(line);
      }

   }

   private void addRemoveCodeLine(String line)
   {
      iRemoveLines.add(line);
   }

   public String[] increaseArray(String[] original, int newSize)
   {
      String[] temp = new String[newSize];

      for (int i = 0; i < original.length; i++)
      {
         temp[i] = original[i];
      }

      return temp;
   }

   private static String[] getParameters(String operator, String line)
   {
      String paramLine = line.substring(operator.length() + 1);
      String[] arr = paramLine.split(",");
      for (int i = 0; i < arr.length; i++)
      {
         arr[i] = " " + arr[i].trim();
      }

      return arr;
   }

}
