package common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * The Class ZipUtil.
 */
public class ZipUtil
{

    /**
     * Zip.
     *
     * @param folderName the folder name
     * @param zipFileName the zip file name
     */
    public static void zip(String folderName, String zipFileName)
    {
        try
        {
            // create a ZipOutputStream to zip the data to
            ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFileName + ".zip"));
            // assuming that there is a directory named inFolder (If there
            // isn't create one) in the same directory as the one the code
            // runs from,
            // call the zipDir method
            zipDir(folderName, zos);
            // close the stream
            zos.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Zip dir.
     *
     * @param dir2zip the dir 2 zip
     * @param zos the zos
     */
    // here is the code for the method
    public static void zipDir(String dir2zip, ZipOutputStream zos)
    {
        try
        {
            // create a new File object based on the directory we
            // have to zip
            File zipDir = new File(dir2zip);
            // get a listing of the directory content
            String[] dirList = zipDir.list();
            if (dirList == null)
            {
                throw new RuntimeException(dir2zip + " is not a directory");
            }

            byte[] readBuffer = new byte[2156];
            int bytesIn = 0;
            // loop through dirList, and zip the files
            if (dirList.length == 0)
            {
                // create a new zip entry for empty directory
                if (!dir2zip.endsWith("/"))
                {
                    dir2zip += "/";
                }
                ZipEntry anEntry = new ZipEntry(dir2zip);
                // place the zip entry in the ZipOutputStream object
                zos.putNextEntry(anEntry);
            }
            else
            {
                for (int i = 0; i < dirList.length; i++)
                {
                    File f = new File(zipDir, dirList[i]);
                    if (f.isDirectory())
                    {
                        // if the File object is a directory, call this
                        // function again to add its content recursively
                        String filePath = f.getPath();
                        zipDir(filePath, zos);
                        // loop again
                        continue;
                    }
                    // if we reached here, the File object f was not
                    // a directory
                    // create a FileInputStream on top of f
                    FileInputStream fis = new FileInputStream(f);
                    // create a new zip entry
                    ZipEntry anEntry = new ZipEntry(f.getPath());
                    // place the zip entry in the ZipOutputStream object
                    zos.putNextEntry(anEntry);
                    // now write the content of the file to the ZipOutputStream
                    while ((bytesIn = fis.read(readBuffer)) != -1)
                    {
                        zos.write(readBuffer, 0, bytesIn);
                    }
                    // close the Stream
                    fis.close();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}
