package common.utils;

import java.util.ArrayList;
import java.util.List;

public class TreeParent
{

   @Override
   public String toString()
   {
      return iName + " (" + iImage.size() + ", " + iStl.size() + ", " + iKeyValue.size() + ", " + iChildren.size()
            + ")";
   }

   private String iName;
   private int iIntroSpaces;
   private TreeParent iParent;
   private List<TreeParent> iChildren = new ArrayList<>();
   private List<TreeKeyValue> iKeyValue = new ArrayList<>();
   private List<TreeKeyValue> iStl = new ArrayList<>();
   private List<TreeKeyValue> iImage = new ArrayList<>();
   private String iType;

   public TreeParent(String name, int introSpaces, TreeParent parent)
   {
      iName = name;
      iIntroSpaces = introSpaces;
      iParent = parent;
   }

   public void addChild(TreeParent child)
   {
      iChildren.add(child);
      child.setParent(this);
   }

   private void setParent(TreeParent parent)
   {
      iParent = parent;
   }

   public void addProperty(String text)
   {
      TreeKeyValue kv = new TreeKeyValue(text);

      if (kv.isStl())
      {
         iStl.add(kv);
      }
      else if (kv.isImage())
      {
         iImage.add(kv);
      }
      else if (kv.isType())
      {
         iType = kv.getValue();
      }
      else
      {
         iKeyValue.add(kv);
      }
   }

   public TreeParent getParent()
   {
      return iParent;
   }

   public int getIntroSpaces()
   {
      return iIntroSpaces;
   }

   public String getName()
   {
      return iName;
   }

   public List<TreeParent> getChildren()
   {
      return iChildren;
   }

   public List<TreeKeyValue> getKeyValues()
   {
      return iKeyValue;
   }

   public List<TreeKeyValue> getStl()
   {
      return iStl;
   }

   public List<TreeKeyValue> getImage()
   {
      return iImage;
   }

   public String getType()
   {
      return iType;
   }

}
