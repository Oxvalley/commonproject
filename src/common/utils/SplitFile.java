package common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import files.lists.FileLister;

public class SplitFile
{
   private SplitFile()
   {
   }

   public static void main(String[] args)
   {
//      copySplitFiles("W:\\music\\MP3\\Kassettbanden", "f:");
      copySplitFiles("Z:\\music\\MP3\\Kassettbanden\\test\\all", "F:");
   }

   private static void copySplitFiles(String fromDir, String toDir)
   {
      List<String> tags = Arrays.asList(new String[] { "mp3", "wma", "ogg" });
      List<String> files = FileLister.getAllFiles(fromDir, tags);
      Collections.sort(files);
      for (String filePath : files)
      {
         splitFile(filePath, toDir);
//         try
//         {
//            Thread.sleep(10000);
//         }
//         catch (InterruptedException e)
//         {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//         }         
      }
   }

   public static final int SPLIT_SIZE_SECONDS = 240; // 4 minutes
   public static final int SPLIT_SIZE_BYTES = 3840000; // About 4 minutes

   public static void splitFile(String filePath)
   {
      splitFile(filePath, null);
   }

   public static void splitFile(String filePath, String toDir)
   {
      System.out.println(filePath);
      File file = new File(filePath);
      String parentPath = file.getParent();
      if (toDir == null)
      {
         toDir = parentPath;
      }
      String fileName = file.getName();
      FileInputStream fis = null;
      FileOutputStream fos = null;
      long filesize = file.length();
      int seconds = MP3MetaData.getDuration(filePath);
      System.out.println("Seconds: " + seconds);
      long splitval;
      if (seconds > -1)
      {
         splitval = Math.round((seconds * 1.0 / SPLIT_SIZE_SECONDS));
      }
      else
      {
         splitval = Math.round((filesize * 1.0 / SPLIT_SIZE_BYTES));
      }

      System.out.println("splitval: " + splitval);
      if (splitval > 1)
      {
         System.out.println("File size: " + filesize);
         int splitsize =
               (int) (filesize / splitval) + (int) (filesize % splitval);
         System.out.println("Split size: " + splitsize);
         byte[] b = new byte[splitsize];
         try
         {
            fis = new FileInputStream(file);
            String tag = FileHelper.getFileTag(file.getName());
            String fileNamePart;
            if (tag.length() > 0)
            {
               tag = "." + tag;
               fileNamePart =
                     fileName.substring(0, fileName.length() - tag.length());
            }
            else
            {
               fileNamePart = fileName;
            }

            for (int j = 1; j <= splitval; j++)
            {
               String splitFileName =
                     toDir + "/" + fileNamePart + "_split_" + j + tag;
               fos = new FileOutputStream(splitFileName);
               int i = fis.read(b);
               fos.write(b, 0, i);
               System.out.println("Writing " + splitFileName);
               fos.close();
               fos = null;
            }
         }
         catch (FileNotFoundException e)
         {
            e.printStackTrace();
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
         finally
         {
            try
            {
               if (fis != null)
               {
                  fis.close();
               }
               if (fos != null)
               {
                  fos.close();
               }
            }
            catch (IOException e)
            {
               e.printStackTrace();
            }
         }
      }
   }

   private static void mergeFileParts(String filename1, int splitval)
      throws IOException
   {
      FileInputStream fis = null;
      FileOutputStream fos = null;
      try
      {
         String name1 = filename1.replaceAll(".mp3", "");
         String mergeFile = name1 + "_merge.mp3";
         fos = new FileOutputStream(mergeFile);
         for (int j = 1; j <= splitval; j++)
         {
            String filecalled = name1 + "_split_" + j + ".mp3";
            File partFile = new File(filecalled);
            fis = new FileInputStream(partFile);
            int partFilesize = (int) partFile.length();
            byte[] b = new byte[partFilesize];
            int i = fis.read(b, 0, partFilesize);
            fos.write(b, 0, i);
            fis.close();
            fis = null;
         }
      }
      finally
      {
         if (fis != null)
         {
            fis.close();
         }
         if (fos != null)
         {
            fos.close();
         }
      }
   }

   private void check(String expectedPath, String actualPath)
      throws IOException, NoSuchAlgorithmException
   {
      System.out.println("check...");

      FileInputStream fis = null;

      try
      {

         File expectedFile = new File(expectedPath);
         long expectedSize = expectedFile.length();

         File actualFile = new File(actualPath);
         long actualSize = actualFile.length();

         System.out.println("exp=" + expectedSize);
         System.out.println("act=" + actualSize);

         // Assert.assertEquals(expectedSize, actualSize);

         fis = new FileInputStream(expectedFile);
         String expected = makeMessageDigest(fis);
         fis.close();
         fis = null;

         fis = new FileInputStream(actualFile);
         String actual = makeMessageDigest(fis);
         fis.close();
         fis = null;

         System.out.println("exp=" + expected);
         System.out.println("act=" + actual);

         // Assert.assertEquals(expected, actual);

      }
      finally
      {
         if (fis != null)
         {
            fis.close();
         }
      }
   }

   public String makeMessageDigest(InputStream is)
      throws NoSuchAlgorithmException, IOException
   {
      byte[] data = new byte[1024];
      MessageDigest md = MessageDigest.getInstance("SHA1");
      int bytesRead = 0;

      while (-1 != (bytesRead = is.read(data, 0, 1024)))
      {
         md.update(data, 0, bytesRead);
      }

      return toHexString(md.digest());
   }

   private String toHexString(byte[] digest)
   {
      StringBuffer sha1HexString = new StringBuffer();
      for (int i = 0; i < digest.length; i++)
      {
         sha1HexString.append(String.format("%1$02x", Byte.valueOf(digest[i])));
      }

      return sha1HexString.toString();
   }
}