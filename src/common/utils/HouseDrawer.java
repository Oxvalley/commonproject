package common.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang3.StringUtils;

public class HouseDrawer
{

   public static void main(String[] args)
   {
     readFile("huset.scad");

   }
   
   public static void readFile(String filename)
   {
      Map<String, String> mainVariables = new HashMap<String, String>();
      List<ModuleContainer> moduleList = new ArrayList<ModuleContainer>();
      
      
      BufferedReader reader = null;
      int inModuleCount=0;
      int moduleIndex =0;
      ModuleContainer moduleContainerNow = null;
      
      try
      {
         reader = new BufferedReader(new FileReader(filename));
         String line = reader.readLine();

         while (line != null)
         {
            // Read next line for while condition
            line = reader.readLine();
            if (line != null)
            {


               
               if (line.contains("=") & line.trim().endsWith(";"))
               {
                  String key=line.substring(0,line.indexOf("=")).trim();
                  String value = line.substring(line.indexOf("=")+1,line.indexOf(";")).trim();
                  value = ModuleContainer.replaceUnScale(value);
                  value = ModuleContainer.replaceScale(value);

                  
                  
                  
                  if (inModuleCount == 0)
                  {
                     mainVariables.put(key,value);
                  }
                  else
                  {
                     moduleContainerNow.put(key,value);
                  }
               }
               

               
               if (line.toLowerCase().trim().startsWith("module "))
               {
                  
                  String function = StringUtils.substringBetween(line, "module ", "(");
                  moduleContainerNow = new ModuleContainer(function, mainVariables);
                  moduleList.add(moduleContainerNow);
                  inModuleCount=1;                  
                  moduleContainerNow.addLine(line);
               }
               else
               {
                  if (inModuleCount == 0)
                  {
                     System.out.println(line);
                  }
                  else
                  {
                     inModuleCount++;
                     moduleContainerNow.addLine(line);
                  }                  
               }               
            }
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }
      
      for (ModuleContainer module : moduleList)
      {
         System.out.println(module);
      }
      
   }


}
