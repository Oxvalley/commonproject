package common.utils.tove.forms;

import java.util.List;

public class PointsArrayElement
{

   @Override
   public String toString()
   {
      String text = ">>> " + iPointArrayName;
      if (iUsed)
      {
         text += " Used=Keep";
      }
      else
      {
         text += " Unused=Remove";
      }

      if (iUsedNames == null)
      {
         text += "\n(Not called)\n\n";
      }
      else
      {
         text += "\n" + iUsedNames+"\n\n";
      }
      

      return text;
   }

   private String iPointArrayName;
   private List<String> iModulesList;
   private String iUsedNames;
   private boolean iUsed = false;

   public PointsArrayElement(String pointArrayName)
   {
      iPointArrayName = pointArrayName;
   }

   public void setModulesList(List<String> modulesList)
   {
      iModulesList = modulesList;
   }

   public void addUser(String usedName)
   {
      if (iUsedNames == null)
      {
         iUsedNames = usedName;
      }
      else
      {
         iUsedNames += ", " + usedName;
      }

      if (iUsed == false && !usedName.contains("*") && !usedName.contains("//"))
      {
         iUsed = true;
      }

   }

   public boolean isPartOfModule(String moduleName)
   {
      for (String myModuleName : iModulesList)
      {
         if (moduleName.contains(myModuleName))
         {
            return true;
         }
      }
      return false;
   }

}
