package common.utils.tove.forms;

public class Cordi
{

   private int iX;
   private int iY;
   private String iName;

   public Cordi(int x, int y, String name)
   {
      iX = x;
      iY = y;
      iName = name;
   }

   @Override
   public String toString()
   {
      return iX + ", " + iY + " // " + iName;
   }

   public int getX()
   {
      return iX;
   }

   public int getY()
   {
      return iY;
   }

   public String getName()
   {
      return iName;
   }

}
