package common.utils.tove.forms;

public class PointsConverter
{

   private int iLowestX;
   private int iLowestY;
   private double iWidth;

   public PointsConverter(int lowestX, int lowestY, double width)
   {
      iLowestX = lowestX;
      iLowestY = lowestY;
      iWidth = width;

   }

   public double getX(Cordi cordi)
   {
      double pointX = getScadValue(cordi.getX(), iLowestX, iWidth);
      return pointX;
   }

   public double getY(Cordi cordi)
   {
      double pointY = Math.round((100.0 - getScadValue(cordi.getY(), iLowestY, iWidth)) * 100) / 100.0;
      return pointY;
   }

   private static double getScadValue(int value, int lowest, double width)
   {
      double retVal = Math.round(((value - lowest) / width) * 10000.0) / 100.0;
      return retVal;
   }

   public String getCordString(Cordi cord)
   {
      return getX(cord) + ", " + getY(cord) + " // " + cord.getName();
   }

}
