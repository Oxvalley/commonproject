package common.utils.tove.forms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import common.utils.RunPrograms;
import common.utils.open.scad.AppInfoToveForm;

class ImageWindow extends JFrame
{
   private static final int PIXEL_MOVE_Y = 32;
   private static final int PIXEL_MOVE_X = 8;

   @Override
   public void paint(Graphics g)
   {
      super.paint(g);

      int squareWidth = 5;

      g.setColor(Color.RED);

      Cordi oldCordi = null;
      int count = 0;
      for (Cordi cordi : iCordis)
      {
         count++;
         if (oldCordi != null)
         {
            g.drawLine((int) (oldCordi.getX() * iZoomFactor) + PIXEL_MOVE_X,
                  (int) (oldCordi.getY() * iZoomFactor) + PIXEL_MOVE_Y,
                  (int) (cordi.getX() * iZoomFactor) + PIXEL_MOVE_X, (int) (cordi.getY() * iZoomFactor) + PIXEL_MOVE_Y);
         }
         oldCordi = cordi;

         if (count == iCordis.size())
         {
            g.setColor(Color.GREEN);
         }

         g.fillRect((int) (cordi.getX() * iZoomFactor) + PIXEL_MOVE_X - (squareWidth / 2),
               (int) (cordi.getY() * iZoomFactor) + PIXEL_MOVE_Y - (squareWidth / 2), squareWidth, squareWidth);
      }

   }

   /**
    * 
    */
   private static final long serialVersionUID = 1L;
   private String iFileName;
   private int iX;
   private int iY;
   private List<Cordi> iCordis = new ArrayList<>();
   private Stack<Cordi> iUndoStack = new Stack<>();
   private String iName;
   private String iArea;
   private String iPath;
   private JLabel iImageLabel;
   private Image iOriginlaImage;

   private double iZoomFactor = 1;
   private int iOriginalSizeX;
   private int iOriginalSizeY;
   private int iScrolledX = 0;
   private int iScrolledY = 0;
   private JScrollPane scrollPane;
   private long closest;

   public ImageWindow(String fileName, String name, String area,  int zoomx, int zoomy)
   {
      iFileName = fileName;
      setIconImage(getToolkit().getImage("icontove.jpg"));
      iPath = new File(fileName).getParent();
      File file = new File(fileName);
      iName = name;
      iArea = area;
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setTitle();
      iOriginalSizeX = zoomx;
      iOriginalSizeY = zoomy;

      String txtFileName = iPath + "/" + name + "-" + area + ".txt";
      File file2 = new File(txtFileName);
      if (file2.exists())
      {
         readFile(txtFileName);
         repaint();
      }

      ImageIcon image = new ImageIcon(fileName);

      iOriginlaImage = image.getImage(); // transform it
      Image newimg = iOriginlaImage.getScaledInstance(zoomx, zoomy, java.awt.Image.SCALE_SMOOTH);

      image = new ImageIcon(newimg); // transform it back

      iImageLabel = new JLabel(image);
      scrollPane = new JScrollPane(iImageLabel);
      scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

      scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener()
      {
         @Override
         public void adjustmentValueChanged(AdjustmentEvent e)
         {
            iScrolledY = e.getValue();
         }
      });

      scrollPane.getHorizontalScrollBar().addAdjustmentListener(new AdjustmentListener()
      {
         @Override
         public void adjustmentValueChanged(AdjustmentEvent e)
         {
            iScrolledX = e.getValue();
         }
      });

      iImageLabel.addMouseListener(new MouseListener()
      {

         @Override
         public void mouseClicked(MouseEvent e)
         {
            iX = (int) (e.getX() / iZoomFactor) - iScrolledX;
            iY = (int) (e.getY() / iZoomFactor) - iScrolledY;
            setTitle();
            addCord(iX, iY);
         }

         @Override
         public void mousePressed(MouseEvent e)
         {
            // TODO Auto-generated method stub

         }

         @Override
         public void mouseReleased(MouseEvent e)
         {
            // TODO Auto-generated method stub

         }

         @Override
         public void mouseEntered(MouseEvent e)
         {
         }

         @Override
         public void mouseExited(MouseEvent e)
         {
            // TODO Auto-generated method stub

         }
      });

      iImageLabel.addMouseMotionListener(new MouseMotionListener()
      {

         @Override
         public void mouseDragged(MouseEvent e)
         {
         }

         @Override
         public void mouseMoved(MouseEvent e)
         {
            Cordi cord = getClosestCord(e.getPoint().x + PIXEL_MOVE_X, e.getPoint().y + PIXEL_MOVE_Y);
            if (cord == null || closest > 15)
            {
               iImageLabel.setToolTipText(null);
            }
            else
            {
               PointsConverter pc = getPointsConverter();
               iImageLabel.setToolTipText(pc.getCordString(cord));
            }
         }
      });

      addKeyListener(new KeyListener()
      {

         @Override
         public void keyTyped(KeyEvent e)
         {
            if (e.getKeyChar() == 's')
            {
               System.out.println("Save");
               saveCordis();
               requestFocusInWindow();
            }

            if (e.getKeyChar() == 'g')
            {
               System.out.println("Generate Scad");
               String filePath = generateScad();
               RunPrograms.browseURL(filePath);
               requestFocusInWindow();
            }

            if (e.getKeyChar() == KeyEvent.VK_DELETE)
            {
               int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to remove all coordinates?",
                     "Confirm", JOptionPane.OK_CANCEL_OPTION);

               if (confirm == JOptionPane.OK_OPTION)
               {
                  iCordis.clear();
                  repaint();
                  requestFocusInWindow();
                  // TODO Remove saved file as well
               }
            }

            if (e.getKeyChar() == 'z')
            {
               if (iCordis.size() > 0)
               {
                  Cordi node = iCordis.remove(iCordis.size() - 1);
                  iUndoStack.push(node);
                  repaint();
                  requestFocusInWindow();
                  // TODO redo after delete all?
               }
            }

            if (e.getKeyChar() == 'y')
            {
               if (iUndoStack.size() > 0)
               {
                  Cordi node = iUndoStack.pop();
                  iCordis.add(node);
                  repaint();
                  requestFocusInWindow();
               }
            }

            if (e.getKeyChar() == '+')
            {
               System.out.println("Zoom +");
               iZoomFactor += .1;
               changeImageZoom(iZoomFactor);
               repaint();
               requestFocusInWindow();
            }

            if (e.getKeyChar() == '-')
            {
               System.out.println("Zoom -");
               iZoomFactor -= .1;
               changeImageZoom(iZoomFactor);
               repaint();
               requestFocusInWindow();
            }

            if (e.getKeyChar() == 'h')
            {
               String text = "Usage:\n";
               text += "Click on coordinates in picture in order\n";
               text += "Hover over coordinate for info.\n";
               text += "First and last coordinate  connects automatically\n";
               text += "s - Save coordinates (to next run)(Not necessary for generating)\n";
               text += "g - Generate OpenScad code. File name and area will be part of .scad file name\n";
               text += "z - Undo\n";
               text += "y - Redo\n";
               text += "h - This help text\n";
               // TODO text += "n - New figure\n";
               // TODO text += "e - Edit coordinate colors\n";
               text += "+ - Zoom in picture (Experimental)\n"; // TODO Fix                                                               
               text += "- - Zoom out picture (Experimental)\n";
               text += "Delete - Delete all coordinates\n\n";
               text += AppInfoToveForm.getInstance().getApplicationName() + " v."
                     + AppInfoToveForm.getInstance().getVersion(); 
               System.out.println(text);
               JOptionPane.showMessageDialog(null, text);
            }

         }

         @Override
         public void keyReleased(KeyEvent e)
         {

         }

         @Override
         public void keyPressed(KeyEvent e)
         {

         }
      });

      add(scrollPane, BorderLayout.NORTH);
      pack();
      setVisible(true);
   }

   public void changeImageZoom(double zoom)
   {
      Image newimg = iOriginlaImage.getScaledInstance((int) (iOriginalSizeX * zoom), (int) (iOriginalSizeY * zoom),
            java.awt.Image.SCALE_SMOOTH);
      ImageIcon image = new ImageIcon(newimg); // transform it back
      iImageLabel.setIcon(image);

   }

   private String generateScad()
   {
      String fullPath = null;
      try
      {
         fullPath = iPath + "/" + iName + "-" + iArea + ".scad";
         FileWriter fw = new FileWriter(fullPath);
         System.out.println("Written to " + fullPath);
         PointsConverter pc = getPointsConverter();

         fw.write(iName + "_" + iArea + "_polygon = [\n");
         double pointYMin = Double.MAX_VALUE;

         for (Cordi cordi : iCordis)
         {
            double pointY = pc.getY(cordi);
            String line = pc.getX(cordi) + ", " + pointY + "],";
            line += "                                  ".substring(0, 15 - line.length());
            fw.write("[" + line + " // " + cordi.getName() + "\n");

            if (pointYMin > pointY)
            {
               pointYMin = pointY;
            }
         }
         fw.write("];\n\n");

         fw.write("translate([-6, 0, -" + (pointYMin * .1) + "])\n");
         fw.write("  " + iName + "_" + iArea + "(.1, 60);\n\n");
         fw.write("module " + iName + "_" + iArea + "(zoom=.1, depth=60)\n");
         fw.write("{\n");

         fw.write("  rotate([90,0,90])\n");
         fw.write("    scale([zoom, zoom, zoom])\n");
         fw.write("      linear_extrude(depth)\n");
         fw.write("        polygon(" + iName + "_" + iArea + "_polygon);\n");
         fw.write("}\n");
         fw.close();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }

      return fullPath;
   }

   public PointsConverter getPointsConverter()
   {
      int lowestX = getLowestX();
      int highestX = getHighestX();
      int lowestY = getLowestY();
      // int highestY = getHighestY();

      double width = (highestX - lowestX);
      PointsConverter aaa = new PointsConverter(lowestX, lowestY, width);
      return aaa;
   }

   private int getHighestY()
   {
      int highest = -1;

      for (Cordi cordi : iCordis)
      {
         if (cordi.getY() > highest)
         {
            highest = cordi.getY();
         }
      }

      return highest;
   }

   private int getLowestY()
   {
      int lowest = Integer.MAX_VALUE;

      for (Cordi cordi : iCordis)
      {
         if (cordi.getY() < lowest)
         {
            lowest = cordi.getY();
         }
      }

      return lowest;
   }

   private int getHighestX()
   {
      int highest = -1;

      for (Cordi cordi : iCordis)
      {
         if (cordi.getX() > highest)
         {
            highest = cordi.getX();
         }
      }

      return highest;
   }

   private int getLowestX()
   {
      int lowest = Integer.MAX_VALUE;

      for (Cordi cordi : iCordis)
      {
         if (cordi.getX() < lowest)
         {
            lowest = cordi.getX();
         }
      }

      return lowest;
   }

   private void setTitle()
   {
      setTitle(iFileName + " - " + iArea + " - Use h for help! "
            + AppInfoToveForm.getInstance().getApplicationName() + " v." + AppInfoToveForm.getInstance().getVersion());

   }

   public void addCord(int x, int y)
   {
      Cordi cord = new Cordi(x, y, "P" + (iCordis.size() + 1));
      iCordis.add(cord);
      System.out.println(cord);
      repaint();
   }

   private Cordi getClosestCord(int x, int y)
   {
      closest = Long.MAX_VALUE;
      Cordi closestCordi = null;

      for (Cordi cordi : iCordis)
      {
         // Pythagoras
         int xNow = (int) (x / iZoomFactor) - iScrolledX - PIXEL_MOVE_X;
         int yNow = (int) (y / iZoomFactor) - iScrolledY - PIXEL_MOVE_Y;
         int xDist = Math.abs(xNow - cordi.getX());
         int yDist = Math.abs(yNow - cordi.getY());
         long distance = (long) Math.sqrt(xDist * xDist + yDist * yDist);
         if (closest > distance)
         {
            closest = distance;
            closestCordi = cordi;
         }
      }

      return closestCordi;
   }

   private void saveCordis()
   {
      try
      {
         FileWriter fw = new FileWriter(iPath + "/" + iName + "-" + iArea + ".txt");

         for (Cordi cordi : iCordis)
         {
            fw.write(cordi.toString() + "\n");
         }

         fw.close();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }

   }

   public static void main(String[] args)
   {

      // ImageWindow a = new ImageWindow("oak_leaf.jpg", "oak_leaf", "dummy",
      // 33, 1000,3);
   }

   public void readFile(String fileName)
   {
      try
      {
         File file = new File(fileName);

         BufferedReader reader = new BufferedReader(new FileReader(file));

         String line = reader.readLine();

         while (line != null)
         {
            Cordi cordi = createCordi(line);
            if (cordi != null)
            {
               iCordis.add(cordi);
            }

            line = reader.readLine();
         }
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   private static Cordi createCordi(String line)
   {
      if (line == null || line.trim().isEmpty())
      {
         return null;
      }
      line = line.replace("//", ",");      
      String[] arr = line.split(",");
      return new Cordi(Integer.valueOf(arr[0].trim()), Integer.valueOf(arr[1].trim()), arr[2].trim());

   }

}