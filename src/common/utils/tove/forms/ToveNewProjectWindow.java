package common.utils.tove.forms;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import org.apache.commons.io.FileUtils;

import common.properties.SettingProperties;
import common.utils.open.scad.AppInfoToveForm;
import files.lists.ImageFileAndDirFilter;

public class ToveNewProjectWindow extends JFrame
{

   /**
    * 
    */
   private static final long serialVersionUID = 1L;

   private static ToveNewProjectWindow iThisWindow;

   // TODO Change to name of your icon
   private String ICON_NAME = null;

   private JTextField txtAreaName;
   private JLabel lbl1;
   private JLabel lbl2;
   private JButton btnBrowsePicture;
   private JTextField txtPicturePath;
   private JLabel lblImage;
   private JButton btnOK;
   private JButton btnCancel;

   private int width = 630;
   private int height = 300;

   private ToveIcon toveIcon;

   private JLabel lblZoom;

   private JTextField txtZoom;

   public static ToveNewProjectWindow getInstance()
   {
      if (iThisWindow == null)
      {
         // One and only instance
         iThisWindow = new ToveNewProjectWindow();
      }
      return iThisWindow;
   }

   public static void main(String[] args)
   {
      ToveNewProjectWindow ui = ToveNewProjectWindow.getInstance();
      ui.setVisible(true);

   }

   private ToveNewProjectWindow()
   {
      init();
   }

   public void init()
   {
      setIconImage(getToolkit().getImage("icontove.jpg"));
      setLayout(null);
      setSize(width, height);
      SettingProperties prop = SettingProperties.getInstance();

      setTitle(AppInfoToveForm.getInstance().getApplicationName() + " v." + AppInfoToveForm.getInstance().getVersion());
      if (ICON_NAME != null)
      {
         setIconImage(getToolkit().getImage(ICON_NAME));
      }

      lbl1 = new JLabel("Area name:");
      add(lbl1);

      txtAreaName = new JTextField(prop.getProperty("area", "outline"));
      add(txtAreaName);

      lbl2 = new JLabel("Picture path:");
      add(lbl2);

      txtPicturePath = new JTextField(prop.getProperty("path", "C:/"));
      add(txtPicturePath);

      btnBrowsePicture = new JButton("Browse");
      add(btnBrowsePicture);
      btnBrowsePicture.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnBrowsePictureClicked(e);
            showImage();
         }
      });

      lblZoom = new JLabel("Zoom:");
      add(lblZoom);

      txtZoom = new JTextField(prop.getProperty("zoom", "3"));
      add(txtZoom);

      toveIcon = new ToveIcon();

      lblImage = new JLabel(toveIcon);
      add(lblImage);

      btnOK = new JButton("OK");
      add(btnOK);
      btnOK.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnOKClicked(e);
         }
      });

      btnCancel = new JButton("Cancel");
      add(btnCancel);
      btnCancel.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnCancelClicked(e);
         }
      });

      setButtonsBounds();

      addComponentListener(new ComponentAdapter()
      {
         @Override
         public void componentResized(ComponentEvent evt)
         {
            setButtonsBounds();
         }
      });

      setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

   }

   public void showImage()
   {
      File imageFile = new File("txtPicturePath.getText()");
      if (imageFile.exists())
      {
         ImageIcon imgThisImg = new ImageIcon(txtPicturePath.getText());
         lblImage.setIcon(imgThisImg);
         lblImage.setIcon(new ImageIcon(getClass().getResource(txtPicturePath.getText())));

         try
         {
            ImageIcon pimage = new ImageIcon(ImageIO.read(imageFile));
            toveIcon.showImage(pimage);

            lblImage.setIcon(pimage);
            //
            filterIcon(lblImage, imageFile);
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
      }
   }

   private final void filterIcon(JLabel label, File file)
   {

      ImageIcon icon = (ImageIcon) label.getIcon();

      Image sourceImg = null;
      Image scaledImg = null;
      int hints = -1;

      sourceImg = icon.getImage();

      int compWidth = label.getSize().width;
      int compHeight = label.getSize().height;

      int w = compWidth;
      int h = compHeight;

      BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
      Graphics2D g2d = (Graphics2D) image.getGraphics();

      g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

      g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      g2d.drawImage(sourceImg, 0, 0, w, h, label);

      if (w < 800)
      {
         hints = Image.SCALE_SMOOTH;
         scaledImg = image.getScaledInstance(width / 2, height, hints);
         scaledImg.flush();
      }

      else if (w >= 800)
      {
         hints = Image.SCALE_FAST;
         scaledImg = image.getScaledInstance(width, height, hints);
         scaledImg.flush();
      }

      sourceImg.flush();
      label.setIcon(null);
      label.setIcon(new ImageIcon(scaledImg));
      g2d.dispose();

   }

   private void setButtonsBounds()
   {
      btnBrowsePicture.setBounds(getWidth() - 113, 13, 78, 35);
      btnCancel.setBounds(getWidth() - 118, getHeight() - 90, 83, 35);
      btnOK.setBounds(getWidth() - 229, getHeight() - 90, 78, 35);
      lbl1.setBounds(17, 86, 121, 28);
      lblImage.setBounds(17, 160, getWidth() - 50, getHeight() - 180);
      lbl2.setBounds(17, 13, 121, 28);
      txtPicturePath.setBounds(100, 13, getWidth() - 236, 35);
      txtAreaName.setBounds(100, 80, 243, 35);
      lblZoom.setBounds(17, 147, 121, 28);
      txtZoom.setBounds(100, 147, 243, 35);
   }

   private void btnBrowsePictureClicked(ActionEvent e)
   {
      JFileChooser fc = new JFileChooser();
      fc.setDialogTitle("Image path");
      fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fc.setSelectedFile(new File(txtPicturePath.getText()));

      // Add an image file filter
      fc.setFileFilter(new ImageFileAndDirFilter());

      int retval = fc.showOpenDialog(null);
      if (retval == JFileChooser.APPROVE_OPTION)
      {
         File myFile = fc.getSelectedFile();
         String path = safePath(myFile.getAbsolutePath());
         txtPicturePath.setText(path);
      }
   }

   public static String safePath(String absolutePath)
   {
      return absolutePath.replace("\\\\", "/").replace("\\", "/");
   }

   private void btnOKClicked(ActionEvent e)
   {
      File file = new File(txtPicturePath.getText());
      if (file.exists())
      {
         SettingProperties prop = SettingProperties.getInstance();

         prop.setProperty("path", txtPicturePath.getText());
         prop.setProperty("area", txtAreaName.getText());
         prop.setProperty("zoom", txtZoom.getText());
         prop.save();
         setVisible(false);

         ImageIcon pimage;

         try
         {
            pimage = new ImageIcon(ImageIO.read(file));
         }
         catch (Exception e1)
         {
            System.out.println("File " + file.getAbsolutePath() + " is not a picture file.");
            return;
         }

         double zoom = Double.parseDouble(txtZoom.getText());
         int pictWidth = (int) (pimage.getIconWidth() * zoom);
         int pictHeight = (int) (pimage.getIconHeight() * zoom);
         String fileName = txtPicturePath.getText();
         String Name = file.getName();

         int i = Name.lastIndexOf(".");
         if (i > -1)
         {
            Name = Name.substring(0, i);
         }

         ImageWindow a = new ImageWindow(fileName, Name.replace("-", "_").replace(".", "_"), txtAreaName.getText(),
               pictWidth, pictHeight);
      }
      else
      {
         System.out.println("File " + file.getAbsolutePath() + " does not exist");
      }
   }

   public void copyPicture(String folderPath)
   {
      File sourceDirectory = new File(txtPicturePath.getText());
      File destDirectory = new File(folderPath);

      // copy source to target using Files Class
      try
      {
         FileUtils.copyFileToDirectory(sourceDirectory, destDirectory);
      }
      catch (IOException e1)
      {
         e1.printStackTrace();
      }
   }

   private void btnCancelClicked(ActionEvent e)
   {

   }

}
