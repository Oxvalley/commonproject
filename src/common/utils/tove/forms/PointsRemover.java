package common.utils.tove.forms;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PointsRemover
{
   public static void main(String[] args)
   {
      List<PointsArrayElement> removeList = getRemoveList("Z:\\3D\\mycars.scad", "Z:\\3D\\144_points.scad");
      for (PointsArrayElement pointsmodule : removeList)
      {
         System.out.println(pointsmodule);
      }
   }

   private static List<PointsArrayElement> getRemoveList(String mainFile, String pointsFile)
   {
      List<PointsArrayElement> removeList = new ArrayList<>();

      List<String> pointsArrayLists = getPointsModules(pointsFile);
      for (String pointModule : pointsArrayLists)
      {
         System.out.println(pointModule);
      }
      Map<String, List<String>> mainModuleList = getMainModules(mainFile);
      for (String pointsName : mainModuleList.keySet())
      {
         System.out.println(">>> " + pointsName);
         List<String> lst = mainModuleList.get(pointsName);
         if (lst != null)
         {
            for (String moduleName : lst)
            {
               System.out.println(moduleName);
            }
         }
      }
      List<String> usedModuleList = getUsedModules(mainFile);
      for (String moduleName : usedModuleList)
      {
         System.out.println(moduleName);
      }

      for (String pointArrayName : pointsArrayLists)
      {

         PointsArrayElement elem = new PointsArrayElement(pointArrayName);
         removeList.add(elem);
         List<String> modulesList = mainModuleList.get(pointArrayName);
         elem.setModulesList(modulesList);
         if (modulesList != null)
         {
            for (String module : modulesList)
            {
               for (String usedName : usedModuleList)
               {
                  if (usedName.contains(module))
                  {
                     elem.addUser(usedName);
                  }
               }
            }

         }
      }

      return removeList;
   }

   private static List<String> getUsedModules(String mainFile)
   {
      List<String> lst = new ArrayList<>();
      BufferedReader reader = null;
      try
      {
         reader = new BufferedReader(new FileReader(mainFile));
         String line = reader.readLine();
         String moduleName = null;

         while (line != null)
         {
            line = line.trim();
            if (line.endsWith(");"))
            {
               int i = line.indexOf("(");
               moduleName = line.substring(0, i);
               int i2 = moduleName.lastIndexOf(" ");
               moduleName = moduleName.substring(i2 + 1);
               lst.add(moduleName);
            }

            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }

      return lst;
   }

   private static Map<String, List<String>> getMainModules(String mainFile)
   {
      Map<String, List<String>> lst = new HashMap<>();
      BufferedReader reader = null;
      try
      {
         reader = new BufferedReader(new FileReader(mainFile));
         String line = reader.readLine();
         String moduleName = null;
         String pointsName = null;

         while (line != null)
         {
            line = line.trim();
            if (line.startsWith("module "))
            {
               int i = line.indexOf("(");
               if (i > -1)
               {
                  moduleName = line.substring(7, i);
               }
            }

            if (line.startsWith("polygon("))
            {
               int i = line.indexOf(")");
               if (i > -1)
               {
                  pointsName = line.substring(8, i).trim();
                  if (moduleName != null)
                  {
                     List<String> lst2 = lst.get(pointsName);
                     if (lst2 == null)
                     {
                        lst2 = new ArrayList<>();
                        lst.put(pointsName, lst2);
                     }

                     lst2.add(moduleName);
                  }
               }
            }

            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }

      return lst;
   }

   private static List<String> getPointsModules(String pointsFile)
   {
      List<String> lst = new ArrayList<>();
      BufferedReader reader = null;
      try
      {
         reader = new BufferedReader(new FileReader(pointsFile));
         String line = reader.readLine();

         while (line != null)
         {
            if (line.trim().endsWith("= ["))
            {
               lst.add(line.trim().substring(0, line.length() - 3).trim());
            }

            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }

      return lst;
   }

}
