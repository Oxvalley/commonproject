package common.utils;

import javax.swing.JFrame;
import javax.swing.JLabel;

import common.ui.components.ext.JPanelExt;


/**
 * The Class AboutBox.
 */
public class AboutBox extends JFrame
{
   
   /**
    * Instantiates a new about box.
    *
    * @param appInfo the app info
    */
   public AboutBox(AppInfoInterface appInfo)
   {
     
      JPanelExt panel = new JPanelExt(300, 300);
      
      setTitle("About " + appInfo.getApplicationName());
      setIconImage(appInfo.getIcon().getImage());
      
      JLabel appName = new JLabel(appInfo.getApplicationName());      
      panel.addComponentExt(appName, 65, 28, 200, 15);

      JLabel version = new JLabel("Version: " +  appInfo.getVersion());
      panel.addComponentExt(version, 65, 45, 200, 15);
      
      JLabel label = new JLabel(appInfo.getIcon());      
      panel.addComponentExt(label, 10, 20, 50, 50);
      
      JLabel copywriteText = new JLabel(appInfo.getCopywrightText());      
      panel.addComponentExt(copywriteText, 10, 73, 200, 15);
      
      getContentPane().add(panel);
      
      setSize(300, 300);

   }
   
   
   /**
    * The main method.
    *
    * @param args the arguments
    */
   public static void main(String[] args)
   {
      new AboutBox(AppInfo.getInstance()).setVisible(true);
   }
   
}
