package common.utils;

import java.awt.Color;

public class LineElement
{

   private int iX;
   private int iY;
   private int iDirection;
   private Color iColor;

   public LineElement(int x, int y, int direction, Color color)
   {
      iX = x;
      iY = y;
      iDirection = direction;
      iColor = color;
   }

   public int getX()
   {
      return iX;
   }

   public int getY()
   {
      return iY;
   }

   public int getDirection()
   {
      return iDirection;
   }

   public Color getColor()
   {
      return iColor;
   }
   
   @Override
   public String toString()
   {
     return "X="+iX+", Y="+iY+", Direction="+iDirection + ", Color="+iColor;
   }


}
