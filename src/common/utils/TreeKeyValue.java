package common.utils;

public class TreeKeyValue
{

   private String iProperty;
   private String iKey = null;
   private String iValue = null;

   @Override
   public String toString()
   {
      String value = iValue;
      if (value == null)
      {
         value = "";
      }

      return iKey + ": " + value;
   }

   public TreeKeyValue(String property)
   {
      iProperty = property.trim();
      int i = iProperty.indexOf(":");
      if (i == -1 || i >= iProperty.length())
      {
         iKey = iProperty.trim();
      }
      else
      {
         iKey = iProperty.substring(0, i).trim();
         iValue = iProperty.substring(i + 1).trim();
         if (iValue.isEmpty())
         {
            iValue = null;
         }
      }

   }

   public boolean isStl()
   {
      return (iProperty.toLowerCase().startsWith("stl:"));
   }

   public boolean isSource()
   {
      return (iProperty.toLowerCase().startsWith("source:"));
   }

   public boolean isImage()
   {
      return (iProperty.toLowerCase().startsWith("image:"));
   }

   public boolean isDate()
   {
      return (iValue != null && iValue.charAt(4) == '-' && iValue.charAt(7) == '-');
   }

   public String getValue()
   {
      return iValue;
   }

   public String getProperty()
   {
      return iProperty;
   }

   public String getKey()
   {
      return iKey;
   }

   public void setValue(String value)
   {
      iValue = value;
   }

   public void correctSlashes()
   {
      iValue = iValue.replace("\\\\", "/").replace("\\", "/");
   }

   public boolean isEndDate()
   {
      return (iProperty.toLowerCase().startsWith("enddate:"));
   }

   public boolean isStartDate()
   {
      return (iProperty.toLowerCase().startsWith("startdate:"));
   }

   public boolean isPath()
   {
      return (iProperty.toLowerCase().startsWith("path:"));
   }

   public boolean isType()
   {
      return (iProperty.toLowerCase().startsWith("type:"));
   }
}
