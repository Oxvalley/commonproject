package common.utils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class GenericSlider extends JPanel
{

   private String iLabel;
   private int iWidth;
   private int iHeight;
   private int iMin;
   private int iMax;
   private int iSteps;
   private JTextField text;
   private JSlider slider;
   private int iValue;
   private List<ActionListener> iListeners = new ArrayList<>();
   private int iId;

   public GenericSlider(int id, String label, int width, int height, int min, int max, int value, int steps)
   {
      iId = id;
      iLabel = label;
      iWidth = width;
      iHeight = height;
      iMin = min;
      iMax = max;
      iValue = value;
      iSteps = steps;

      setLayout(null);

      int labelWidth = iLabel.length() * 8;
      int leftArrowWidth = 50;
      int sliderWidth = (int) Math.round(iWidth * .6);
      int textfieldWidth = 35;
      int rightArrowWidth = leftArrowWidth;
      int buttonHeight = 35;

      int labelX = 0;
      int leftArrowX = labelX + labelWidth;
      int sliderX = leftArrowX + leftArrowWidth;
      int rightArrowX = sliderX + sliderWidth;
      int textfieldX = rightArrowX + rightArrowWidth + 5;
      int buttonY = 10;

      JLabel jlabel = new JLabel(iLabel);
      jlabel.setBounds(labelX, 0, labelWidth, iHeight);
      add(jlabel);

      JButton lbutton = new JButton("<");
      lbutton.setBounds(leftArrowX, buttonY, leftArrowWidth, buttonHeight);
      add(lbutton);
      lbutton.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            leftButtonClicked(e);

         }
      });

      slider = new JSlider(iMin, iMax, iValue);
      slider.setBounds(sliderX, 10, sliderWidth, iHeight);
      add(slider);

      slider.setToolTipText(slider.getValue() + "");
      slider.setMajorTickSpacing(20);
      slider.setPaintTicks(true);
      slider.setPaintLabels(true);
      slider.addChangeListener(new ChangeListener()
      {

         @Override
         public void stateChanged(ChangeEvent e)
         {
            slider.setToolTipText(slider.getValue() + "");
            text.setText(slider.getValue() + "");
            tellListeners(iId);
         }
      });

      JButton rbutton = new JButton(">");
      rbutton.setBounds(rightArrowX, buttonY, rightArrowWidth, buttonHeight);
      add(rbutton);
      rbutton.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            rightButtonClicked(e);

         }
      });

      text = new JTextField();
      text.setText(slider.getValue() + "");
      text.setBounds(textfieldX, buttonY, textfieldWidth, buttonHeight);
      add(text);

   }

   protected void tellListeners(int id)
   {
      for (ActionListener listener : iListeners)
      {
         ActionEvent a = new ActionEvent(this, id, "Slider changed");
         listener.actionPerformed(a);
      }
   }

   private void leftButtonClicked(ActionEvent e)
   {
      slider.setValue(slider.getValue() - iSteps);
   }

   private void rightButtonClicked(ActionEvent e)
   {
      slider.setValue(slider.getValue() + iSteps);
   }

   public int getValue()
   {
      return slider.getValue();
   }

   public void addActionListener(ActionListener listener)
   {
      iListeners.add(listener);
   }

}
