package common.utils;

public interface UndoInterface
{
   /**
    * Save Changes/state Put on top in Stack
    */
   void saveSnapshot();

   /**
    * A small change. Normally do nothing. A saveSnapshot() will be called
    * automatically when enough changes are collected
    */
   void saveChange();

   /**
    * Pop last saved state Save what we popped to a redo-stack, if we want to
    * redo Go back to last saved state
    */
   void undo();

   /**
    * Regret an undo. Put it back on the stack Remove from regret-list Go back
    * to last saved regret-state
    */
   void redo();

   /**
    * Pop oldest saved state from bottom of stack
    */
   void removeOldestSave();
}
