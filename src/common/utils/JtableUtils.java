/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common.utils;

import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * 
 * @author Lars Svensson
 */
public class JtableUtils
{

   public static void setTableRows(JTable table, int rows)
   {
      int cols = table.getModel().getColumnCount();
      if (table.getModel() instanceof DefaultTableModel)
      {
         DefaultTableModel model = (DefaultTableModel) table.getModel();

         // Erase rest of the values from old list if too many rows
         while (table.getRowCount() > rows)
         {
            model.removeRow(0);
         }

         // Add rows if too few rows
         while (table.getRowCount() < rows)
         {
            model.addRow(new Object[cols]);
         }
      }
   }

   private JTable i_table = null;
   private int i_row = 0;
   private int i_column = 0;

   JtableUtils(JTable table)
   {
      i_table = table;
   }

   public void setValue(String value)
   {
      i_table.getModel().setValueAt(value, i_row, i_column++);
   }

   // Usage:
   // JtableUtils.setTableRows(table, filteredPeopleList.size());
   // JtableUtils util = new JtableUtils(table);
   // for (Person person : filteredPeopleList) {
   // addTableRow(util, person);

   // util.setValue(getHighlighted(person.getFirstName(), lst));
   // util.setValue(getHighlighted(person.getLastName(), lst));
   // util.setValue(getHighlighted(person.getAddress(), lst));
   // util.setValue(getHighlighted(person.getZipCode(), lst));
   // util.setValue(getHighlighted(person.getCity(), lst));
   // util.setValue(getHighlighted(person.getPhone(), lst));
   // util.setValue(getHighlighted(person.getEmailAddress(), lst));
   // util.newRow();

   public void newRow()
   {
      i_row++;
      i_column = 0;
   }

   public static String getHighlightedText(String line, List<String> searchwords)
   {

      for (String word : searchwords)
      {

         line = highLigthWord(word, line);
      }

      return line;
   }

   private static boolean isTextHighlighted(String highLine, int i2)
   {
      if ((highLine == null) || (highLine.length() < i2) || (i2 < 1))
      {
         return false;
      }

      String before = highLine.substring(0, i2);
      int i = before.lastIndexOf("<b>");

      if (i == -1)
      {
         return false;
      }

      // There is a <b> before this position. Find out if it has a </b> as well
      String middle = before.substring(i, i2);

      if (middle.indexOf("</b>") == -1)
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   public static String highLigthWord(String word, String highLine)
   {
      if (highLine == null)
      {
         return null;
      }

      int i2 = 0;

      // Search case insensitive
      String UpperHighLine = highLine.toUpperCase();

      while ((i2 = UpperHighLine.indexOf(word.toUpperCase(), i2)) > -1)
      {

         // i2 is the index
         // Is it already highlighted?
         if (!isTextHighlighted(highLine, i2))
         {
            String beforeString = "";

            if (i2 > 0)
            {
               beforeString = highLine.substring(0, i2);
            }

            String hitString = highLine.substring(i2, word.length() + i2);
            String afterString = highLine.substring(word.length() + i2,
                  highLine.length());

            highLine = beforeString + "<b>" + hitString + "</b>" + afterString;
            UpperHighLine = highLine.toUpperCase();
            i2 += 4; // <b>+1
         }
      }

      return highLine;
   }

}