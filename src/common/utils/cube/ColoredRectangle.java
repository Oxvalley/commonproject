package common.utils.cube;

import java.awt.Color;
import java.util.List;

public class ColoredRectangle implements Comparable<ColoredRectangle>
{

   private int iX;
   private int iY;
   private int iWidth;
   private int iHeight;
   private Color iColor;
   private int iGreenCount;
   private List<ColorSequence> iSeqLst;
   private Color iAverageColor;
   private int iExtraPoints = 0;

   public ColoredRectangle(int x, int y, int width, int height, Color color,
         int greenCount, List<ColorSequence> seqLst)
   {
      iX = x;
      iY = y;
      iWidth = width;
      iHeight = height;
      iColor = color;
      iGreenCount = greenCount;
      iSeqLst = seqLst;
   }

   public List<ColorSequence> getSeqLst()
   {
      return iSeqLst;
   }

   public int getX()
   {
      return iX;
   }

   public int getY()
   {
      return iY;
   }

   public int getWidth()
   {
      return iWidth;
   }

   public int getHeight()
   {
      return iHeight;
   }

   public Color getColor()
   {
      return iColor;
   }

   public int getGreenCount()
   {
      return iGreenCount;
   }

   public int getTotalCount()
   {
      return iGreenCount+iExtraPoints;
   }
   
   public Color getAvergeColor()
   {
      if (iSeqLst.size() > 0)
      {
         int r=0;
         int g=0;
         int b=0;
         
         for (ColorSequence colorSequence : iSeqLst)
         {
            r+= colorSequence.getAverageColor().getRed();
            g+= colorSequence.getAverageColor().getGreen();
            b+= colorSequence.getAverageColor().getBlue();
         }
         
         iAverageColor = new Color(r/iSeqLst.size(),g/iSeqLst.size(),b/iSeqLst.size()); 
         return iAverageColor;
      }
      else
      {
         return null;
      }
   }
   
   
   
   public String getBestColor()
   {
      if (iAverageColor == null)
      {
        iAverageColor = getAvergeColor();
      }

      if (iAverageColor != null)
      {
         return ColorSequence.getClosestColorName(iAverageColor);
      }  
      
      return "";
   }

   @Override
   public int compareTo(ColoredRectangle o)
   {
      return o.iGreenCount-iGreenCount;
   }

   public void setColor(Color color)
   {
      iColor = color;
   }

   public void setExtraPoint(int extraPoints)
   {
      iExtraPoints = extraPoints;
   }

   public int getExtraPoints()
   {
      return iExtraPoints;
   }

}
