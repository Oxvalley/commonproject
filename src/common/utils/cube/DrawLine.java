package common.utils.cube;

import java.awt.Color;

public class DrawLine
{

   private int iX1;
   private int iY1;
   private int iX2;
   private int iY2;
   private Color iColor;

   public DrawLine(double x1, double y1, double x2, double y2, Color color)
   {
      this((int) Math.round(x1), (int) Math.round(y1), (int) Math.round(x2),
            (int) Math.round(y2), color);
   }

   public DrawLine(int x1, int y1, int x2, int y2, Color color)
   {
      iX1 = x1;
      iY1 = y1;
      iX2 = x2;
      iY2 = y2;
      iColor = color;
   }

   public int getX1()
   {
      return iX1;
   }

   public int getY1()
   {
      return iY1;
   }

   public int getX2()
   {
      return iX2;
   }

   public int getY2()
   {
      return iY2;
   }

   public Color getColor()
   {
      return iColor;
   }

}
