package common.utils.cube;

public enum RelationEnum
{
  leftTopChild, rightTopChild, toRightLeftListChild, toLeftChild, downLeftListChild, upLeftChild;
}
