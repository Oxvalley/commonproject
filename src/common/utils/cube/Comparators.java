package common.utils.cube;

import java.util.Comparator;

public class Comparators
{

   public static Comparator<ColoredSquare> widthHeightComparator =
         new Comparator<ColoredSquare>()
         {
            @Override
            public int compare(ColoredSquare e1, ColoredSquare e2)
            {
               return (int) (e1.getAreaDiff() - e2.getAreaDiff());
            }
         };

   public static Comparator<CrossDistance> closestComparator =
         new Comparator<CrossDistance>()
         {
            @Override
            public int compare(CrossDistance e1, CrossDistance e2)
            {
               return e1.getDistance() - e2.getDistance();
            }
         };

   public static Comparator<ColoredSquare> distanceComparator =
         new Comparator<ColoredSquare>()
         {
            @Override
            public int compare(ColoredSquare e1, ColoredSquare e2)
            {
               return (int) (e1.getDistance() - e2.getDistance());
            }
         };

   public static Comparator<ColoredSquare> ChildrenComparator =
         new Comparator<ColoredSquare>()
         {
            @Override
            public int compare(ColoredSquare e1, ColoredSquare e2)
            {
               int diff = e2.getChildrenCount() - e1.getChildrenCount();
               if (diff == 0)
               {
                  diff = Math.abs(e1.getLowestY() - e2.getLowestY());
                  if (diff <= ColoredSquare.getIdealHeight()
                        * CubeConstants.HEIGHT_DIFF_ACCEPTED)
                  {
                     return e1.getLowestX() - e2.getLowestX();
                  }
                  else
                  {
                     return e1.getLowestY() - e2.getLowestY();
                  }
               }

               return diff;
            }
         };

   public static Comparator<ColoredSquare> positionComparator =
         new Comparator<ColoredSquare>()
         {
            @Override
            public int compare(ColoredSquare e1, ColoredSquare e2)
            {
               int diff = Math.abs(e1.getLowestY() - e2.getLowestY());
               if (diff <= ColoredSquare.getIdealHeight()
                     * CubeConstants.HEIGHT_DIFF_ACCEPTED)
               {
                  return e1.getLowestX() - e2.getLowestX();
               }
               else
               {
                  return e1.getLowestY() - e2.getLowestY();
               }
            }
         };

   public static Comparator<ColoredSquare> PointsComparator =
         new Comparator<ColoredSquare>()
         {
            @Override
            public int compare(ColoredSquare e1, ColoredSquare e2)
            {
               return (int) (e1.getPlacement() - e2.getPlacement());
            }
         };

   public static Comparator<ColoredSquare> YPosComparator =
         new Comparator<ColoredSquare>()
         {
            @Override
            public int compare(ColoredSquare e1, ColoredSquare e2)
            {
               return e1.getLowestY() - e2.getLowestY();
            }
         };

   public static Comparator<ColoredSquare> placementComparator =
         new Comparator<ColoredSquare>()
         {
            @Override
            public int compare(ColoredSquare e1, ColoredSquare e2)
            {
               return (int) (e1.getPlacement() - e2.getPlacement());
            }
         };

}
