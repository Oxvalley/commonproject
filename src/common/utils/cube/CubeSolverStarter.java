package common.utils.cube;

import java.io.IOException;

public class CubeSolverStarter
{

   public void findAllSquares() throws IOException
   {
      CubeSolver cs1 = new CubeSolver();
      CubeSolver cs2 = new CubeSolver();
      CubeSolver cs3 = new CubeSolver();
      CubeSolver cs4 = new CubeSolver();

     // cs1.findColorsFromOnePicture("2019-10-04 12.23.28.jpg", 1);
     // cs2.findColorsFromOnePicture("2019-10-04 12.23.18.jpg", 2);

      cs3.findColorsFromOnePicture("2019-10-28 15.34.37.jpg", 1);
    //  cs4.findColorsFromOnePicture("2019-10-28 15.34.49.jpg", 2);
   }

   public static void main(String args[]) throws IOException
   {
      CubeSolverStarter cs = new CubeSolverStarter();
      cs.findAllSquares();
   }
}
