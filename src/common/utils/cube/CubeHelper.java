package common.utils.cube;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CubeHelper
{
   private static double maxRGB = Math.sqrt(Math.pow(255, 2) + Math.pow(255, 2) + Math.pow(255, 2));

   public static int getYFromX(int start_x, int start_y, double angle, int x)
   {
      int y = (int) ((x - start_x) * angle) + start_y;

      if (angle == CubeConstants.ANGLE_HORIZONTAL)
      {
         y = start_y;
      }

      return y;
   }

   public static Color getAverageColor(Color color, Color color2)
   {
//      System.out.println(color);
//      System.out.println(color2);
      int r = (int)Math.round(Math.sqrt((Math.pow(color.getRed(), 2) + Math.pow(color2.getRed(), 2)) / 2));
      int g = (int)Math.round(Math.sqrt((Math.pow(color.getGreen(), 2) + Math.pow(color2.getGreen(), 2)) / 2));
      int b = (int)Math.round(Math.sqrt((Math.pow(color.getBlue(), 2) + Math.pow(color2.getBlue(), 2)) / 2));
      //System.out.println("Result: " + r + ", " + g + ", " + b);

      if (r > 255 || r < 0 || g > 255 || g < 0 || b > 255 || b < 0 )
      {
       System.out.println("Nu!!");  
      }
      
      
      Color result = new Color(r, g,b);

      return result;
   }

   public static Color getAverageColor(List<Color> colorList)
   {
      List<Color> lst = new ArrayList<>();
      for (int i = 0; i < colorList.size(); i += 2)
      {
         if (i < colorList.size() - 1)
         {
            lst.add(getAverageColor(colorList.get(i), colorList.get(i + 1)));
         }
         else
         {
            lst.add(colorList.get(i));
         }
      }

      if (lst.size() > 1)
      {
         return getAverageColor(lst);
      }
      else
      {
         return lst.get(0);
      }
   }

   public static double getColordiffPercent(Color color, Color color2)
   {
      double root = Math.pow(color.getRed() - color2.getRed(), 2) + Math.pow(color.getGreen() - color2.getGreen(), 2)
            + Math.pow(color.getBlue() - color2.getBlue(), 2);

      double d = Math.sqrt(root);

      return d / maxRGB;
   }

   public static void setPlacementPoints(List<ColoredSquare> topList, String title, int factor, boolean showUI)
   {
      int i;
      i = 0;
      if (showUI)
      {
         System.out.println("\n" + title);
      }
      for (ColoredSquare coloredSquare : topList)
      {
         coloredSquare.addPlacement(i * factor, showUI);
         i++;
      }
   }

   public static void calcDistanceToOthers(List<ColoredSquare> topList)
   {

      for (int i = 0; i < topList.size() - 1; i++)
      {
         for (int j = i + 1; j < topList.size(); j++)
         {
            ColoredSquare coli = topList.get(i);
            ColoredSquare colj = topList.get(j);
            double dist = getDistance((coli.getWidth()) / 2, (coli.getHeight()) / 2, (colj.getWidth()) / 2,
                  (colj.getHeight()) / 2);
            coli.addDistance(dist);
            colj.addDistance(dist);
         }
      }
   }

   public static void calcWidthToOthers(List<ColoredSquare> topList)
   {

      for (int i = 0; i < topList.size() - 1; i++)
      {
         for (int j = i + 1; j < topList.size(); j++)
         {
            ColoredSquare coli = topList.get(i);
            ColoredSquare colj = topList.get(j);
            int width1 = Math.abs(coli.getWidth());
            int width2 = Math.abs(colj.getWidth());
            int widthdiff = Math.abs(width1 - width2);
            coli.addWidthDiff(widthdiff);
            colj.addWidthDiff(widthdiff);
         }
      }
   }

   public static void calcHeightToOthers(List<ColoredSquare> topList)
   {

      for (int i = 0; i < topList.size() - 1; i++)
      {
         for (int j = i + 1; j < topList.size(); j++)
         {
            ColoredSquare coli = topList.get(i);
            ColoredSquare colj = topList.get(j);
            int height1 = Math.abs(coli.getHeight());
            int height2 = Math.abs(colj.getHeight());
            int heightDiff = Math.abs(height1 - height2);
            coli.addHeightDiff(heightDiff);
            colj.addHeightDiff(heightDiff);
         }
      }

   }

   public static boolean isOKColor(ColoredSquare colSquare)
   {
      switch (colSquare.getColorType())
      {
         case UNUSED:
            return false;

         default:
            return true;
      }
   }

   public static boolean isWidthinTestRec(ColorSequence col)
   {
      if (col.getStartPosX() >= CubeConstants.TEST_RECT_X && col.getStartPosY() >= CubeConstants.TEST_RECT_Y
            && col.getEndPosX() <= CubeConstants.TEST_RECT_X + CubeConstants.TEST_RECT_WIDTH
            && col.getEndPosY() <= CubeConstants.TEST_RECT_Y + +CubeConstants.TEST_RECT_HIGHT)
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   public static void addSequence(List<ColorSequence> seqList, int x, int y, int x2, int y2)
   {
      ColorSequence seq = new ColorSequence(Color.white, 30, x, x, y, x2, y2);
      seqList.add(seq);
   }

   public static boolean seqUsed(ColorSequence colSequence, List<ColoredSquare> squareList)
   {
      for (ColoredSquare colSq : squareList)
      {
         if (colSq.getList().contains(colSequence))
         {
            return true;
         }
      }
      return false;
   }

   public static List<ColorSequence> getSequencesInside(Rectangle rect, List<ColorSequence> list)
   {
      return getSequencesInside(rect.x, rect.y, rect.width, rect.height, list);
   }

   public static List<ColorSequence> getSequencesInside(int x, int y, int width, int height, List<ColorSequence> list)
   {
      List<ColorSequence> insideList = new ArrayList<>();
      for (ColorSequence colorSequence : list)
      {
         if (isCompletelyInside(x, y, width, height, colorSequence))
         {
            insideList.add(colorSequence);
         }
      }

      return insideList;
   }

   public static boolean isInside(int x, int y, int spanX, int spanY, ColorSequence colorSequence)
   {
      Rectangle r1 = new Rectangle(x, y, spanX, spanY);
      Line2D l1 = new Line2D.Float(colorSequence.getStartPosX(), colorSequence.getStartPosY(),
            colorSequence.getEndPosX(), colorSequence.getEndPosY());
      if (l1.intersects(r1))
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   public static boolean isCompletelyInside(int x, int y, int spanX, int spanY, ColorSequence colorSequence)
   {
      int minX = Math.min(colorSequence.getStartPosX(), colorSequence.getEndPosX());
      int minY = Math.min(colorSequence.getStartPosY(), colorSequence.getEndPosY());
      int maxX = Math.max(colorSequence.getStartPosX(), colorSequence.getEndPosX());
      int maxY = Math.max(colorSequence.getStartPosY(), colorSequence.getEndPosY());

      if (x <= minX && x + spanX >= maxX && y <= minY && y + spanY >= maxY)
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   public static Color getConstrastColor(Color origColor, Color lightColor, Color darkColor)
   {
      Color retVal;
      int colSum = origColor.getRed() + origColor.getGreen() + origColor.getBlue();
      if (colSum < 128 * 3)
      {
         retVal = lightColor;
      }
      else
      {
         retVal = darkColor;
      }
      return retVal;
   }

   public static double getDistance(int x, int y, int halfX, int halfY)
   {
      return Math.sqrt(Math.pow(Math.abs(x - halfX), 2) + Math.pow(Math.abs(y - halfY), 2));
   }

   public int getHalfPos(Map<Integer, Integer> treeMap, int totCount)
   {
      int halfsum = 0;
      for (Integer x : treeMap.keySet())
      {
         halfsum += treeMap.get(x);
         if (halfsum >= totCount / 2)
         {
            return x;
         }
      }
      return -1;
   }

   public static boolean intersects(ColorSequence seq1, ColorSequence seq2)
   {

      if (seq1.getAverageColor() != null && seq2.getAverageColor() != null)
      {
         if (CubeHelper.getColordiffPercent(seq1.getAverageColor(), seq2.getAverageColor()) > .2)
         {
            return false;
         }
      }

      Point point = calculateIntersectionPoint(seq1, seq2);

      if (point == null)
      {
         if (seq1.getK() == null && seq2.getK() != null)
         {
            double x = seq1.getStartPosX();
            double y = (int) (seq2.getK() * x + seq2.getM());
            point = new Point();
            point.setLocation(x, y);
         }
         else if (seq1.getK() != null && seq2.getK() == null)
         {
            double x = seq2.getStartPosX();
            double y = (int) (seq1.getK() * x + seq1.getM());
            point = new Point();
            point.setLocation(x, y);
         }
         else
         {
            // Both K are null
            return false;
         }
      }

      if (!isPointOnLine(point, seq1))
      {
         return false;
      }

      if (!isPointOnLine(point, seq2))
      {
         return false;
      }

      return true;
   }

   public static boolean colorDiff(Color lastColor, Color lastColor2)
   {
      // TODO Auto-generated method stub
      return false;
   }

   public static boolean isPointOnLine(Point point, ColorSequence seq)
   {
      int x1;
      int y1;
      int x2;
      int y2;

      if (seq.getStartPosX() <= seq.getEndPosX())
      {
         x1 = seq.getStartPosX();
         x2 = seq.getEndPosX();
      }
      else
      {
         x1 = seq.getEndPosX();
         x2 = seq.getStartPosX();
      }

      if (seq.getStartPosY() <= seq.getEndPosY())
      {
         y1 = seq.getStartPosY();
         y2 = seq.getEndPosY();
      }
      else
      {
         y1 = seq.getEndPosY();
         y2 = seq.getStartPosY();
      }

      if (point.x >= x1 && point.x <= x2 && point.y >= y1 && point.y <= y2)
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   public static Point calculateIntersectionPoint(ColorSequence seq1, ColorSequence seq2)
   {
      return calculateIntersectionPoint(seq1.getK(), seq1.getM(), seq2.getK(), seq2.getM());
   }

   public static Point calculateIntersectionPoint(Double k1, double m1, Double k2, double m2)
   {

      if (k1 == null || k2 == null || k1 == k2)
      {
         return null;
      }

      double x = (m2 - m1) / (k1 - k2);
      double y = k1 * x + m1;

      Point point = new Point();
      point.setLocation(x, y);
      return point;
   }

   public static boolean isFree(double percent)
   {
      return (percent >= CubeConstants.PERCENTAGE_OF_CORNER_FREE);
   }

   public static Color getOppositeBlackWhite(Color color)
   {
      System.out.println(CubeHelper.getColordiffPercent(Color.WHITE, color));
      
      if (CubeHelper.getColordiffPercent(Color.WHITE, color) > .6)
      {
         System.out.println("WHITE");
         return Color.WHITE;
      }
      else
      {
         System.out.println("BLACK");
         return Color.BLACK;
      }
   }

}
