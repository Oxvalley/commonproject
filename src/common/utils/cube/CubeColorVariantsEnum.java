package common.utils.cube;

import java.awt.Color;

public enum CubeColorVariantsEnum
{
   BLACK(Color.BLACK, CubeColorEnum.UNUSED),
   // BLUE(Color.BLUE, CubeColorEnum.BLUE),
   // BLUE2(new Color(51, 71, 119), CubeColorEnum.BLUE),
   BLUE3(new Color(101, 144, 220), CubeColorEnum.BLUE),
   // BLUE4(new Color(27, 117, 190), CubeColorEnum.BLUE),
   BLUE5(new Color(20, 113, 189), CubeColorEnum.BLUE),
   BLUE6(new Color(60, 59, 76), CubeColorEnum.BLUE),
   // GREEN(Color.GREEN, CubeColorEnum.GREEN),
   GREEN2(new Color(68, 86, 51), CubeColorEnum.GREEN),
   // GREEN3(new Color(43, 101, 103), CubeColorEnum.GREEN),
   GREEN4(new Color(80, 170, 205), CubeColorEnum.GREEN),
   // ORANGE(Color.ORANGE, CubeColorEnum.ORANGE),
   ORANGE2(new Color(175, 75, 26), CubeColorEnum.ORANGE),
   ORANGE3(new Color(225, 158, 168), CubeColorEnum.ORANGE),
   ORANGE4(new Color(240, 124, 89), CubeColorEnum.ORANGE),
   // RED(Color.RED, CubeColorEnum.RED),
   RED2(new Color(125, 44, 34), CubeColorEnum.RED),
   RED3(new Color(196, 130, 185), CubeColorEnum.RED),
   // YELLOW(Color.YELLOW, CubeColorEnum.YELLOW),
   YELLOW2(new Color(192, 144, 27), CubeColorEnum.YELLOW),
   YELLOW3(new Color(210, 203, 190), CubeColorEnum.YELLOW),
   // YELLOW4(new Color(220, 230, 105), CubeColorEnum.YELLOW),
   // YELLOW5(new Color(222, 231, 101), CubeColorEnum.YELLOW),
   YELLOW6(new Color(170, 190, 101), CubeColorEnum.YELLOW),
   // WHITE(Color.WHITE, CubeColorEnum.WHITE),
   WHITE2(new Color(195, 175, 151), CubeColorEnum.WHITE),
   WHITE3(new Color(194, 200, 206), CubeColorEnum.WHITE);
   // WHITE4(new Color(166, 178, 202), CubeColorEnum.WHITE);

   private Color iColor;
   private CubeColorEnum iType;

   CubeColorVariantsEnum(Color color, CubeColorEnum type)
   {
      iColor = color;
      iType = type;
   }

   public Color getColor()
   {
      return iColor;
   }

   public CubeColorEnum getType()
   {
      return iType;
   }

}
