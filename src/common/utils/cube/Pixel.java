package common.utils.cube;

import java.awt.Color;

public class Pixel
{

   public int getX()
   {
      return iX;
   }

   public int getY()
   {
      return iY;
   }

   public Color getColor()
   {
      return iColor;
   }

   private int iX;
   private int iY;
   private Color iColor;

   public Pixel(int x, int y, Color color)
   {
      iX = x;
      iY = y;
      iColor = color;
   }



}
