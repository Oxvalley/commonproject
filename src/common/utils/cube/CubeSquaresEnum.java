package common.utils.cube;

public enum CubeSquaresEnum
{

   TOP1(1, 30), TOP21(4, 29), TOP22(2, 33), TOP31(7, 28), TOP32(5, 32),
   TOP33(3, 36), TOP41(8, 31), TOP42(6, 35), TOP5(9, 34),

   LEFT11(19, 45), LEFT12(20, 44), LEFT13(21, 43), LEFT21(22, 42),
   LEFT22(23, 41), LEFT23(24, 40), LEFT31(25, 39), LEFT32(26, 38),
   LEFT33(27, 37),

   RIGHT11(10, 54), RIGHT12(11, 53), RIGHT13(12, 52), RIGHT21(13, 51),
   RIGHT22(14, 50), RIGHT23(15, 49), RIGHT31(16, 48), RIGHT32(17, 47),
   RIGHT33(18, 46);

   private int iPlacePicture1;

   public int getPlacePicture1()
   {
      return iPlacePicture1;
   }

   public int getPlacePicture2()
   {
      return iPlacePicture2;
   }

   private int iPlacePicture2;

   CubeSquaresEnum(int placePicture1, int PlacePicture2)
   {
      iPlacePicture1 = placePicture1;
      iPlacePicture2 = PlacePicture2;
   }

}
