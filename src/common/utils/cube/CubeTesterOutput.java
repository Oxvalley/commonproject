package common.utils.cube;

public class CubeTesterOutput implements Comparable<CubeTesterOutput>
{

   private String iResultString = null;
   private int iReconstructedCount = 0;
   private int iDeeperFoundCount = 0;
   private long iLeftRightDiffs = 0;
   private String iExpectedResult = null;
   private String iDisplayName = null;
   private int iCorrectCount = 0;

   public CubeTesterOutput(String resultString, int reconstructedCount,
         int deeperFoundCount, long leftRightDiffs)
   {
      iResultString = resultString;
      iReconstructedCount = reconstructedCount;
      iDeeperFoundCount = deeperFoundCount;
      iLeftRightDiffs = leftRightDiffs;
   }

   public void setExcpectedResult(String expectedResult)
   {
      iExpectedResult = expectedResult;
      iCorrectCount = 0;

      for (int i = 0; i < iResultString.length(); i++)
      {
         if (iResultString.charAt(i) == iExpectedResult.charAt(i))
         {
            iCorrectCount++;
         }
      }

   }

   public void setDisplayName(String displayName)
   {
      iDisplayName =
            displayName + " " + iCorrectCount + " " + iReconstructedCount + " "
                  + iDeeperFoundCount + " " + iLeftRightDiffs;
   }

   @Override
   public String toString()
   {
      return iDisplayName;
   }

   @Override
   public int compareTo(CubeTesterOutput o)
   {
      if (iCorrectCount == o.iCorrectCount)
      {
         if (iReconstructedCount == o.iReconstructedCount)
         {
            if (iDeeperFoundCount == o.iDeeperFoundCount)
            {
               if (iLeftRightDiffs == o.iLeftRightDiffs)
               {
                  return iDisplayName.compareTo(o.iDisplayName);
               }
               else
               {
                  return (int) (iLeftRightDiffs - o.iLeftRightDiffs);
               }

            }
            else
            {
               return iDeeperFoundCount - o.iDeeperFoundCount;
            }
         }
         else
         {
            return iReconstructedCount - o.iReconstructedCount;
         }
      }
      else
      {
         return o.iCorrectCount - iCorrectCount;
      }
   }

   public String getResultString()
   {
      return iResultString;
   }

   public int getReconstructedCount()
   {
      return iReconstructedCount;
   }

   public int getDeeperFoundCount()
   {
      return iDeeperFoundCount;
   }

   public long getLeftRightDiffs()
   {
      return iLeftRightDiffs;
   }

   public String getExpectedResult()
   {
      return iExpectedResult;
   }

   public String getDisplayName()
   {
      return iDisplayName;
   }

   public int getCorrectCount()
   {
      return iCorrectCount;
   }

   public void addOutput(CubeTesterOutput cto)
   {
     iCorrectCount += cto.getCorrectCount();
     iReconstructedCount += cto.getReconstructedCount();
     iDeeperFoundCount += cto.getDeeperFoundCount();
     iLeftRightDiffs += cto.getLeftRightDiffs();     
      
   }
}
