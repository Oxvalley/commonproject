package common.utils.cube;

import java.awt.Color;

public class ColorSequence
{

   public static String getClosestColorName(Color lastColor)
   {
      CubeColorVariantsEnum retVal = getClosestCubeColor(lastColor);
      if (retVal == null)
      {
         return  CubeColorEnum.UNUSED.toString();
      }
      else
      {
         return retVal.toString();
      }
   }

   public static CubeColorEnum getClosestColorType(Color lastColor)
   {
      CubeColorVariantsEnum retVal = getClosestCubeColor(lastColor);
      if (retVal == null)
      {
         return CubeColorEnum.UNUSED;
      }
      else
      {
        return retVal.getType();
      }
   }

   
   
   public static CubeColorVariantsEnum getClosestCubeColor(Color color)
   {

      double minP = 2.0;
      CubeColorVariantsEnum retVal = null;
      for (CubeColorVariantsEnum cubeColor : CubeColorVariantsEnum.values())
      {
         double p = CubeHelper.getColordiffPercent(cubeColor.getColor(), color);
         if (p < minP && cubeColor.ordinal() != CubeConstants.COLOR_VARIANT_EXCLUDED)
         {
            minP = p;
            retVal = cubeColor;
         }
      }

      return retVal;
   }
   
   private int iColorCount;

   private Color iAverageColor;

   private int iStartpos;

   private int iStartPosX;

   private int iStartPosY;

   public int getStartPosX()
   {
      return iStartPosX;
   }

   public int getStartPosY()
   {
      return iStartPosY;
   }

   public int getEndPosX()
   {
      return iEndPosX;
   }

   public int getEndPosY()
   {
      return iEndPosY;
   }

   private int iEndPosX;

   private int iEndPosY;

   private Double iK;

   private double iM;

   public ColorSequence(Color averageColor, int colorCount, int startpos,
         int startPosX, int startPosY, int endPosX, int endPosY)
   {
      iAverageColor = averageColor;
      iColorCount = colorCount;
      iStartpos = startpos;
      iStartPosX = startPosX;
      iStartPosY = startPosY;
      iEndPosX = endPosX;
      iEndPosY = endPosY;
      
      if (endPosY < 0 )
      {
         System.out.println("Double negative?");
      }

      if (endPosX == startPosX)
      {
         iK = null;
         iM = 0;
      }
      else
      {
         iK = (endPosY - startPosY) * 1.0 / (endPosX - startPosX);
         iM = startPosY - startPosX * iK;
      }

   }

   public Double getK()
   {
      return iK;
   }

   public double getM()
   {
      return iM;
   }

   public int getColorCount()
   {
      return iColorCount;
   }

   public int getEndPos()
   {
      return (iStartpos + iColorCount);
   }

   public Color getAverageColor()
   {
      return iAverageColor;
   }

   public int getStartpos()
   {
      return iStartpos;
   }

   @Override
   public String toString()
   {
      return getStartPosX() + "," + getStartPosY() + "," + getEndPosX() + ","
            + getEndPosY() + "=" + iColorCount + ": " + iAverageColor
            + getClosestColorName(iAverageColor);
   }

   public String getClosestColor()
   {
      return getClosestColorName(iAverageColor);
   }

}
