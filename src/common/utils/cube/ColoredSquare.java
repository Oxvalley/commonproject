package common.utils.cube;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class ColoredSquare
{

   @Override
   public String toString()
   {
      Color color = getAverageColor();
      return getColorType() + " (" + color.getRed() + ", " + color.getGreen()
            + ", " + color.getBlue() + ") " + getLowestX() + ", " + getLowestY()
            + ", " + (getWidth()) + ", " + (getHeight());
   }

   public CubeColorEnum getColorType()
   {
      if (getWidth() < CubeConstants.MIN_PIXELS_SQUARE
            || getHeight() < CubeConstants.MIN_PIXELS_SQUARE)
      {
         return CubeColorEnum.UNUSED;
      }

      return ColorSequence.getClosestColorType(getAverageColor());
   }

   private List<ColorSequence> iList = new ArrayList<>();
   private Color iAverageColor;
   private int iLowestX = -1;
   private int iLowestY = -1;
   private int iHighestX = -1;
   private int iHighestY = -1;
   private double iTopLeftFree;
   private double iTopRightFree;
   private double iBottomLeftFree;
   private double iBottomRightFree;
   private int iTopLeftPixels;
   private int iTopRightPixels;
   private int iBottomLeftPixels;
   private int iBottomRightPixels;
   private double iDistance = 0.0;
   private int iWidthDiff = 0;
   private int iHeightDiff = 0;
   private int iTotalPlacement = 0;
   private int iChildren = 0;
   private CubeSquaresEnum iCubeSquare;
   private SolutionEnum iSolutionType = SolutionEnum.NORMAL;
   private static int iIdealHeight;
   private static int iIdealWidth;

   public CubeSquaresEnum getCubeSquare()
   {
      return iCubeSquare;
   }

   public List<ColorSequence> getList()
   {
      return iList;
   }

   public ColoredSquare(ColorSequence colorSequence)
   {
      iList.add(colorSequence);
   }

   public ColoredSquare(List<ColorSequence> insideList)
   {
      iList = insideList;
   }

   public boolean findIntersection(List<ColorSequence> seqList,
         List<ColoredSquare> squaresList)
   {
      int i = 0;
      boolean inRectOnce = false;
      boolean inRect;
      while (i < iList.size())
      {
         ColorSequence cs = iList.get(i);
         i++;

         // Find all sequences crossing this sequence
         for (ColorSequence colorSequence : seqList)
         {
            if (!CubeHelper.seqUsed(colorSequence, squaresList))
            {
               if (CubeHelper.intersects(cs, colorSequence))
               {
                  inRect = CubeHelper.isWidthinTestRec(colorSequence);

                  if (inRect)
                  {
                     // System.out.println("Now3!! " + colorSequence);
                     inRectOnce = true;
                  }

                  iList.add(colorSequence);
               }
            }
         }
      }

      if (inRectOnce)
      {
         // System.out.println("Now4!! " + iList.size());
         return true;
      }
      else
      {
         return false;
      }

   }

   public Color getAverageColor()
   {
      if (iAverageColor == null)
      {
         long r = 0;
         long g = 0;
         long b = 0;

         for (ColorSequence colorSequence : iList)
         {
            Color col = colorSequence.getAverageColor();
            r += col.getRed();
            g += col.getGreen();
            b += col.getBlue();
         }

         int size = iList.size();

         int r2 = (int) Math.floor(r / size);
         int g2 = (int) Math.floor(g / size);
         int b2 = (int) Math.floor(b / size);
         iAverageColor = new Color(r2, g2, b2);
      }
      return iAverageColor;
   }

   public int getLowestX()
   {
      if (iLowestX == -1)
      {
         int x = Integer.MAX_VALUE;
         for (ColorSequence colorSequence : iList)
         {
            int xTest = Math.min(colorSequence.getStartPosX(),
                  colorSequence.getEndPosX());
            if (xTest < x)
            {
               x = xTest;
            }
         }

         iLowestX = x;
      }

      return iLowestX;
   }

   public int getLowestY()
   {
      if (iLowestY == -1)
      {
         int y = Integer.MAX_VALUE;
         for (ColorSequence colorSequence : iList)
         {
            int yTest = Math.min(colorSequence.getStartPosY(),
                  colorSequence.getEndPosY());
            if (yTest < y)
            {
               y = yTest;
               if (y < 0)
               {
                  System.out.println("Negative?");
               }

            }
         }

         iLowestY = y;
      }

      return iLowestY;
   }

   public Point getMiddle()
   {
      return new Point(getLowestX() + (getWidth() / 2),
            getLowestY() + (getHeight() / 2));
   }

   public int getHighestX()
   {
      if (iHighestX == -1)
      {
         int x = -1;
         for (ColorSequence colorSequence : iList)
         {
            int xTest = Math.max(colorSequence.getStartPosX(),
                  colorSequence.getEndPosX());
            if (xTest > x)
            {
               x = xTest;
            }
         }

         iHighestX = x;
      }

      return iHighestX;
   }

   public int getHighestY()
   {
      if (iHighestY == -1)
      {
         int y = -1;
         for (ColorSequence colorSequence : iList)
         {
            int yTest = Math.max(colorSequence.getStartPosY(),
                  colorSequence.getEndPosY());
            if (yTest > y)
            {
               y = yTest;
            }
         }

         iHighestY = y;
      }

      return iHighestY;
   }

   public void setFreeCorners()
   {
      // Top Left Corner
      int i = 1;
      while (isUncrossedLine(getLowestX(), getLowestY() + i, getLowestX() + i,
            getLowestY()))
      {
         i++;
      }

      iTopLeftPixels = i - 1;
      iTopLeftFree = calcPercentFromPixels(iTopLeftPixels);

      // Top Right Corner
      i = 1;
      while (isUncrossedLine(getHighestX(), getHighestY() - i,
            getHighestX() - 1, getHighestY()))
      {
         i++;
      }

      iTopRightPixels = i - 1;
      iTopRightFree = calcPercentFromPixels(iTopRightPixels);

      // Bottom Left Corner
      i = 1;
      while (isUncrossedLine(getLowestX(), getHighestY() - i, getLowestX() + i,
            getHighestY()))
      {
         i++;
      }

      iBottomLeftPixels = i - 1;
      iBottomLeftFree = calcPercentFromPixels(iBottomLeftPixels);

      // Bottom Left Corner
      i = 1;
      while (isUncrossedLine(getHighestX(), getHighestY() - i,
            getHighestX() - i, getHighestY()))
      {
         i++;
      }

      iBottomRightPixels = i - 1;
      iBottomRightFree = calcPercentFromPixels(iBottomRightPixels);

   }

   private double calcPercentFromPixels(int i)
   {
      return (200.0 * i) / ((getWidth() + getHeight()));
   }

   private boolean isUncrossedLine(int x, int y, int x2, int y2)
   {
      if (iList.size() == 0 || x > getHighestX() || y > getHighestY()
            || x < getLowestX() || y < getLowestY())
      {
         return false;
      }

      ColorSequence cs = new ColorSequence(null, 5, x, x, y, x2, y2);

      for (ColorSequence colorSequence : iList)
      {
         if (CubeHelper.intersects(cs, colorSequence))
         {
            return false;
         }
      }

      return true;
   }

   public int getTopLeftPixels()
   {
      return iTopLeftPixels;
   }

   public int getTopRightPixels()
   {
      return iTopRightPixels;
   }

   public int getBottomLeftPixels()
   {
      return iBottomLeftPixels;
   }

   public int getBottomRightPixels()
   {
      return iBottomRightPixels;
   }

   public boolean hasMoreThan3FreeCorners()
   {
      int i = 0;
      if (CubeHelper.isFree(iTopLeftFree))
      {
         i++;
      }

      if (CubeHelper.isFree(iTopRightFree))
      {
         i++;
      }

      if (CubeHelper.isFree(iBottomLeftFree))
      {
         i++;
      }

      if (CubeHelper.isFree(iBottomRightFree))
      {
         i++;
      }

      if (i > 3)
      {
         return true;
      }
      return false;
   }

   public void addDistance(double dist)
   {
      iDistance += dist;
   }

   public void addWidthDiff(int widthdiff)
   {
      iWidthDiff += widthdiff;
   }

   public void addHeightDiff(int heightdiff)
   {
      iHeightDiff += heightdiff;
   }

   public long getAreaDiff()
   {
      return iHeightDiff * iWidthDiff;
   }

   public long getHeightWidthApart()
   {
      return Math.abs(iHeightDiff) + Math.abs(iWidthDiff);
   }

   public double getDistance()
   {
      return iDistance;
   }

   public void addPlacement(int placement, boolean showUI)
   {
      iTotalPlacement += placement;
      if (showUI)
      {
         System.out.println(iTotalPlacement + " + " + placement + " = "
               + (iTotalPlacement + placement) + " " + toString());
      }
   }

   public long getPlacement()
   {
      return iTotalPlacement;
   }

   public void setNumberOfTopChildren(List<ColoredSquare> topList, int width,
         int height)
   {
      iChildren = 0;

      // Find one layer under
      ColoredSquare child = getLeftChild(topList, width, height);
      if (child != null)
      {
         child.setNumberOfTopChildren(topList, width, height);
         iChildren += child.getChildrenCount() + 1;
      }

      child = getRightChild(topList, width, height);
      if (child != null)
      {
         child.setNumberOfTopChildren(topList, width, height);
         iChildren += child.getChildrenCount() + 1;
      }
   }

   public int getChildrenCount()
   {
      return iChildren;
   }

   public int getWidth()
   {
      return getHighestX() - getLowestX();
   }

   public int getHeight()
   {
      return getHighestY() - getLowestY();
   }

   public ColoredSquare getLeftChild(List<ColoredSquare> topList, int width,
         int height)
   {
      for (ColoredSquare square : topList)
      {
         // Acceptable below and to left
         // Acceptable size
         long oneWidth = square.getWidth();
         long oneHeight = square.getHeight();

         if (this != square && getColorType() != CubeColorEnum.UNUSED
               && square.getLowestX() < this.getLowestX()
               && square.getLowestY() > this.getLowestY()
               && square.getLowestX() > this.getLowestX() - width
               && square.getLowestY() < this.getLowestY()
                     + height * CubeConstants.TOP_HEIGHT_DIFF_MAX
               && Math.abs(oneWidth - width) <= width
                     * CubeConstants.MAX_SQUARE_SIZE_DIFF
               && Math.abs(oneHeight - height) <= height
                     * CubeConstants.MAX_SQUARE_SIZE_DIFF)
         {
            return square;
         }
      }
      return null;
   }

   public ColoredSquare getRightChild(List<ColoredSquare> topList, int width,
         int height)
   {
      for (ColoredSquare square : topList)
      {
         // Acceptable below and to right
         // Acceptable size
         long oneWidth = square.getWidth();
         long oneHeight = square.getHeight();

         if (this != square && getColorType() != CubeColorEnum.UNUSED
               && square.getLowestX() > this.getLowestX()
               && square.getLowestY() > this.getLowestY()
               && square.getLowestX() < this.getLowestX() + width
               && square.getLowestY() < this.getLowestY()
                     + height * CubeConstants.TOP_HEIGHT_DIFF_MAX
               && Math.abs(oneWidth - width) <= width
                     * CubeConstants.MAX_SQUARE_SIZE_DIFF
               && Math.abs(oneHeight - height) <= height
                     * CubeConstants.MAX_SQUARE_SIZE_DIFF)

         {
            return square;
         }
      }
      return null;
   }

   public void setPosition(CubeSquaresEnum cubeSquare)
   {
      iCubeSquare = cubeSquare;
   }

   public static void setIdealHeight(int height)
   {
      iIdealHeight = height;

   }

   public static void setIdealWidth(int width)
   {
      iIdealWidth = width;

   }

   public static int getIdealHeight()
   {
      return iIdealHeight;
   }

   public static int getIdealWidth()
   {
      return iIdealWidth;
   }

   public void setSolutionType(SolutionEnum value)
   {
      iSolutionType = value;
   }

   public SolutionEnum getSolutionType()
   {
      return iSolutionType;
   }
}
