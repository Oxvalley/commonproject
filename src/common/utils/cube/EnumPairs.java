package common.utils.cube;

public class EnumPairs
{

   private CubeSquaresEnum iEnum1;
   private CubeSquaresEnum iEnum2;

   public CubeSquaresEnum getEnum1()
   {
      return iEnum1;
   }

   public CubeSquaresEnum getEnum2()
   {
      return iEnum2;
   }

   public EnumPairs(CubeSquaresEnum enum1, CubeSquaresEnum enum2)
   {
      iEnum1 = enum1;
      iEnum2 = enum2;
      // TODO Auto-generated constructor stub
   }

}
