package common.utils.cube;

import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

public class DisplayImage
{

   private ImageComponent iComponent;
   private JScrollPane iScroll;
   private JFrame iFrame;

   public DisplayImage(String filePath, ImageComponent iComp) throws IOException
   {
      iComponent = iComp;
      BufferedImage img = ImageIO.read(new File(filePath));
      iFrame = new JFrame();
      iFrame.setLayout(null);
      iFrame.setSize(2000, 1000);
      iComp.setPreferredSize(new Dimension(img.getWidth(), img.getHeight()));
      iScroll = new JScrollPane();
      iScroll.setVerticalScrollBarPolicy(
            ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
      iScroll.setHorizontalScrollBarPolicy(
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      iScroll.setViewportView(iComp);
      // scroll.setSize(2000,2000);
      iComp.requestFocusInWindow();
      iFrame.add(iScroll);
      iFrame.setVisible(true);
      iFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

  
      iFrame.addComponentListener(new ComponentAdapter()
      {
         public void componentResized(ComponentEvent evt)
         {
            JFrame frame = (JFrame) evt.getSource();
            iComponent.setBounds(0, 0, frame.getWidth(), frame.getHeight());
            iScroll.setBounds(0, 0, frame.getWidth(), frame.getHeight());
            // ........
         }
      });
   }

   public void repaint()
   {
      iFrame.setSize(2600, 1000);
   }

   public void setTitle(String title)
   {
      if (iFrame != null)
      {
         iFrame.setTitle(title);
      }
   }

}