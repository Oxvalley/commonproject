package common.utils.cube;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Reconstructor
{
   private Map<CubeSquaresEnum, ColoredSquare> iFoundCorrectMap;
   private Rectangle iRect;
   private CubeSquaresEnum iMother;
   private List<ColoredSquare> iList;

   public Reconstructor(Map<CubeSquaresEnum, ColoredSquare> foundCorrectMap,
         CubeSquaresEnum mother, List<ColoredSquare> topList,
         RelationEnum childRelation)
   {
      iFoundCorrectMap = foundCorrectMap;
      iMother = mother;
      iList = topList;
      int x = getAverageX(childRelation);
      int y = getAverageY(childRelation);
      int width = getAverageWidth();
      int height = getAverageHeight();
      ColoredSquare motherSquare = getSquare(iMother);

      switch (childRelation)
      {
         case leftTopChild:
         case toLeftChild:
            iRect = new Rectangle(motherSquare.getLowestX() - x,
                  motherSquare.getLowestY() - y, width, height);
            break;

         case rightTopChild:
         case toRightLeftListChild:
            iRect = new Rectangle(motherSquare.getLowestX() + x,
                  motherSquare.getLowestY() + y, width, height);
            break;

         case downLeftListChild:
            iRect = new Rectangle(motherSquare.getLowestX(),
                  motherSquare.getLowestY() - y, width, height);
            break;

         case upLeftChild:
            iRect = new Rectangle(motherSquare.getLowestX(),
                  motherSquare.getLowestY() + y, width, height);
            break; 
            
         default:
            break;
      }
   }

   private int getAverageWidth()
   {
      int widthSum = 0;
      for (ColoredSquare square : iList)
      {
         widthSum += square.getWidth();
      }
      return Math.round(widthSum / iList.size());
   }

   private int getAverageHeight()
   {
      int heightSum = 0;
      for (ColoredSquare square : iList)
      {
         heightSum += square.getHeight();
      }
      return Math.round(heightSum / iList.size());
   }

   private int getAverageY(RelationEnum childRelation)
   {
      int count = 0;
      int xSum = 0;
      List<EnumPairs> xyList = getPairsList(childRelation);
      for (EnumPairs enumPair : xyList)
      {
         Integer x = getYDiff(enumPair.getEnum1(), enumPair.getEnum2());
         if (x != null)
         {
            xSum += x;
            count++;
         }
      }

      if (count == 0)
      {
         return 0;
      }
      else
      {
         return Math.round(xSum / count);
      }
   }

   
   private static List<EnumPairs> getLeftUpDownPairs()
   {
         List<EnumPairs> list = new ArrayList<>();
         list.add(new EnumPairs(CubeSquaresEnum.LEFT11, CubeSquaresEnum.LEFT21));
         list.add(new EnumPairs(CubeSquaresEnum.LEFT12, CubeSquaresEnum.LEFT22));
         list.add(new EnumPairs(CubeSquaresEnum.LEFT13, CubeSquaresEnum.LEFT23));
         list.add(new EnumPairs(CubeSquaresEnum.LEFT21, CubeSquaresEnum.LEFT31));
         list.add(new EnumPairs(CubeSquaresEnum.LEFT22, CubeSquaresEnum.LEFT32));
         list.add(new EnumPairs(CubeSquaresEnum.LEFT23, CubeSquaresEnum.LEFT33));
         return list;
   }

   private static List<EnumPairs> getPairsList(RelationEnum childRelation)
   {
      switch (childRelation)
      {
         case leftTopChild:
         case rightTopChild:
            return getTopXYPairs();

         case toLeftChild:
         case toRightLeftListChild:
            return getLeftXYPairs();


         case downLeftListChild:
         case upLeftChild:
            return getLeftUpDownPairs();

         default:
            break;
      }

      return null;
   }

   private int getAverageX(RelationEnum childRelation)
   {
      int count = 0;
      int xSum = 0;
      List<EnumPairs> xyList = getPairsList(childRelation);
      for (EnumPairs enumPair : xyList)
      {
         Integer x = getXDiff(enumPair.getEnum1(), enumPair.getEnum2());
         if (x != null)
         {
            xSum += x;
            count++;
         }
      }

      if (count == 0)
      {
         return 0;
      }
      else
      {
         return Math.round(xSum / count);
      }

   }

   private static List<EnumPairs> getTopXYPairs()
   {
      List<EnumPairs> list = new ArrayList<>();
      list.add(new EnumPairs(CubeSquaresEnum.TOP1, CubeSquaresEnum.TOP21));
      list.add(new EnumPairs(CubeSquaresEnum.TOP1, CubeSquaresEnum.TOP22));
      list.add(new EnumPairs(CubeSquaresEnum.TOP21, CubeSquaresEnum.TOP31));
      list.add(new EnumPairs(CubeSquaresEnum.TOP21, CubeSquaresEnum.TOP32));
      list.add(new EnumPairs(CubeSquaresEnum.TOP22, CubeSquaresEnum.TOP32));
      list.add(new EnumPairs(CubeSquaresEnum.TOP22, CubeSquaresEnum.TOP33));
      list.add(new EnumPairs(CubeSquaresEnum.TOP31, CubeSquaresEnum.TOP41));
      list.add(new EnumPairs(CubeSquaresEnum.TOP32, CubeSquaresEnum.TOP41));
      list.add(new EnumPairs(CubeSquaresEnum.TOP32, CubeSquaresEnum.TOP42));
      list.add(new EnumPairs(CubeSquaresEnum.TOP33, CubeSquaresEnum.TOP42));
      list.add(new EnumPairs(CubeSquaresEnum.TOP41, CubeSquaresEnum.TOP5));
      list.add(new EnumPairs(CubeSquaresEnum.TOP42, CubeSquaresEnum.TOP5));
      return list;
   }

   private static List<EnumPairs> getLeftXYPairs()
   {
      List<EnumPairs> list = new ArrayList<>();
      list.add(new EnumPairs(CubeSquaresEnum.LEFT11, CubeSquaresEnum.LEFT12));
      list.add(new EnumPairs(CubeSquaresEnum.LEFT12, CubeSquaresEnum.LEFT13));
      list.add(new EnumPairs(CubeSquaresEnum.LEFT21, CubeSquaresEnum.LEFT22));
      list.add(new EnumPairs(CubeSquaresEnum.LEFT22, CubeSquaresEnum.LEFT23));
      list.add(new EnumPairs(CubeSquaresEnum.LEFT31, CubeSquaresEnum.LEFT32));
      list.add(new EnumPairs(CubeSquaresEnum.LEFT32, CubeSquaresEnum.LEFT33));
      return list;
   }

   private Integer getXDiff(CubeSquaresEnum key1, CubeSquaresEnum key2)
   {
      ColoredSquare square1 = getSquare(key1);
      if (square1 == null)
      {
         return null;
      }

      ColoredSquare square2 = getSquare(key2);
      if (square2 == null)
      {
         return null;
      }

      return Math.abs(square1.getLowestX() - square2.getLowestX());
   }

   private Integer getYDiff(CubeSquaresEnum key1, CubeSquaresEnum key2)
   {
      ColoredSquare square1 = getSquare(key1);
      if (square1 == null)
      {
         return null;
      }

      ColoredSquare square2 = getSquare(key2);
      if (square2 == null)
      {
         return null;
      }

      return Math.abs(square1.getLowestY() - square2.getLowestY());
   }

   private ColoredSquare getSquare(CubeSquaresEnum key)
   {
      return iFoundCorrectMap.get(key);
   }

   public Rectangle getResultRect()
   {
      return iRect;
   }

}
