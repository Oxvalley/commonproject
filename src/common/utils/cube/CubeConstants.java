package common.utils.cube;

public class CubeConstants
{

   public static final double ANGLE_VERTICAL = 1000;
   public static final double ANGLE_HORIZONTAL = 2000;

   public static final double ANGLE_DOWN_RIGHT = 0.88;

   public static final int VOID_COUNT_IN_COLOR_SEQUENCE = 0;
   public static final double ANGLE_DOWN_LEFT = -ANGLE_DOWN_RIGHT;

   public static final int TEST_RECT_HIGHT = 200;
   public static final int TEST_RECT_WIDTH = 360;
   public static final int TEST_RECT_Y = 490;
   public static final int TEST_RECT_X = 2550;
   public static final double HEIGHT_DIFF_ACCEPTED = 0.3;
   public static final double TOP_HEIGHT_DIFF_MAX = .7;
   public static final double MAX_SQUARE_SIZE_DIFF = .6;
   public static final int LEFT_MARGIN = 20;
   public static final double SQUARES_MAX_APART_FACTOR = 1.5;
   public static double LEFT_VERTICAL_K = -9.6;

   public static double LEFT_RECTANGLE_ADDED_HEIGHT_IN_SQUARE_PERCENT = .09;
   public static double LEFT_RECTANGLE_EXTRA_HEIGHT_IN_SQUARE_PERCENT = 0.5;

   public static double LEFT_COLUMN_1_PERCENT = 0.2;
   public static double LEFT_COLUMN_2_PERCENT = 0.6;
   public static double LEFT_COLUMN_3_PERCENT = 0.83;

   public static double PERCENTAGE_OF_CORNER_FREE = 4.7; // 0.01 - 9.4 -> 4.7

   public static int MIN_PIXELS_SQUARE = 45; // 1 - 89 -> 45

   public static int MIN_COLOR_SEQUENCE = 72; // 63-74 -> 72
   public static int MAX_COLOR_SEQUENCE = 328; // 311-330 -> 328
   public static int SEQUENCES_FOUND_COUNT = 2;

   public static int LEFT_Y_PARTS = 8;
   public static double LEFT_Y_OVERLAP_PERCENT = -.15;
   public static double LEFT_K_ADJUSTER = 1.2;

   public static int GRID_SIZE = 21;
   // public static double POINTS_LIMIT = 0.22;
   public static double SPLIT_PERCENTIGE_COLOR = .0276;
   public static int COLOR_VARIANT_EXCLUDED = -1;

}
