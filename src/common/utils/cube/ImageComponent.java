package common.utils.cube;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

public class ImageComponent extends JComponent
{
   private BufferedImage image;
   private List<Pixel> lst = new ArrayList<>();
   private List<ColoredSquare> iSquaresList;
   private boolean drawOne = false;
   private boolean drawThree = true;
   private List<ColoredSquare> iTopList;
   private Rectangle iLeftRec = null;
   List<ColoredSquare> iLeftList;
   List<ColoredSquare> iRightList;
   private List<DrawLine> iDrawLines;
   private List<Point> iPointsList;
   private Rectangle iRightRec;

   public ImageComponent(BufferedImage img, List<ColoredSquare> squaresList,
         List<ColoredSquare> topList, List<ColoredSquare> leftList,
         List<ColoredSquare> rightList, List<DrawLine> drawLines,
         List<Point> pointsList)
   {
      image = img;
      iSquaresList = squaresList;
      iTopList = topList;
      iLeftList = leftList;
      iRightList = rightList;
      iDrawLines = drawLines;
      iPointsList = pointsList;
   }

   public ImageComponent()
   {
      // TODO Auto-generated constructor stub
   }

   @Override
   public void paintComponent(Graphics g)
   {
      g.drawImage(image, 3, 4, this);

      if (drawOne)
      {
         for (Pixel dim : lst)
         {
            image.setRGB(dim.getX(), dim.getY(), dim.getColor().getRGB());
         }
      }

      if (drawThree)
      {

         // All sequences that cross each other
         for (ColoredSquare colSquare : iSquaresList)
         {

            // if (CubeSolver.iTestRecs.contains(colSquare))
            // {
            // int x = colSquare.getLowestX();
            // int y = colSquare.getLowestY();
            // int x2 = colSquare.getHighestX();
            // int y2 = colSquare.getHighestY();
            //
            // g.setColor(Color.ORANGE);
            // g.drawRect(x, y, x2 - x, y2 - y);
            // }

            for (ColorSequence colorSequence : colSquare.getList())
            {
               g.setColor(CubeHelper.getConstrastColor(
                     colSquare.getAverageColor(), Color.WHITE, Color.BLACK));

               g.drawLine(colorSequence.getStartPosX(),
                     colorSequence.getStartPosY(), colorSequence.getEndPosX(),
                     colorSequence.getEndPosY());
            }
         }

         // g.setColor(Color.GREEN);
         // g.drawRect(CubeConstants.TEST_RECT_X, CubeConstants.TEST_RECT_Y,
         // CubeConstants.TEST_RECT_WIDTH, CubeConstants.TEST_RECT_HIGHT);

         if (iTopList != null)
         {
            for (ColoredSquare colSquare : iTopList)
            {
               int x = colSquare.getLowestX();
               int y = colSquare.getLowestY();
               int x2 = colSquare.getHighestX();
               int y2 = colSquare.getHighestY();

               Color color = getSolutionColor(colSquare);
               g.setColor(color);
               g.drawRect(x, y, x2 - x, y2 - y);

               // Top Left Corner
               int pixels = colSquare.getTopLeftPixels();
               for (int i = 0; i <= pixels; i++)
               {
                  g.drawLine(x + i, y, x, y + i);
               }
               // Top Right Corner
               pixels = colSquare.getTopRightPixels();
               for (int i = 0; i <= pixels; i++)
               {
                  g.drawLine(x2 - i, y, x2, y + i);
               }
               // Bottom Left Corner
               pixels = colSquare.getBottomLeftPixels();
               for (int i = 0; i <= pixels; i++)
               {
                  g.drawLine(x + i, y2, x, y2 - i);
               }
               // Bottom Right Corner
               pixels = colSquare.getBottomRightPixels();
               for (int i = 0; i <= pixels; i++)
               {
                  g.drawLine(x2 - i, y2, x2, y2 - i);
               }

               g.setColor(CubeHelper.getConstrastColor(
                     colSquare.getAverageColor(), Color.WHITE, Color.BLACK));
               g.drawString(colSquare.getColorType().toString(),
                     x + (x2 - x) / 2, y + (y2 - y) / 2 - 12);

               String rgb = "(" + colSquare.getAverageColor().getRed() + ", "
                     + colSquare.getAverageColor().getGreen() + ", "
                     + colSquare.getAverageColor().getBlue() + ") ";
               g.drawString(rgb, x + (x2 - x) / 2 - 12, y + (y2 - y) / 2 + 2);
            }
         }

         if (iPointsList != null)
         {
            for (Point point : iPointsList)
            {
               g.fillOval((int) point.getX(), (int) point.getY(), 10, 10);
            }
         }

         if (iLeftList != null)
         {

            for (ColoredSquare colSquare : iLeftList)
            {
               int x = colSquare.getLowestX();
               int y = colSquare.getLowestY();
               int x2 = colSquare.getHighestX();
               int y2 = colSquare.getHighestY();

               Color color = getSolutionColor(colSquare);
               // Color color = Color.WHITE;
               g.setColor(color);
               Graphics2D g2 = (Graphics2D) g;
               double thickness = 3;
               Stroke oldStroke = g2.getStroke();
               g2.setStroke(new BasicStroke((float) thickness));
               g2.drawRect(x, y, x2 - x, y2 - y);
               g2.setStroke(oldStroke);

               g.setColor(CubeHelper.getConstrastColor(
                     colSquare.getAverageColor(), Color.WHITE, Color.BLACK));
               g.drawString(colSquare.getColorType().toString(),
                     x + (x2 - x) / 2, y + (y2 - y) / 2 - 12);

               String rgb = "(" + colSquare.getAverageColor().getRed() + ", "
                     + colSquare.getAverageColor().getGreen() + ", "
                     + colSquare.getAverageColor().getBlue() + ") ";
               g.drawString(rgb, x + (x2 - x) / 2 - 12, y + (y2 - y) / 2 + 2);
            }
         }

         if (iRightRec != null)
         {
            g.setColor(Color.GREEN);
            Graphics2D g2 = (Graphics2D) g;
            double thickness = 3;
            Stroke oldStroke = g2.getStroke();
            g2.setStroke(new BasicStroke((float) thickness));
            g.drawRect(iRightRec.x, iRightRec.y, iRightRec.width,
                  iRightRec.height);
            g2.setStroke(oldStroke);
         }

         if (iRightList != null)
         {

            for (ColoredSquare colSquare : iRightList)
            {
               int x = colSquare.getLowestX();
               int y = colSquare.getLowestY();
               int x2 = colSquare.getHighestX();
               int y2 = colSquare.getHighestY();

               Color color = getSolutionColor(colSquare);
               // Color color = Color.WHITE;
               g.setColor(color);
               Graphics2D g2 = (Graphics2D) g;
               double thickness = 3;
               Stroke oldStroke = g2.getStroke();
               g2.setStroke(new BasicStroke((float) thickness));
               g2.drawRect(x, y, x2 - x, y2 - y);
               g2.setStroke(oldStroke);

               g.setColor(CubeHelper.getConstrastColor(
                     colSquare.getAverageColor(), Color.WHITE, Color.BLACK));
               g.drawString(colSquare.getColorType().toString(),
                     x + (x2 - x) / 2, y + (y2 - y) / 2 - 12);

               String rgb = "(" + colSquare.getAverageColor().getRed() + ", "
                     + colSquare.getAverageColor().getGreen() + ", "
                     + colSquare.getAverageColor().getBlue() + ") ";
               g.drawString(rgb, x + (x2 - x) / 2 - 12, y + (y2 - y) / 2 + 2);
            }
         }

         if (iLeftRec != null)
         {
            g.setColor(Color.GREEN);
            Graphics2D g2 = (Graphics2D) g;
            double thickness = 3;
            Stroke oldStroke = g2.getStroke();
            g2.setStroke(new BasicStroke((float) thickness));
            g.drawRect(iLeftRec.x, iLeftRec.y, iLeftRec.width, iLeftRec.height);
            g2.setStroke(oldStroke);
         }

         for (DrawLine dl : iDrawLines)
         {
            g.setColor(dl.getColor());
            g.drawLine(dl.getX1(), dl.getY1(), dl.getX2(), dl.getY2());
         }

      }
   }

   private static Color getSolutionColor(ColoredSquare colSquare)
   {
      switch (colSquare.getSolutionType())
      {
         case NORMAL:
            return Color.YELLOW;

         case DEEP:
            return Color.ORANGE;

         case RECONSTRUCTED:
            return Color.GREEN;

         case NOT_AT_ALL:
            return Color.RED;

         default:
            break;
      }
      return null;
   }

   public void plot(int x, int y, Color color)
   {
      if (x >= 0 && y >= 0)
      {
         Pixel dim = new Pixel(x, y, color);
         lst.add(dim);
      }
   }

   public void setLeftRect(Rectangle leftRect)
   {
      iLeftRec = leftRect;
   }

   public void setRightRect(Rectangle rightRect)
   {
      iRightRec = rightRect;
   }

}
