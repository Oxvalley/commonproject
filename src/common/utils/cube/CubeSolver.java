package common.utils.cube;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.imageio.ImageIO;

public class CubeSolver
{
   private List<ColorSequence> iSeqList = new ArrayList<>();
   private List<DrawLine> iDrawLines = new ArrayList<>();
   private BufferedImage iImage;
   private int iImageWidth;
   private int iImageHeight;
   private ImageComponent iImageComponent;
   private List<ColoredSquare> iSquaresList = new ArrayList<>();
   private DisplayImage iDisplayImage;
   private List<ColoredSquare> iTopList = new ArrayList<>();
   public static ArrayList<ColoredSquare> iTestRecs = new ArrayList<>();
   private String iFileName;
   private int iPictureNumber;
   private Map<CubeSquaresEnum, ColoredSquare> iFoundCorrectMap =
         new TreeMap<>();
   private Rectangle iLeftRect;
   private List<ColoredSquare> iLeftList = new ArrayList<>();
   private Rectangle iRightRect;
   private List<ColoredSquare> iRightList = new ArrayList<>();
   private List<CrossDistance> iClosestList = new ArrayList<>();
   private double heigth;
   private long iTotalLeftDistances = 0;
   private boolean iShowUI = true;
   private String iResultString = null;
   private int iReconstructedCount = 0;
   private int iDeeperFoundCount = 0;

   public List<Point> iPointsList = new ArrayList<Point>();

   // kociemba.org
   // https://rubiks-cube-solver.com/solution.php?cube=0346461224165236533613525665635151451434446352231212142&x=1

   public Map<CubeSquaresEnum, ColoredSquare> getFoundCorrectMap()
   {
      return iFoundCorrectMap;
   }

   public CubeTesterOutput findColorsFromOnePicture(String fileName,
         int pictureNumber)
      throws IOException
   {
      iFileName = fileName;
      iPictureNumber = pictureNumber;
      iSeqList = findColorSequences(iFileName);
      iSquaresList = findColoredSquares();
      setFreeCorners();
      findTopSquares();
      findLeftSquares();
      findRightSquares();
      repaint();
      CubeTesterOutput output = getResult();
      if (iShowUI)
      {
         System.out.println(getResultString());
      }
      return output;
   }

   private CubeTesterOutput getResult()
   {
      return new CubeTesterOutput(getResultString(), getReconstructedCount(),
            getDeeperFoundCount(), getLeftRightDiffs());
   }

   private long getLeftRightDiffs()
   {
      return iTotalLeftDistances;
   }

   private int getDeeperFoundCount()
   {
      return iDeeperFoundCount;
   }

   private int getReconstructedCount()
   {
      return iReconstructedCount;
   }

   private String getResultString()
   {
      if (iResultString == null)
      {
         iResultString = createResultString();
      }
      return iResultString;
   }

   private String createResultString()
   {
      String resultString =
            "------------------------------------------------------";
      for (CubeSquaresEnum enumKey : iFoundCorrectMap.keySet())
      {
         String newResultString = "";
         int i = 0;
         if (iPictureNumber == 1)
         {
            i = enumKey.getPlacePicture1() - 1;
         }
         else
         {
            i = enumKey.getPlacePicture2() - 1;
         }

         ColoredSquare value = iFoundCorrectMap.get(enumKey);

         CubeColorEnum colorEnum = value.getColorType();

         // if (enumKey == CubeSquaresEnum.LEFT11)
         // {
         // colorEnum = CubeColorEnum.YELLOW;
         // }

         if (i > 0)
         {
            newResultString += resultString.substring(0, i);
         }
         newResultString += colorEnum.getAbbriviation();
         if (newResultString.length() < resultString.length())
         {
            newResultString += resultString.substring(i + 1);
         }

         resultString = newResultString;

      }
      return resultString;
   }

   private void findLeftSquares()
   {
      iLeftList = getLeftList();
      if (iLeftList.size() == 0)
      {
         return;
      }

      CubeHelper.calcWidthToOthers(iLeftList);
      CubeHelper.calcHeightToOthers(iLeftList);
      Collections.sort(iLeftList, Comparators.widthHeightComparator);
      CubeHelper.setPlacementPoints(iLeftList, "Left List Size difference:", 3,
            iShowUI);
      Collections.sort(iLeftList, Comparators.placementComparator);
      listNineAndTheRest(iLeftList);
      int height = getAverageHeight(iLeftList, 4);
      int width = getAverageWeight(iLeftList, 4);
      double k = getTopK() * CubeConstants.LEFT_K_ADJUSTER;
      double k2 = CubeConstants.LEFT_VERTICAL_K;
      int[] yArr = new int[3];
      int[] mArr = new int[3];

      double overlapY = height * CubeConstants.LEFT_Y_OVERLAP_PERCENT;
      double x1 = iLeftRect.getX();
      double sameX = x1;
      double x2 = iLeftRect.getX() + iLeftRect.getWidth();
      double compensateY = height
            * CubeConstants.LEFT_RECTANGLE_ADDED_HEIGHT_IN_SQUARE_PERCENT;
      double y1 = iLeftRect.getY()
            + height
                  * CubeConstants.LEFT_RECTANGLE_EXTRA_HEIGHT_IN_SQUARE_PERCENT
            + overlapY + compensateY;
      double y2 = y1 + (iLeftRect.getWidth() * k);
      iLeftList.clear();
      Color color = Color.RED;

      yArr[0] = (int) Math.round(y1);
      mArr[0] = (int) (y1 - x1 * k);

      // System.out.println(x1 + ", " + y1 + " -0- " + x2 + ", " + y2);
      iDrawLines.add(new DrawLine(x1, y1, x2, y2, color));
      y1 += height + overlapY;
      y2 += height + overlapY;

      yArr[1] = (int) Math.round(y1);
      mArr[1] = (int) (y1 - x1 * k);
      // System.out.println(x1 + ", " + y1 + " -1- " + x2 + ", " + y2);
      iDrawLines.add(new DrawLine(x1, y1, x2, y2, color));
      y1 += height + overlapY;
      y2 += height + overlapY;

      yArr[2] = (int) Math.round(y1);
      mArr[2] = (int) (y1 - x1 * k);
      // System.out.println(x1 + ", " + y1 + " -2- " + x2 + ", " + y2);
      iDrawLines.add(new DrawLine(x1, y1, x2, y2, color));

      y1 = iLeftRect.getY();
      y2 = y1 + iLeftRect.getHeight();
      double rectWidth = iLeftRect.getWidth();
      // double eight = rectWidth / CubeConstants.LEFT_Y_PARTS;

      x1 = sameX + rectWidth * CubeConstants.LEFT_COLUMN_1_PERCENT;
      x2 = x1;

      double x22 = x1 + (y2 - y1) / k2;
      double m2 = -((k2 * x1) - y1);
      // System.out.println("k=" + k + ", m=" + mArr[0] + ", k2=" + k2 + ", m2="
      // + m2);
      setLeftSquare(k, mArr[0], k2, m2, CubeSquaresEnum.LEFT11);
      setLeftSquare(k, mArr[1], k2, m2, CubeSquaresEnum.LEFT21);
      setLeftSquare(k, mArr[2], k2, m2, CubeSquaresEnum.LEFT31);
      iDrawLines.add(new DrawLine(x1, y1, x22, y2, color));
      // System.out.println(x1 + ", " + y1 + " - " + x22 + ", " + y2);

      // setLeftSquare(x1, yArr[0] + (x1 - sameX) * k, CubeSquaresEnum.LEFT11);
      // setLeftSquare(x1, yArr[1] + (x1 - sameX) * k, CubeSquaresEnum.LEFT21);
      // setLeftSquare(x1, yArr[2] + (x1 - sameX) * k, CubeSquaresEnum.LEFT31);
      // iDrawLines.add(new DrawLine(x1, y1, x2, y2, color));

      x1 = sameX + rectWidth * CubeConstants.LEFT_COLUMN_2_PERCENT;
      x2 = x1;
      x22 = x1 + (y2 - y1) / k2;
      m2 = -((k2 * x1) - y1);
      // System.out.println("k=" + k + ", m=" + mArr[0] + ", k2=" + k2 + ", m2="
      // + m2);
      setLeftSquare(k, mArr[0], k2, m2, CubeSquaresEnum.LEFT12);
      setLeftSquare(k, mArr[1], k2, m2, CubeSquaresEnum.LEFT22);
      setLeftSquare(k, mArr[2], k2, m2, CubeSquaresEnum.LEFT32);
      iDrawLines.add(new DrawLine(x1, y1, x22, y2, color));
      // System.out.println(x1 + ", " + y1 + " - " + x22 + ", " + y2);

      x1 = sameX + rectWidth * CubeConstants.LEFT_COLUMN_3_PERCENT;
      x2 = x1;
      x22 = x1 + (y2 - y1) / k2;
      m2 = -((k2 * x1) - y1);
      // System.out.println("k=" + k + ", m=" + mArr[0] + ", k2=" + k2 + ", m2="
      // + m2);
      setLeftSquare(k, mArr[0], k2, m2, CubeSquaresEnum.LEFT13);
      setLeftSquare(k, mArr[1], k2, m2, CubeSquaresEnum.LEFT23);
      setLeftSquare(k, mArr[2], k2, m2, CubeSquaresEnum.LEFT33);
      iDrawLines.add(new DrawLine(x1, y1, x22, y2, color));
      // System.out.println(x1 + ", " + y1 + " - " + x22 + ", " + y2);

      Collections.sort(iClosestList, Comparators.closestComparator);
      while (iClosestList.size() > 0)
      {
         CrossDistance cd = iClosestList.remove(0);
         ColoredSquare square = cd.getSquare();
         if (square != null)
         {
            iLeftList.add(square);
            setCubePlacement(square, cd.getPlacement());
         }

         for (CrossDistance cross : iClosestList)
         {
            ColoredSquare sq = cross.getSquare();
            if (sq != null && sq == square)
            {
               // Find centre closest to this point that is still not taken
               ColoredSquare square2 = getCenterClosestTo(cross.getX(),
                     cross.getY(), iSquaresList, iLeftList);
               Point p = square.getMiddle();
               if (Math.abs(cross.getX()
                     - p.x) > CubeConstants.SQUARES_MAX_APART_FACTOR * width
                     || Math.abs(cross.getY()
                           - p.y) > CubeConstants.SQUARES_MAX_APART_FACTOR
                                 * heigth)
               {
                  cross.setSquare(null);
               }
               else
               {
                  int distance = (int) CubeHelper.getDistance(cross.getX(),
                        cross.getY(), p.x, p.y);
                  cross.setDistance(distance);
                  cross.setSquare(square2);
               }
            }
         }

         // Sort again
         Collections.sort(iClosestList, Comparators.closestComparator);
      }

      if (iLeftList.size() < 2)
      {
         // boolean old = iShowUI;
         // iShowUI = true;
         // listSolved();
         // iShowUI = old;
         System.err.println("Can not reconstruct left side. To few placed: "
               + iLeftList.size());
         return;
      }

      reconstruct(CubeSquaresEnum.LEFT11);
      reconstruct(CubeSquaresEnum.LEFT12);
      reconstruct(CubeSquaresEnum.LEFT13);
      reconstruct(CubeSquaresEnum.LEFT21);
      reconstruct(CubeSquaresEnum.LEFT22);
      reconstruct(CubeSquaresEnum.LEFT23);
      reconstruct(CubeSquaresEnum.LEFT31);
      reconstruct(CubeSquaresEnum.LEFT32);
      reconstruct(CubeSquaresEnum.LEFT33);
      recalcList(iLeftList);

      listSolved();

      // removeAfterNine(iLeftList);

   }

   private void findRightSquares()
   {
      iRightList = getRightList();
      if (iRightList.size() == 0)
      {
         return;
      }

      CubeHelper.calcWidthToOthers(iRightList);
      CubeHelper.calcHeightToOthers(iRightList);
      Collections.sort(iRightList, Comparators.widthHeightComparator);
      CubeHelper.setPlacementPoints(iRightList, "Right List Size difference:",
            3, iShowUI);
      Collections.sort(iRightList, Comparators.placementComparator);
      listNineAndTheRest(iRightList);
      int height = getAverageHeight(iRightList, 4);
      int width = getAverageWeight(iRightList, 4);
      double k = getTopK() * CubeConstants.LEFT_K_ADJUSTER;
      double k2 = CubeConstants.LEFT_VERTICAL_K;
      int[] yArr = new int[3];
      int[] mArr = new int[3];

      double overlapY = height * CubeConstants.LEFT_Y_OVERLAP_PERCENT;
      double x1 = iRightRect.getX();
      double sameX = x1;
      double x2 = iRightRect.getX() + iRightRect.getWidth();
      double compensateY = height
            * CubeConstants.LEFT_RECTANGLE_ADDED_HEIGHT_IN_SQUARE_PERCENT;
      double y1 = iRightRect.getY()
            + height
                  * CubeConstants.LEFT_RECTANGLE_EXTRA_HEIGHT_IN_SQUARE_PERCENT
            + overlapY + compensateY;
      double y2 = y1 + (iRightRect.getWidth() * k);
      iRightList.clear();
      Color color = Color.RED;

      yArr[0] = (int) Math.round(y1);
      mArr[0] = (int) (y1 - x1 * k);

      // System.out.println(x1 + ", " + y1 + " -0- " + x2 + ", " + y2);
      iDrawLines.add(new DrawLine(x1, y1, x2, y2, color));
      y1 += height + overlapY;
      y2 += height + overlapY;

      yArr[1] = (int) Math.round(y1);
      mArr[1] = (int) (y1 - x1 * k);
      // System.out.println(x1 + ", " + y1 + " -1- " + x2 + ", " + y2);
      iDrawLines.add(new DrawLine(x1, y1, x2, y2, color));
      y1 += height + overlapY;
      y2 += height + overlapY;

      yArr[2] = (int) Math.round(y1);
      mArr[2] = (int) (y1 - x1 * k);
      // System.out.println(x1 + ", " + y1 + " -2- " + x2 + ", " + y2);
      iDrawLines.add(new DrawLine(x1, y1, x2, y2, color));

      y1 = iRightRect.getY();
      y2 = y1 + iRightRect.getHeight();
      double rectWidth = iRightRect.getWidth();
      // double eight = rectWidth / CubeConstants.LEFT_Y_PARTS;

      x1 = sameX + rectWidth * CubeConstants.LEFT_COLUMN_1_PERCENT;
      x2 = x1;

      double x22 = x1 + (y2 - y1) / k2;
      double m2 = -((k2 * x1) - y1);
      // System.out.println("k=" + k + ", m=" + mArr[0] + ", k2=" + k2 + ", m2="
      // + m2);
      setLeftSquare(k, mArr[0], k2, m2, CubeSquaresEnum.RIGHT11);
      setLeftSquare(k, mArr[1], k2, m2, CubeSquaresEnum.RIGHT21);
      setLeftSquare(k, mArr[2], k2, m2, CubeSquaresEnum.RIGHT31);
      iDrawLines.add(new DrawLine(x1, y1, x22, y2, color));
      // System.out.println(x1 + ", " + y1 + " - " + x22 + ", " + y2);

      // setLeftSquare(x1, yArr[0] + (x1 - sameX) * k, CubeSquaresEnum.LEFT11);
      // setLeftSquare(x1, yArr[1] + (x1 - sameX) * k, CubeSquaresEnum.LEFT21);
      // setLeftSquare(x1, yArr[2] + (x1 - sameX) * k, CubeSquaresEnum.LEFT31);
      // iDrawLines.add(new DrawLine(x1, y1, x2, y2, color));

      x1 = sameX + rectWidth * CubeConstants.LEFT_COLUMN_2_PERCENT;
      x2 = x1;
      x22 = x1 + (y2 - y1) / k2;
      m2 = -((k2 * x1) - y1);
      // System.out.println("k=" + k + ", m=" + mArr[0] + ", k2=" + k2 + ", m2="
      // + m2);
      setLeftSquare(k, mArr[0], k2, m2, CubeSquaresEnum.RIGHT12);
      setLeftSquare(k, mArr[1], k2, m2, CubeSquaresEnum.RIGHT22);
      setLeftSquare(k, mArr[2], k2, m2, CubeSquaresEnum.RIGHT32);
      iDrawLines.add(new DrawLine(x1, y1, x22, y2, color));
      // System.out.println(x1 + ", " + y1 + " - " + x22 + ", " + y2);

      x1 = sameX + rectWidth * CubeConstants.LEFT_COLUMN_3_PERCENT;
      x2 = x1;
      x22 = x1 + (y2 - y1) / k2;
      m2 = -((k2 * x1) - y1);
      // System.out.println("k=" + k + ", m=" + mArr[0] + ", k2=" + k2 + ", m2="
      // + m2);
      setLeftSquare(k, mArr[0], k2, m2, CubeSquaresEnum.RIGHT13);
      setLeftSquare(k, mArr[1], k2, m2, CubeSquaresEnum.RIGHT23);
      setLeftSquare(k, mArr[2], k2, m2, CubeSquaresEnum.RIGHT33);
      iDrawLines.add(new DrawLine(x1, y1, x22, y2, color));
      // System.out.println(x1 + ", " + y1 + " - " + x22 + ", " + y2);

      Collections.sort(iClosestList, Comparators.closestComparator);
      while (iClosestList.size() > 0)
      {
         CrossDistance cd = iClosestList.remove(0);
         ColoredSquare square = cd.getSquare();
         if (square != null)
         {
            iRightList.add(square);
            setCubePlacement(square, cd.getPlacement());
         }

         for (CrossDistance cross : iClosestList)
         {
            ColoredSquare sq = cross.getSquare();
            if (sq != null && sq == square)
            {
               // Find centre closest to this point that is still not taken
               ColoredSquare square2 = getCenterClosestTo(cross.getX(),
                     cross.getY(), iSquaresList, iRightList);
               Point p = square.getMiddle();
               if (Math.abs(cross.getX()
                     - p.x) > CubeConstants.SQUARES_MAX_APART_FACTOR * width
                     || Math.abs(cross.getY()
                           - p.y) > CubeConstants.SQUARES_MAX_APART_FACTOR
                                 * heigth)
               {
                  cross.setSquare(null);
               }
               else
               {
                  int distance = (int) CubeHelper.getDistance(cross.getX(),
                        cross.getY(), p.x, p.y);
                  cross.setDistance(distance);
                  cross.setSquare(square2);
               }
            }
         }

         // Sort again
         Collections.sort(iClosestList, Comparators.closestComparator);
      }

      if (iRightList.size() < 2)
      {
         // boolean old = iShowUI;
         // iShowUI = true;
         // listSolved();
         // iShowUI = old;
         System.err.println("Can not reconstruct Right side. To few placed: "
               + iRightList.size());
         return;
      }

      reconstruct(CubeSquaresEnum.RIGHT11);
      reconstruct(CubeSquaresEnum.RIGHT12);
      reconstruct(CubeSquaresEnum.RIGHT13);
      reconstruct(CubeSquaresEnum.RIGHT21);
      reconstruct(CubeSquaresEnum.RIGHT22);
      reconstruct(CubeSquaresEnum.RIGHT23);
      reconstruct(CubeSquaresEnum.RIGHT31);
      reconstruct(CubeSquaresEnum.RIGHT32);
      reconstruct(CubeSquaresEnum.RIGHT33);
      recalcList(iRightList);

      listSolved();

      // removeAfterNine(iLeftList);

   }

   //

   private void setLeftSquare(double k1, double m1, double k2, double m2,
         CubeSquaresEnum placement)
   {
      Point point = CubeHelper.calculateIntersectionPoint(k1, m1, k2, m2);
      iPointsList.add(point);
      // System.out.println(point);

      // Find center closest to this point
      ColoredSquare square = getCenterClosestTo((int) point.getX(),
            (int) point.getY(), iSquaresList, iLeftList);
      Point p = square.getMiddle();
      int distance = (int) CubeHelper.getDistance((int) point.getX(),
            (int) point.getY(), p.x, p.y);
      iTotalLeftDistances += distance;
      CrossDistance cd = new CrossDistance((int) point.getX(),
            (int) point.getY(), distance, square, placement);
      iClosestList.add(cd);

   }

   private void setLeftSquare(double x, double y, CubeSquaresEnum placement)
   {
      // Find center closest to this point
      ColoredSquare square =
            getCenterClosestTo((int) x, (int) y, iSquaresList, iLeftList);
      Point p = square.getMiddle();
      int distance = (int) CubeHelper.getDistance((int) x, (int) y, p.x, p.y);
      iTotalLeftDistances += distance;
      CrossDistance cd =
            new CrossDistance((int) x, (int) y, distance, square, placement);
      iClosestList.add(cd);

   }

   private static ColoredSquare getCenterClosestTo(int x, int y,
         List<ColoredSquare> squaresList, List<ColoredSquare> usedList)
   {
      int closest = Integer.MAX_VALUE;
      ColoredSquare closestSquare = null;

      for (ColoredSquare square : squaresList)
      {
         if (!usedList.contains(square)
               && square.getColorType() != CubeColorEnum.UNUSED)
         {
            Point p = square.getMiddle();
            int distance = (int) CubeHelper.getDistance(x, y, p.x, p.y);
            if (distance < closest)
            {
               closest = distance;
               closestSquare = square;
            }
         }
      }

      return closestSquare;
   }

   private void removeCubePlacement(CubeSquaresEnum key)
   {
      iFoundCorrectMap.remove(key);
   }

   private CubeSquaresEnum getKeyFromMap(ColoredSquare closestSquare)
   {
      for (CubeSquaresEnum key : iFoundCorrectMap.keySet())
      {
         ColoredSquare square = iFoundCorrectMap.get(key);
         if (square == closestSquare)
         {
            return key;
         }
      }

      return null;
   }

   private double getTopK()
   {
      // Find best k in kx+m
      double totK = 0.0;
      totK += getK(getSquare(CubeSquaresEnum.TOP1),
            getSquare(CubeSquaresEnum.TOP22));
      totK += getK(getSquare(CubeSquaresEnum.TOP22),
            getSquare(CubeSquaresEnum.TOP33));
      totK += getK(getSquare(CubeSquaresEnum.TOP21),
            getSquare(CubeSquaresEnum.TOP32));
      totK += getK(getSquare(CubeSquaresEnum.TOP32),
            getSquare(CubeSquaresEnum.TOP42));
      totK += getK(getSquare(CubeSquaresEnum.TOP31),
            getSquare(CubeSquaresEnum.TOP41));
      totK += getK(getSquare(CubeSquaresEnum.TOP41),
            getSquare(CubeSquaresEnum.TOP5));
      return Math.round((totK / 6) * 100) * 1.0 / 100;
   }

   private static double getK(ColoredSquare square, ColoredSquare square2)
   {
      return (square2.getLowestY() - square.getLowestY()) * 1.0
            / (square2.getLowestX() - square.getLowestX());
   }

   private static int getAverageHeight(List<ColoredSquare> list, int count)
   {
      int j = 0;
      int totHeight = 0;
      for (int i = 0; i < count && list.size() > i; i++)
      {
         totHeight += list.get(i).getHeight();
         j++;
      }

      return Math.round(totHeight / j);
   }

   private static int getAverageWeight(List<ColoredSquare> list, int count)
   {
      int j = 0;
      int totWeight = 0;
      for (int i = 0; i < count && list.size() > i; i++)
      {
         totWeight += list.get(i).getWidth();
         j++;
      }

      return Math.round(totWeight / j);
   }

   public List<ColoredSquare> getLeftList()
   {
      // Make a large enough rectangle
      ColoredSquare squareTop31 = getSquare(CubeSquaresEnum.TOP31);
      if (squareTop31 == null)
      {
         showlistSolved();
         System.out.println("No TOP31, can not continue");
         return iLeftList;
      }

      ColoredSquare squareTop5 = getSquare(CubeSquaresEnum.TOP5);
      if (squareTop5 == null)
      {
         showlistSolved();
         System.out.println("No TOP5, can not continue");
         return iLeftList;
      }

      int x = (int) Math
            .round(squareTop31.getLowestX() - .2 * (squareTop31.getWidth()));
      int y = (int) Math
            .round(squareTop31.getLowestY() + .5 * (squareTop31.getHeight()));

      int width = (int) Math
            .round(squareTop5.getLowestX() + .5 * (squareTop5.getWidth()) - x);

      int height = (int) Math.round(8.5 * (squareTop31.getHeight()));

      iLeftRect = new Rectangle(x - CubeConstants.LEFT_MARGIN,
            y - CubeConstants.LEFT_MARGIN,
            width + 2 * CubeConstants.LEFT_MARGIN,
            height + 2 * CubeConstants.LEFT_MARGIN);
      iImageComponent.setLeftRect(iLeftRect);
      for (ColoredSquare square : iSquaresList)
      {
         if (square.getColorType() != CubeColorEnum.UNUSED
               && !iTopList.contains(square)
               && (square.getLowestX() >= x && square.getLowestX() <= x + width
                     && square.getLowestY() >= y
                     && square.getLowestY() < y + height)
               && (square.getHighestX() >= x
                     && square.getHighestX() <= x + width
                     && square.getHighestY() >= y
                     && square.getHighestY() < y + height))
         {
            iLeftList.add(square);
         }
      }

      return iLeftList;
   }

   public List<ColoredSquare> getRightList()
   {
      // Make a large enough rectangle
      ColoredSquare squareTop33 = getSquare(CubeSquaresEnum.TOP33);
      if (squareTop33 == null)
      {
         showlistSolved();
         System.out.println("No TOP33, can not continue");
         return iLeftList;
      }

      ColoredSquare squareTop5 = getSquare(CubeSquaresEnum.TOP5);
      if (squareTop5 == null)
      {
         showlistSolved();
         System.out.println("No TOP5, can not continue");
         return iLeftList;
      }

      int x = (int) Math
            .round(squareTop5.getLowestX() - .01 * (squareTop5.getWidth()));
      int y = (int) Math
            .round(squareTop33.getLowestY() + .5 * (squareTop33.getHeight()));

      int width = (int) Math
            .round(squareTop33.getHighestX() + .5 * (squareTop33.getWidth()) - x);

      int height = (int) Math.round(8.5 * (squareTop33.getHeight()));

      iRightRect = new Rectangle(x - CubeConstants.LEFT_MARGIN,
            y - CubeConstants.LEFT_MARGIN,
            width + 2 * CubeConstants.LEFT_MARGIN,
            height + 2 * CubeConstants.LEFT_MARGIN);
      iImageComponent.setRightRect(iRightRect);
      for (ColoredSquare square : iSquaresList)
      {
         if (square.getColorType() != CubeColorEnum.UNUSED
               && !iTopList.contains(square)&& !iLeftList.contains(square)
               && (square.getLowestX() >= x && square.getLowestX() <= x + width
                     && square.getLowestY() >= y
                     && square.getLowestY() < y + height)
               && (square.getHighestX() >= x
                     && square.getHighestX() <= x + width
                     && square.getHighestY() >= y
                     && square.getHighestY() < y + height))
         {
            iRightList.add(square);
         }
      }

      return iRightList;
   }

   private void showlistSolved()
   {
      boolean old = iShowUI;
      iShowUI = true;
      listSolved();
      iShowUI = old;
   }

   public static void removeAfterNine(List<ColoredSquare> list)
   {
      while (list.size() > 9)
      {
         list.remove(list.size() - 1);
      }
   }

   private ColoredSquare getSquare(CubeSquaresEnum key)
   {
      return iFoundCorrectMap.get(key);
   }

   public void findTopSquares()
   {
      List<ColoredSquare> topList = getTopList();
      CubeHelper.calcDistanceToOthers(topList);

      Collections.sort(topList, Comparators.distanceComparator);
      CubeHelper.setPlacementPoints(topList, "Distance apart:", 3, iShowUI);

      CubeHelper.calcWidthToOthers(topList);
      CubeHelper.calcHeightToOthers(topList);

      Collections.sort(topList, Comparators.widthHeightComparator);
      CubeHelper.setPlacementPoints(topList, "Size difference:", 3, iShowUI);

      Collections.sort(topList, Comparators.placementComparator);

      calcChildren();

      Collections.sort(topList, Comparators.ChildrenComparator);
      CubeHelper.setPlacementPoints(topList, "Child Count:", 1, iShowUI);

      Collections.sort(topList, Comparators.positionComparator);

      Collections.sort(topList, Comparators.PointsComparator);

      listNineAndTheRest(topList);

      removeAfterNine(topList);

      // Recalc children
      calcChildren();

      if (iShowUI)
      {
         System.out.println("");
      }

      Collections.sort(topList, Comparators.YPosComparator);

      // Take the one highest up with at least 6 children
      if (iTopList.isEmpty())
      {
         return;
      }

      int i = 0;
      ColoredSquare topSquare = topList.get(i++);

      while (topList.size() > i && topSquare.getChildrenCount() <= 5)
      {
         topSquare = topList.get(i++);
      }

      if (i == topList.size())
      {
         if (iShowUI)
         {
            System.out.println("Top might be missing");
         }
      }
      else
      {

         // Reset topList with found values

         setCubePlacement(topSquare, CubeSquaresEnum.TOP1);
         findChildren(topSquare, CubeSquaresEnum.TOP1);

         recalcList(topList);

         // Are they all there?
         if (!isFound(CubeSquaresEnum.TOP1))
         {
            if (iShowUI)
            {
               System.out.println("Can not go on when Top Square is missing");
            }
            return;
         }

         reconstruct(CubeSquaresEnum.TOP21);
         reconstruct(CubeSquaresEnum.TOP22);
         reconstruct(CubeSquaresEnum.TOP31);
         reconstruct(CubeSquaresEnum.TOP32);
         reconstruct(CubeSquaresEnum.TOP33);
         reconstruct(CubeSquaresEnum.TOP41);
         reconstruct(CubeSquaresEnum.TOP42);
         reconstruct(CubeSquaresEnum.TOP5);
         recalcList(topList);
         // Top 9 done
      }
   }

   public void listNineAndTheRest(List<ColoredSquare> topList)
   {
      if (!iShowUI)
      {
         return;
      }

      int count = 0;
      System.out.println("");

      for (ColoredSquare square : topList)
      {
         count++;
         if (count == 10)
         {
            System.out
                  .println("-------------------------------------------------");
         }

         System.out.println(
               count + ". " + square.getPlacement() + " " + square.toString());
      }
   }

   public void recalcList(List<ColoredSquare> topList)
   {
      int i;
      topList.clear();
      i = 0;
      if (iShowUI)
      {
         System.out.println("");
      }
      for (CubeSquaresEnum key : iFoundCorrectMap.keySet())
      {
         i++;
         ColoredSquare square = iFoundCorrectMap.get(key);
         topList.add(square);
         if (iShowUI)
         {
            System.out.println(i + ". " + key + " " + square.toString());
         }
      }
   }

   public void listSolved()
   {
      if (!iShowUI)
      {
         return;
      }

      System.out.println("");
      Set<CubeSquaresEnum> keys = iFoundCorrectMap.keySet();
      if (keys == null || keys.size() == 0)
      {
         System.out.println("No correctly placed colors found");
         return;
      }

      EnumSet<CubeSquaresEnum> keysSet = EnumSet.copyOf(keys);

      for (CubeSquaresEnum key : keysSet)
      {
         ColoredSquare square = iFoundCorrectMap.get(key);
         System.out.println(key + " " + square.toString());
      }
   }

   private void reconstruct(CubeSquaresEnum key)
   {
      // System.out.println(key);
      switch (key)
      {
         case TOP1:
            if (!isFound(CubeSquaresEnum.TOP1))
            {
               if (iShowUI)
               {
                  System.out
                        .println("Can not go on when Top Square is missing");
               }
               return;
            }

         case TOP21:
            reconstructSquare(key, CubeSquaresEnum.TOP1,
                  RelationEnum.leftTopChild);
            break;

         case TOP22:
            reconstructSquare(key, CubeSquaresEnum.TOP1,
                  RelationEnum.rightTopChild);
            break;

         case TOP31:
            reconstructSquare(key, CubeSquaresEnum.TOP21,
                  RelationEnum.leftTopChild);
            break;

         case TOP32:
            reconstructSquare(key, CubeSquaresEnum.TOP21,
                  RelationEnum.rightTopChild);
            break;

         case TOP33:
            reconstructSquare(key, CubeSquaresEnum.TOP22,
                  RelationEnum.rightTopChild);
            break;

         case TOP41:
            reconstructSquare(key, CubeSquaresEnum.TOP31,
                  RelationEnum.rightTopChild);
            break;

         case TOP42:
            reconstructSquare(key, CubeSquaresEnum.TOP32,
                  RelationEnum.rightTopChild);
            break;

         case TOP5:
            reconstructSquare(key, CubeSquaresEnum.TOP41,
                  RelationEnum.rightTopChild);
            break;

         case LEFT11:
            reconstructSquare(key, CubeSquaresEnum.LEFT12,
                  RelationEnum.toLeftChild);
            break;

         case LEFT12:
            reconstructSquare(key, CubeSquaresEnum.LEFT13,
                  RelationEnum.toLeftChild);
            break;

         case LEFT13:
            reconstructSquare(key, CubeSquaresEnum.LEFT23,
                  RelationEnum.upLeftChild);
            break;

         case LEFT21:
            reconstructSquare(key, CubeSquaresEnum.LEFT22,
                  RelationEnum.toLeftChild);
            break;

         case LEFT22:
            reconstructSquare(key, CubeSquaresEnum.LEFT12,
                  RelationEnum.downLeftListChild);
            break;

         case LEFT23:
            reconstructSquare(key, CubeSquaresEnum.LEFT33,
                  RelationEnum.upLeftChild);
            break;

         case LEFT31:
            reconstructSquare(key, CubeSquaresEnum.LEFT21,
                  RelationEnum.downLeftListChild);
            break;

         case LEFT32:
            reconstructSquare(key, CubeSquaresEnum.LEFT31,
                  RelationEnum.toRightLeftListChild);
            break;

         case LEFT33:
            reconstructSquare(key, CubeSquaresEnum.LEFT32,
                  RelationEnum.toRightLeftListChild);
            break;

         default:
            break;
      }// TODO Auto-generated method stub

      // Do the work here

   }

   private ColoredSquare reconstructSquare(CubeSquaresEnum child,
         CubeSquaresEnum mother, RelationEnum childRelation)
   {
      if (isFound(child))
      {
         return null;
      }

      if (!isFound(mother))
      {
         reconstruct(mother);
      }

      // Create green square
      if (iShowUI)
      {
         System.out.println("Create Green square for " + child);
      }
      List<ColoredSquare> list = null;

      switch (childRelation)
      {
         case leftTopChild:
         case rightTopChild:
            list = iTopList;
            break;

         case downLeftListChild:
         case upLeftChild:
         case toRightLeftListChild:
         case toLeftChild:
            list = iLeftList;
            break;

         default:
            break;
      }

      Reconstructor reco =
            new Reconstructor(iFoundCorrectMap, mother, list, childRelation);
      List<ColorSequence> insideList =
            CubeHelper.getSequencesInside(reco.getResultRect(), iSeqList);

      ColoredSquare square = new ColoredSquare(insideList);
      square.setFreeCorners();
      square.setSolutionType(SolutionEnum.RECONSTRUCTED);
      iFoundCorrectMap.put(child, square);
      iReconstructedCount++;
      return square;
   }

   private boolean isFound(CubeSquaresEnum key)
   {
      return iFoundCorrectMap.containsKey(key);
   }

   public void findChildren(ColoredSquare square, CubeSquaresEnum placement)
   {
      findChildren(square, placement, null);
   }

   public void findChildren(ColoredSquare square, CubeSquaresEnum placement,
         ArrayList<ColoredSquare> externTopList)
   {
      ColoredSquare child = null;
      int width = ColoredSquare.getIdealWidth();
      int height = ColoredSquare.getIdealHeight();
      List<ColoredSquare> topList;
      if (externTopList == null)
      {
         topList = getTopList();
      }
      else
      {
         topList = externTopList;
      }

      CubeSquaresEnum childName;

      switch (placement)
      {
         case TOP1:
            childName = CubeSquaresEnum.TOP21;
            child = square.getLeftChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findLeftChildDeeper(square, childName, width, height);
            }

            childName = CubeSquaresEnum.TOP22;
            child = square.getRightChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findRightChildDeeper(square, childName, width, height);
            }
            break;

         case TOP21:
            childName = CubeSquaresEnum.TOP31;
            child = square.getLeftChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findLeftChildDeeper(square, childName, width, height);
            }

            childName = CubeSquaresEnum.TOP32;
            child = square.getRightChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findRightChildDeeper(square, childName, width, height);
            }
            break;

         case TOP22:
            childName = CubeSquaresEnum.TOP32;
            child = square.getLeftChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findLeftChildDeeper(square, childName, width, height);
            }

            childName = CubeSquaresEnum.TOP33;
            child = square.getRightChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findRightChildDeeper(square, childName, width, height);
            }
            break;

         case TOP31:
            childName = CubeSquaresEnum.TOP41;
            child = square.getRightChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findRightChildDeeper(square, childName, width, height);
            }
            break;

         case TOP32:
            childName = CubeSquaresEnum.TOP41;
            child = square.getLeftChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findLeftChildDeeper(square, childName, width, height);
            }

            childName = CubeSquaresEnum.TOP42;
            child = square.getRightChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findRightChildDeeper(square, childName, width, height);
            }
            break;

         case TOP33:
            childName = CubeSquaresEnum.TOP42;
            child = square.getLeftChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findLeftChildDeeper(square, childName, width, height);
            }
            break;

         case TOP41:
            childName = CubeSquaresEnum.TOP5;
            child = square.getRightChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findRightChildDeeper(square, childName, width, height);
            }
            break;

         case TOP42:
            childName = CubeSquaresEnum.TOP5;
            child = square.getLeftChild(topList, width, height);
            if (child != null)
            {
               setCubePlacement(child, childName);
               findChildren(child, childName);
            }
            else
            {
               findLeftChildDeeper(square, childName, width, height);
            }
            break;

         case TOP5:
            break;

         default:
            break;
      }

   }

   private void findLeftChildDeeper(ColoredSquare square,
         CubeSquaresEnum childName, int width, int height)
   {

      ColoredSquare child = square.getLeftChild(iSquaresList, width, height);
      if (child != null)
      {
         child.setSolutionType(SolutionEnum.DEEP);
         iDeeperFoundCount++;
         setCubePlacement(child, childName);
         findChildren(child, childName);
      }
      else
      {
         createSquare(childName);
      }
   }

   private void findRightChildDeeper(ColoredSquare square,
         CubeSquaresEnum childName, int width, int height)
   {

      ColoredSquare child = square.getRightChild(iSquaresList, width, height);
      if (child != null)
      {
         child.setSolutionType(SolutionEnum.DEEP);
         setCubePlacement(child, childName);
         findChildren(child, childName);
      }
      else
      {
         createSquare(childName);
      }
   }

   private void createSquare(CubeSquaresEnum child)
   {
      // System.out.println("Create Square " + child);
   }

   public void setCubePlacement(ColoredSquare square, CubeSquaresEnum placement)
   {
      square.setPosition(placement);
      iFoundCorrectMap.put(placement, square);
   }

   private void calcChildren()
   {

      if (iTopList.isEmpty())
      {
         return;
      }

      // Find average of top 4 squares
      int calc = 4;
      if (iTopList.size() < calc)
      {
         calc = iTopList.size();
      }

      int i = 0;
      int width = 0;
      int height = 0;

      while (i < calc)
      {
         ColoredSquare square = iTopList.get(i++);
         width += (square.getWidth());
         height += (square.getHeight());
      }

      height = Math.round(height / calc);
      width = Math.round(width / calc);
      ColoredSquare.setIdealHeight(height);
      ColoredSquare.setIdealWidth(width);

      // Count children from each square
      for (ColoredSquare square : iTopList)
      {
         square.setNumberOfTopChildren(iTopList, width, height);
      }
   }

   public List<ColoredSquare> getTopList()
   {
      if (iTopList.size() == 0)
      {

         for (ColoredSquare colSquare : iSquaresList)
         {
            if (colSquare.getList().size() > 8
                  && colSquare.hasMoreThan3FreeCorners()
                  && CubeHelper.isOKColor(colSquare))
            {
               int x = colSquare.getLowestX();
               int y = colSquare.getLowestY();
               int x2 = colSquare.getHighestX();
               int y2 = colSquare.getHighestY();

               if (x2 - x > y2 - y)
               {
                  iTopList.add(colSquare);
               }
            }
         }
      }
      // System.out.println("top=" + iTopList.size());
      return iTopList;

   }

   private void repaint()
   {
      if (iShowUI)
      {
         iDisplayImage.repaint();

      }
   }

   private void setFreeCorners()
   {
      for (ColoredSquare square : iSquaresList)
      {
         square.setFreeCorners();
      }

   }

   private List<ColoredSquare> findColoredSquares()
   {
      for (ColorSequence colSequence : iSeqList)
      {
         if (!CubeHelper.seqUsed(colSequence, iSquaresList))
         {

            ColoredSquare colSq = new ColoredSquare(colSequence);
            iSquaresList.add(colSq);

            boolean inTestRec = colSq.findIntersection(iSeqList, iSquaresList);
         }
      }

      return iSquaresList;
   }

   public List<ColorSequence> findColorSequences(String fileName)
      throws IOException
   {
      iImage = null;
      File f = null;

      // read image

      try
      {
         f = new File(fileName);
         iImage = ImageIO.read(f);
      }
      catch (IOException e)
      {
         System.out.println(e);
         System.exit(1);
      }

      iImageComponent = new ImageComponent(iImage, iSquaresList, iTopList,
            iLeftList, iRightList, iDrawLines, iPointsList);

      if (iShowUI)
      {
         iDisplayImage = new DisplayImage(fileName, iImageComponent);
      }

      // get image width and height
      iImageWidth = iImage.getWidth();
      iImageHeight = iImage.getHeight();

      linesFromTopOfBottom(iImage, iImageComponent, iImageWidth, iImageHeight,
            CubeConstants.ANGLE_DOWN_RIGHT);

      linesFromLeft(iImage, iImageComponent, iImageWidth, iImageHeight,
            CubeConstants.ANGLE_DOWN_RIGHT);

      linesFromTopOfBottom(iImage, iImageComponent, iImageWidth, iImageHeight,
            CubeConstants.ANGLE_DOWN_LEFT);

      linesFromLeft(iImage, iImageComponent, iImageWidth, iImageHeight,
            CubeConstants.ANGLE_DOWN_LEFT);

      linesFromLeft(iImage, iImageComponent, iImageWidth, iImageHeight,
            CubeConstants.ANGLE_HORIZONTAL);

      linesFromTopOfBottom(iImage, iImageComponent, iImageWidth, iImageHeight,
            CubeConstants.ANGLE_VERTICAL);

      return iSeqList;
   }

   public void linesFromLeft(BufferedImage img, ImageComponent ic, int width,
         int height, double angle)
   {
      {
         int start_x = 0;
         for (int start_y = 0; start_y < height; start_y +=
               CubeConstants.GRID_SIZE)
         {
            drawLine(img, ic, width, height, start_x, start_y, angle);
         }
      }
   }

   public void linesFromTopOfBottom(BufferedImage img, ImageComponent ic,
         int width, int height, double angle)
   {
      int start_y = 0;

      if (angle < 0)
      {
         start_y = height;
      }

      for (int start_x = 0; start_x < width; start_x += CubeConstants.GRID_SIZE)
      {
         drawLine(img, ic, width, height, start_x, start_y, angle);
      }
   }

   public void drawLine(BufferedImage img, ImageComponent ic, int width,
         int height, int start_x, int start_y, double angle)
   {
      int count = 1;
      long accR = 0;
      long accG = 0;
      long accB = 0;
      int colorStartPos = 0;
      Color thisColor = null;
      int colorCount = 0;
      int voidCount = 0;
      int startPosX = 0;
      int startPosY = 0;
      int endPosX = 0;
      int endPosY = 0;
      List<ColorSequence> lst = new ArrayList<>();
      Color lastColor = null;
      int p;

      if (angle == CubeConstants.ANGLE_VERTICAL)
      {
         width = height + start_x;
      }

      for (int x = start_x; x < width; x++)
      {
         int xValue = x;
         // get pixel value
         int y = CubeHelper.getYFromX(start_x, start_y, angle, xValue);

         if (angle == CubeConstants.ANGLE_VERTICAL)
         {
            y = x - start_x;
            xValue = start_x;
         }

         if (y < height && y >= 0)
         {
            p = img.getRGB(xValue, y);

            // get red
            int r = (p >> 16) & 0xff;

            // get green
            int g = (p >> 8) & 0xff;

            // get blue
            int b = p & 0xff;
            thisColor = new Color(r, g, b);

            if (lastColor != null)
            {
               double percent =
                     CubeHelper.getColordiffPercent(thisColor, lastColor);
               count++;
               if (percent <= CubeConstants.SPLIT_PERCENTIGE_COLOR
                     || voidCount < CubeConstants.VOID_COUNT_IN_COLOR_SEQUENCE)
               {
                  if (percent > CubeConstants.SPLIT_PERCENTIGE_COLOR)
                  {
                     voidCount++;
                  }

                  if (colorCount == 0)
                  {
                     colorStartPos = count;
                     startPosX = xValue;
                     startPosY = y;
                  }
                  accR += r;
                  accG += g;
                  accB += b;
                  colorCount++;
               }
               else
               {
                  if (colorCount >= CubeConstants.MIN_COLOR_SEQUENCE
                        && colorCount <= CubeConstants.MAX_COLOR_SEQUENCE)
                  {
                     endPosX = xValue;
                     endPosY = y;
                     ColorSequence seq = new ColorSequence(
                           new Color((int) accR / colorCount,
                                 (int) accG / colorCount,
                                 (int) accB / colorCount),
                           colorCount, colorStartPos, startPosX, startPosY,
                           endPosX, endPosY);
                     lst.add(seq);
                  }
                  colorCount = 0;
                  voidCount = 0;
                  accR = 0;
                  accG = 0;
                  accB = 0;
               }

            }

            lastColor = thisColor;
         }
      }

      if (colorCount >= CubeConstants.MIN_COLOR_SEQUENCE
            && colorCount <= CubeConstants.MAX_COLOR_SEQUENCE)
      {
         endPosX = width;
         endPosY = CubeHelper.getYFromX(start_x, start_y, angle, width);

         if (endPosY >= 0 && endPosY <= height)
         {
            ColorSequence seq = new ColorSequence(lastColor, colorCount,
                  colorStartPos, startPosX, startPosY, endPosX, endPosY);
            lst.add(seq);
            colorCount = 0;
         }
      }

      // Sum up
      iSeqList.addAll(lst);
      int i = 0;
      int startPos = 0;
      int endPos = 0;
      for (ColorSequence colorSequence : lst)
      {
         i++;

         if (i == 1)
         {
            startPos = colorSequence.getStartpos();
         }

         if (i == lst.size())
         {
            endPos = colorSequence.getEndPos();
         }
      }

      // Paint line
      count = 0;
      for (int x = start_x; x < width; x++)
      {

         int xValue = x;
         // get pixel value
         int y = CubeHelper.getYFromX(start_x, start_y, angle, xValue);

         if (angle == CubeConstants.ANGLE_VERTICAL)
         {
            y = x - start_x;
            xValue = start_x;
         }

         if (y < height && y >= 0)
         {
            count++;

            if (lst.size() < CubeConstants.SEQUENCES_FOUND_COUNT
                  || count < startPos || count > endPos)
            {
               ic.plot(xValue, y, Color.RED);
            }
            else
            {
               ic.plot(xValue, y, Color.GREEN);
            }
         }
      }

   }

   public void showUI(boolean showUI)
   {
      iShowUI = showUI;
   }
}
