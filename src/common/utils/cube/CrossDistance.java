package common.utils.cube;

public class CrossDistance
{

  
   private int iX;
   private int iY;
   public int getX()
   {
      return iX;
   }


   public int getY()
   {
      return iY;
   }


   public int getDistance()
   {
      return iDistance;
   }


   public ColoredSquare getSquare()
   {
      return iSquare;
   }


   public CubeSquaresEnum getPlacement()
   {
      return iPlacement;
   }


   private int iDistance;
   private ColoredSquare iSquare;
   private CubeSquaresEnum iPlacement;


   public CrossDistance(int x, int y, int distance, ColoredSquare square,
         CubeSquaresEnum placement)
   {
      iX = x;
      iY = y;
      iDistance = distance;
      iSquare = square;
      iPlacement = placement;
      // TODO Auto-generated constructor stub
   }


   public void setDistance(int distance)
   {
      iDistance = distance;
   }


   public void setSquare(ColoredSquare square)
   {
      iSquare = square;
   }


}
