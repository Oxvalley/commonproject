package common.utils.cube;

public enum CubeColorEnum
{
   WHITE("U"), RED("R"), GREEN("F"), YELLOW("D"), ORANGE("L"), BLUE("B"), UNUSED("-"); 

   private String iAbbr;

   CubeColorEnum(String abbr)
   {
      iAbbr = abbr;
   }

   public String getAbbriviation()
   {
      return iAbbr;
   }

}
