package common.utils.cube;

public class CubeTesterInput
{

   private String iPicturePath1;
   private int iPhotoNumber1;
   private String iResultPath1;
   private boolean iIsOnePicture;
   private int iPhotoNumber2;
   private String iPicturePath2;
   private String iResultPath2;

   public CubeTesterInput(String picturePath, int photoNumber,
         String resultPath)
   {
      iPicturePath1 = picturePath;
      iPhotoNumber1 = photoNumber;
      iResultPath1 = resultPath;
      iIsOnePicture = true;
   }

   public CubeTesterInput(String picturePath1, int photoNumber1,
         String resultPath1, String picturePath2, int photoNumber2,
         String resultPath2)
   {
      iPicturePath1 = picturePath1;
      iPhotoNumber1 = photoNumber1;
      iResultPath1 = resultPath1;
      iPicturePath2 = picturePath2;
      iPhotoNumber2 = photoNumber2;
      iResultPath2 = resultPath2;
      iIsOnePicture = false;
   }

   public boolean isIsOnePicture()
   {
      return iIsOnePicture;
   }

   public int getPhotoNumber2()
   {
      return iPhotoNumber2;
   }

   public String getPicturePath2()
   {
      return iPicturePath2;
   }

   public String getResultPath2()
   {
      return iResultPath2;
   }

   public String getPicturePath1()
   {
      return iPicturePath1;
   }

   public int getPhotoNumber1()
   {
      return iPhotoNumber1;
   }

   public String getResultPath1()
   {
      return iResultPath1;
   }

   @Override
   public String toString()
   {
      String retVal = super.toString();
      return retVal;
   }

   public String getDisplayName()
   {
      return "";
   }

}
