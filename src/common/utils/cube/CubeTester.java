package common.utils.cube;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CubeTester
{

   public static void main(String[] args)
   {
      String testDisplayName;
      List<CubeTesterInput> inList = new ArrayList<>();

      inList.add(new CubeTesterInput("2019-10-04 12.23.28.jpg", 1,
            "DBLLDDDRR---------UFFBUDLFR---------------------------"));
      inList.add(new CubeTesterInput("2019-10-04 12.23.18.jpg", 2,
            "---------------------------UBDFFRBLFUURLRDFUD---------"));
      inList.add(new CubeTesterInput("2019-10-28 15.34.37.jpg", 1,
            "BDRBDLBFR---------DRUBLBLUU---------------------------"));
      inList.add(new CubeTesterInput("2019-10-28 15.34.49.jpg", 2,
            "---------------------------FFBDUUBDFLURRFRURD---------"));

      List<CubeTesterOutput> outList = new ArrayList<>();
      int count = 0;
           
      for (double testInt1 = 0.03; testInt1 <= 0.03; testInt1 += 0.04)
      {
       //  CubeConstants.LEFT_RECTANGLE_ADDED_HEIGHT_IN_SQUARE_PERCENT = testInt1;
         for (double testDouble1 = 0.5; testDouble1 <= 0.5; testDouble1 +=
               0.05)
         {
          //  CubeConstants.LEFT_COLUMN_2_PERCENT = testDouble1;

            for (double testDouble2 =
                  -11; testDouble2 <= -9; testDouble2 += .1)
            {
               CubeConstants.LEFT_VERTICAL_K = testDouble2;

               testDisplayName = "Value1=" + testInt1 + ", Value2="
                     + testDouble1 + ", Value3=" + testDouble2;
               try
               {
                  CubeTesterOutput totalOutput =
                        new CubeTesterOutput("", 0, 0, 0);
                  totalOutput.setDisplayName(
                        testDisplayName + " " + totalOutput.getDisplayName());
                  outList.add(totalOutput);
                  for (CubeTesterInput cti : inList)
                  {
                     if (cti.isIsOnePicture())
                     {
                        CubeSolver cs = new CubeSolver();
                        cs.showUI(false);

                        CubeTesterOutput cto = cs.findColorsFromOnePicture(
                              cti.getPicturePath1(), cti.getPhotoNumber1());
                        cto.setExcpectedResult(cti.getResultPath1());
                        cto.setDisplayName(
                              testDisplayName + " " + cti.getDisplayName());
                        totalOutput.addOutput(cto);
                        count++;
                        System.out.println("Test " + count + " done: " + cto);
                     }
                     else
                     {

                     }

                  }
                  totalOutput.setDisplayName(testDisplayName);

               }
               catch (IOException e)
               {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
               }

            }
         }

      }
      Collections.sort(outList);
      int i = 1;
      for (CubeTesterOutput cubeTesterOutput : outList)
      {
         System.out.println(i + ". " + cubeTesterOutput);
         i++;
      }

   }
}
