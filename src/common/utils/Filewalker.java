package common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Filewalker
{

   public String[] iLangArr = { "Swedish", "English" };
   public List<Language> iLangObjects = new ArrayList<>();

   public void walk(String path)
   {
      File root = new File(path);
      File[] list = root.listFiles();

      if (list == null)
         return;

      for (File f : list)
      {
         if (f.isDirectory())
         {
            walk(f.getAbsolutePath());
         }
         else
         {
            String fileName = f.getAbsolutePath();
            if (fileName.toLowerCase().endsWith(".java"))
            {
               checkForLang(f);
            }

            System.out.println("File:" + f.getAbsoluteFile());
         }
      }
   }

   public void initLangObjects()
   {
      iLangObjects.clear();
      for (String language : iLangArr)
      {
         Language lang = Language.getNewInstance();
         lang.setLanguage(language, "E:/Dropbox/Eclipse Projects/CrossWordCreator/languages");
         iLangObjects.add(lang);

      }
   }

   private void checkForLang(File f)
   {
      try
      {

         BufferedReader reader = new BufferedReader(new FileReader(f));
         String line = reader.readLine();

         while (line != null)
         {
            if (line.contains("iLang.getText("))
            {
               int i = line.indexOf("iLang.getText(") + 15;
               int i2 = line.indexOf("\"", i);
               String text = line.substring(i, i2);

               for (Language lang : iLangObjects)
               {
               System.out.println(lang.getText(text));
               }
            }

            line = reader.readLine();
         }
      }
      catch (FileNotFoundException e)
      {
         e.printStackTrace();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
   }

   public static void main(String[] args)
   {
      Filewalker fw = new Filewalker();
      fw.initLangObjects();
      fw.walk("E:/Dropbox/Eclipse Projects/CrossWordCreator/src/");
      fw.saveLangFiles();
   }

   private void saveLangFiles()
   {
      for (Language lang : iLangObjects)
      {
         lang.save();
      }

   }

}