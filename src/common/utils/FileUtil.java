package common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

/**
 * The Class FileHelper.
 */
public class FileUtil
{

   /**
    * Gets the folder file from path.
    *
    * @param destinationPath
    *           the destination path
    * @return the folder file from path
    */
   public static File getFolderFileFromPath(String destinationPath)
   {
      File destinationFile;
      if (destinationPath != null)
      {
         destinationFile = new File(destinationPath);
         if (!destinationFile.exists())
         {
            System.err.println("Destination folder " + destinationPath
                  + " does not exist. Exiting...");
            System.exit(1);
         }

         if (!destinationFile.isDirectory())
         {
            System.err.println("Destination " + destinationPath
                  + " is not a folder, but a file. Exiting...");
            System.exit(1);
         }
      }
      else
      {
         destinationFile = null;
      }
      return destinationFile;
   }

   /**
    * Removes the directories recursive.
    *
    * @param destDir
    *           the dest dir
    * @param folderName
    *           the folder name
    */
   public static void removeDirectoriesRecursive(String destDir,
         String folderName)
   {
      File dir = new File(destDir);

      List<File> files = (List<File>) FileUtils.listFilesAndDirs(dir,
            new NotFileFilter(TrueFileFilter.INSTANCE),
            DirectoryFileFilter.DIRECTORY);

      for (File file : files)
      {
         if (file.getName().equals(folderName))
         {
            try
            {
               FileUtils.deleteDirectory(file);
            }
            catch (IOException e)
            {
               System.out.println(e.getMessage());
            }
         }
      }
   }

   /**
    * Removes the files recursive.
    *
    * @param destDir
    *           the dest dir
    * @param fileName
    *           the file name
    */
   public static void removeFilesRecursive(String destDir,
         final String fileName)
   {
      File dir = new File(destDir);

      List<File> files = (List<File>) FileUtils.listFiles(dir,
            FileFilterUtils.asFileFilter(new FilenameFilter()
            {
               @Override
               public boolean accept(File dir, String name)
               {
                  return (name.equals(fileName));
               }
            }), DirectoryFileFilter.DIRECTORY);

      for (File file : files)
      {
         if (file.getName().equals(fileName))
         {
            try
            {
               FileUtils.forceDelete(file);
            }
            catch (IOException e)
            {
               LogUtil.print("Could not delete file " + fileName + "\n"
                     + e.getMessage());
            }
         }
      }
   }

   /**
    * Removes the tag.
    *
    * @param path
    *           the path
    * @return the string
    */
   public static String removeTag(String path)
   {

      int i = path.lastIndexOf(".");
      if (i > -1)
      {
         return path.substring(0, i);
      }
      else
      {
         return path;
      }
   }

   /**
    * Copy.
    *
    * @param fromFileName
    *           the from file name
    * @param toFileName
    *           the to file name
    * @param owerwrite
    *           the owerwrite
    * @throws IOException
    *            Signals that an I/O exception has occurred.
    */
   public static void copy(String fromFileName, String toFileName,
         boolean owerwrite)
      throws IOException
   {
      copy(new File(fromFileName), new File(toFileName), owerwrite);
   }

   /**
    * Copy.
    *
    * @param fromFile
    *           the from file
    * @param toFile
    *           the to file
    * @param owerwrite
    *           the owerwrite
    * @throws IOException
    *            Signals that an I/O exception has occurred.
    */
   public static void copy(File fromFile, File toFile, boolean owerwrite)
      throws IOException
   {

      if (!fromFile.exists())
         throw new IOException(
               "FileCopy: " + "no such source file: " + fromFile.getName());
      if (!fromFile.isFile())
         throw new IOException(
               "FileCopy: " + "can't copy directory: " + fromFile.getName());
      if (!fromFile.canRead())
         throw new IOException("FileCopy: " + "source file is unreadable: "
               + fromFile.getName());

      if (toFile.isDirectory())
         toFile = new File(toFile, fromFile.getName());

      if (toFile.exists() && !owerwrite)
      {
         if (!toFile.canWrite())
         {
            LogUtil.printDialog("Destination file " + toFile.getAbsolutePath()
                  + " is unwriteable: " + toFile.getName());
         }
         else
         {
            LogUtil.printDialog("Existing file " + toFile.getAbsolutePath()
                  + " was not overwritten.");
         }
      }
      else
      {
         String parent = toFile.getParent();
         if (parent == null)
            parent = System.getProperty("user.dir");
         File dir = new File(parent);
         if (!dir.exists())
            throw new IOException("FileCopy: "
                  + "destination directory doesn't exist: " + parent);
         if (dir.isFile())
            throw new IOException(
                  "FileCopy: " + "destination is not a directory: " + parent);
         if (!dir.canWrite())
            throw new IOException("FileCopy: "
                  + "destination directory is unwriteable: " + parent);
      }

      FileInputStream from = null;
      FileOutputStream to = null;
      try
      {
         from = new FileInputStream(fromFile);
         to = new FileOutputStream(toFile);
         byte[] buffer = new byte[4096];
         int bytesRead;

         while ((bytesRead = from.read(buffer)) != -1)
            to.write(buffer, 0, bytesRead); // write
      }
      finally
      {
         if (from != null)
            try
            {
               from.close();
            }
            catch (IOException e)
            {
            }
         if (to != null)
            try
            {
               to.close();
            }
            catch (IOException e)
            {
            }
      }
   }

   /**
    * 
    * @param filePath
    * @return filename withut end tag
    */

   public static String getFileNameNoTag(String filePath)
   {
      File file = new File(filePath);

      String name = file.getName();

      int i = name.lastIndexOf(".");

      if (i == -1)
      {
         // No Tag
         return name;
      }
      else
      {
         return name.substring(0, i);
      }
   }

   /**
    * Gets the file tag.
    *
    * @param filename
    *           the filename
    * @return the file tag
    */
   public static String getFileTag(String filename)
   {
      int i = filename.lastIndexOf(".");
      if (i == -1 || i == filename.length() - 1)
      {
         return "";
      }
      else
      {
         return filename.substring(i + 1);
      }
   }

   /**
    * Creates the dir if not exist.
    *
    * @param path
    *           the path
    */
   public static void createDirIfNotExist(String path)
   {
      File file = new File(path);
      if (!file.exists())
      {
         file.mkdir();
      }

   }

   /**
    * Copy folder.
    *
    * @param fromPath
    *           the from path
    * @param toPath
    *           the to path
    */
   public static void copyFolder(String fromPath, String toPath)
   {
      try
      {
         FileUtils.copyDirectory(new File(fromPath), new File(toPath));
      }
      catch (IOException e)
      {
         LogUtil.print("Could not copy folder " + fromPath + " to " + toPath
               + "\n" + e.getMessage());
      }

   }

   /**
    * Renames file. Warning! If toFileName already exists, it will be deleted!
    *
    * @param fromFileName
    *           the from file name
    * @param toFileName
    *           the to file name
    */
   public static void rename(String fromFileName, String toFileName)
   {
      // File (or directory) with old name
      File file = new File(fromFileName);

      // File (or directory) with new name
      File file2 = new File(toFileName);
      if (file2.exists())
      {
         file2.delete();
      }

      // Rename file (or directory)
      boolean success = file.renameTo(file2);
      if (!success)
      {
         LogUtil.print(
               "Could not rename file " + fromFileName + " to " + toFileName);
      }
   }

   /**
    * Delete folder.
    *
    * @param folderName
    *           the folder name
    */
   public static void deleteFolder(String folderName)
   {
      File file = new File(folderName);
      if (file.exists())
      {
         try
         {
            FileUtils.deleteDirectory(file);
         }
         catch (IOException e)
         {
            LogUtil.print(e.getMessage());
         }
      }
   }

   /**
    * Delete folders.
    *
    * @param path
    *           the path
    * @param removePrefix
    *           the remove prefix
    */
   public static void deleteFolders(String path, String removePrefix)
   {
      File[] dirs = new File(path).listFiles();
      if (dirs != null)
      {
         for (File file : dirs)
         {
            if (file.isDirectory() && file.getName().startsWith(removePrefix))
            {
               LogUtil.print("Deleting folder " + file.getAbsolutePath());
               FileUtil.deleteFolder(file.getAbsolutePath());
            }
         }
      }
   }

   /**
    * Copy folderto folder.
    *
    * @param fromPath
    *           the from path
    * @param toPath
    *           the to path
    */
   public static void copyFoldertoFolder(String fromPath, String toPath)
   {
      try
      {
         FileUtils.copyDirectoryToDirectory(new File(fromPath),
               new File(toPath));
      }
      catch (IOException e)
      {
         LogUtil.print("Could not copy folder " + fromPath + " to " + toPath
               + "\n" + e.getMessage());
      }

   }

   /**
    * Move directory.
    *
    * @param fromPath
    *           the from path
    * @param toPath
    *           the to path
    */
   public static void moveDirectory(String fromPath, String toPath)
   {
      try
      {
         FileUtils.moveDirectory(new File(fromPath), new File(toPath));
      }
      catch (IOException e)
      {
         LogUtil.print("Could not move folder " + fromPath + " to " + toPath
               + "\n" + e.getMessage());
      }
   }

   /**
    * Count files.
    *
    * @param folderPath
    *           the folder path
    * @return the int
    */
   public static int countFiles(String folderPath)
   {
      File folder = new File(folderPath);
      if (!folder.exists())
      {
         return 0;
      }

      int i = 0;
      for (File file : folder.listFiles())
      {
         if (file.isFile())
         {
            i++;
         }
         else if (file.isDirectory())
         {
            i += countFiles(file.getAbsolutePath());
         }
      }
      return i;
   }

   public static String getLastFolder(String path)
   {
      String path2 = addSlash(path);
      int i = path2.lastIndexOf("/");
      if (i <= 0)
      {
         System.out.println("Bad path: " + path);
         return null;
      }
      else
      {
         String path3 = path2.substring(0, i);
         int i2 = path3.lastIndexOf("/");
         if (i <= 0)
         {
            // Just one folder
            return path2;
         }

         if (i == path3.length() - 1)
         {
            // Path ended with double slash 'C:/xx//'
            return "";
         }

         return path3.substring(i2 + 1);
      }

   }

   private static String addSlash(String path)
   {
      String path2 = path.toLowerCase();
      path2 = path2.replace("\\", "/");
      if (!path2.endsWith("/"))
      {
         return path2 + "/";
      }
      else
      {
         return path2;
      }
   }

   public static String getForwardSlashes(String path)
   {
      if (path == null)
      {
         return null;
      }

      return path.replace("\\\\", "/").replace("\\", "/");
   }

}
