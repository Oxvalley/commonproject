package common.utils;

import javax.swing.JOptionPane;


/**
 * The Class LogUtil.
 */
public class LogUtil
{
   
   /**
    * Prints the.
    *
    * @param text the text
    */
   public static void print(String text)
   {
      System.out.println(text);
   }

   /**
    * Prints the dialog.
    *
    * @param text the text
    */
   public static void printDialog(String text)
   {
      // custom title, warning icon
      JOptionPane.showMessageDialog(null, text, "Observera!",
            JOptionPane.WARNING_MESSAGE);
   }

}
