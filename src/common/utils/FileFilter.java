package common.utils;

import java.io.File;

import org.apache.commons.io.filefilter.IOFileFilter;

public class FileFilter implements IOFileFilter
{

   private String iTag;

   public FileFilter(String tag)
   {
      if (tag != null)
      {
         iTag = tag.toLowerCase();
      }      
   }

   @Override
   public boolean accept(File file)
   {
      return (file.getName().toLowerCase().endsWith(iTag.toLowerCase()));
   }

   @Override
   public boolean accept(File file, String fileNamePart)
   {
      return (file.getName().toLowerCase().endsWith(fileNamePart.toLowerCase()));
   }

}
