package common.utils.threede;

public class CompareResult implements Comparable<CompareResult>
{

   private String iName;
   private String iLine;
   private int iStringCompPoints;
   private String iLastHeading;

   public CompareResult(String name, String line, int stringCompPoints, String lastHeading)
   {
      iName = name;
      iLine = line;
      iStringCompPoints = stringCompPoints;
      iLastHeading = lastHeading;
   }

   @Override
   public String toString()
   {
      return iName + ", " + iLine + " ++ " + iLastHeading + " - " + iStringCompPoints;
   }

   @Override
   public int compareTo(CompareResult o)
   {
      if (o.iStringCompPoints == iStringCompPoints)
      {
         return iName.compareTo(o.iName);
      }
      else
      {
         return o.iStringCompPoints - iStringCompPoints;
      }
   }

}
