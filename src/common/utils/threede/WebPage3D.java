package common.utils.threede;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class WebPage3D
{

   public static void main(String[] args)
   {
      String pathTo = "Z:\\3D\\3d-utskrivet\\root";
      String pathFrom = "E:\\Dropbox\\Eclipse Projects\\php\\Sudoku\\openscad models.txt";

      BufferedReader reader = null;
      try
      {
         reader = new BufferedReader(new FileReader(pathFrom));
         String line = reader.readLine();

         String rootPath = pathTo;
         
        String pathNow = rootPath;
         while (line != null)
         {
            line = line.trim();
            
            if (line.startsWith("="))
            {
             int i = line.lastIndexOf("=");
             String folderName = line.substring(i+1).trim();
             
             pathNow += "/"+folderName;
             File file = new File(pathNow);
             file.mkdirs();               
            }
            else if(line.isEmpty())
            {
               pathNow = rootPath;
            }
            else
            {
               File file = new File(pathNow + "/"+line);
               file.mkdirs();               
            }            
            
            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }

   }

}
