package common.utils.threede;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.spi.FileSystemProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import common.utils.TreeKeyValue;
import common.utils.TreeParent;

public class Parent3D
{

   private static List<String> iMissingSTL = new ArrayList<>();
   private static List<String> iMissingImage = new ArrayList<>();
   private static List<String> iDoubleSTL = new ArrayList<>();
   private static List<String> iDoubleImage = new ArrayList<>();
   private static Set<String> iStlHasSet = new HashSet<>();
   private static Set<String> iStlHasSetFileNames = new HashSet<>();
   private static Set<String> iImageHasSet = new HashSet<>();
   private static List<String> iDoubleSTLFileNames = new ArrayList<>();

   public static void main(String[] args)
   {
      TreeParent root = loadRoot();
      //transfer(root);
      printTree(root);

      printMissing("List of no stl", iMissingSTL);
      printMissing("List of no Image", iMissingImage);
      printMissing("List of Double STL", iDoubleSTL);
      printMissing("List of Double Image", iDoubleImage);
      printMissing("List of Double STL file names", iDoubleSTLFileNames);

   }

   private static void printMissing(String title, List<String> arr2)
   {
      System.out.println("\n" + title + "\n" + "========================");
      for (String name : arr2)
      {
         System.out.println(name);
      }

   }

//   private static void transfer(TreeParent root)
//   {
//      // TODO Auto-generated method stub
//
//   }

   private static void printTree(TreeParent root)
   {
      String savePath2 = "E:\\Dropbox\\Eclipse Projects\\php\\Sudoku\\root5.txt";
      try
      {
         FileWriter fw = new FileWriter(savePath2);
         printNode(fw, root);
         fw.close();
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

   }

   private static void printNode(FileWriter fw, TreeParent node) throws IOException
   {
      if (node != null)
      {

         String path = getPathRecursive(node, "");
         // findDateIntervall(node);
         addIfMissing(node, "Type", "Artifact");
         addIfMissing(node, "Path", path);
         addIfMissing(node, "URL", null);
         addIfMissing(node, "Description", null);

         collectMissingStl(node, iMissingSTL);
         collectMissingImages(node, iMissingImage);
         collectDoubleStl(node);
         collectDoubleImages(node);
         collectDoubleStlFileNames(node);

         fw.write(addSpaces(node.getIntroSpaces()) + node.getName() + "\n");

         System.out.println(node.getName());

         boolean sourceDone = hasSource(node.getKeyValues());
         boolean endDateDone = hasEndDate(node.getKeyValues());
         boolean startDateDone = hasStartDate(node.getKeyValues());

         for (TreeKeyValue keyValue : node.getKeyValues())
         {
            if (keyValue.getValue() != null)

               if (keyValue.isSource())
               {
                  keyValue.correctSlashes();
               }
            {
               fw.write(addSpaces(node.getIntroSpaces() + 3) + keyValue + "\n");

               if ((keyValue.isEndDate() && !sourceDone) || (keyValue.isStartDate() && !endDateDone)
                     || (keyValue.isPath() && !startDateDone))
               {
                  // Time to add Source; and Module:
//                  String source = null;
//                  String module = null;
//                  String searchResult = FindScadModules.lookForHeading(node.getName());
//                  int i = searchResult.indexOf(" module ");
//                  if (i == -1)
//                  {
//                     // Tractor -> Tractor, tractor_t1_manual.scad (23 matches)
//                     // + - 87
//                     int i2 = searchResult.indexOf(",");
//                     int i3 = searchResult.indexOf(".scad");
//                     source = searchResult.substring(i2 + 1, i3 + 5).trim();
//                  }
//                  else
//                  {
//                     // Tia Carbin -> Tia Carbin, 110: module full_carbin(diam =
//                     // 4, diam2 = 4) + tia.scad (12 matches) - 61
//                     int i2 = searchResult.indexOf("(", i);
//                     module = searchResult.substring(i + 8, i2).trim();
//                     int i3 = searchResult.indexOf(" ++ ");
//                     int i4 = searchResult.indexOf(".scad");
//                     source = searchResult.substring(i3 + 3, i4 + 5).trim();
//                  }

                 // fw.write(addSpaces(node.getIntroSpaces() + 3) + "Source: " + getValue(source) + "\n");
//                  if (module != null)
//                  {
//                     fw.write(addSpaces(node.getIntroSpaces() + 3) + "Module: " + getValue(module) + "\n");
//                  }
               }

            }
         }

         for (TreeKeyValue keyValue : node.getImage())
         {
            if (keyValue.getValue() != null)
            {
               fw.write(addSpaces(node.getIntroSpaces() + 3) + getValue(keyValue.toString()) + "\n");
            }
         }

         for (TreeKeyValue keyValue : node.getStl())
         {
            if (keyValue.getValue() != null)
            {
               keyValue.correctSlashes();
               fw.write(addSpaces(node.getIntroSpaces() + 3) + getValue(keyValue.toString()) + "\n");
            }
         }

         fw.write("\n");

         for (TreeParent child : node.getChildren())
         {
            printNode(fw, child);
         }

      }

   }

   public static void addIfMissing(TreeParent node, String key, String value)
   {
      if (!keyExists(node, key))
      {
         node.getKeyValues().add(0, new TreeKeyValue(key + ": " + value));
      }
   }

   private static boolean keyExists(TreeParent node, String key)
   {
      for (TreeKeyValue kv : node.getKeyValues())
      {
         if (kv.getKey().trim().equals(key))
         {
           return true;  
         }
      }
      return false;
   }

//   private static boolean attributeExists(TreeParent node, String string)
//   {
//      // TODO Auto-generated method stub
//      return false;
//   }

   private static String getValue(String source)
   {
      if (source == null)
      {
         return "";
      }
      else
      {
         return source;
      }
   }

   private static boolean hasStartDate(List<TreeKeyValue> keyValues)
   {
      for (TreeKeyValue line : keyValues)
      {
         if (line.isStartDate())
         {
            return true;
         }
      }
      return false;
   }

   private static boolean hasEndDate(List<TreeKeyValue> keyValues)
   {
      for (TreeKeyValue line : keyValues)
      {
         if (line.isEndDate())
         {
            return true;
         }
      }
      return false;
   }

   private static boolean hasSource(List<TreeKeyValue> keyValues)
   {
      for (TreeKeyValue line : keyValues)
      {
         if (line.isSource())
         {
            return true;
         }
      }
      return false;
   }

   private static String getPathRecursive(TreeParent node, String path)
   {
      String pathHere = "/" + node.getName() + path;

      if (node.getParent() != null)
      {
         return getPathRecursive(node.getParent(), pathHere);
      }
      else
      {
         return pathHere;
      }

   }

   private static void collectDoubleImages(TreeParent node)
   {
      for (TreeKeyValue keyValue : node.getImage())
      {
         String value = keyValue.getValue();
         if (value != null && !value.isEmpty() && iImageHasSet.contains(value))
         {
            iDoubleImage.add(value);
         }
         iImageHasSet.add(value);
      }
   }

   private static void collectDoubleStl(TreeParent node)
   {
      for (TreeKeyValue keyValue : node.getStl())
      {
         String value = keyValue.getValue();
         if (value != null && !value.isEmpty() && iStlHasSet.contains(value))
         {
            iDoubleSTL.add(value);
         }
         iStlHasSet.add(value);
      }
   }

   private static void collectDoubleStlFileNames(TreeParent node)
   {
      for (TreeKeyValue keyValue : node.getStl())
      {
         String value2 = keyValue.getValue();
         File file = new File(value2);
         String value = file.getName();

         if (value != null && !value.isEmpty() && iStlHasSetFileNames.contains(value))
         {
            iDoubleSTLFileNames.add(value);
         }
         iStlHasSetFileNames.add(value);
      }
   }

   private static void collectMissingStl(TreeParent node, List<String> arr)
   {
      if (node.getStl().isEmpty() && node.getChildren().isEmpty() && node.getType().equalsIgnoreCase("Artifact"))
      {
         arr.add(node.toString());
      }

   }

   private static void collectMissingImages(TreeParent node, List<String> arr)
   {
      if (node.getImage().isEmpty() && node.getChildren().isEmpty())
      {
         arr.add(node.toString());
      }

   }

   public static void findDateIntervall(TreeParent node)
   {
      List<String> arr = new ArrayList<>();
      for (TreeKeyValue keyValue : node.getImage())
      {
         if (keyValue.isDate())
         {
            arr.add(keyValue.getValue());
         }
      }

      Collections.sort(arr);

      if (arr.size() > 1)
      {
         node.getKeyValues().add(0, new TreeKeyValue("EndDate: " + arr.get(arr.size() - 1).substring(0, 10)));
      }

      if (arr.size() > 0)
      {
         node.getKeyValues().add(0, new TreeKeyValue("StartDate: " + arr.get(0).substring(0, 10)));
      }

   }

   public static TreeParent loadRoot()
   {
      // String pathRoot = "Z:\\3D\\3d-utskrivet\\root";
      String savePath = "E:\\Dropbox\\Eclipse Projects\\php\\Sudoku\\root4.txt";
      TreeParent root = null;
      Map<Integer, TreeParent> iParentsMap = new HashMap<>();

      BufferedReader reader = null;
      int spacesLastLine = 0;
      TreeParent node = null;
      try
      {
         reader = new BufferedReader(new FileReader(savePath));
         String line = reader.readLine();
         TreeParent parentForNextNode = null;

         while (line != null)
         {
           // System.out.println(line);

            if (!line.trim().isEmpty())
            {

               int introSpaces = getIntroSpaces(line);
               String text = getText(line);
               if (root == null)
               {
                  if (!"root".equals(text) && introSpaces != 0)
                  {
                     System.out.println("File " + savePath + " must start with root on column 0");
                     System.exit(1);
                  }
                  else
                  {
                     root = new TreeParent(text, introSpaces, null);
                     parentForNextNode = root;
                     node = root;
                  }
               }
               else
               {
                  node = new TreeParent(text, introSpaces, null);
                  if (text.contains(":"))
                  {
                     parentForNextNode.addProperty(text);
                  }
                  else if (introSpaces <= spacesLastLine)
                  {
                     parentForNextNode = iParentsMap.get(introSpaces - 3);
                     if (parentForNextNode == null)
                     {
                        System.out
                              .println("No parent for  " + node + " that has " + (introSpaces - 3) + " spaces first.");
                        System.exit(1);
                     }
                     parentForNextNode.addChild(node);
                     parentForNextNode = node;
                  }
                  else
                  {
                     parentForNextNode.addChild(node);
                     parentForNextNode = node;
                  }
               }

               iParentsMap.put(introSpaces, node);
               spacesLastLine = introSpaces;
            }

            //System.out.println(parentForNextNode);
            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }

      return root;
   }

//   private static TreeParent getParent(TreeParent node, int spaces)
//   {
//      TreeParent parent = node.getParent();
//      if (parent.getIntroSpaces() >= spaces)
//      {
//         return getParent(parent, spaces);
//      }
//
//      return parent;
//   }
//
   private static String getText(String line)
   {
      return line.trim();
   }

   private static int getIntroSpaces(String line)
   {
      for (int i = 0; i < line.length(); i++)
      {
         if (line.charAt(i) == '\t')
         {
            System.out.println("Tab not allowed in line " + line);
            System.exit(1);
         }
         if (line.charAt(i) != ' ')
         {
            return i;
         }
      }
      return Integer.MAX_VALUE;
   }

//   private static void printFolder(String folderPath, FileWriter fw, int motherSpaces) throws IOException
//   {
//      File file = new File(folderPath);
//
//      fw.write(addSpaces(motherSpaces) + file.getName() + "\n");
//      int spaces = motherSpaces + 3;
//      for (File fileChild : file.listFiles())
//      {
//         if (fileChild.isDirectory())
//         {
//            printFolder(fileChild.getAbsolutePath(), fw, spaces);
//         }
//
//         if (fileChild.isFile())
//         {
//            printChildLine("Description", fw, spaces);
//            printChildLine("Image", fw, spaces, fileChild.getName());
//            printChildLine("Date", fw, spaces);
//            printChildLine("Source", fw, spaces);
//            printChildLine("Module", fw, spaces);
//            printChildLine("Url", fw, spaces);
//            printChildLine("Stl", fw, spaces);
//            fw.write("\n");
//         }
//      }
//   }

//   private static void printChildLine(String label, FileWriter fw, int spaces) throws IOException
//   {
//      printChildLine(label, fw, spaces, "");
//   }

   private static void printChildLine(String label, FileWriter fw, int spaces, String value) throws IOException
   {
      fw.write(addSpaces(spaces) + label + ": " + getValue(value) + "\n");
   }

   private static String addSpaces(int spaces)
   {
      return "                                                                                                           "
            .substring(0, spaces);
   }

}
