package common.utils.threede;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import common.utils.StringCompare;

public class FindScadModules
{

   public static void main(String[] args)
   {
      loadRoot2();

   }

   public static void loadRoot2()
   {
      String savePath = "E:\\Dropbox\\Eclipse Projects\\php\\Sudoku\\root2.txt";

      BufferedReader reader = null;
      try
      {
         reader = new BufferedReader(new FileReader(savePath));
         String line = reader.readLine();
         String lastHeading = null;

         while (line != null)
         {
            line = line.trim();

            if (!line.isEmpty())
            {
               if (!line.contains(":"))
               {
                  lastHeading = line;
               }

               if (line.startsWith("StartDate:"))
               {
//                  String result = lookForHeading(lastHeading);
//                  System.out.println(lastHeading + " -> " + result);
               }

            }

            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }
   }

   public static String lookForHeading(String name)
   {
      String savePath = "Z:\\3D\\3d-utskrivet\\youtube clips.txt";

      BufferedReader reader = null;

      List<CompareResult> lst = new ArrayList<>();

      try
      {
         reader = new BufferedReader(new FileReader(savePath));
         String line = reader.readLine();
         String lastHeading = null;

         while (line != null)
         {
            line = line.trim();
            //System.out.println(line);

            if (!line.isEmpty())
            {

               if (line.toLowerCase().contains(".scad"))
               {
                  lastHeading = line;
                  lst.add(new CompareResult(name, line, StringCompare.stringCompPoints(line, name), ""));
               }

               if (line.toLowerCase().contains(" module "))
               {
                  lst.add(new CompareResult(name, line, StringCompare.stringCompPoints(line, name), lastHeading));
               }

               
            }

            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }

      Collections.sort(lst);
      System.out.println("");
      int i = 0;
      for (CompareResult result : lst)
      {
         if (i < 10)
         {
            System.out.println(result);
         }
         i++;
      }

      if (lst.size() > 0)
      {
         return lst.get(0).toString();
      }
      else
      {
         return null;
      }
   }

}
