package common.utils.threede;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Rename3D
{

   public static void main(String[] args)
   {
      String pathRoot = "Z:\\3D\\3d-utskrivet\\root";
      String savePath = "E:\\Dropbox\\Eclipse Projects\\php\\Sudoku\\root.txt";

      try
      {
         FileWriter fw = new FileWriter(savePath);
         int spaces = 0;

         printFolder(pathRoot, fw, spaces);
         fw.close();
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

   }

   private static void printFolder(String folderPath, FileWriter fw, int motherSpaces) throws IOException
   {
      File file = new File(folderPath);

      fw.write(addSpaces(motherSpaces) + file.getName() + "\n");
      int spaces = motherSpaces + 3;
      for (File fileChild : file.listFiles())
      {
         if (fileChild.isDirectory())
         {
            printFolder(fileChild.getAbsolutePath(), fw, spaces);
         }

         if (fileChild.isFile())
         {
            printChildLine("Description", fw, spaces);
            printChildLine("Image", fw, spaces, fileChild.getName());
            printChildLine("Date", fw, spaces);
            printChildLine("Source", fw, spaces);
            printChildLine("Module", fw, spaces);
            printChildLine("Url", fw, spaces);
            printChildLine("Stl", fw, spaces);
            fw.write("\n");
         }
      }
   }

   private static void printChildLine(String label, FileWriter fw, int spaces) throws IOException
   {
      printChildLine(label, fw, spaces, "");
   }

   private static void printChildLine(String label, FileWriter fw, int spaces, String value) throws IOException
   {
      fw.write(addSpaces(spaces) + label + ": " + value + "\n");
   }

   private static String addSpaces(int spaces)
   {
      return "                                                                                                           "
            .substring(0, spaces);
   }

}
