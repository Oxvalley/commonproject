package common.utils;

import java.util.ArrayList;

public class WallList
{

   private ArrayList<Wall> iUnsolvedList;
   private ArrayList<Wall> iOKList;
   private ArrayList<Wall> iForbiddenList;

   public WallList()
   {
      iUnsolvedList = new ArrayList<>();
      iOKList = new ArrayList<>();
      iForbiddenList = new ArrayList<>();
   }

   public ArrayList<Wall> getUnsolvedList()
   {
      return iUnsolvedList;
   }

   public ArrayList<Wall> getOKList()
   {
      return iOKList;
   }

   public ArrayList<Wall> getForbiddenList()
   {
      return iForbiddenList;
   }

   public void addUnsolved(int x, int y, int direction)
   {
      iUnsolvedList.add(new Wall(x, y, direction));      
   }

   public void addOK(int x, int y, int direction)
   {
      iOKList.add(new Wall(x, y, direction));      
   }

   public void addForbidden(int x, int y, int direction)
   {
      iForbiddenList.add(new Wall(x, y, direction));      
   }

   
}
