package common.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.StringEscapeUtils;

public class AxosoftExporter
{

   public static void main(String[] args)
   {
      BufferedReader reader = null;
      boolean stopNow = false;
      Map<String, List<Map<String, String>>> all = new TreeMap<String, List<Map<String, String>>>();
      // List<Integer> ids = new ArrayList<Integer>();
      Set<String> possibleValues = new TreeSet<String>();
      try
      {
         reader = new BufferedReader(
               new InputStreamReader(new FileInputStream("20170926_items.csv"), "UTF8"));
         String line = reader.readLine();
         List<String> headers = null;

         while (line != null)
         {
            // if line not ends with , - Concat next line as well

            if (headers == null)
            {
               line = line.substring(1);
               headers = getValues(line);
               // System.out.println(headers);
            }
            else
            {
               // System.out.println(line);
               List<String> lines = getValues(line);

               while (lines.size() < headers.size())
               {
                  line += reader.readLine();
                  lines = getValues(line);
               }
               // System.out.println("Count: " + lines.size());
               int i = 0;
               Map<String, String> map = new HashMap<String, String>();
               for (String header : headers)
               {
                  String value = lines.get(i++);
                  map.put(header, value);

                  if (true)
                     ; // header.contains("Status"))// && value.equals("0"))
                  {
                     stopNow = true;
                     System.out.println(header + "=" + value);
                  }
               }
               // System.out.println("");

               String projectId = map.get("Project ID");
               String projectName = map.get("Project");

               String value1 = map.get("Release 2");
               if (value1 != null)
               {
                  possibleValues.add(value1);
               }

               // String Id = map.get("ID");
               // ids.add(Integer.parseInt(Id));

               String key = projectId + "_" + projectName;

               List<Map<String, String>> value = all.get(key);
               if (value == null)
               {
                  value = new ArrayList<Map<String, String>>();
                  all.put(key, value);
               }

               value.add(map);
            }

            if (stopNow)
            {
               System.out.println(line);
               // System.out.println("Now");
            }

            // Read next line for while condition
            line = reader.readLine();
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }

      for (String name : all.keySet())
      {
         List<Map<String, String>> lst = all.get(name);
         System.out.println(name + ". " + lst.size());
         createJson(name, lst);
      }
      System.out.println(possibleValues);

      // Collections.sort(ids);
      // for (Integer id : ids)
      // {
      // System.out.println(id);
      // }
   }

   private static void createJson(String name, List<Map<String, String>> lst)
   {
      try
      {
         FileWriter fw = new FileWriter(name + ".json");

         fw.write("      {\n");
         fw.write("         \"milestones\" : [],\n");
         fw.write("         \"attachments\" : [],\n");

         Set<String> releases = new TreeSet<String>();
         for (Map<String, String> case1 : lst)
         {
            String release = case1.get("Release");
            if (release != null && !release.isEmpty())
            {
               releases.add(release);
            }
         }

         String line = "         \"versions\" : [";
         if (releases.size() == 0)
         {
            fw.write(line + "],\n");
         }
         else
         {
            fw.write(line + "{\n");
            line = null;
            for (String release : releases)
            {
               if (line != null)
               {
                  fw.write(line);
               }
               fw.write("      \"name\" : \"" + release + "\"\n ");
               line = "      }, {\n";
            }
            fw.write("      }\n");
            fw.write("      ],\n");
            // "versions" : [{
            // "name" : "Eva24"
            // }, {
            // "name" : "Kalle25"
            // }
            // ],
         }
         fw.write("         \"comments\" : [],\n");
         fw.write("      \"meta\" : {\n");
         fw.write("      \"default_milestone\" : null,\n");
         fw.write("      \"default_assignee\" : null,\n");
         fw.write("      \"default_kind\" : \"bug\",\n");
         fw.write("      \"default_component\" : null,\n");
         fw.write("      \"default_version\" : null\n");
         fw.write("   },\n");
         fw.write("   \"components\" : [],\n");

         fw.write("   \"issues\" : [");

         String prevLine = null;

         for (Map<String, String> case1 : lst)
         {
            String axosoft = "(Axosoft " + case1.get("ID") + ")";
            if (prevLine != null)
            {
               fw.write(prevLine);
            }

            fw.write("      {\n");
            fw.write("         \"status\" : \"" + getStatus(case1.get("Workflow Step")) + "\",\n");
            fw.write("         \"priority\" : \"" + getPriority(case1.get("Priority")) + "\",\n");
            fw.write("         \"kind\" : \"" + getKind(case1.get("Item Type")) + "\",\n");
            fw.write("         \"content_updated_on\" : null,\n");
            fw.write("         \"voters\" : [],\n");
            fw.write("         \"title\" : \"" + getSafeText(case1.get("Name") + " " + axosoft)
                  + "\",\n");
            fw.write("         \"reporter\" : \"Oxvalley\",\n");
            fw.write("         \"component\" : null,\n");
            fw.write("         \"watchers\" : [\"Oxvalley\"],\n");
            fw.write("         \"content\" : \"" + StringEscapeUtils.unescapeHtml4(getSafeText(case1.get("Description"))) + "\",\n");
            fw.write("         \"assignee\" : null,\n");
            fw.write("         \"created_on\" : \"" + getDate(case1.get("Created Date")) + "\",\n");
            fw.write("         \"version\" : \"" + case1.get("Release") + "\",\n");
            // "version" : "Kalle25",
            fw.write("         \"edited_on\" : null,\n");
            fw.write("         \"milestone\" : null,\n");
            fw.write("         \"updated_on\" : \"" + getDate(case1.get("Last Modified Date"))
                  + "\",\n");
            fw.write("         \"id\" : " + case1.get("ID") + "\n");
            prevLine = "      },\n";
         }

         if (lst.size() > 0)
         {
            fw.write("      }\n");
         }

         fw.write("],\n");

         fw.write("   \"logs\" : []\n");
         fw.write("}\n");

         fw.close();
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }

   }

   private static String getSafeText(String text)
   {
      text = text.replace("\\", "\\\\");
      text = text.replace("\"\"", "Kalleponken");
      text = text.replace("\"", "\\\"");
      text = text.replace("&quot;", "\\\"");
      text = text.replace("Kalleponken", "\\\"");
      return text;
   }

   private static String getDate(String oldDate)
   {
      // Old: 31/03/2017 15:52:14
      // New: 2016-11-20T19:53:46.659362+00:00

      String retVal = null;
      SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
      SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ");
      try
      {
         Date date = formatter1.parse(oldDate);
         retVal = formatter2.format(date);
      }
      catch (ParseException e)
      {
         e.printStackTrace();
         System.exit(1);
      }

      return retVal;
   }

   private static String getKind(String oldKind)
   {
      // Old: defects
      // New: bug, enhancement, proposal, task

      String retVal = "bug";
      return retVal;
   }

   private static String getPriority(String oldPriority)
   {
      // Old: [, High, Higher, Highest, Low , Lower, Lowest, Medium]
      // New: trivial minor, major, critical, blocker

      String retVal = null;
      oldPriority = oldPriority.trim();

      if (oldPriority.equals("Highest"))
      {
         retVal = "blocker";
      }
      else if (oldPriority.equals("Higher"))
      {
         retVal = "critical";
      }
      else if (oldPriority.equals("High"))
      {
         retVal = "major";
      }
      else if (oldPriority.equals("Medium") || oldPriority.equals("Low"))
      {
         retVal = "minor";
      }
      else if (oldPriority.equals("Lower") || oldPriority.equals("Lowest") || oldPriority.isEmpty())
      {
         retVal = "trivial";
      }
      else
      {
         System.out.println("Bad priority: " + oldPriority);
         System.exit(1);
      }

      return retVal;
   }

   private static String getStatus(String oldStatus)
   {
      // Old: [Done, New, Rejected]
      // New: new, open, on hold, resolved, duplicate, invalid, wontfix, closed

      String retVal = null;

      if (oldStatus.equals("New"))
      {
         retVal = "new";
      }
      else if (oldStatus.equals("Done"))
      {
         retVal = "closed";
      }
      else if (oldStatus.equals("Rejected"))
      {
         retVal = "wontfix";
      }
      else
      {
         System.out.println("Bad Status: " + oldStatus);
         System.exit(1);
      }

      return retVal;
   }

   public static List<String> getValues(String line)
   {
      List<String> headers = new ArrayList<String>();
      String[] headArr = line.split("\",\"");
      for (String header : headArr)
      {
         if (header.startsWith("\""))
         {
            header = header.substring(1);
         }
         else
         {
            // System.out.println("Should start with quotation: " + header);
         }

         if (header.endsWith("\""))
         {
            header = header.substring(0, header.length() - 1);
         }
         else
         {
            // System.out.println("Should end with quotation: " + header);
         }
         headers.add(header);
      }

      return headers;
   }

}
