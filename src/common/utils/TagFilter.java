package common.utils;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class TagFilter extends FileFilter
{

   private String iTag;

   /**
    * Instantiates a new tag filter.
    *
    * @param tag the tag
    */
   public TagFilter(String tag)
   {
      iTag = tag;
   }

   /* (non-Javadoc)
    * @see javax.swing.filechooser.FileFilter#accept(java.io.File)
    */
   @Override
   public final boolean accept(File f)
   {
      return (f.isDirectory() || f.getName().endsWith("." + iTag));
   }

   /* (non-Javadoc)
    * @see javax.swing.filechooser.FileFilter#getDescription()
    */
   @Override
   public String getDescription()
   {
      return "." + iTag;
   }

}
