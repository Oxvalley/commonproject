package common.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class WebPageUtil
{

   public static String readStringFromURL(String requestURL) 
   {
       try (Scanner scanner = new Scanner(new URL(requestURL).openStream(),
               StandardCharsets.UTF_8.toString()))
       {
           scanner.useDelimiter("\\A");
           return scanner.hasNext() ? scanner.next() : "";
       }
      catch (MalformedURLException e)
      {
         return null;
      }
      catch (IOException e)
      {
         return null;
      }
   }
}
