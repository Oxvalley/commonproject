package common.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang3.StringUtils;

public class ModuleContainer
{

   private String iName;
   private ArrayList<String> iLines;
   private Map<String, String> iVariables;
   private ScriptEngineManager iMgr;
   private static ScriptEngine iEngine;

   public ModuleContainer(String name, Map<String, String> mainVariables)
   {
      super();
      iName = name;

      // Copy mainVaribles to iVariables
      iVariables = new HashMap<String, String>();
      for (String key : mainVariables.keySet())
      {
         iVariables.put(key, mainVariables.get(key));
      }
      iLines = new ArrayList<String>();
      iMgr = new ScriptEngineManager();
      iEngine = iMgr.getEngineByName("JavaScript");

   }

   @Override
   public String toString()
   {
      String text = "// new module\n";
      for (String line : iLines)
      {
         text += line + "\n";
      }

      text += iVariables + "\n\n";

      List<String> keyList = new ArrayList<>();

      for (String key : iVariables.keySet())
      {
         keyList.add(key);
      }

      Collections.sort(keyList, new LengthComparator());

      List<String> replaceList = new ArrayList<>();

      for (String line : iLines)
      {
         String replacedLine = line;

         replacedLine = replaceUnScale(replacedLine);
         replacedLine = replaceScale(replacedLine);

         for (String repKey : keyList)
         {
            if (replacedLine.contains(repKey))
            {
               replacedLine =
                     replacedLine.replace(repKey, iVariables.get(repKey));
               // System.out.println(">> " + replacedLine+ " " +
               // iVariables.get(repKey));

            }

         }

         replaceList.add(replacedLine);

         text += replacedLine + "\n";
      }

      List<String> replaceList2 = new ArrayList<>();
      for (String line : replaceList)
      {
         String replacedLine = line;

         for (String repKey : keyList)
         {
            if (replacedLine.contains(repKey))
            {
               replacedLine =
                     replacedLine.replace(repKey, iVariables.get(repKey));
               // System.out.println(">> " + replacedLine+ " " +
               // iVariables.get(repKey));

            }

         }
 
         replacedLine = evalArguments(replacedLine, "translate([", "]");
         replacedLine = evalArguments(replacedLine, "cube([", "]");
         replacedLine = evalArguments(replacedLine, "room(", ")");
         replacedLine = evalArguments(replacedLine, "window(", ")");
         replacedLine = evalArguments(replacedLine, "door(", ")");
         
         

         replaceList2.add(replacedLine);

         text += replacedLine + "\n";
      }

      return text;

   }

   public String evalArguments(String replacedLine, String prefix,
         String suffix)
   {
      int transIndex = replacedLine.indexOf(prefix);        
      if (transIndex > -1)
      {
         String inside = StringUtils.substringBetween(replacedLine, prefix,suffix);
         String[] each = inside.split(",");
         for (String eachString : each)
         {
            replacedLine = replacedLine.replace(eachString, eval(eachString));
         }            
      }
      return replacedLine;
   }

   public static String replaceUnScale(String line)
   {
      String[] x = StringUtils.substringsBetween(line, "unscale(", ")");

      if (x != null)
      {
         for (String match : x)
         {
            line = line.replace("unscale(" + match + ")",
                  "(" + match + ")*10");
         }
      }
      return line;
   }

   public static String replaceScale(String line)
   {
      String[] x = StringUtils.substringsBetween(line, "scale(", ")");

      if (x != null)
      {

         for (String match : x)
         {
            line = line.replace("scale(" + match + ")", "(" + match + ")/10");
         }
      }

      return line;
   }

   public void addLine(String line)
   {
      iLines.add(line);
   }

   public void put(String key, String value)
   {
      iVariables.put(key, value);
   }

   public static String eval(String expression)
   {
      if (expression ==  null || expression.isEmpty())
      {
         return expression;
      }
      
      if (expression.trim().startsWith("\"") && expression.trim().endsWith("\""))
      {
         return expression;
      }
      
      
      try
      {
         Object retVal = iEngine.eval(expression);
         
         if( retVal instanceof String)
         {
            return (String) retVal;
         }
         
         if( retVal instanceof Boolean || retVal instanceof Integer)
         {
            return String.valueOf(retVal);
         }

         
         return String.valueOf(Math.round((Double) retVal ));
      }
      catch (ScriptException e)
      {
         return expression;
      }
   }
}
