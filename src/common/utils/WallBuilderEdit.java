package common.utils;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class WallBuilderEdit extends WallBuilderBoard
{

   private static final long serialVersionUID = 1L;
   protected static final char CURSOR = '*';
   private int posX = 0;
   private int posY = 0;
   private boolean cursorVisible = true; // Used for blinking cursor
   private Timer blinkTimer;
   private char currentChar;
   private boolean iEditWalls = false;
   private int posDirection = 0;
   private Color currentWallColor;
   private Color CURSOR_COLOR;
   private Color RESET_WALL_COLOR;
   private Color WALL_COLOR;

   public WallBuilderEdit(String[] board, List<Wall> kw)
   {
      super(board, kw,
            "Use cursors and digit. Erase with <Space>. Toggle walls with 'P'. Done with 'D' (Stand in an empty square when using 'D').");

      CURSOR_COLOR = TESTING_COLOR;
      RESET_WALL_COLOR = UNSOLVED_COLOR;
      WALL_COLOR = OK_COLOR;
      currentWallColor = RESET_WALL_COLOR;

      // Create a Timer for blinking every 500 ms
      blinkTimer = new Timer(400, new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            cursorVisible = !cursorVisible; // Toggle visibility

            // Check if there is a character at the cursor position and blink it
            // every second time

            if (iEditWalls)
            {
               Color color = currentWallColor;
               if (cursorVisible)
               {
                  color = CURSOR_COLOR;
               }
               pokeWall(color);
            }
            else
            {
               poke(cursorVisible ? CURSOR : currentChar);
            }
            repaint();
         }

      });
      blinkTimer.start();

      addMouseListener(new MouseAdapter()
      {
         @Override
         public void mouseClicked(MouseEvent e)
         {
            WallBuilder.print("Mouse clicked");
         }
      });

      addKeyListener(new KeyListener()
      {

         @Override
         public void keyTyped(KeyEvent e)
         {
            // TODO Auto-generated method stub
         }

         @Override
         public void keyReleased(KeyEvent e)
         {
            // TODO Auto-generated method stub
         }

         @Override
         public void keyPressed(KeyEvent e)
         {
            char c = e.getKeyChar();
            // WallBuilder.print("Key pressed: " + c);

            if (c == 'p')
            {
               // WallBuilder.print("Toggle Walls");
               iEditWalls = !iEditWalls;

               if (iEditWalls)
               {
                  // Turn off cursor
                  poke('|');

               }
               else
               {
                  if (posX > 8)
                  {
                     posX = 8;
                  }

                  if (posY > 8)
                  {
                     posY = 8;
                  }
               }
            }

            if (c == 'd')
            {
               scriptBoard();
            }

            if (((Character.isDigit(c) && (c - '0') < 4) || c == ' ') && !iEditWalls)
            {
               poke(c);
               moveRight();
               repaint();
            }

            if (c == ' ' && iEditWalls)
            {
               if (currentWallColor.equals(WALL_COLOR))
               {
                  pokeWall(RESET_WALL_COLOR);
               }
               else
               {
                  pokeWall(WALL_COLOR);
               }
            }

            // Move with cursors
            switch (e.getKeyCode())
            {
               case KeyEvent.VK_UP:
                  if (iEditWalls)
                  {
                     if (posY > 0 || (posY == 0 && posDirection == 1))
                     {
                        pokeWall(currentWallColor);
                        posDirection = 1 - posDirection;
                        if (posDirection == 1)
                        {
                           posY--;
                        }
                        pokeWall(CURSOR_COLOR);
                     }
                  }
                  else
                  {
                     if (posY > 0)
                     {
                        poke('|');
                        posY--;
                        poke(CURSOR);
                     }
                  }
                  break;
               case KeyEvent.VK_DOWN:
                  if (iEditWalls)
                  {
                     if (posY < SIZE)
                     {
                        pokeWall(currentWallColor);
                        posDirection = 1 - posDirection;
                        if (posDirection == 0)
                        {
                           posY++;
                        }
                        pokeWall(CURSOR_COLOR);
                     }
                  }
                  else
                  {
                     if (posY < SIZE - 1)
                     {
                        poke('|');
                        posY++;
                        poke(CURSOR);
                     }
                  }
                  break;
               case KeyEvent.VK_LEFT:
                  if (posX > 0)
                  {
                     if (iEditWalls)
                     {
                        pokeWall(currentWallColor);
                        posX--;
                        pokeWall(CURSOR_COLOR);
                     }
                     else
                     {
                        poke('|');
                        posX--;
                        poke(CURSOR);
                     }
                  }
                  break;
               case KeyEvent.VK_RIGHT:
                  if (iEditWalls)
                  {
                     if ((posX < SIZE - 1 && posDirection == 0) || (posX < SIZE && posDirection == 1))
                     {

                        pokeWall(currentWallColor);
                        posX++;
                        pokeWall(CURSOR_COLOR);
                     }
                  }
                  else
                  {
                     if (posX < SIZE - 1)
                     {
                        poke('|');
                        posX++;
                        poke(CURSOR);
                     }
                  }
                  break;
               default:
                  break;
            }

            repaint(); // M�la om efter flytt

         }

      });

      SwingUtilities.invokeLater(() -> createAndShowGUI(this));

   }

   private void scriptBoard()
   {
      WallBuilder.print("\n      // Number X");
      WallBuilder.print("      board = new String[SIZE];");
      int i = 0;
      for (String line : iBoard)
      {
         WallBuilder.print("      board[" + i + "] = \"" + line.replace("*", " ") + "\";");
         i++;
      }
      WallBuilder.print("      kw = new ArrayList<Wall>();");
      for (int y = 0; y < SIZE + 1; y++)
      {
         for (int x = 0; x < SIZE + 1; x++)
         {
            int x2 = x;
            int y2 = y;

            if (colors[x2][y2][0].equals(WALL_COLOR))
            {
               String direction = "ABOVE";
               if (y2 == SIZE)
               {
                  direction = "BELOW";
                  y2--;
               }

               WallBuilder.print("      kw.add(new Wall(" + x2 + ", " + y2 + ", " + direction + "));");
            }

            if (colors[x2][y2][1].equals(WALL_COLOR))
            {
               String direction = "LEFT";
               if (x2 == SIZE)
               {
                  direction = "RIGHT";
                  x2--;
               }
               WallBuilder.print("      kw.add(new Wall(" + x2 + ", " + y2 + ", " + direction + "));");
            }
         }
      }

      WallBuilder.print("      iBoardList.add(new SavedBoard(board, kw));");
   }

   private void moveRight()
   {
      if (posX < SIZE - 1)
      {
         posX++;
      }
      poke(CURSOR);
   }

   private void poke(char c)
   {
      char cc = iBoard[posY].charAt(posX);
      if (cc != '*')
      {
         currentChar = cc;
      }

      if (c == '|' && currentChar != '|')
      {
         poke(currentChar);
         return;
      }

      String line = iBoard[posY];
      String retVal = "";
      if (posX > -1)
      {
         retVal = line.substring(0, posX);
      }

      retVal += c;

      if (posX < SIZE)
      {
         retVal += line.substring(posX + 1);
      }

      iBoard[posY] = retVal;
   }

   private void pokeWall(Color color)
   {
      Color cc = getWallColor();
      if (!cc.equals(CURSOR_COLOR))
      {
         currentWallColor = cc;
      }

      colors[posX][posY][posDirection] = color;
      cc = getWallColor();
      if (!cc.equals(CURSOR_COLOR))
      {
         currentWallColor = cc;
      }

      repaint();
   }

   private Color getWallColor()
   {
      return colors[posX][posY][posDirection];
   }

   public static void main(String[] args)
   {
      String[] board3 = { "*        ", "         ", "         ", "         ", "         ", "         ", "         ",
         "         ", "         " };
      List<Wall> kw3 = new ArrayList<>();
      WallBuilderEdit wbe = new WallBuilderEdit(board3, kw3);
   }

}
