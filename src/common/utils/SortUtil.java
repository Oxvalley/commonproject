package common.utils;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class SortUtil
{

   public static List<String> sortSwedishStrings(List<String> list)
   {
      Collections.sort(list, new Comparator<String>()
      {

         @Override
         public int compare(String o1, String o2)
         {
            Collator sweCollator = Collator.getInstance(new Locale("sv", "SE"));
            sweCollator.setStrength(Collator.PRIMARY);
            return sweCollator.compare(o1, o2);
         }
      });

      return list;
   }

}
