package common.utils;

public class BasicConstants
{
   public static final String PROP_USED_LANGUAGE = "language.used";
   public static final String PROP_LANGUAGE_SWEDISH = "language.swedish";
   public static final String PROP_LANGUAGE_ENGLISH = "language.english";
}
