package common.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;

import common.properties.SettingProperties;
import common.ui.components.ext.AppInfoExample;

/**
 * The Class PackInstallationUtil.
 */
public class PackInstallationUtil
{

   /** The Version. */
   protected String iVersion;

   /** The App name. */
   protected String iAppName;

   /** The Folder name. */
   protected String iFolderName;

   /** The App name version. */
   protected static String iAppNameVersion;

   /** The Jdk version. */
   private String iJdkVersion;

   /** The Constant PREFS_JDK_PATH. */
   public static final String PREFS_JDK_PATH = "Jdk.Path";

   /** The Constant LIB_DEST_DIR. */
   public static final String LIB_DEST_DIR = "lib";

   /** The Main class N ame. */
   private String iMainClassNAme;

   /** The Pack source. */
   private boolean iPackSource;

   private boolean iCreateJavadoc;

   /**
    * Instantiates a new pack installation util.
    *
    * @param appName
    *           the app name
    * @param version
    *           the version
    * @param jdkVersion
    *           the jdk version
    * @param appNameVersion
    *           the app name version
    * @param mainClassNAme
    *           the main class N ame
    * @param packSource
    *           the pack source
    */
   public PackInstallationUtil(String appName, String version, String jdkVersion,
         String appNameVersion, String mainClassNAme, boolean packSource, boolean createJavadoc)
   {
      iAppName = appName;
      iVersion = version;
      iJdkVersion = jdkVersion;
      iAppNameVersion = appNameVersion;
      iMainClassNAme = mainClassNAme;
      iPackSource = packSource;
      iCreateJavadoc = createJavadoc;
      iFolderName = iAppName + "_v" + iVersion;
   }

   /**
    * Gets the version no dots.
    *
    * @param version
    *           the version
    * @return the version no dots
    */
   protected static String getVersionNoDots(String version)
   {
      return version.replace(".", "_");
   }

   /**
    * The main method.
    *
    * @param args
    *           the arguments
    */
   public static void main(String[] args)
   {
      String name = AppInfoExample.getApplicationName();
      String version = AppInfoExample.getVersion();
      String jdkVersion = AppInfoExample.getJdkVersion();
      PackInstallationUtil pi = new PackInstallationUtil(name, version, jdkVersion,
            getVersionNoDots(version), "my.fake.main.class", false, false);

      pi.execute();
   }

   /**
    * Execute.
    */
   public void execute()
   {
      String jdkPath = locateJDKJar(getJdkVersion());
      packAll(jdkPath);
   }

   /**
    * Gets the jdk version.
    *
    * @return the jdk version
    */
   private String getJdkVersion()
   {
      return iJdkVersion;
   }

   /**
    * Pack source.
    */
   protected void packSource()
   {
      if (iPackSource)
      {
         for (String project : getProjectDependendies())
         {
            LogUtil.print("Copying folder " + project + File.separator + "src");
            FileUtil.copyFoldertoFolder(project + File.separator + "src", iFolderName);
         }
      }
   }

   /**
    * Creates the folder and log.
    *
    * @param folderName
    *           the folder name
    */
   protected void createFolderAndLog(String folderName)
   {
      LogUtil.print("Creating folder " + folderName);
      createFolder(folderName);
   }

   /**
    * Creates the folder.
    *
    * @param folderName
    *           the folder name
    */
   protected void createFolder(String folderName)
   {
      new File(iFolderName + File.separator + folderName).mkdirs();
   }

   /**
    * Copy file.
    *
    * @param fileName
    *           the file name
    */
   protected void copyFile(String fileName)
   {
      try
      {
         FileUtil.copy(fileName, iFolderName, true);
      }
      catch (IOException e)
      {
         LogUtil.print(e.getMessage());
      }
   }

   /**
    * Copy file and log.
    *
    * @param fileName
    *           the file name
    */
   protected void copyFileAndLog(String fileName)
   {
      LogUtil.print("Copying file " + fileName);
      copyFile(fileName);
   }

   /**
    * Copy file and log.
    *
    * @param fileName
    *           the file name
    * @param newFileName
    *           the new file name
    */
   protected void copyFileAndLog(String fileName, String newFileName)
   {
      LogUtil.print("Copying file " + fileName + " to " + newFileName);
      copyFile(fileName, newFileName);
   }

   /**
    * Copy file.
    *
    * @param fileName
    *           the file name
    * @param newFileName
    *           the new file name
    */
   protected void copyFile(String fileName, String newFileName)
   {
      try
      {
         FileUtil.copy(fileName, iFolderName, true);
      }
      catch (IOException e)
      {
         LogUtil.print(e.getMessage());
      }
      FileUtil.rename(iFolderName + File.separator + fileName,
            iFolderName + File.separator + newFileName);
   }

   /**
    * Copy jar to lib.
    *
    * @param fileName
    *           the file name
    */
   protected void copyJarToLib(String fileName)
   {
      try
      {
         FileUtil.copy(fileName, iFolderName + File.separator + "lib", true);
      }
      catch (IOException e)
      {
         LogUtil.print(e.getMessage());
      }
   }

   /**
    * Copy jar to lib.
    *
    * @param fileName
    *           the file name
    * @param newJarName
    *           the new jar name
    */
   protected void copyJarToLib(String fileName, String newJarName)
   {
      try
      {
         FileUtil.copy(fileName, iFolderName + File.separator + "lib", true);
      }
      catch (IOException e)
      {
         LogUtil.print(e.getMessage());
      }
      FileUtil.rename(iFolderName + File.separator + "lib" + File.separator + fileName,
            iFolderName + File.separator + "lib" + File.separator + newJarName);
   }

   /**
    * Pack all.
    *
    * @param jdkPath
    *           the jdk path
    */
   public void packAll(String jdkPath)
   {
      cleanInstallation();
      createDir();
      createManifest();
      createJar(jdkPath);
      copyLibFolder();
      copyFilesAndFolders();
      if (iCreateJavadoc)
      {
         createJavaDoc(jdkPath);         
      }
      createRunFiles();
      if (iPackSource)
      {
         packSource();
      }
      removeSvnFolders();
      zipFolder();
      LogUtil.print("Installation zip complete!");
   }

   public void createJavaDoc(String JDKjarPath)
   {
       List<String> list = new ArrayList<String>();
      list.add(JDKjarPath + "/javadoc");
      list.add("-d");
      list.add(iFolderName + "/javadoc");
      list.add("-sourcepath");
      list.add("src");
      list.add("-subpackages");

      String subDirs = "";
      File dirs = new File("src");
      for (File dir : dirs.listFiles())
      {
         if (dir.isDirectory())
         {
            if (!subDirs.isEmpty())
            {
               subDirs += RunDosUtil.getOSDependentPathSeparator();
            }
            subDirs += dir.getName();
         }
      }

      list.add(subDirs);
      LogUtil.print(list.toString());

      ProcessBuilder pb = new ProcessBuilder(list);
      try
      {
         pb.directory(new File("."));
         Process p = pb.start();
         ProcMon procMon = ProcMon.create(p);
         while (!procMon.isComplete())
            ;
      }
      catch (IOException e)
      {
         LogUtil.print(e.getMessage());
      }
      catch (Exception e)
      {
         LogUtil.print(e.getMessage());
      }

   }

   /**
    * Removes the svn folders.
    */
   private void removeSvnFolders()
   {
      LogUtil.print("Removing .svn folders and .gitignore files");
      FileUtil.removeDirectoriesRecursive(iFolderName, ".svn");
      FileUtil.removeFilesRecursive(iFolderName, ".gitignore");
   }

   /**
    * Copy lib folder.
    */
   protected void copyLibFolder()
   {
      for (String projectPath : getProjectDependendies())
      {
         copyLibFolder(projectPath + File.separator + LIB_DEST_DIR);
      }

   }

   /**
    * Copy lib folder.
    *
    * @param folderName
    *           the folder name
    */
   protected void copyLibFolder(String folderName)
   {
      File file = new File(folderName);
      if (file.exists() && file.isDirectory())
      {
         LogUtil.print("Copying " + folderName + " to lib folder");
         FileUtil.copyFolder(folderName, iFolderName + File.separator + LIB_DEST_DIR);
      }
      else
      {
         LogUtil.print("Skipping Folder " + folderName + ". It does not exist");
      }
   }

   /**
    * Gets the project dependendies.
    *
    * @return the project dependendies
    */
   protected List<String> getProjectDependendies()
   {
      List<String> list = new ArrayList<String>();
      list.add(".");
      return list;
   }

   /**
    * Gets the jar path.
    *
    * @return the jar path
    */
   protected String getJarPath()
   {
      return "../" + iAppNameVersion + ".jar";
   }

   /**
    * Copy folder.
    *
    * @param folder
    *           the folder
    */
   protected void copyFolder(String folder)
   {
      FileUtil.copyFoldertoFolder(folder, iFolderName);
   }

   /**
    * Copy folder and log.
    *
    * @param folder
    *           the folder
    */
   protected void copyFolderAndLog(String folder)
   {
      System.out.println("Copying folder " + folder);
      copyFolder(folder);
   }

   /**
    * Copy folder.
    *
    * @param folder
    *           the folder
    * @param toFolder
    *           the to folder
    */
   protected void copyFolder(String folder, String toFolder)
   {
      FileUtil.copyFoldertoFolder(folder, iFolderName + File.separator + toFolder);
   }

   /**
    * Locate JDK jar.
    *
    * @param jdkVersion
    *           the jdk version
    * @return the string
    */
   public static String locateJDKJar(String jdkVersion)
   {
      SettingProperties prop = SettingProperties.getInstance();
      String jdkPath =
            prop.getProperty(PREFS_JDK_PATH, "C:\\Program Files\\Java\\jdk1.6.0_45b\\bin");
      String jarPath = jdkPath + "/jar.exe";
      File jarFile = new File(jarPath);
      if (!jarFile.exists())
      {
         JFileChooser fc = new JFileChooser();
         fc.setCurrentDirectory(new File(jdkPath));
         fc.setDialogTitle("You need to set correct path to your installed jdk/bin folder ("
               + jdkVersion + "+)");
         fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
         int retval = fc.showOpenDialog(null);
         if (retval == JFileChooser.APPROVE_OPTION)
         {
            File myFile = fc.getSelectedFile();
            jdkPath = myFile.getAbsolutePath();
            jarPath = jdkPath + "/jar.exe";
            jarFile = new File(jarPath);
            if (!jarFile.exists())
            {
               // Try looking for a bin folder
               jdkPath += "/bin";
               jarPath = jdkPath + "/jar.exe";
               jarFile = new File(jarPath);
               if (!jarFile.exists())
               {
                  locateJDKJar(jdkVersion);
               }
            }
            prop.setProperty(PREFS_JDK_PATH, jdkPath);
            SettingProperties.getInstance().save();
         }
         else
         {
            System.exit(1);
         }
      }

      return jdkPath;
   }

   /**
    * Zip folder.
    */
   protected void zipFolder()
   {
      LogUtil.print("Zipping folder " + iFolderName);
      String zipFileName;
      if (iPackSource)
      {
         zipFileName = iFolderName + "-src";
      }
      else
      {
         zipFileName = iFolderName;
      }

      ZipUtil.zip(iFolderName, zipFileName);

   }

   /**
    * Clean installation.
    */
   protected void cleanInstallation()
   {
      LogUtil.print("Deleting folder " + iFolderName);
      deleteFolder(iFolderName);
   }

   /**
    * Copy files and folders.
    */
   protected void copyFilesAndFolders()
   {
      LogUtil.print("Copying files and folders");
      // Todo Add stuff here
   }

   /**
    * Creates the manifest.
    */
   protected void createManifest()
   {
      LogUtil.print("Creating Manifest file");
      File file = new File("bin/Manifest.mf");
      try
      {
         Writer w = new FileWriter(file);
         w.write("Manifest-Version: 1.0\n");
         w.write("Main-Class:  " + iMainClassNAme + "\n");
         w.close();
      }
      catch (IOException e)
      {
         LogUtil.print(e.getMessage());
      }
   }

   /**
    * Creates the run files.
    */
   protected void createRunFiles()
   {
      createRunCMDFile();
      createRunSHFile();
   }

   /**
    * Creates the run CMD file.
    */
   protected void createRunCMDFile()
   {
      String fileName = "run " + iAppNameVersion + ".cmd";
      LogUtil.print("Creating file " + fileName);
      File file = new File(iFolderName + File.separator + fileName);
      try
      {
         Writer w = new FileWriter(file);
         // java -cp "%~dp0CC_v_2_3.jar;%~dp0lib/*;." core.CrossWordCreator %*         
         w.write("java -cp \"%~dp0" + iAppNameVersion + ".jar;%~dp0lib/*;.\" " + iMainClassNAme + " %*\n");
         w.close();
      }
      catch (IOException e)
      {
         LogUtil.print(e.getMessage());
      }
   }

   /**
    * Creates the run SH file.
    */
   protected void createRunSHFile()
   {
      String fileName = "run_" + iAppNameVersion + ".sh";
      LogUtil.print("Creating file " + fileName);
      File file = new File(iFolderName + File.separator + fileName);
      try
      {
         Writer w = new FileWriter(file);
         w.write("java -cp \"" + iAppNameVersion + ".jar:lib/*:.\" " + iMainClassNAme + "\n");
         w.close();
         // TODO Make executable (X-flag)
         // Java 1.7
         // Set<PosixFilePermission> perms = new HashSet<>();
         // perms.add(PosixFilePermission.OWNER_READ);
         // perms.add(PosixFilePermission.OWNER_WRITE);
         //
         // Files.setPosixFilePermissions(file.toPath(), perms);
      }
      catch (IOException e)
      {
         LogUtil.print(e.getMessage());
      }
   }

   /**
    * Creates the dir.
    */
   protected void createDir()
   {
      LogUtil.print("Creating folder " + iFolderName);
      File file = new File(iFolderName);
      file.mkdir();
   }

   /**
    * Creates the jar.
    *
    * @param JDKjarPath
    *           the JD kjar path
    */
   protected void createJar(String JDKjarPath)
   {
      LogUtil.print("Creating jar file");
      String classFolder = iFolderName + "/bin";
      LogUtil.print("Creating folder " + classFolder);
      File classFolderFile = new File(classFolder);
      classFolderFile.mkdir();
      for (String dirPath : getProjectDependendies())
      {
         LogUtil.print("Copying folder " + dirPath + File.separator + "bin");
         FileUtil.copyFolder(dirPath + File.separator + "bin", classFolder);
      }

      List<String> list = new ArrayList<String>();
      list.add(JDKjarPath + "/jar.exe");
      list.add("cfm");
      list.add(getJarPath());
      list.add("Manifest.mf");

      File dir = new File(classFolder);
      if (!dir.exists())
      {
         LogUtil.print("Folder does not exist: " + dir.getAbsolutePath());
      }
      else
      {
         for (File file : dir.listFiles())
         {
            if (file.isDirectory() && !file.getName().equals(".svn"))
            {
               list.add(file.getName());
            }
         }

         ProcessBuilder pb = new ProcessBuilder(list);
         try
         {
            pb.directory(new File(classFolder));
            Process p = pb.start();
            ProcMon procMon = ProcMon.create(p);
            while (!procMon.isComplete())
               ;
            LogUtil.print("Deleting folder " + classFolder);
            deleteFolder(classFolder);
         }
         catch (IOException e)
         {
            LogUtil.print(e.getMessage());
         }
         catch (Exception e)
         {
            LogUtil.print(e.getMessage());
         }
      }

   }

   /**
    * Delete folder.
    *
    * @param folderPath
    *           the folder path
    */
   private static void deleteFolder(String folderPath)
   {
      try
      {
         FileUtil.deleteFolder(folderPath);

         // Wait until done or maximum 20 seconds
         long millis = System.currentTimeMillis();
         while (new File(folderPath).exists() && System.currentTimeMillis() - millis < 20000)
         {
         }
      }
      catch (Exception e)
      {
         LogUtil.print(e.getMessage());
      }
   }

   /**
    * Prints the command line.
    *
    * @param pb
    *           the pb
    */
   public static void printCommandLine(ProcessBuilder pb)
   {
      String commandLine = "";
      for (String command : pb.command())
      {
         commandLine += "\"" + command + "\" ";
      }

      LogUtil.print(commandLine.trim());
   }

}
