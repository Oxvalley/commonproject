package common.utils;

public class SystemUtil
{

   public static String getComputerName()
   {
      try
      {
         java.net.InetAddress i = java.net.InetAddress.getLocalHost();
         return i.getHostName();
      }
      catch (Exception e)
      {
         System.out.println("Hostname can not be resolved");
         return "Unknown";
      }
   }
   
   /**
    * For debugging. See the stacktrace getting here
    * @param message
    */
   public static void throwRuntimeException(String message)
   {
      try
      {
         throw new RuntimeException(message);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

}
