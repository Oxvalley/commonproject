package common.utils;

import javax.swing.ImageIcon;


/**
 * The Interface AppInfoInterface.
 */
public interface AppInfoInterface
{
   
   /**
    * Gets the application name.
    *
    * @return the application name
    */
   public String getApplicationName();
   
   /**
    * Gets the version.
    *
    * @return the version
    */
   public String getVersion();
   
   /**
    * Gets the icon.
    *
    * @return the icon
    */
   public ImageIcon getIcon();

   /**
    * Gets the copywright text.
    *
    * @return the copywright text
    */
   public String getCopywrightText();
   
   /**
    * Gets the jdk version.
    *
    * @return the jdk version
    */
   public String getJdkVersion();   
}
