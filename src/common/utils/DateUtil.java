package common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * The Class DateUtil.
 */
public class DateUtil
{
   
   // TODO prio 3 read properties to change date format
   private static final String i_DateFormatString = "yyyy-MM-dd";
   public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

   
   public static String now() {
     Calendar cal = Calendar.getInstance();
     SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_FORMAT);
     return sdf.format(cal.getTime());

   }



   private static DateFormat i_DateFormat = new SimpleDateFormat(
         i_DateFormatString);

   public static Date getDateFromString(String value)
   {
      if (value == null)
      {
         return null;
      }

      try
      {
         return i_DateFormat.parse(value);
      }
      catch (ParseException e)
      {
         System.out.println("Bad Date: " + value);
         return null;
      }
   }

   public static String getDateFormatString()
   {
      return i_DateFormatString;
   }

   public static String getStringFromDate(Date value)
   {
      if (value == null)
      {
         return null;
      }

      return i_DateFormat.format(value);
   }

}
