package common.utils;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class RunPrograms
{

   public static void browseURL(String pdfName)
   {
      browseURL(null, pdfName);

   }

   public static void browseURL(String folder, String fileName)
   {
      if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE))
      {
         try
         {
            String url;

            if (folder == null)
            {
               url = "file:///" + fileName;
            }
            else
            {
               url = "file:///" + new File(folder).getAbsolutePath() + "/" + fileName;
            }

            url = url.replace("\\", "/").replace(" ", "%20");
            Desktop.getDesktop().browse(new URI(url));
         }
         catch (IOException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         catch (URISyntaxException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }
   }
   
}
