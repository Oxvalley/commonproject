package common.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class ImageHelper
{

   private static final double FONT_SIZE_FACTOR = 1.8;
   private static Map<String, String> i_taglist;
   private static String iLangFolderPath;

   public static JLabel getJLabelWithImage(String filename)
   {
      JLabel image = null;
      try
      {
         image = new JLabel(new ImageIcon(ImageIO.read(new File(filename))));
      }
      catch (IOException e)
      {
         System.out.println("File " + filename + " not found!");
         e.printStackTrace();
      }
      return image;
   }

   public static String getLangFilesFolderPath(String currentPath)
   {
      if (iLangFolderPath == null)
      {
         String languageFileFolder = "languages";
         if (currentPath != null)
         {
            languageFileFolder = currentPath + "/" + languageFileFolder;
         }

         iLangFolderPath = new File(languageFileFolder).getAbsolutePath();
      }

      return iLangFolderPath;
   }

   public static String getLangFilesFolderPath()
   {
      return getLangFilesFolderPath(null);
   }

   public static JLabel getJLabelWithImage(String imagefilePath, int x, int y, int width, int height, String tooltip)
   {
      JLabel label = new JLabel();
      setImageOnJComponent(x, y, width, height, tooltip, label);
      label.setIcon(ImageHelper.loadIcon(imagefilePath, width, height));
      return label;
   }

   public static boolean isImageFile(String filename2)
   {
      String filetag = getFileExtentension(filename2);
      if (i_taglist == null)
      {
         i_taglist = getFileExtensionsList();
      }
      return i_taglist.containsKey(filetag);
   }

   public static ImageIcon loadIcon(String filePath)
   {
      return loadIcon(filePath, 0, 0);
   }

   // This method returns a buffered image with the contents of an image
   public static BufferedImage toBufferedImage(Image image2)
   {
      Image image = image2;
      if (image instanceof BufferedImage)
      {
         return (BufferedImage) image;
      }

      // This code ensures that all the pixels in the image are loaded
      image = new ImageIcon(image).getImage();

      // Determine if the image has transparent pixels; for this method's
      // implementation, see e661 Determining If an Image Has Transparent Pixels
      boolean hasAlpha = hasAlpha(image);

      // Create a buffered image with a format that's compatible with the screen
      BufferedImage bimage = null;
      GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
      try
      {
         // Determine the type of transparency of the new buffered image
         int transparency = Transparency.OPAQUE;
         if (hasAlpha)
         {
            transparency = Transparency.BITMASK;
         }

         // Create the buffered image
         GraphicsDevice gs = ge.getDefaultScreenDevice();
         GraphicsConfiguration gc = gs.getDefaultConfiguration();
         bimage = gc.createCompatibleImage(image.getWidth(null), image.getHeight(null), transparency);
      }
      catch (HeadlessException e)
      {
         // The system does not have a screen
      }

      if (bimage == null)
      {
         // Create a buffered image using the default color model
         int type = BufferedImage.TYPE_INT_RGB;
         if (hasAlpha)
         {
            type = BufferedImage.TYPE_INT_ARGB;
         }
         bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
      }

      // Copy image to buffered image
      Graphics g = bimage.createGraphics();

      // Paint the image onto the buffered image
      g.drawImage(image, 0, 0, image.getWidth(null), image.getHeight(null), null);
      g.dispose();

      return bimage;
   }

   // This method returns true if the specified image has transparent pixels
   public static boolean hasAlpha(Image image)
   {
      // If buffered image, the color model is readily available
      if (image instanceof BufferedImage)
      {
         BufferedImage bimage = (BufferedImage) image;
         return bimage.getColorModel().hasAlpha();
      }

      // Use a pixel grabber to retrieve the image's color model;
      // grabbing a single pixel is usually sufficient
      PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
      try
      {
         pg.grabPixels();
      }
      catch (InterruptedException e)
      {
      }

      // Get the image's color model
      ColorModel cm = pg.getColorModel();
      return cm.hasAlpha();
   }

   public static ImageIcon loadIcon(String filePath, int width, int height)
   {
      Image img1 = loadIconImage(filePath);
      Image img2 = null;
      if (width + height > 0)
      {
         img2 = img1.getScaledInstance(width, height, Image.SCALE_SMOOTH);
      }
      else
      {
         img2 = img1;
      }

      return new ImageIcon(img2);
   }

   public static Image loadIconImage(String filePath)
   {
      return Toolkit.getDefaultToolkit().createImage(loadImage(filePath).getSource());
   }

   public static BufferedImage loadImage(String filenmame)
   {
      BufferedImage image;

      try
      {
         image = ImageIO.read(new File(filenmame));
      }
      catch (IOException ex)
      {
         JOptionPane.showMessageDialog(null, "Could not load image " + filenmame, null, JOptionPane.ERROR_MESSAGE);
         return null;
      }
      return image;
   }

   public static JButton getJButtonWithImage(String imagefilePath, int x, int y, int width, int height, String tooltip)
   {
      JButton button = new JButton();
      setImageOnJComponent(x, y, width, height, tooltip, button);
      if (new File(imagefilePath).exists())
      {
         button.setIcon(ImageHelper.loadIcon(imagefilePath, width, height));
      }
      return button;
   }

   public static void setImageOnJComponent(int x, int y, int width, int height, String tooltip, JComponent label)
   {
      label.setToolTipText(tooltip);
      label.setBounds(x, y, width, height);
   }

   public static ImageIcon loadImage(BufferedImage image, int width, int height)
   {
      BufferedImage scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

      Graphics2D graphics2D = scaledImage.createGraphics();
      graphics2D.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,
            RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
      graphics2D.drawImage(image, 0, 0, width, height, null);
      graphics2D.dispose();
      ImageIcon myIcon = new ImageIcon(scaledImage);
      return myIcon;

   }

   public static String getFileExtentension(String filename)
   {
      int i = filename.lastIndexOf(".");
      if (i >= 0)
      {
         return filename.substring(i + 1).toUpperCase();
      }
      else
      {
         return null;
      }
   }

   public static Map<String, String> getFileExtensionsList()
   {
      Map<String, String> mp = new HashMap<String, String>();
      String readerNames[] = ImageIO.getReaderFileSuffixes();
      for (int i = 0, n = readerNames.length; i < n; i++)
      {
         mp.put(readerNames[i].toUpperCase(), readerNames[i].toUpperCase());
      }

      return mp;
   }

   public static void drawSquare(Graphics g, int x, int y, int width, int height, Color color, final int borderWidth)
   {
      // Frame
      g.setColor(Color.BLACK);
      g.fillRect(x, y, width, height);

      // Create white background
      g.setColor(color);
      g.fillRect(x + borderWidth, y + borderWidth, width - 2 * borderWidth, height - 2 * borderWidth);
   }

   public static void drawSplitStrings(Graphics g, String text, int x, int y, int maxwidth, int maxHeight)
   {
      if (text != null)
      {

         // FontMetrics gives us information about the width,
         // height, etc. of the current Graphics object's Font.
         FontMetrics fm = g.getFontMetrics();
         int usedHeight = getTextHeight(fm, text, x, maxwidth);

         int curX = x;
         int curY = y + (int) Math.round(fm.getHeight() * 0.8);
         if (usedHeight < maxHeight)
         {
            curY += ((maxHeight - usedHeight) / 2);
         }

         String[] words = text.split(" ");
         for (String word1 : words)
         {

            String[] newWords = word1.split("\n");
            int i = 0;
            for (String word : newWords)
            {
               // Find out the width of the word.
               int wordWidth = fm.stringWidth(word + " ");
               int wordWidthWithoutSpace = fm.stringWidth(word);

               // If text exceeds the width, then move to next line.
               // If there is more than one i newWords array, we make a newline
               // after each word
               if (curX + wordWidthWithoutSpace >= x + maxwidth || i > 0)
               {
                  curY += fm.getHeight();
                  curX = x;
               }

               g.drawString(word, curX, curY);

               // Move over to the right for next word.
               curX += wordWidth;
               i++;
            }

         }
      }
   }

   private static int getTextHeight(FontMetrics fm, String text, int x, int maxwidth)
   {
      int curX = x;
      int curY = fm.getHeight();
      String[] words = text.split(" ");
      for (String word : words)
      {
         // Find out the width of the word.
         int wordWidth = fm.stringWidth(word + " ");
         int wordWidthWithoutSpace = fm.stringWidth(word);

         // If text exceeds the width, then move to next line.
         if (curX + wordWidthWithoutSpace >= x + maxwidth)
         {
            curY += fm.getHeight();
            curX = x;
         }

         // Move over to the right for next word.
         curX += wordWidth;
      }
      return curY;
   }

   public static Font scaleFont(Font font)
   {
      // Make font size readable in properties
      return new Font(font.getName(), font.getStyle(), (int) (font.getSize() * FONT_SIZE_FACTOR));
   }

   public static int getDecFromHex(String hex)
   {
      return Integer.parseInt(hex.trim(), 16);
   }

   public static Color getColor(String colorString, Color defaultColor)
   {
      if (colorString == null)
      {
         return defaultColor;
      }

      try
      {
         String redHex = colorString.substring(0, 2);
         String greenHex = colorString.substring(2, 4);
         String blueHex = colorString.substring(4, 6);
         return new Color(ImageHelper.getDecFromHex(redHex), ImageHelper.getDecFromHex(greenHex),
               ImageHelper.getDecFromHex(blueHex));
      }
      catch (Exception e)
      {
         System.out.println(colorString + " - " + defaultColor);
         System.out.println(e.getMessage());
         return defaultColor;
      }
   }

   public static String getColorString(Color color)
   {
      return getHexString(color.getRed(), 2) + getHexString(color.getGreen(), 2) + getHexString(color.getBlue(), 2);
   }

   private static String getHexString(int number, int size)
   {
      String hex = Integer.toHexString(number).toUpperCase();
      if (hex.length() < size)
      {
         hex = "0000000000".substring(0, size - hex.length()) + hex;
      }

      return hex;
   }

   public static Rectangle getSizeKeepAspectRatio(float srcWidth, float srcHeight, float targetWidth, float targetHight)
   {
      float width = targetWidth;
      float height = targetHight;
      float srcRatio = srcWidth / srcHeight;
      float targetRatio = targetWidth / targetHight;

      if (targetRatio >= srcRatio)
      {
         // Keep height, less width
         width = height * srcRatio;
      }
      else
      {
         // Keep width, less height
         height = width / srcRatio;
      }

      return new Rectangle((int) Math.round(width), (int) Math.round(height));
   }

}
