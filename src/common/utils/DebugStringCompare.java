/*
 * Created on Oct 1, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code && Comments
 */
package common.utils;


/**
 * The Class DebugStringCompare.
 *
 * @author svelar0
 * 
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code && Comments
 */
public class DebugStringCompare
{
   
   /** The Constant CONST_MAXCHAR. */
   private final static int CONST_MAXCHAR = 512;
   
   /** The Arr. */
   private static int[] Arr = new int[CONST_MAXCHAR];

   /**
    * Instantiates a new debug string compare.
    */
   public DebugStringCompare()
   {
      super();
   }

   /**
    * The main method.
    *
    * @param args the command line arguments
    */
   public static void main(String args[])
   {
      String str1 = "ABBAWATERLOOMP3";
      String str2 = "ABBAWATERLOVE";
      printResult(str1, str2);
   }

   /**
    * Prints the result.
    *
    * @param str1 the str 1
    * @param str2 the str 2
    */
   private static void printResult(String str1, String str2)
   {
      System.out.println(
         ">"
            + str1
            + "<-->"
            + str2
            + "< = "
            + DebugStringCompare.stringCompPoints(str1, str2));

   }

   /**
    * String comp points.
    *
    * @param str1 the str 1
    * @param Str2 the str 2
    * @return the int
    */
   public static int stringCompPoints(String str1, String Str2)
   {
      String str1b;
      String str2b;

      str1b = str1.toUpperCase();
      str2b = Str2.toUpperCase();

      return caseSensitiveCompPoints(str1b, str2b);

   }

   /**
    * Removes the not char.
    *
    * @param Text the text
    * @return the string
    */
   private static String removeNotChar(String Text)
   {
      String Temp = "";
      for (int i = 0; i < Text.length(); i++)
      {
         if (Character.isLetterOrDigit(Text.charAt(i)))
         {
            Temp += Text.charAt(i);
         }
      }
      return Temp;
   }

   /**
    * Case sensitive comp points.
    *
    * @param str1 the str 1
    * @param Str2 the str 2
    * @return the int
    */
   public static int caseSensitiveCompPoints(String str1, String Str2)
   {
      double P = 0.0;
      double Pa = 0.0;
      double Pb = 0.0;
      double Pc = 0.0;
      double Count = 0.0;

      str1 = removeNotChar(str1);
      Str2 = removeNotChar(Str2);
      String Msg = "Java " + "  <" + str1 + ">--<" + Str2 + ">" + "\n";
      if (str1 == null || Str2 == null || str1.equals("") || Str2.equals(""))
      {
         return 0;
      }

      if (str1.equals(Str2))
      {
         return 100;
      }

      int[] Point = new int[6];

      //Original 4 
      Point[1] = 8;
      Point[2] = 4;
      Point[3] = 1;
      Point[4] = 1;
      Point[5] = 16;
      Pa = Points1(str1, Str2);
      Pb = Points1(Str2, str1);
      Msg += "Points1: Pa=" + Pa + " Pb=" + Pb;
      if (Pa < Pb)
      {
         Pa = Pb;
      }
      Pc = Pa * Point[1];
      Msg += " Pc=" + Pa + "*" + Point[1] + "=" + Pc + "\n";
      P = P + Pc;
      Pa = Points2(str1, Str2);
      Pb = Points2(Str2, str1);
      Msg += "Points2: Pa=" + Pa + " Pb=" + Pb;
      if (Pa < Pb)
      {
         Pa = Pb;
      }
      Pc = Pa * Point[2];
      Msg += " Pc=" + Pa + "*" + Point[2] + "=" + Pc + "\n";
      P = P + Pc;
      Pa = Points3(str1, Str2);
      Pb = Points3(Str2, str1);
      Msg += "Points3: Pa=" + Pa + " Pb=" + Pb;
      if (Pa < Pb)
      {
         Pa = Pb;
      }
      Pc = Pa * Point[3];
      Msg += " Pc=" + Pa + "*" + Point[3] + "=" + Pc + "\n";
      P = P + Pc;

      Pa = Points4(str1, Str2);
      Msg += "Points4: Pa=" + Pa;
      Pc = Pa * Point[4];
      Msg += " Pc=" + Pa + "*" + Point[4] + "=" + Pc + "\n";
      P = P + Pc;

      Pa = Points5(str1, Str2);
      Pb = Points5(Str2, str1);
      Msg += "Points5: Pa=" + Pa + " Pb=" + Pb;
      if (Pa < Pb)
      {
         Pa = Pb;
      }
      Pc = Pa * Point[5];
      Msg += " Pc=" + Pa + "*" + Point[5] + "=" + Pc + "\n";
      P = P + Pc;

      Count = Point[1] + Point[2] + Point[3] + Point[4] + Point[5];

      Msg += "Total: " + P + "/" + Count + "=" + (int) (P / Count);
      System.out.println(Msg);
      if (Count > 0)
      {
         return (int) (P / Count);
      }
      else
      {
         return 0;
      }
   }

   /**
    * Info.
    *
    * @return the string
    */
   public static String Info()
   {
      String Text = "StringCompare gives you an integer from 0-100";
      return Text
         + " telling you how well the two input strings is alike. 0 = totally dif(ferent. 100 = The same string.";
   }

   /**
    * Points 1.
    *
    * @param str1 the str 1
    * @param str2 the str 2
    * @return the int
    */
   private static int Points1(String str1, String str2)
   {
      //How many letters in string1 can we find in string2, Placed anywhere;
      int P1 = 0;
      int i2;
      char Char;
      if (str1.length() > CONST_MAXCHAR)
      {
         System.out.println(
            "String too long! (Over " + CONST_MAXCHAR + " characters)");
         return 0;
      }
      else
      {
         Arr = getEmptyArr();

         for (int i = 0; i < str1.length(); i++)
         {
            Char = str1.charAt(i);
            i2 = 0;
            i2 = str2.indexOf(Char, i2);
            while (i2 > -1)
            {
               if (Arr[i2] == -1)
               {
                  Arr[i2] = i;
                  P1++;
                  break;
               }
               i2 = str2.indexOf(Char, i2 + 1);
            }
         }
      }

      return P1 * 100 / str1.length();
   }

   /**
    * Gets the empty arr.
    *
    * @return the empty arr
    */
   private static int[] getEmptyArr()
   {

      //Empty Array;
      for (int i = 0; i < CONST_MAXCHAR; i++)
      {
         Arr[i] = -1;
      }
      return Arr;
   }

   /**
    * Points 2.
    *
    * @param str1 the str 1
    * @param Str2 the str 2
    * @return the int
    */
   private static int Points2(String str1, String Str2)
   {
      //How many letters with the same letter after it can be found in the strings;
      int P2 = 0;
      int i;
      int ArrEnd;

      ArrEnd = getArrEnd();
      if (ArrEnd == -1)
      {
         i = Points1(str1, Str2);
         // Just to get an array
         ArrEnd = getArrEnd();
      }

      for (i = 0; i < ArrEnd; i++)
      {
         if (Arr[i] + 1 == Arr[i + 1])
         {
            P2++;
         }
      }

      return P2 * 100 / str1.length();
   }

   /**
    * Points 3.
    *
    * @param str1 the str 1
    * @param Str2 the str 2
    * @return the int
    */
   private static int Points3(String str1, String Str2)
   {
      //How long part of string 1 can be found in string 2;
      int P3 = 0;

      for (int i = 1; i < str1.length(); i++)
      {
         if (Str2.indexOf(str1.substring(0, i)) > -1)
         {
            P3 = i;
         }
         else
         {
            break;
         }
      }
      return P3 * 100 / str1.length();
   }

   /**
    * Points 4.
    *
    * @param str1 the str 1
    * @param Str2 the str 2
    * @return the int
    */
   private static int Points4(String str1, String Str2)
   {
      //How close to the same length have string1 && string2;
      double a;

      if (str1 == "" || Str2 == "")
      {
         return 0;
      }

      double len1 = str1.length();
      double len2 = Str2.length();
      a = len1 / len2;
      if (a > 1)
      {
         a = 1 / a;
      }
      return (int) (a * 100);
   }

   /**
    * Points 5.
    *
    * @param str1 the str 1
    * @param Str2 the str 2
    * @return the int
    */
   private static int Points5(String str1, String Str2)
   {
      //How many letters with the same letter after it can be found in the strings;
      int P2 = 0;
      int i;
      int ArrEnd;

      ArrEnd = getArrEnd();
      if (ArrEnd == -1)
      {
         i = Points1(str1, Str2);
         // Just to get an array
         ArrEnd = getArrEnd();
      }

      for (i = 0; i < ArrEnd; i++)
      {
         if (Arr[i] != -1 && Arr[i + 1] != -1)
         {
            P2++;
         }
      }

      return P2 * 100 / str1.length();
   }

   /**
    * Gets the arr end.
    *
    * @return the arr end
    */
   private static int getArrEnd()
   {

      for (int i = CONST_MAXCHAR - 1; i >= 0; i--)
      {
         if (Arr[i] != -1)
         {
            return i + 1;
         }
      }
      return -1;
   }
}