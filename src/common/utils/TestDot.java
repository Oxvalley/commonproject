package common.utils;

import java.util.List;

public class TestDot implements Comparable<TestDot>
{

   private int iX;
   private int iY;
   private List<Wall> iUnsolved;

   public TestDot(int x, int y, List<Wall> unsolved)
   {
      iX = x;
      iY = y;
      iUnsolved = unsolved;
   }

   public int getX()
   {
      return iX;
   }

   public int getY()
   {
      return iY;
   }

   public List<Wall> getUnsolved()
   {
      return iUnsolved;
   }

   @Override
   public int compareTo(TestDot o)
   {
      return iUnsolved.size() - o.iUnsolved.size();
   }

   @Override
   public String toString()
   {
      return iX + ", " + iY + " " + iUnsolved;
   }

}
