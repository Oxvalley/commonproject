package common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;

public class CHexLister
{

   public static void main(String[] args)
   {
      if (args.length == 0)
      {
         // 0x08, /* ----X--- */
         // 0x3e, /* --XXXXX- */
         // 0x5f, /* -X-XXXXX */
         // 0x5f, /* -X-XXXXX */
         // 0x47, /* -X---XXX */
         // 0x61, /* -XX----X */
         // 0x3e, /* --XXXXX- *//* 16 */

         // String[] args2 =
         // { "0x08", "$3e", "$5f", "$5f", "0x47", "0x61", "$3e" };
         //
         // listNumbers(args2);

         ArrayList<String> list = new ArrayList<>();
         splitLine("$FF, $81, $81, $81, $81, $81, $81, $FF", list);
         splitLine("$E7, $66, $3C, $18, $3C, $66, $E7, $00", list);
         splitLine("$7C, $EE, $C6, $C6, $C6, $EE, $7C, $00", list);

         listNumbers(GetStringArray(list));

      }

   }

   private static void splitLine(String line, ArrayList<String> list)
   {
      String[] arr = line.split(",");
      for (String string : arr)
      {
         list.add(string.trim());
      }
   }

   public static String[] GetStringArray(List<String> arr)
   {

      // Convert ArrayList to object array
      Object[] objArr = arr.toArray();

      // convert Object array to String array
      String[] str = Arrays.copyOf(objArr, objArr.length, String[].class);

      return str;
   }

   public static void listNumbers(List<String> arr)
   {
      listNumbers(GetStringArray(arr));
   }

   public static void listNumbers(String[] args)
   {
      for (String numberString : args)
      {
         int number = -1;

         if (numberString.toLowerCase().startsWith("0x"))
         {
            number = Integer.parseInt(numberString.substring(2), 16);
         }
         else if (numberString.toLowerCase().startsWith("$"))
         {
            number = Integer.parseInt(numberString.substring(1), 16);
         }
         else
         {
            number = NumberUtils.toInt(numberString);
         }

         System.out.println(printNumbers(numberString, number));

      }
   }

   public static void listNumbers(int[] args)
   {
      for (int number : args)
      {
         System.out.println(printNumbers(String.valueOf(number), number));
      }
   }

   public static String printNumbers(String numberString, int number)
   {
      return printNumbers(numberString, number, null);
   }

   public static String printNumbers(int number)
   {
      return printNumbers(String.valueOf(number), number, null);
   }

   public static String printNumbers(int number, String title)
   {
      return printNumbers(String.valueOf(number), number, title);
   }

   public static String printNumbers(String numberString, int number,
         String title)
   {
      if (number < 0 || number > 255)
      {
         System.out.println("Nunber not accepted: " + numberString);
         System.exit(1);
      }

      String plot = "";
      int numberCopy = number;
      for (int i = 128; i >= 1; i = i / 2)
      {
         if (numberCopy >= i)
         {
            plot += "X";
            numberCopy -= i;
         }
         else
         {
            plot += "-";
         }
      }

      if (title != null)
      {
         plot += " " + title;
      }

      String hex = Integer.toHexString(number).toUpperCase();
      if (hex.length() == 1)
      {
         hex = "0" + hex;
      }

      return ("0x" + hex + ", /* " + plot + " */");
   }

}
