/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package common.properties;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The Class SettingProperties.
 *
 * @author Lars Svensson
 */
public class SettingProperties extends Properties
{

   /** The File. */
   private File iFile;

   /** The Is dirty. */
   private boolean iIsDirty = false;

   /** The Host name. */
   private static String iHostName = null;

   /** The Constant iMap. */
   private final static Map<String, SettingProperties> iMap =
         new ConcurrentHashMap<String, SettingProperties>();

   /**
    * Instantiates a new setting properties.
    *
    * @param filename
    *           the filename
    */
   private SettingProperties(String filename)
   {
      iFile = new File(filename);
      try
      {
         load(new FileReader(iFile));
      }
      catch (FileNotFoundException ex)
      {
         System.out.println("Property settings file " + iFile.getAbsolutePath()
               + " not found. Creating it");
      }
      catch (IOException ex)
      {
         System.out.println("IO Exception: " + ex);
      }
   }

   /**
    * Gets the property key list.
    *
    * @param prefix
    *           the prefix
    * @return the property key list
    */
   public List<String> getPropertyKeyList(String prefix)
   {
      List<String> coll = new ArrayList<String>();
      for (Object key : this.keySet())
      {
         if (key instanceof String)
         {
            String keyString = (String) key;
            if (keyString.startsWith(prefix))
            {
               coll.add(keyString);
            }
         }
      }
      Collections.sort(coll);
      return coll;
   }

   /**
    * Gets the property value list.
    *
    * @param prefix
    *           the prefix
    * @return the property value list
    */
   public List<String> getPropertyValueList(String prefix)
   {
      List<String> coll = new ArrayList<String>();
      List<String> keys = getPropertyKeyList(prefix);
      for (String key : keys)
      {
         coll.add(getProperty(key));
      }
      Collections.sort(coll);
      return coll;
   }

   /*
    * (non-Javadoc)
    * 
    * @see java.util.Properties#getProperty(java.lang.String, java.lang.String)
    */
   @Override
   public synchronized String getProperty(String key, String defaultValue)
   {
      String value = super.getProperty(key, defaultValue);
      if (value != null && value.equals(defaultValue))
      {
         setProperty(key, value);
         iIsDirty = true;
      }
      return value;
   }

   /**
    * 
    * @param key
    * @param defaultValue
    * @return
    */
   public synchronized String getPropertyOrSave(String key, String defaultValue)
   {
      String value = getProperty(key);
      if (value == null)
      {
         setProperty(key, defaultValue);
      }
      return getProperty(key, defaultValue);
   }

   /**
    * Gets the single instance of SettingProperties.
    *
    * @return single instance of SettingProperties
    */
   public static SettingProperties getInstanceWithName(String name)
   {
      return getInstance(getComputerName().toLowerCase() + "_"
            + name.toLowerCase() + ".properties");
   }

   /**
    * Gets the single instance of SettingProperties.
    *
    * @return single instance of SettingProperties
    */
   public static SettingProperties getInstance()
   {
      return getInstanceWithName("_settings");
   }

   /**
    * 
    * @param key
    * @param defaultValue
    * @return
    */
   public synchronized Color getColorPropertyOrSave(String key,
         Color defaultValue)
   {
      String value = getProperty(key);
      if (value == null)
      {
         setColorProperty(key, defaultValue);
      }
      return getColorProperty(key, defaultValue);
   }

   /**
    * Sets the property.
    *
    * @param key
    *           the key
    * @param value
    *           the value
    * @return the object
    */
   public synchronized void setColorProperty(String key, Color color)
   {
      String value =
            color.getRed() + "," + color.getGreen() + "," + color.getBlue();
      setProperty(key, value);
   }

   public synchronized Color getColorProperty(String key, Color defaultColor)
   {
      String value = getProperty(key);

      if (value == null)
      {
         return defaultColor;
      }

      String[] colorArr = value.split(",");
      Color color2;
      try
      {
         color2 = new Color(Integer.parseInt(colorArr[0].trim()),
               Integer.parseInt(colorArr[1].trim()),
               Integer.parseInt(colorArr[2].trim()));
      }
      catch (NumberFormatException e)
      {
         System.out.println("Bad syntax of value for key " + key
               + " (Not a number). Using defaults: " + value);
         return defaultColor;
      }
      catch (IndexOutOfBoundsException e)
      {
         System.out.println("Bad syntax of value for key " + key
               + " (Not 3 RGB values) Using defaults: " + value);
         return defaultColor;
      }

      return color2;
   }

   /**
    * Gets the computer name.
    *
    * @return the computer name
    */
   private static String getComputerName()
   {
      if ((iHostName == null))
      {
         try
         {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            iHostName = addr.getHostName();
         }
         catch (UnknownHostException ex)
         {
            System.out.println("Hostname can not be resolved");
            return "Unknown";
         }
      }
      return iHostName;
   }

   /**
    * Gets the single instance of SettingProperties.
    *
    * @param fileName
    *           the file name
    * @return single instance of SettingProperties
    */
   public static SettingProperties getInstance(String fileName)
   {
      SettingProperties instance = iMap.get(fileName);
      if (instance == null)
      {
         instance = new SettingProperties(fileName);
         iMap.put(fileName, instance);
         instance.setDirty(false);
      }

      return instance;
   }

   /**
    * Overrides, called by the store method to sort Alphabetically.
    *
    * @return the enumeration
    */
   @Override
   @SuppressWarnings("unchecked")
   public synchronized Enumeration keys()
   {
      Enumeration keysEnum = super.keys();
      Vector keyList = new Vector();
      while (keysEnum.hasMoreElements())
      {
         keyList.add(keysEnum.nextElement());
      }
      Collections.sort(keyList);
      return keyList.elements();
   }

   /**
    * Sets the dirty.
    *
    * @param value
    *           the new dirty
    */
   private void setDirty(boolean value)
   {
      iIsDirty = value;
   }

   /**
    * Save.
    */
   public void save()
   {
      try
      {
         for (String key : iMap.keySet())
         {
            SettingProperties sp = iMap.get(key);

            if (sp.isDirty())
            {
               FileWriter fw = new FileWriter(new File(key));
               sp.store(fw, "Saving " + key);
               fw.close();
            }
            sp.setDirty(false);
         }
      }
      catch (IOException ex)
      {
         ex.printStackTrace();
      }
   }

   /**
    * Checks if is dirty.
    *
    * @return true, if is dirty
    */
   public boolean isDirty()
   {
      return iIsDirty;
   }

   /*
    * (non-Javadoc)
    * 
    * @see java.util.Properties#setProperty(java.lang.String, java.lang.String)
    */
   @Override
   public synchronized Object setProperty(String key, String value)
   {
      Object oldValue = super.setProperty(key, value);
      if (!isDirty())
      {
         iIsDirty = !isTheSame(oldValue, value);
      }
      return oldValue;
   }

   /**
    * Checks if is the same.
    *
    * @param value1
    *           the value 1
    * @param value2
    *           the value 2
    * @return true, if is the same
    */
   private static boolean isTheSame(Object value1, Object value2)
   {
      if (value1 == null)
      {
         if (value2 == null)
         {
            return true;
         }
         else
         {
            return value2.equals(value1);
         }
      }
      else
      {
         return value1.equals(value2);
      }
   }

   /**
    * Sets the property.
    *
    * @param key
    *           the key
    * @param value
    *           the value
    * @return the object
    */
   public synchronized Object setProperty(String key, int value)
   {
      Object oldValue = super.setProperty(key, String.valueOf(value));
      iIsDirty = !isTheSame(oldValue, value);
      return oldValue;
   }

   /**
    * Sets the property.
    *
    * @param key
    *           the key
    * @param value
    *           the value
    * @return the object
    */
   public synchronized Object setProperty(String key, boolean value)
   {
      Object oldValue = super.setProperty(key, String.valueOf(value));
      iIsDirty = !isTheSame(oldValue, value);
      return oldValue;
   }

   /**
    * Gets the int property.
    *
    * @param key
    *           the key
    * @param defaultValue
    *           the default value
    * @return the int property
    */
   public synchronized int getIntProperty(String key, int defaultValue)
   {
      String value = getProperty(key);

      try
      {
         int retVal = Integer.valueOf(value);
         return retVal;
      }
      catch (NumberFormatException e)
      {
         return defaultValue;
      }

   }

   /**
    * Gets the double property.
    *
    * @param key
    *           the key
    * @param defaultValue
    *           the default value
    * @return the int property
    */
   public synchronized double getDoubleProperty(String key, double defaultValue)
   {
      String value = getProperty(key);

      try
      {
         double retVal = Double.valueOf(value);
         return retVal;
      }
      catch (NumberFormatException e)
      {
         return defaultValue;
      }
      catch (NullPointerException e)
      {
         return defaultValue;
      }

   }

   /**
    * Gets the boolean property.
    *
    * @param key
    *           the key
    * @param defaultValue
    *           the default value
    * @return the boolean property
    */
   public synchronized boolean getBooleanProperty(String key,
         boolean defaultValue)
   {
      String value = getProperty(key);

      if (value != null && value.equalsIgnoreCase("true"))
      {
         return true;
      }

      if (value != null && value.equalsIgnoreCase("false"))
      {
         return false;
      }

      return defaultValue;
   }

   /**
    * Gets the int property.
    *
    * @param key
    *           the key
    * @param defaultValue
    *           the default value
    * @return the int property
    */
   public synchronized int getIntPropertyOrSave(String key, int defaultValue)
   {
      String value = getProperty(key);
      if (value == null)
      {
         setProperty(key, defaultValue);
      }
      return getIntProperty(key, defaultValue);
   }

   /**
    * Gets the double property.
    *
    * @param key
    *           the key
    * @param defaultValue
    *           the default value
    * @return the int property
    */
   public synchronized double getDoublePropertyOrSave(String key,
         double defaultValue)
   {
      String value = getProperty(key);
      if (value == null)
      {
         setProperty(key, String.valueOf(defaultValue));
      }
      return getDoubleProperty(key, defaultValue);
   }

   /**
    * Gets the boolean property.
    *
    * @param key
    *           the key
    * @param defaultValue
    *           the default value
    * @return the boolean property
    */
   public synchronized boolean getBooleanPropertyOrSave(String key,
         boolean defaultValue)
   {
      String value = getProperty(key);
      if (value == null)
      {
         setProperty(key, defaultValue);
      }
      return getBooleanProperty(key, defaultValue);
   }

   /**
    * Gets the boolean property with no default value.
    *
    * @param key
    *           the key
    * @param defaultValue
    *           the default value
    * @return the boolean property or null
    */
   public synchronized Boolean getBooleanProperty(String key)
   {
      String value = getProperty(key);

      if (value != null && value.equalsIgnoreCase("true"))
      {
         return true;
      }

      if (value != null && value.equalsIgnoreCase("false"))
      {
         return false;
      }

      return null;
   }

   /**
    * Find a number at the end of a property key and add one. Return
    * defaultValue if no number found.
    *
    * @param key
    *           the key
    * @param defaultNumberAsString
    *           the default number as string
    * @param prefixWithKey
    *           the prefix with key
    * @return the inner next number
    */
   public String getInnerNextNumber(String key, String defaultNumberAsString,
         boolean prefixWithKey)
   {
      boolean foundLetter = false;
      int i = -1;
      String retVal = "1";

      // Get a list of all keys starting with this key
      List<String> list = getPropertyKeyList(key);

      if (list.size() == 0)
      {
         retVal = defaultNumberAsString;
      }
      else
      {
         // Sort in alphabetical order
         Collections.sort(list);

         // Test last key
         String lastKey = list.get(list.size() - 1);
         if (lastKey != null)
         {
            for (i = lastKey.length() - 1; i > 0 && !foundLetter; i--)
            {
               char last = lastKey.charAt(i);
               if (!Character.isDigit(last))
               {
                  foundLetter = true;
               }
            }

            if (foundLetter)
            {
               String lastNumber = lastKey.substring(i + 2);
               String nextNumber =
                     "00000000000" + (Integer.valueOf(lastNumber) + 1);
               nextNumber = nextNumber
                     .substring(nextNumber.length() - lastNumber.length());
               retVal = lastKey.substring(0, i + 2) + nextNumber;
            }
         }
      }

      if (prefixWithKey)
      {
         if (!retVal.startsWith(key))
         {
            retVal = key + retVal;
         }
      }
      else
      {
         if (retVal.startsWith(key))
         {
            retVal = retVal.substring(key.length());
         }
      }

      return retVal;

   }

   /**
    * Gets the key and next number.
    *
    * @param key
    *           the key
    * @param defaultNumberAsString
    *           the default number as string
    * @return the key and next number
    */
   public String getKeyAndNextNumber(String key, String defaultNumberAsString)
   {
      return getInnerNextNumber(key, defaultNumberAsString, true);
   }

   /**
    * Gets the file name.
    *
    * @return the file name
    */
   public String getFileName()
   {
      return iFile.getAbsolutePath();
   }

}
