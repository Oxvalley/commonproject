package common.summarum;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Summarum
{
   private final static int SUM_1 = 0;
   private final static int SUM_2 = 1;
   private final static int SUM_3 = 2;
   private final static int SUM_4 = 3;
   private final static int COL_1 = 4;
   private final static int COL_2 = 5;
   private final static int COL_3 = 6;
   private final static int ROW_1 = 7;
   private final static int ROW_2 = 8;
   private final static int ROW_3 = 9;

   private int[] sums = { 21, 19, 18, 15, 19, 13, 13, 16, 14, 15 };
   private int[] solutions = { -1, -1, -1, -1, 1, -1, -1, -1, -1 };
   private List<SummarumSquares> list = new ArrayList<SummarumSquares>();
   List<SumFormula> sumList = new ArrayList<SumFormula>();

   String receipe =
         "21,19,18,15,18,13,13,16,14,15,----1----,23456789,3456789,23456789,456789,1,456789,23456789,3456789,23456789";

   public static void main(String[] args)
   {

      Summarum sum = new Summarum();
      sum.run();

   }

   private void run()
   {
      for (int i = 0; i < solutions.length; i++)
      {
         list.add(new SummarumSquares(solutions[i]));
      }

      addCombination(SUM_1, 0, 1, 3, 4);
      addCombination(SUM_2, 1, 2, 4, 5);
      addCombination(SUM_3, 3, 4, 6, 7);
      addCombination(SUM_4, 4, 5, 7, 8);
      addCombination(ROW_1, 0, 1, 2);
      addCombination(ROW_2, 3, 4, 5);
      addCombination(ROW_3, 6, 7, 8);
      addCombination(COL_1, 0, 3, 6);
      addCombination(COL_2, 1, 4, 7);
      addCombination(COL_3, 2, 5, 8);
      printResult();

      bruteForce();

      // printResult();

   }

   public void printResult()
   {
      System.out.println("");
      for (SummarumSquares summarumSquares : list)
      {
         System.out.println(summarumSquares);
      }
   }

   public void bruteForce()
   {

      String fileName = FindAllCombinations.createFile();

      FindAllCombinations.startClock();
      BufferedReader reader = null;
      int count = 0;
      List<String> solutions = new ArrayList<String>();
      try
      {
         reader = new BufferedReader(new FileReader(fileName));
         String line = reader.readLine();

         while (line != null)
         {
            // Read next line for while condition
            line = reader.readLine();
            if (line != null)
            {
               if (isCandidate(line))
               {
                  System.out.println((++count) + " >>> " + line + "\n");
                  if (isSolution(line))
                  {
                     solutions.add(line);
                  }
               }

            }
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }

      printSolutions(solutions);

      FindAllCombinations.printElapsedTIme();

      // int index = 0;
      // int value = 0;
      // while (index < 9 && value > -2)
      // {
      // value = list.get(index).getSelected();
      // System.out.println("Index "+ index + " gets " + value);
      // if (!isSelected(value, index))
      // {
      // index++;
      // }
      // else
      // {
      // value = -1;
      // int tempIndex = index;
      // while (value == -1)
      // {
      // tempIndex++;
      // if (tempIndex == 10)
      // {
      // System.out.println("Done!");
      // value = -2;
      // }
      // else
      // {
      // value = list.get(index).getNextSelected();
      // System.out.println("tempIndex "+ index + " gets " + value);
      // }
      // }
      // }
      // }
      //
      // System.out.println("Found Candidate:");
      printResult();

   }

   private void printSolutions(List<String> solutions)
   {
      for (String solution : solutions)
      {
         System.out.println("Solution: " + solution);
      }

   }

   private boolean isSolution(String line)
   {
      for (SumFormula sumForm : sumList)
      {
         if (!sumForm.isOKLine(line))
         {
            return false;
         }
      }
      return true;
   }

   private boolean isCandidate(String line)
   {
      String printLine = line + "\n";

      for (int i = 0; i < line.length(); i++)
      {
         String c = String.valueOf(line.charAt(i));
         if (!list.get(i).getPossible().contains(c))
         {
            return false;
         }

         printLine += c + " is OK in " + list.get(i).getPossible() + "\n";
      }

      System.out.println(printLine);
      return true;
   }

   public void addCombination(int sumsIndex, int index1, int index2, int index3)
   {
      addCombination(sumsIndex, index1, index2, index3, -1);
   }

   public void addCombination(int sumsIndex, int index1, int index2, int index3, int index4)
   {
      List<SummarumSquares> tempList = new ArrayList<SummarumSquares>();
      tempList.add(list.get(index1));
      tempList.add(list.get(index2));
      tempList.add(list.get(index3));
      if (index4 != -1)
      {
         tempList.add(list.get(index4));
      }
      List<Integer> posList = new ArrayList<Integer>();
      posList.add(index1);
      posList.add(index2);
      posList.add(index3);
      if (index4 != -1)
      {
         posList.add(index4);
      }
      sumList.add(new SumFormula(sumsIndex, sums[sumsIndex], posList));
      removeTooLowNumbers(sums[sumsIndex], tempList);
      removeTooHighNumbers(sums[sumsIndex], tempList);
      removeOccupiedNumbers(list);

   }

   // private boolean isSelected(int value, int notIncluded)
   // {
   // int i = 0;
   // for (SummarumSquares summarumSquares : list)
   // {
   // if (i < notIncluded) // TODO or is single value
   // {
   // if (summarumSquares.getSelected() == value)
   // {
   // return true;
   // }
   // }
   // i++;
   // }
   // return false;
   // }

   private static void removeOccupiedNumbers(List<SummarumSquares> list)
   {
      Set<Integer> occupied = new HashSet<Integer>();

      for (SummarumSquares summarumSquares : list)
      {
         occupied.add(summarumSquares.getSingleValue());
      }

      for (SummarumSquares summarumSquares : list)
      {
         summarumSquares.removeNumbers(occupied);
      }

   }

   public static void removeTooLowNumbers(int sum, List<SummarumSquares> tempList)
   {
      int max = 0;
      for (int j = 0; j <= 3; j++)
      {
         max = 0;
         SummarumSquares tempSquare = null;
         int k = 0;
         for (SummarumSquares square : tempList)
         {
            if (j == k)
            {
               tempSquare = square;
            }
            else
            {
               max += square.getMax();
            }
            k++;
         }

         if (sum > max)
         {
            int diff = sum - max;
            tempSquare.setMin(diff);
         }

      }
   }

   public static void removeTooHighNumbers(int sum, List<SummarumSquares> tempList)
   {
      int min = 0;
      for (int j = 0; j < tempList.size(); j++)
      {
         min = 0;
         SummarumSquares tempSquare = null;
         int k = 0;
         for (SummarumSquares square : tempList)
         {
            if (j == k)
            {
               tempSquare = square;
            }
            else
            {
               min += square.getMin();
            }
            k++;
         }

         if (sum < min)
         {
            int diff = min - sum;
            tempSquare.setMax(diff);
         }

      }
   }

}
