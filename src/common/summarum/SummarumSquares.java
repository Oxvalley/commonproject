package common.summarum;

import java.util.ArrayList;
import java.util.Set;

public class SummarumSquares extends ArrayList<Integer>
{

   private int iIndex;

   public SummarumSquares(int i)
   {
      if (i <= 0)
      {
         for (int j = 1; j <= 9; j++)
         {
            add(j);
         }
      }
      else
      {
         add(i);
      }

      iIndex = 0;

   }

   @Override
   public String toString()
   {
      return getSelected() + " => " + super.toString();
   }

   public String getPossible()
   {
      return super.toString();
   }
   
   
   private SummarumSquares()
   {
   }

   public int getMax()
   {
      int max = 0;
      for (Integer value : this)
      {
         if (value > max)
         {
            max = value;
         }
      }
      return max;
   }

   public int getMin()
   {
      int min = 10;
      for (Integer value : this)
      {
         if (value < min)
         {
            min = value;
         }
      }
      return min;
   }

   public void setMin(int min)
   {
      SummarumSquares removeList = new SummarumSquares();

      for (Integer value : this)
      {
         if (value < min)
         {
            removeList.add(value);
         }
      }

      for (Integer value : removeList)
      {
         remove(value);
      }
   }

   public void setMax(int max)
   {
      SummarumSquares removeList = new SummarumSquares();

      for (Integer value : this)
      {
         if (value > max)
         {
            removeList.add(value);
         }
      }

      for (Integer value : removeList)
      {
         remove(value);
      }
   }

   public Integer getSingleValue()
   {
      if (size() == 1)
      {
         return get(0);
      }
      else
      {
         return null;
      }
   }

   public void removeNumbers(Set<Integer> occupied)
   {
      if (size() > 1)
      {
         SummarumSquares removeList = new SummarumSquares();

         for (Integer value : this)
         {
            if (occupied.contains(value))
            {
               removeList.add(value);
            }
         }

         for (Integer value : removeList)
         {
            remove(value);
         }

      }

   }

   public int getSelected()
   {
      return get(iIndex);
   }

   public int getNextSelected()
   {
      iIndex++;
      if (iIndex == size())
      {
         iIndex = 0;
         return -1;
      }
      else
      {
         return getSelected();
      }
   }

  
}
