package common.summarum;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class FindAllCombinations
{

   private static long iStartTime;

   public static void main(String[] args)
   {

     String fileName =  createFile();
     readFile(fileName);
   }

   public static String createFile()
   {
      String filename = "summarum.txt";
      if (!new File(filename).exists())
      {
         System.out.println("Creating file. his might take about 10 minutes the first time");
         try
         {
            startClock();
            long i = 123456789;
            // int count = 0;
            FileWriter fw = new FileWriter(filename);
            while (i <= 987654321)
            {
               if (isOK(i))
               {
                  // count++;
                  // System.out.println(count + " " + i);
                  fw.write(String.valueOf(i) + "\n");
               }
               i++;
            }
            fw.close();
            printElapsedTIme();
         }
         catch (IOException e)
         {
            e.printStackTrace();
         }
      }
      return filename;
   }

   public static void readFile(String filename)
   {
      startClock();
      BufferedReader reader = null;
      try
      {
         reader = new BufferedReader(new FileReader(filename));
         String line = reader.readLine();

         while (line != null)
         {
            // Read next line for while condition
            line = reader.readLine();
            if (line != null)
            {
               System.out.println(line);
            }
         }
      }
      catch (IOException ioe)
      {
         System.out.println(ioe.getMessage());
      }
      finally
      {
         try
         {
            if (reader != null)
            {
               reader.close();
            }
         }
         catch (Exception e)
         {
         }
      }
      printElapsedTIme();
   }

   static void printElapsedTIme()
   {
      long elapsedTime = System.nanoTime() - iStartTime;
      System.out.println("Elapsed time: "
            + TimeUnit.MINUTES.convert(elapsedTime, TimeUnit.NANOSECONDS) + " minutes");
      System.out.println("Elapsed time: "
            + TimeUnit.SECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS) + " seconds");
   }

   public static void startClock()
   {
      iStartTime = System.nanoTime();
      System.out.println("Timer is started");
   }

   private static boolean isOK(long i)
   {
      String line = String.valueOf(i);
      if (line.contains("0"))
      {
         return false;
      }

      for (int j = 1; j < 10; j++)
      {
         if (!line.contains(String.valueOf(j)))
         {
            return false;
         }
      }

      return true;
   }

}
