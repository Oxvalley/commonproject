package common.summarum;

import java.util.List;

public class SumFormula
{

   private int iSumsIndex;
   private int iSum;
   private List<Integer> iPosList;

   public SumFormula(int sumsIndex, int sum, List<Integer> posList)
   {
      iSumsIndex = sumsIndex;
      iSum = sum;
      iPosList = posList;
   }

   public boolean isOKLine(String line)
   {
      String sum = iSum + " = ";
      int intSum = 0;
      for (Integer pos : iPosList)
      {
         int value = Integer.valueOf(line.substring(pos, pos+1));
         sum += value + " + ";
         intSum += value;
      }

      sum = sum.substring(0, sum.length() - 3);

      if (iSum == intSum)
      {
         System.out.println(sum + " is OK");
         return true;
      }
      return false;
   }

}
