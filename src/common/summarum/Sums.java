package common.summarum;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Sums
{
   public static void main(String[] args)
   {
      Set<String> set = new TreeSet<String>();
      for (int i = 0; i < 100000; i++)
      {
         int[] randomArr = getRandomArr(1, 9);
         printArr(randomArr);
         int sums = 3 * randomArr[0] + 4 * randomArr[1] + 3 * randomArr[2] + 4 * randomArr[3]
               + 6 * randomArr[4] + 4 * randomArr[5] + 3 * randomArr[6] + 4 * randomArr[7]
               + 3 * randomArr[8];
         System.out.println(sums);
         Set<Integer> set2 = new TreeSet<Integer>();
         set2.add(randomArr[1]);
         set2.add(randomArr[3]);
         set2.add(randomArr[5]);
         set2.add(randomArr[6]);
         Iterator<?> iter = set2.iterator();
         set.add(sums + "" +randomArr[4] + iter.next().toString() + iter.next().toString()
               + iter.next().toString() + iter.next().toString());
      }

      System.out.println(set);

   }

   private static void printArr(int[] arr)
   {
      System.out.println(Arrays.toString(arr).substring(1));
   }

   private static int[] getRandomArr(int min, int max)
   {
      int[] arr = new int[max - min + 1];
      int valueNow = min;
      for (int i = 0; i <= max - min; i++)
      {
         arr[i] = valueNow++;
      }

      int maxNow = max;
      while (maxNow > min)
      {
         double rand = Math.random();
         int value = (int) (Math.floor(rand * maxNow) + min);
         int temp = arr[maxNow - 1];
         arr[maxNow - 1] = arr[value - min];
         arr[value - min] = temp;
         maxNow--;
      }

      return arr;
   }
}
