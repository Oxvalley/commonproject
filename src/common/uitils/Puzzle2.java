package common.uitils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class Puzzle2
{

    public static void main(String[] args)
    {
        List<String> list = new ArrayList<>();

        BufferedReader reader = null;

        int valid = 0;
        int notValid = 0;

        try
        {
            reader = new BufferedReader(new FileReader("puzzle_input.txt"));

            String line = reader.readLine();

            while (line != null)
            {
                if (!line.trim().isEmpty())
                {
                    line = line.trim();
                    list.add(line);
                    int minus = line.indexOf("-");
                    if (minus != -1)
                    {
                        int space = line.indexOf(" ");
                        if (space != -1)
                        {
                            int kolon = line.indexOf(":");
                            if (kolon != -1)
                            {
                                int min = Integer.parseInt(line.substring(0, minus));
                                int max = Integer.parseInt(line.substring(minus + 1, space));
                                String letter = line.substring(space + 1, kolon);
                                String text = line.substring(kolon + 1).trim();
                                String first = text.substring(min -1, min);
                                String second = text.substring(max - 1, max);
                                int hits=0;
                                
                                if (first.equals(letter))
                                {
                                    hits++;
                                }
                                
                                if ( second.equals(letter))
                                {
                                    hits++;
                                }
                                
                                

                                if (hits == 1)
                                {
                                    valid++;
                                    System.out.println(line + " Valid");
                                }
                                else
                                {
                                    notValid++;
                                    System.out.println(line + " Not valid");
                                }
                            }

                        }

                    }

                    line = reader.readLine();
                }
            }

            System.out.println("Valid: " + valid + ", Not valid: " + notValid);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static int getletterCount(String text, String letter)
    {
        int count = 0;
        for (int i = 0; i < text.length(); i++)
        {
            String part = text.substring(i, letter.length() + i);
            if (letter.equals(part))
            {
                count++;
            }
        }

        return count;
    }

}
