package common.uitils;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import common.ui.components.ext.CompExtEnum;
import common.ui.components.ext.JPanelExt;

public class OpenSCADUI extends JFrame
{
   private static final long serialVersionUID = 1L;

   // TODO Change to name of your icon
   private String ICON_NAME = null;

   // TODO init fields here


   /////////// Java GUI Designer version 1.1 Starts here. ////
   // Text below will be regenerated automatically.
   // Generated 2020-02-22 16:38:07

   private JPanelExt iPanelExt;
   private JTextField txt1;
   private JButton btnBrowse;
   private JLabel lbl1;
   private JButton btnFormatFile;
   private JButton btnFormatLast;
   private JButton btnFormatAll;

   private int width = 725;
   private int height = 251;

   public static void main(String[] args)
   {
      OpenSCADUI openSCADUI = new OpenSCADUI();
      openSCADUI.setVisible(true);
   }

   public OpenSCADUI()
   {
      super();
      setTitle("OpenSCAD Formatter");
      setSize(width, height);
      if (ICON_NAME != null)
      {
         setIconImage(getToolkit().getImage(ICON_NAME));
      }

      getContentPane().setLayout(new BorderLayout());
      iPanelExt = new JPanelExt(width, height);
      iPanelExt.setLayout(null);
      getContentPane().add(iPanelExt, BorderLayout.CENTER);

      txt1 = new JTextField("");
      iPanelExt.addComponentExt(txt1, 124, 19, 450, 55, CompExtEnum.Normal);

      btnBrowse = new JButton("Browse");
      iPanelExt.addComponentExt(btnBrowse, 615, 17, 83, 55, CompExtEnum.Normal);
      btnBrowse.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnBrowseClicked(e);
         }
      });

      lbl1 = new JLabel("Source Path:");
      iPanelExt.addComponentExt(lbl1, 19, 42, 87, 38, CompExtEnum.Normal);

      btnFormatFile = new JButton("Format File");
      iPanelExt.addComponentExt(btnFormatFile, 50, 98, 184, 83, CompExtEnum.Normal);
      btnFormatFile.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnFormatFileClicked(e);
         }
      });

      btnFormatLast = new JButton("Format Last Saved File");
      iPanelExt.addComponentExt(btnFormatLast, 250, 98, 184, 83, CompExtEnum.Normal);
      btnFormatLast.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnFormatLastClicked(e);
         }
      });

      btnFormatAll = new JButton("Format  All Source Files");
      iPanelExt.addComponentExt(btnFormatAll, 450, 98, 184, 83, CompExtEnum.Normal);
      btnFormatAll.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            btnFormatAllClicked(e);
         }
      });

      setDefaultCloseOperation(EXIT_ON_CLOSE);

      /////////// Java GUI Designer version 1.1 Generation ends here. ///

      // TODO Add init declarations here

   }

   private void btnBrowseClicked(ActionEvent e)
   {
      // TODO Implement

   }

   private void btnFormatFileClicked(ActionEvent e)
   {
      // TODO Implement

   }

   private void btnFormatLastClicked(ActionEvent e)
   {
      // TODO Implement

   }

   private void btnFormatAllClicked(ActionEvent e)
   {
      // TODO Implement

   }

}
