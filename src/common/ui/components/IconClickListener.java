package common.ui.components;

public interface IconClickListener
{
   public void clickIcon(String xmlConstant, boolean isFrameLit);
   
   public void clickIcon(String xmlConstant);
   
   public void clickIconAndSetFrame(String xmlConstant);

   public void doubleClickIcon(String xmlConstant);
}
