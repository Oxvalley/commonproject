package common.ui.components.ext;

import java.awt.Color;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;


/**
 * The Class JPanelExt.
 */
public class JPanelExt extends JPanel implements ComponentListener
{

   /** The Constant serialVersionUID. */
   private static final long serialVersionUID = 8367819056448636222L;

   /** The Component height. */
   private int iComponentHeight;

   /** The Components. */
   private List<JComponentExt> iComponents = new ArrayList<JComponentExt>();

   /** The Component width. */
   private int iComponentWidth;

   /**
    * Instantiates a new j panel ext.
    *
    * @param width the width
    * @param height the height
    */
   public JPanelExt(int width, int height)
   {
      iComponentWidth = width;
      iComponentHeight = height;
      addComponentListener(this);
      setBorder(BorderFactory.createLineBorder(Color.BLACK));
      setLayout(null);
   }

   /**
    * Adds the component ext.
    *
    * @param comp the comp
    * @param x the x
    * @param y the y
    * @param width the width
    * @param height the height
    */
   public final void addComponentExt(JComponent comp, int x, int y, int width,
         int height)
   {
      addComponentExt(comp, x, y, width, height, CompExtEnum.Normal);
   }

   /**
    * Adds the component ext.
    *
    * @param comp the comp
    * @param x the x
    * @param y the y
    * @param width the width
    * @param height the height
    * @param status the status
 * @return
    */
   public final JComponentExt addComponentExt(JComponent comp, int x, int y, int width,
         int height, CompExtEnum status)
   {
      JComponentExt compExt =
            new JComponentExt(comp, x, y, width, height, status,
                  iComponentWidth, iComponentHeight);
      add(comp);
      iComponents.add(compExt);
      return compExt;
   }

   /* (non-Javadoc)
    * @see java.awt.event.ComponentListener#componentHidden(java.awt.event.ComponentEvent)
    */
   @Override
   public void componentHidden(ComponentEvent e)
   {
   }

   /* (non-Javadoc)
    * @see java.awt.event.ComponentListener#componentMoved(java.awt.event.ComponentEvent)
    */
   @Override
   public void componentMoved(ComponentEvent e)
   {
   }

   /* (non-Javadoc)
    * @see java.awt.event.ComponentListener#componentResized(java.awt.event.ComponentEvent)
    */
   @Override
   public void componentResized(ComponentEvent e)
   {
      updateComponentExt();
   }

   /* (non-Javadoc)
    * @see java.awt.event.ComponentListener#componentShown(java.awt.event.ComponentEvent)
    */
   @Override
   public void componentShown(ComponentEvent e)
   {
   }

   /**
    * Gets the comp ext height.
    *
    * @return the comp ext height
    */
   public final int getCompExtHeight()
   {
      return iComponentHeight;
   }

   /**
    * Gets the comp ext width.
    *
    * @return the comp ext width
    */
   public final int getCompExtWidth()
   {
      return iComponentWidth;
   }

   /**
    * Update component ext.
    */
   public final void updateComponentExt()
   {
      updateComponentExt(getWidth(), getHeight());
   }

   /**
    * Update component ext.
    *
    * @param frameWidth the frame width
    * @param frameHeight the frame height
    */
   public final void updateComponentExt(int frameWidth, int frameHeight)
   {
      for (JComponentExt comp : iComponents)
      {
         comp.updateComponents(frameWidth, frameHeight);
      }
   }

   public void updateFrameSize(int frame_width, int frame_height)
   {
      iComponentWidth = frame_width;
      iComponentHeight = frame_height;
   }

}
