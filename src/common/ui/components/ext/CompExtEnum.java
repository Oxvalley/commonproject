package common.ui.components.ext;


/**
 * The Enum CompExtEnum.
 */
public enum CompExtEnum
{
   
   /** The Move with height. */
   MoveWithHeight, 
 /** The Move with width. */
 MoveWithWidth, 
 /** The Move with width and height. */
 MoveWithWidthAndHeight, 
 /** The Normal. */
 Normal,
   
   /** The Resize with height. */
   ResizeWithHeight, 
 /** The Resize with width. */
 ResizeWithWidth, 
 /** The Resize with width and height. */
 ResizeWithWidthAndHeight,
   
   /** The Move with width and resize with height. */
   MoveWithWidthAndResizeWithHeight, 
 /** The Move with heighth and resize with width. */
 MoveWithHeighthAndResizeWithWidth

}
