package common.ui.components.ext;

import java.awt.Rectangle;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JScrollPane;

/**
 * The Class JScrollPaneExt.
 */
public class JScrollPaneExt extends JScrollPane implements ComponentListener
{

   /** The Constant serialVersionUID. */
   private static final long serialVersionUID = 4885911318394313536L;

   /** The Component height. */
   private int iComponentHeight;

   /** The Components. */
   private List<JComponent> iComponents = new ArrayList<JComponent>();

   /** The Component width. */
   private int iComponentWidth;

   /**
    * Instantiates a new j scroll pane ext.
    *
    * @param width
    *           the width
    * @param height
    *           the height
    */
   public JScrollPaneExt(int width, int height)
   {
      init(width, height);
   }

   /**
    * Instantiates a new j scroll pane ext.
    *
    * @param width
    *           the width
    * @param height
    *           the height
    * @param viewPortComponent
    *           the view port component
    */
   public JScrollPaneExt(int width, int height, JComponent viewPortComponent)
   {
      super(viewPortComponent);
      init(width, height);
   }

   /**
    * Adds the view port ext.
    *
    * @param comp
    *           the comp
    */
   public final void addViewPortExt(JComponent comp)
   {
      getViewport().add(comp);
      iComponents.add(comp);
   }

   // public final void addComponentExt(JComponent comp, int x, int y, int
   // width,
   // int height)
   // {
   // addComponentExt(comp, x, y, width, height, CompExtEnum.Normal);
   // }
   //
   // public final void addComponentExt(JComponent comp, int x, int y, int
   // width,
   // int height, CompExtEnum status)
   // {
   // JComponentExt compExt =
   // new JComponentExt(comp, x, y, width, height, status,
   // iComponentWidth, iComponentHeight);
   // add(comp);
   // iComponents.add(compExt);
   // }

   /*
    * (non-Javadoc)
    * 
    * @see java.awt.event.ComponentListener#componentHidden(java.awt.event.
    * ComponentEvent)
    */
   @Override
   public void componentHidden(ComponentEvent e)
   {
      // TODO Auto-generated method stub

   }

   /*
    * (non-Javadoc)
    * 
    * @see java.awt.event.ComponentListener#componentMoved(java.awt.event.
    * ComponentEvent)
    */
   @Override
   public void componentMoved(ComponentEvent e)
   {
      // TODO Auto-generated method stub

   }

   /*
    * (non-Javadoc)
    * 
    * @see java.awt.event.ComponentListener#componentResized(java.awt.event.
    * ComponentEvent)
    */
   @Override
   public void componentResized(ComponentEvent e)
   {
      updateComponentExt();
   }

   /*
    * (non-Javadoc)
    * 
    * @see java.awt.event.ComponentListener#componentShown(java.awt.event.
    * ComponentEvent)
    */
   @Override
   public void componentShown(ComponentEvent e)
   {
      // TODO Auto-generated method stub

   }

   /**
    * Gets the comp ext height.
    *
    * @return the comp ext height
    */
   public final int getCompExtHeight()
   {
      return iComponentHeight;
   }

   /**
    * Gets the comp ext width.
    *
    * @return the comp ext width
    */
   public final int getCompExtWidth()
   {
      return iComponentWidth;
   }

   /**
    * Inits the.
    *
    * @param width
    *           the width
    * @param height
    *           the height
    */
   private void init(int width, int height)
   {
      iComponentWidth = width;
      iComponentHeight = height;
      addComponentListener(this);
   }

   /*
    * (non-Javadoc)
    * 
    * @see javax.swing.JComponent#scrollRectToVisible(java.awt.Rectangle)
    */
   @Override
   public void scrollRectToVisible(Rectangle aRect)
   {
      super.scrollRectToVisible(aRect);

      if (!getVisibleRect().contains(aRect)
            && (aRect.width != 0 || aRect.height != 0))
      {
         getHorizontalScrollBar()
               .setValue((int) (aRect.x - (0.2 * getVisibleRect().width)));
      }

   }

   /**
    * Update component ext.
    */
   public final void updateComponentExt()
   {
      updateComponentExt(getWidth(), getHeight());
   }

   /**
    * Update component ext.
    *
    * @param frameWidth
    *           the frame width
    * @param frameHeight
    *           the frame height
    */
   public final void updateComponentExt(int frameWidth, int frameHeight)
   {
      for (JComponent comp : iComponents)
      {
         if (comp instanceof JPanelExt)
         {
            JPanelExt compExt = (JPanelExt) comp;
            compExt.setSize(frameWidth, frameHeight);
            compExt.updateComponentExt(frameWidth, frameHeight);
         }
         else
         {
            if (comp instanceof JComponent)
            {
               comp.repaint();
               comp.setSize(frameWidth, frameHeight);
            }
         }
      }
   }
}