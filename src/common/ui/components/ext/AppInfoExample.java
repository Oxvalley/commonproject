package common.ui.components.ext;


/**
 * The Class AppInfoExample.
 */
public class AppInfoExample 
{
   
   /** The Constant APPLICATION_NAME. */
   private static final String APPLICATION_NAME = "My App";
   
   /** The Constant APPLICATION_VERSION. */
   private static final String APPLICATION_VERSION = "1.0";
   
   /** The Constant JDK_VERSION. */
   private static final String JDK_VERSION = "1.6";

   /**
    * Gets the jdk version.
    *
    * @return the jdk version
    */
   public static String getJdkVersion()
   {
      return JDK_VERSION;
   }

   /**
    * Gets the application name.
    *
    * @return the application name
    */
   public static String getApplicationName()
   {
      return APPLICATION_NAME;
   }

   /**
    * Gets the version.
    *
    * @return the version
    */
   public static String getVersion()
   {
      return APPLICATION_VERSION;
   }


}
