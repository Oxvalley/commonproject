package common.ui.components.ext;

import java.awt.Component;
import java.awt.Rectangle;

import javax.swing.JComponent;

/**
 * The Class JComponentExt.
 */
public class JComponentExt extends JComponent
{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5383909347057644963L;

    /** The Component. */
    private JComponent iComponent;

    /** The From left. */
    private int iFromLeft;

    /** The From top. */
    private int iFromTop;

    /** The Height. */
    private int iHeight;

    /** The Height diff. */
    private int iHeightDiff;

    /** The Status. */
    private CompExtEnum iStatus;

    /** The Width. */
    private int iWidth;

    /** The Width diff. */
    private int iWidthDiff;

    /** The X. */
    private int iX;

    /** The Y. */
    private int iY;

    /**
     * Instantiates a new j component ext.
     *
     * @param comp
     *        the comp
     * @param x
     *        the x
     * @param y
     *        the y
     * @param width
     *        the width
     * @param height
     *        the height
     */
    public JComponentExt(JComponent comp, int x, int y, int width, int height)
    {
        this(comp, x, y, width, height, CompExtEnum.Normal, -1, -1);
    }

    /**
     * Instantiates a new j component ext.
     *
     * @param comp
     *        the comp
     * @param x
     *        the x
     * @param y
     *        the y
     * @param width
     *        the width
     * @param height
     *        the height
     * @param status
     *        the status
     * @param frameWidth
     *        the frame width
     * @param frameHeight
     *        the frame height
     */
    public JComponentExt(JComponent comp,
                         int x,
                         int y,
                         int width,
                         int height,
                         CompExtEnum status,
                         int frameWidth,
                         int frameHeight)
    {
        setOpaque(true);
        iX = x;
        iY = y;
        iWidth = width;
        iHeight = height;
        iStatus = status;
        iComponent = comp;
        initCompExt(iX, iY, iWidth, iHeight, iStatus, frameWidth, frameHeight);

    }

   public void initCompExt(int x, int y, int width, int height, CompExtEnum status, int frameWidth,
         int frameHeight)
   {
      switch (status)
        {
            case MoveWithWidth:
                initMoveWithWidth(x, frameWidth);
                break;

            case MoveWithHeight:
                initMoveWithHeight(y, frameHeight);
                break;

            case MoveWithWidthAndHeight:
                initMoveWithWidth(x, frameWidth);
                initMoveWithHeight(y, frameHeight);
                break;

            case ResizeWithWidth:
                initResizeWithWidth(width, frameWidth);
                break;

            case ResizeWithHeight:
                initResizeWithHeight(height, frameHeight);
                break;

            case ResizeWithWidthAndHeight:
                initResizeWithWidth(width, frameWidth);
                initResizeWithHeight(height, frameHeight);
                break;

            case MoveWithWidthAndResizeWithHeight:
                initMoveWithWidth(x, frameWidth);
                initResizeWithHeight(height, frameHeight);
                break;

            case MoveWithHeighthAndResizeWithWidth:
                initResizeWithWidth(width, frameWidth);
                initMoveWithHeight(y, frameHeight);
                break;

            case Normal:
            default:
                break;
        }
   }

    /**
     * Gets the component ext.
     *
     * @return the component ext
     */
    public Component getComponentExt()
    {
        return iComponent;
    }

    /**
     * Inits the move with height.
     *
     * @param y
     *        the y
     * @param frameHeight
     *        the frame height
     */
    private void initMoveWithHeight(int y, int frameHeight)
    {
        iFromTop = frameHeight - y;
    }

    /**
     * Inits the move with width.
     *
     * @param x
     *        the x
     * @param frameWidth
     *        the frame width
     */
    private void initMoveWithWidth(int x, int frameWidth)
    {
        iFromLeft = frameWidth - x;
    }

    /**
     * Inits the resize with height.
     *
     * @param height
     *        the height
     * @param frameHeight
     *        the frame height
     */
    private void initResizeWithHeight(int height, int frameHeight)
    {
        iHeightDiff = frameHeight - height;
    }

    /**
     * Inits the resize with width.
     *
     * @param width
     *        the width
     * @param frameWidth
     *        the frame width
     */
    private void initResizeWithWidth(int width, int frameWidth)
    {
        iWidthDiff = frameWidth - width;
    }

    /**
     * Update children.
     */
    public final void updateChildren()
    {
        if (iComponent instanceof JPanelExt)
        {
            JPanelExt panel = (JPanelExt) iComponent;
            panel.updateComponentExt();
        }
        
        if (iComponent instanceof JScrollPaneExt)
        {
           JScrollPaneExt panel = (JScrollPaneExt) iComponent;
            panel.updateComponentExt();
        }
    }

    public void setBoundsExt(int x, int y, int width, int height)
    {
        iX = x;
        iY = y;
        iWidth = width;
        iHeight = height;
        setBounds(x, y, width, height);
    }

    public void setBoundsExt(Rectangle rect)
    {
        iX = rect.x;
        iY = rect.y;
        iWidth = rect.width;
        iHeight = rect.height;
        setBounds(rect.x, rect.y, rect.width, rect.height);
    }

    /**
     * Update components.
     *
     * @param frameWidth
     *        the frame width
     * @param frameHeight
     *        the frame height
     */
    public final void updateComponents(int frameWidth, int frameHeight)
    {
        int newX = iX;
        int newY = iY;
        int newWidth = iWidth;
        int newHeight = iHeight;

        switch (iStatus)
        {
            case MoveWithWidth:
                newX = frameWidth - iFromLeft;
                break;

            case MoveWithHeight:
                newY = frameHeight - iFromTop;
                break;

            case MoveWithWidthAndHeight:
                newX = frameWidth - iFromLeft;
                newY = frameHeight - iFromTop;
                break;

            case ResizeWithWidth:
                newWidth = frameWidth - iWidthDiff;
                break;

            case ResizeWithHeight:
                newHeight = frameHeight - iHeightDiff;
                break;

            case ResizeWithWidthAndHeight:
                newWidth = frameWidth - iWidthDiff;
                newHeight = frameHeight - iHeightDiff;
                break;

            case MoveWithWidthAndResizeWithHeight:
                newX = frameWidth - iFromLeft;
                newHeight = frameHeight - iHeightDiff;
                break;

            case MoveWithHeighthAndResizeWithWidth:
                newWidth = frameWidth - iWidthDiff;
                newY = frameHeight - iFromTop;
                break;

            case Normal:
            default:
                break;
        }
        iComponent.setBounds(newX, newY, newWidth, newHeight);
        updateChildren();
    }

   public void setCompExt(CompExtEnum type, int width, int height)
   {
      iStatus = type;
      initCompExt(iX, iY, iWidth, iHeight, iStatus, width, height);
   }

}
