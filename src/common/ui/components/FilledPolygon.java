package common.ui.components;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Rectangle;

public class FilledPolygon
{

   private Graphics iG;
   private double iStartX;
   private double iStartY;
   private boolean isFirst = true;
   private int iX;
   private int iY;
   private int iWidth;
   private int iHeight;
   private Polygon iPoly;
   private Rectangle iRect;
   private Integer iOffsetX;
   private Integer iOffsetY;

   public FilledPolygon(Graphics g, Rectangle rect, Integer offsetX,
         Integer offsetY)
   {
      iG = g;
      iRect = rect;
      iOffsetX = offsetX;
      iOffsetY = offsetY;
      iX = (int) rect.getX();
      iY = (int) rect.getY();
      iWidth = (int) rect.getWidth();
      iHeight = (int) rect.getHeight();
      iPoly = new Polygon();
   }

   public FilledPolygon(FilledPolygon fp)
   {
      this(fp.getGraphics(), fp.getRect(), fp.getOffsetX(), fp.getOffsetY());
   }

   private Integer getOffsetY()
   {
      return iOffsetY;
   }

   private Integer getOffsetX()
   {
      return iOffsetX;
   }

   private Rectangle getRect()
   {
      return iRect;
   }

   private Graphics getGraphics()
   {
      return iG;
   }

   public void addPoint(double xPercent, double yPercent)
   {
      if (isFirst)
      {
         iStartX = xPercent;
         iStartY = yPercent;
         isFirst = false;
      }

      int x = (int) Math.round(iX + iWidth * xPercent);
      int y = (int) Math.round(iY + iHeight * yPercent);
      iPoly.addPoint(x, y);
   }

   public void addPoint(int xPercent, int yPercent)
   {
      addPoint((double) xPercent, (double) yPercent);
   }

   public void addPoint(int xPercent, double yPercent)
   {
      addPoint((double) xPercent, yPercent);
   }

   public void addPoint(double xPercent, int yPercent)
   {
      addPoint(xPercent, (double) yPercent);
   }

   public void drawPolygon()
   {
      drawPolygon(null);
   }

   public void drawPolygon(Color color)
   {
      Color oldColor = null;
      if (color != null)
      {
         oldColor = iG.getColor();
         iG.setColor(color);
      }

      // Back to first position
      addPoint(iStartX, iStartY);
      addOffset(iPoly);
      iG.fillPolygon(iPoly);
      reset();

      if (oldColor != null)
      {
         iG.setColor(oldColor);
      }
   }

   private void addOffset(Polygon poly)
   {
      int size = poly.npoints;
      for (int i = 0; i < size; i++)
      {
         int x = poly.xpoints[i];
         if (iOffsetX != null)
         {
            poly.xpoints[i] = Math.round(x + (iOffsetX * iWidth) / 100);
         }

         int y = poly.ypoints[i];
         if (iOffsetY != null)
         {
            poly.ypoints[i] = Math.round(y + (iOffsetY * iHeight) / 100);
         }
      }
   }

   public void reset()
   {
      iPoly.reset();
      isFirst = true;
   }

   public void drawSquare(double startX, double startY, double width,
         double height, Color color)
   {
      Color oldColor = null;
      if (color != null)
      {
         oldColor = iG.getColor();
         iG.setColor(color);
      }

      iG.fillRect((int) Math.round(iX + iWidth * startX),
            (int) Math.round(iY + iHeight * startY),
            (int) Math.round(iWidth * width),
            (int) Math.round(iHeight * height));

      if (color != null)
      {
         iG.setColor(oldColor);

      }
   }

   public void drawColoredSquare(String colorInHex)
   {
      drawColoredSquare(
            common.utils.ImageHelper.getColor(colorInHex, Color.BLUE));
   }

   public void drawColoredSquare(Color color)
   {
      drawSquare(0, 0, 1, 1, color);
   }

   public FilledPolygon turnClockwise(FilledPolygon fp,
         int numberOfNinetyDegrees)
   {

      FilledPolygon fp2 = fp;
      for (int i = 0; i < numberOfNinetyDegrees; i++)
      {
         fp2 = turnClockwise(fp2);
      }

      return fp2;
   }

   public FilledPolygon turnClockwise(FilledPolygon fp)
   {
      FilledPolygon fp2 = new FilledPolygon(fp);
      Polygon poly = fp.getPolygon();
      int size = poly.npoints;
      for (int i = 0; i < size; i++)
      {
         int x = poly.xpoints[i];
         int y = poly.ypoints[i];
         double xPercent = getXPercent(x);
         double yPercent = getYPercent(y);
         fp2.setOffsetX(fp.getOffsetX());
         fp2.setOffsetY(fp.getOffsetY());
         fp2.addPoint(1.0 - yPercent, xPercent);
      }
      fp.reset();

      return fp2;
   }

   public FilledPolygon upsideDown(FilledPolygon fp)
   {
      FilledPolygon fp2 = new FilledPolygon(fp);
      Polygon poly = fp.getPolygon();
      int size = poly.npoints;
      for (int i = 0; i < size; i++)
      {
         int x = poly.xpoints[i];
         int y = poly.ypoints[i];
         double xPercent = getXPercent(x);
         double yPercent = getYPercent(y);
         fp2.setOffsetX(fp.getOffsetX());
         fp2.setOffsetY(fp.getOffsetY());
         fp2.addPoint(1.0 - xPercent, 1.0 - yPercent);
      }
      fp.reset();

      return fp2;
   }

   private double getYPercent(int y)
   {
      return (y - iY) * 1.0 / iHeight;
   }

   private double getXPercent(int x)
   {
      return (x - iX) * 1.0 / iWidth;
   }

   private Polygon getPolygon()
   {
      return iPoly;
   }

   public FilledPolygon mirrorVertical(FilledPolygon fp)
   {
      FilledPolygon fp2 = new FilledPolygon(fp);
      Polygon poly = fp.getPolygon();
      int size = poly.npoints;
      for (int i = 0; i < size; i++)
      {
         int x = poly.xpoints[i];
         int y = poly.ypoints[i];
         double xPercent = getXPercent(x);
         double yPercent = getYPercent(y);
         fp2.setOffsetX(fp.getOffsetX());
         fp2.setOffsetY(fp.getOffsetY());
         fp2.addPoint(1.0 - xPercent, yPercent);
      }
      fp.reset();

      return fp2;
   }

   public FilledPolygon mirrorHorizontal(FilledPolygon fp)
   {
      FilledPolygon fp2 = new FilledPolygon(fp);
      Polygon poly = fp.getPolygon();
      int size = poly.npoints;
      for (int i = 0; i < size; i++)
      {
         int x = poly.xpoints[i];
         int y = poly.ypoints[i];
         double xPercent = getXPercent(x);
         double yPercent = getYPercent(y);
         fp2.setOffsetX(fp.getOffsetX());
         fp2.setOffsetY(fp.getOffsetY());
         fp2.addPoint(xPercent, 1.0 - yPercent);
      }
      fp.reset();

      return fp2;
   }

   public void setOffsetX(Integer offsetX)
   {
      iOffsetX = offsetX;
   }

   public void setOffsetY(Integer offsetY)
   {
      iOffsetY = offsetY;
   }
}
