package common.ui.components;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import common.properties.SettingProperties;
import common.ui.components.ext.CompExtEnum;
import common.ui.components.ext.JPanelExt;


/**
 * The Class ProgressWindow.
 */
public class ProgressWindow extends JFrame implements Runnable
{
   
   /** The progress bar. */
   private JProgressBar progressBar;
   
   /** The txt output. */
   private JTextArea txtOutput;
   
   /** The Text. */
   private String iText = "";
   
   /** The Progress steps. */
   private int iProgressSteps;

   private int width = 600;
   private int height = 700;

   private JPanelExt iPanelExt;

   
   /* (non-Javadoc)
    * @see java.lang.Runnable#run()
    * 
    * 
    */
   
   @Override
   public void run()
   {
      setSize(width, height);

      getContentPane().setLayout(new BorderLayout());
      iPanelExt = new JPanelExt(width, height);
      iPanelExt.setLayout(null);
      getContentPane().add(iPanelExt, BorderLayout.CENTER);

      progressBar = new JProgressBar(0, iProgressSteps);
      progressBar.setValue(0);
      iPanelExt.addComponentExt(progressBar, 50, 100, width-100, height-300, CompExtEnum.ResizeWithWidth);

      txtOutput = new JTextArea(20, 40);
      JScrollPane scroller = new JScrollPane();
      scroller.getViewport().add(txtOutput);
      scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
      iPanelExt.addComponentExt(scroller, 50, 200, width-100, height-300, CompExtEnum.ResizeWithWidthAndHeight);

      pack();
      setLocationRelativeTo(null);
      setVisible(true);
      repaint();
   }

   /**
    * Instantiates a new progress window.
    *
    * @param title the title
    * @param progressSteps the progress steps
    * @param iconName the icon name
    */
   public ProgressWindow(String title, int progressSteps, String iconName)
   {
      super(title);
      setIconImage(getToolkit().getImage(iconName));
      iProgressSteps = progressSteps;

   }

   /**
    * Prints the.
    *
    * @param text the text
    */
   public void print(String text)
   {
      iText += text + "\n";
      if (txtOutput != null)
      {
         txtOutput.setText(iText);
         // Scroll to the end of the text
         txtOutput.setCaretPosition(txtOutput.getDocument().getLength());
      }
   }

   /**
    * Increase progress bar.
    */
   public void increaseProgressBar()
   {
      if (progressBar != null)
      {
         progressBar.setValue(progressBar.getValue() + 1);
      }
      
      repaint();
   }

}
