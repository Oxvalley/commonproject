package common.ui.components;

public interface GroupedButtonListener
{
   void unFrameOtherRadioButtons(FramedJButton framedJButton);
}
