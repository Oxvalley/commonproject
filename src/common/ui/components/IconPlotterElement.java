package common.ui.components;

import javax.swing.JButton;

import common.ui.components.FramedJButton;
import common.ui.components.GroupedButtonListener;

public class IconPlotterElement 
{

   public FramedJButton getFramedJButton()
   {
      return iFramedJButton;
   }

   public String getXmlConstant()
   {
      return iXmlConstant;
   }

   public JButton getJButton()
   {
      return iBtn;
   }

   private FramedJButton iFramedJButton;
   private String iXmlConstant;
   private JButton iBtn;
   private String iImagePath;
   private String iToolTip;
   private int iKeyEvent;
   private int iActionEvent;

   public IconPlotterElement(FramedJButton framedJButton,
         String xmlConstant, JButton btn, String imagePath, String toolTip,int keyEvent, int actionEvent)
   {
      iFramedJButton = framedJButton;
      iXmlConstant = xmlConstant;
      iBtn = btn;
      iImagePath = imagePath;
      iToolTip = toolTip;
      iKeyEvent = keyEvent;
      iActionEvent = actionEvent;
   }

   public String getImagePath()
   {
      return iImagePath;
   }

   public String getToolTip()
   {
      return iToolTip;
   }

   public int getKeyEvent()
   {
      return iKeyEvent;
   }

   public int getActionEvent()
   {
      return iActionEvent;
   }

   public void setRadioGroupName(String groupName, boolean selected, GroupedButtonListener listener)
   {
      iFramedJButton.setRadioGroupName(groupName, selected, listener);
   }

}
