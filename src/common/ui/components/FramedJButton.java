package common.ui.components;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class FramedJButton
{

   private JButton iBtn;
   private boolean iFrameIsLit;
   final static Border thickBorder = new LineBorder(Color.RED, 2);
   final Border origBorder;
   private String iGroupName;
   private GroupedButtonListener iListener;
   private FramedJButton iThis;

   public FramedJButton(final JButton btn, boolean useFrame)
   {
      iBtn = btn;
      origBorder = btn.getBorder();
      iFrameIsLit = false;
      toggleFrame();
      iThis = this;

      if (useFrame)
      {
         btn.addActionListener(new ActionListener()
         {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
               clickedIcon();
            }
         });
      }
   }

   public void clickedIcon()
   {
      toggleFrame();
      if (!isFrameLit() && iGroupName != null)
      {
         iListener.unFrameOtherRadioButtons(iThis);
      }
   }

   public void toggleFrame()
   {
      if (iFrameIsLit)
      {
         iBtn.setBorder(thickBorder);
      }
      else
      {
         iBtn.setBorder(origBorder);
      }
      iFrameIsLit = !iFrameIsLit;
   }

   public void setFrame(boolean isFrameLit)
   {
      iFrameIsLit = isFrameLit;
      toggleFrame();
   }

   public boolean isFrameLit()
   {
      return iFrameIsLit;
   }

   public void setRadioGroupName(String groupName, boolean selected, GroupedButtonListener listener)
   {
      iGroupName = groupName;
      iListener = listener;
      setFrame(selected);
   }

   public String getRadioGroupName()
   {
      return iGroupName;
   }

}
