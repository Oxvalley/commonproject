package common.ui.components;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;

import common.utils.ImageHelper;
import common.utils.Language;

public class IconPlotter implements GroupedButtonListener
{

   private int iButtonheight;
   private int iButtonwidth;
   private int iXStart;
   private int iX;
   private int iY;
   private int iAPART_X;
   private int iAPART_Y;
   private int iNumberPerRow;
   private int iXCount = 0;
   Map<String, IconPlotterElement> iMapNoFrames = new HashMap<>();
   Map<String, IconPlotterElement> iMapAll = new HashMap<>();
   private Container iPanel;
   private IconClickListener iIconClickListener;
   private static Map<String, String> iShortcutsMap = new HashMap<>();
   private Language iLang = null;

   public IconPlotter(Container container, int buttonwidth, int buttonheight,
         int x, int y, int aPART_X, int aPART_Y, int numberPerRow,
         IconClickListener iconClickListener)
   {
      iPanel = container;
      iButtonwidth = buttonwidth;
      iButtonheight = buttonheight;
      iX = x;
      iXStart = x;
      iY = y;
      iAPART_X = aPART_X;
      iAPART_Y = aPART_Y;
      iNumberPerRow = numberPerRow;
      iIconClickListener = iconClickListener;
      iShortcutsMap = IconPlotter.getShortcutsMap();
      //iShortcutsMap.clear();
      iLang = Language.getInstance();
   }

   public static Map<String, String> getShortcutsMap()
   {
      return iShortcutsMap;
   }

   public IconPlotterElement add(String imagePath, String toolTip,
         final String xmlConstant)
   {
      return add(imagePath, toolTip, xmlConstant, true, -1, -1);
   }

   public IconPlotterElement add(String imagePath, String toolTip,
         final String xmlConstant, boolean useFrame)
   {
      return add(imagePath, toolTip, xmlConstant, useFrame, -1, -1);
   }

   public IconPlotterElement add(String imagePath, String toolTip,
         final String xmlConstant, int keyEvent, int actionEvent)
   {
      return add(imagePath, toolTip, xmlConstant, true, keyEvent, actionEvent);
   }

   public IconPlotterElement add(String imagePath, String toolTip,
         final String xmlConstant, boolean useFrame, int keyEvent,
         int actionEvent)
   {

      toolTip = iLang.getText(toolTip);
      
      String fullToolTip = getShortcutAsText(keyEvent, actionEvent);

      String shortc = iShortcutsMap.get(fullToolTip);
      if (shortc != null && !shortc.equals(toolTip))
      {
         System.out.println("Duplicated: " + toolTip + " and " + shortc
               + " for shortcut " + fullToolTip);
      }
      iShortcutsMap.put(fullToolTip, toolTip);
      

      fullToolTip = toolTip + " " + fullToolTip;

      JButton btn = ImageHelper.getJButtonWithImage(imagePath, iX, iY,
            iButtonwidth, iButtonheight, fullToolTip.trim());
      btn.addMouseListener(new MouseListener()
      {
         @Override
         public void mouseReleased(MouseEvent e)
         {
         }

         @Override
         public void mousePressed(MouseEvent e)
         {
         }

         @Override
         public void mouseExited(MouseEvent e)
         {
         }

         @Override
         public void mouseEntered(MouseEvent e)
         {
         }

         @Override
         public void mouseClicked(MouseEvent e)
         {
            if (e.getClickCount() == 2 && !e.isConsumed())
            {
               iIconClickListener.doubleClickIcon(xmlConstant);
            }
         }
      });

      iPanel.add(btn);

      iX += iAPART_X;
      iXCount++;
      if (iXCount == iNumberPerRow)
      {
         iY += iAPART_Y;
         iX = iXStart;
         iXCount = 0;
      }

      final FramedJButton framedJButton = new FramedJButton(btn, useFrame);
      iMapAll.put(xmlConstant, new IconPlotterElement(framedJButton, xmlConstant, btn,
            imagePath, toolTip, keyEvent, actionEvent));
      if (useFrame)
      {
         btn.addActionListener(new ActionListener()
         {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
               iIconClickListener.clickIcon(xmlConstant,
                     framedJButton.isFrameLit());
            }

         });

         IconPlotterElement elem = new IconPlotterElement(framedJButton,
               xmlConstant, btn, imagePath, toolTip, keyEvent, actionEvent);
         iMapNoFrames.put(xmlConstant, elem);
         return elem;
      }
      else
      {

         btn.addActionListener(new ActionListener()
         {
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
               iIconClickListener.clickIcon(xmlConstant);
            }

         });
         return null;
      }

   }

   public static String getShortcutAsText(int keyEvent, int actionEvent)
   {
      String fullToolTip = "";

      if (actionEvent > -1)
      {
         if ((actionEvent & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK)
         {
            fullToolTip += "CTRL";
         }

         if ((actionEvent & ActionEvent.SHIFT_MASK) == ActionEvent.SHIFT_MASK)
         {
            if (fullToolTip.length() > 0)
            {
               fullToolTip += "+";
            }
            fullToolTip += "SHIFT";
         }

         if ((actionEvent & ActionEvent.ALT_MASK) == ActionEvent.ALT_MASK)
         {
            if (fullToolTip.length() > 0)
            {
               fullToolTip += "+";
            }
            fullToolTip += "ALT";
         }
      }

      if (keyEvent > -1)
      {
         fullToolTip += "+" + ((char) keyEvent);
      }
      return fullToolTip;
   }

   public Collection<IconPlotterElement> getButtonList()
   {
      return iMapNoFrames.values();
   }

   public void resetElements()
   {
      for (IconPlotterElement elem : getButtonList())
      {
         if (elem.getFramedJButton().getRadioGroupName() == null)
         {
            elem.getFramedJButton().setFrame(false);
         }
      }
   }

   public void setRadioGroupName(IconPlotterElement elem, String groupName)
   {
      elem.setRadioGroupName(groupName, false, this);
   }

   public void setRadioGroupName(IconPlotterElement elem, String groupName,
         boolean selected)
   {
      elem.setRadioGroupName(groupName, selected, this);
   }

   @Override
   public void unFrameOtherRadioButtons(FramedJButton framedJButton)
   {
      for (IconPlotterElement elem : getButtonList())
      {
         FramedJButton button = elem.getFramedJButton();

         if (framedJButton.getRadioGroupName()
               .equals(button.getRadioGroupName()) && button != framedJButton)
         {
            elem.getFramedJButton().setFrame(false);
         }
      }
   }

   public void setFrame(String xmlConstant)
   {
      IconPlotterElement elem = getIconPlotterElement(xmlConstant);
      FramedJButton button = elem.getFramedJButton();
      if (button != null)
      {
         elem.getFramedJButton().clickedIcon();
      }
   }

   public IconPlotterElement getIconPlotterElement(String xmlConstant)
   {
      return iMapAll.get(xmlConstant);
   }

   public Collection<IconPlotterElement> getAllButtonsList()
   {
      return iMapAll.values();
   }

}
