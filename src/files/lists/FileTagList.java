package files.lists;

import java.util.ArrayList;

public class FileTagList extends ArrayList<String>
{

   public FileTagList(String[] tags)
   {
      for (String tag : tags)
      {
         add(tag);
      }
   }

}
