package files.lists;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import common.utils.FileUtil;

public class FileFilterImpl implements FileFilter
{

   private List<String> i_Tags = null;

   public FileFilterImpl(List<String> tags)
   {
      if (tags != null)
      {
         i_Tags = new ArrayList<String>();
         for (String tag : tags)
         {
            if (tag != null)
            {
               i_Tags.add(tag.toLowerCase());
            }
            else
            {

            }
         }
      }
   }

   @Override
   public boolean accept(File file)
   {
      if (i_Tags == null)
      {
         return true;
      }

      if (file.isDirectory())
      {
         return true;
      }
      else
      {
         String tag = FileUtil.getFileTag(file.getName());
         return i_Tags.contains(tag.toLowerCase());
      }

   }
}
