package files.lists;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import common.utils.FileUtil;

public class FileLister {
   private static boolean i_DestFolderCashed = false;
   private static ArrayList<String> i_destList;
   private static HashMap<Long, ArrayList<File>> i_HashedDestSizes;
   @SuppressWarnings("unused")
   private String i_Path;

   public FileLister(String path) {
      i_Path = path;
   }

   public static ArrayList<String> getAllFiles(String path,
         String destinationPath) {
      ArrayList<String> tags = new ArrayList<String>();
      return getAllFiles(path, tags, destinationPath);
   }

   public static ArrayList<String> getAllFiles(String path, List<String> tags,
         String destinationPath) {
      ArrayList<String> lstFiles;
      File destinationFile = FileUtil.getFolderFileFromPath(destinationPath);

      if (path == null) {
         // if path == null then scan whole computer
         File[] lst = File.listRoots();
         lstFiles = new ArrayList<String>();
         for (File filepath : lst) {
            getAllFilesRecursive(lstFiles, filepath.getPath(), tags,
                  destinationFile);
         }
      } else {
         lstFiles = getAllFilesRecursive(null, path, tags, destinationFile);
      }
      return lstFiles;
   }

   private static ArrayList<String> getAllFilesRecursive(
         ArrayList<String> lstFiles, String path, List<String> tags,
         File destinationFile) {
      ArrayList<String> lst;
      if (lstFiles == null) {
         lst = new ArrayList<String>();
      } else {
         lst = lstFiles;
      }

      FileFilterImpl ffi = new FileFilterImpl(tags);

      File f = new File(path);
      if (f.isDirectory()) {
         getAllFilesRecursive(lst, ffi, path, destinationFile, tags);
      } else {
         lst.add(path);
      }
      return lst;
   }

   private static ArrayList<String> getAllFilesRecursive(ArrayList<String> lst,
         FileFilterImpl ffi, String path, File destinationFile, List<String> tags) {
      File f = new File(path);

      File[] files = f.listFiles(ffi);

      if (files != null) {
         for (File file : files) {
            if (file.isDirectory()) {
               getAllFilesRecursive(lst, ffi, file.getAbsolutePath(),
                     destinationFile, tags);
            } else {
               lst.add(file.getAbsolutePath());
               if (destinationFile != null) {
                  copyFile(destinationFile, file, tags);
               }
            }
         }
      }
      return lst;
   }

   public static void copyFile(String sourcePath, String destinationPath,
         List<String> tags) {
      File sourceFile = new File(sourcePath);
      File destinationFolder = new File(destinationPath);
      copyFile(sourceFile, destinationFolder, tags);
   }

   public synchronized static boolean copyFile(File sourceFile,
         File destinationFolder, List<String> tags) {
      // We want to copy this file to destinationPath

      // Check if destination folder is cashed up
      if (!i_DestFolderCashed) {
         // If no, cash that and subfolders for names...
         i_destList = getAllFiles(destinationFolder.getAbsolutePath(), tags);
         System.out.println("Destination folder has " + i_destList.size()
               + " files");
         // ...and sizes
         i_HashedDestSizes = new HashMap<Long, ArrayList<File>>();
         for (String filePath : i_destList) {
            File file = new File(filePath);
            i_HashedDestSizes = addFileToSizeMap(i_HashedDestSizes, file);
         }

         i_DestFolderCashed = true;
      }

      // Is this file already in destinationPath?
      Long key = new Long(sourceFile.length());
      ArrayList<File> filelist = i_HashedDestSizes.get(key);
      if (filelist != null) {
         // Must have same size...
         // Look through list of files with same size for duplicate
         for (File file2 : filelist) {
            // ...and same binary content
            if (isSameBinary(file2, sourceFile)) {
               // If yes, don't copy
               // No copy, file is already in here
               return false;
            }
         }

      }

      String newFilename = destinationFolder + File.separator
            + sourceFile.getName();

      // Is there a file with the SAME NAME in destpath, but is other file?
      // if yes, add _1
      // does file _1 already exist?
      // If yes, add _2 instead and so on...
      newFilename = getFirstFreeFileName(newFilename);

      // Copy file now
      try {
         FileUtil.copy(sourceFile.getAbsolutePath(), newFilename, true);
      } catch (IOException e) {
         System.out.println("Could not copy file "
               + sourceFile.getAbsolutePath() + " to " + newFilename);
         System.out.println(e.getMessage());
         return false;
      }

      System.out.println("Copying " + sourceFile.getAbsolutePath() + " to "
            + newFilename);

      // Add file to cash
      addFileToSizeMap(i_HashedDestSizes, new File(newFilename));
      return true;
   }

   private static String addUnderscoreNumber(String filename, int number) {
      // remove filetag
      String numberString = "_" + String.valueOf(number);
      int i = filename.lastIndexOf(".");
      if (i != -1 && i < filename.length() - 1) {
         // FileTag exists, remove it
         String file = filename.substring(0, i);
         String tag = filename.substring(i);
         int i2 = getUnderscoreNumber(file);
         if (i2 > 0) {
            // This file already has a underscore plus number i2
            String numberstring2 = "_" + i2;
            if (file.toLowerCase().lastIndexOf(numberstring2) == file.length()
                  - numberstring2.length()) {
               // The tag is there and at the end of the file, cut it out
               file = file.substring(0, file.length() - numberstring2.length());
            }
         }

         return file + numberString + tag;
      } else {
         // No FileTag exists
         return filename + numberString;
      }
   }

   private static String getFirstFreeFileName(String filename) {
      String filename2 = filename;
      File f = new File(filename2);
      int number = -1;

      while (f.exists()) {
         if (number == -1) {
            number = getUnderscoreNumber(filename2);
         }

         number++;
         filename2 = addUnderscoreNumber(filename2, number);
         f = new File(filename2);
      }

      return filename2;
   }

   private static int getUnderscoreNumber(String filename) {
      // remove filetag
      int i = filename.lastIndexOf(".");
      String filename2;
      if (i != -1 && i < filename.length() - 1) {
         // FileTag exists, remove it
         filename2 = filename.substring(0, i);
      } else {
         // No FileTag
         filename2 = filename;
      }

      int i2 = filename2.lastIndexOf("_");
      if (i2 == -1 || i2 == filename2.length() - 1) {
         // No underscore yet, or Underscore is last
         return 0;
      }

      // Cut name after underscore
      String file3 = filename2.substring(i2 + 1);

      try {
         return Integer.parseInt(file3);
      } catch (NumberFormatException e) {
         // No number after underscore
         return 0;
      }

   }

   public static boolean isSameBinary(File file1, File file2) {
      if (file1.length() != file2.length()) {
         return false;
      }

      try {
         BufferedInputStream stream1 = new BufferedInputStream(
               new FileInputStream(file1));
         BufferedInputStream stream2 = new BufferedInputStream(
               new FileInputStream(file2));

         int c1;
         int c2;

         while ((c1 = stream1.read()) != -1) {
            c2 = stream2.read();
            if (c1 != c2) {
               return false;
            }
         }
         System.out.println("File " + file2 + " is identical to " + file1);
         return true;
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
      return false;
   }

   public static HashMap<Long, ArrayList<File>> addFileToSizeMap(
         HashMap<Long, ArrayList<File>> hash, File file) {
      Long key = new Long(file.length());
      ArrayList<File> filelist = hash.get(key);

      if (filelist == null) {
         filelist = new ArrayList<File>();
         hash.put(key, filelist);
      }
      filelist.add(file);
      return hash;
   }

   public static void main(String[] args) {
//      String destinationPath = "D:/sparas/temp";
//      String sourcePath = "D:/sparas";
//      // FileLister pl = new FileLister(sourcePath);
//      String[] tags = { "mp3", "wma", "ogg" };
//      // pl.copyAllFiles(tags, destinationPath);
//      // pl.findFiles(tags);
//      File destFolder = FileHelper.getFolderFileFromPath(destinationPath);
//      ArrayList<String> lst = getAllFiles(sourcePath, tags);
//      System.out.println("Found " + lst.size() + " files in " + sourcePath);
//      for (String filePath : lst) {
//         File sourceFile = new File(filePath);
//         copyFile(sourceFile, destFolder, tags);
//      }
   }

   public static ArrayList<String> getAllFiles(String sourcePath, List<String> tags) {
      return getAllFiles(sourcePath, tags, null);
   }

   public static void findDuplicates(String destPath, List<String> tags,
         boolean removeShortesFilename) {
      HashMap<Long, ArrayList<File>> i_HashedDestSizes = chacheFilesBySize(
            destPath, tags);

      for (ArrayList<File> filelist : i_HashedDestSizes.values()) {
         if (filelist.size() > 1) {
            for (int i = 0; i < filelist.size() - 1; i++) {
               for (int j = i + 1; j < filelist.size(); j++) {
                  File f1 = filelist.get(i);
                  File f2 = filelist.get(j);
                  if (isSameBinary(f1, f2)) {
                     if (removeShortesFilename) {
                        if (f1.getName().length() < f2.getName().length()) {
                           System.out
                                 .println("Deleting shortes filname: " + f1);
                           f1.deleteOnExit();
                        } else {
                           System.out
                                 .println("Deleting shortes filname: " + f2);
                           f2.deleteOnExit();
                        }
                     }
                  }
               }
            }
         }
      }
      System.out.println("Klart!!");

   }

   private static HashMap<Long, ArrayList<File>> chacheFilesBySize(
         String destPath, List<String> tags) {
      // If no, cash that and subfolders for names...
      ArrayList<String> destList = getAllFiles(destPath, tags);
      System.out
            .println("Destination folder has " + destList.size() + " files");
      // ...and sizes
      HashMap<Long, ArrayList<File>> hashedDestSizes = new HashMap<Long, ArrayList<File>>();
      for (String filePath : destList) {
         File file = new File(filePath);
         addFileToSizeMap(hashedDestSizes, file);
      }
      System.out.println("Done cashing " + destList.size() + " files in "
            + hashedDestSizes.keySet().size() + " different sizes");
      int i = 0;
      int max = -1;
      for (ArrayList<File> lst : hashedDestSizes.values()) {
         if (lst.size() > 1) {
            i++;
         }

         if (lst.size() > max) {
            max = lst.size();
         }

      }
      System.out.println(i + " sizes has more than one file.");
      System.out.println("Max files in one size is " + max);
      return hashedDestSizes;
   }
}
