package files.lists;

import java.io.File;
import java.io.FilenameFilter;

import javax.swing.filechooser.FileFilter;

import common.utils.ImageHelper;

public class ImageFileAndDirFilter extends FileFilter implements FilenameFilter
{

   @Override
   public boolean accept(File dir)
   {
      if (dir.isDirectory() || ImageHelper.isImageFile(dir.getAbsolutePath()))
      {
         return true;
      }
      else
      {
         return false;
      }
   }

   @Override
   public String getDescription()
   {
      return null;
   }

   @Override
   public boolean accept(File dir, String name)
   {
      return accept(dir);
   }

}
