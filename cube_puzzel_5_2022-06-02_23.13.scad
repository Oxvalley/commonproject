// Generated 2022-06-02 23:13 by CubePuzzler
// SHOW_SOLUTION can be changed to false to just show the 6 pieces
SHOW_SOLUTION=true;
MOVE=.1;
SHOW=true;
CUBE_THICKNESS = 6.0;
THINNER = .05;

translate([0 * 6 * CUBE_THICKNESS + 5 * CUBE_THICKNESS, 0 * 6 * CUBE_THICKNESS, 1 * CUBE_THICKNESS])
   rotate([0,180,0])
      *piece_1();

translate([1 * 6 * CUBE_THICKNESS + 5 * CUBE_THICKNESS, 0 * 6 * CUBE_THICKNESS, 1 * CUBE_THICKNESS])
   rotate([0,180,0])
      *piece_2();

translate([2 * 6 * CUBE_THICKNESS + 5 * CUBE_THICKNESS, 0 * 6 * CUBE_THICKNESS, 1 * CUBE_THICKNESS])
   rotate([0,180,0])
      *piece_3();

translate([0 * 6 * CUBE_THICKNESS + 5 * CUBE_THICKNESS, 1 * 6 * CUBE_THICKNESS, 1 * CUBE_THICKNESS])
   rotate([0,180,0])
      *piece_4();

translate([1 * 6 * CUBE_THICKNESS + 5 * CUBE_THICKNESS, 1 * 6 * CUBE_THICKNESS, 1 * CUBE_THICKNESS])
   rotate([0,180,0])
      *piece_5();

translate([2 * 6 * CUBE_THICKNESS + 0 * CUBE_THICKNESS, 1 * 6 * CUBE_THICKNESS, 0 * CUBE_THICKNESS])
   *piece_6();
   
   


if(SHOW_SOLUTION)
{
  scaled=1.02;

  translate([0,-CUBE_THICKNESS * 6,0])
  {
    MOVE_IN = THINNER * 3;

    scale([scaled,scaled,scaled])
      piece_1();

    translate([CUBE_THICKNESS * 5 - MOVE_IN, MOVE_IN, MOVE_IN])
      rotate([0,-90,0])
        scale([scaled,scaled,scaled])
          piece_2();

    translate([CUBE_THICKNESS * 5  - MOVE_IN -.1, MOVE_IN - .2, CUBE_THICKNESS *  5  + MOVE_IN])
      rotate([0,180,0])
        scale([scaled,scaled,scaled])
          piece_3();
    translate([MOVE_IN, MOVE_IN, CUBE_THICKNESS * 5 - MOVE_IN])
      rotate([0,90,0])
        scale([scaled,scaled,scaled])
          piece_4();

    translate([MOVE_IN + .1, MOVE_IN -.1, CUBE_THICKNESS * 5 - MOVE_IN])
      rotate([-90,0,0])
        scale([scaled,scaled,scaled])
          piece_5();

    translate([-MOVE_IN, CUBE_THICKNESS * 5 - MOVE_IN, MOVE_IN -.1])
      rotate([90,0,0])
        scale([scaled,scaled,scaled])
          piece_6();
  }
}
module piece_1()
{
   color("GREEN")
      difference()
      {
         union()
         {
             cube([5 * CUBE_THICKNESS, 5 * CUBE_THICKNESS, CUBE_THICKNESS]);
         }
         box_remove(3, 0);
         box_remove(4, 0);
         box_remove(4, 2);
         box_remove(0, 3);
         box_remove(4, 3);
         box_remove(0, 4);
         box_remove(2, 4);
         box_remove(3, 4);
         box_remove(4, 4);
      }
   }

module piece_2()
{
   color("YELLOW")
      difference()
      {
         union()
         {
             cube([5 * CUBE_THICKNESS, 5 * CUBE_THICKNESS, CUBE_THICKNESS]);
         }
         box_remove(0, 0);
         box_remove(1, 0);
         box_remove(2, 0);
         box_remove(4, 0);
         box_remove(0, 1);
         box_remove(4, 1);
         box_remove(0, 4);
         box_remove(1, 4);
      }
   }

module piece_3()
{
   color("BLACK")
      difference()
      {
         union()
         {
             cube([5 * CUBE_THICKNESS, 5 * CUBE_THICKNESS, CUBE_THICKNESS]);
         }
         box_remove(2, 0);
         box_remove(3, 0);
         box_remove(4, 0);
         box_remove(4, 1);
         box_remove(0, 2);
         box_remove(0, 3);
         box_remove(0, 4);
         box_remove(1, 4);
         box_remove(2, 4);
         box_remove(4, 4);
      }
   }

module piece_4()
{
   color("PINK")
      difference()
      {
         union()
         {
             cube([5 * CUBE_THICKNESS, 5 * CUBE_THICKNESS, CUBE_THICKNESS]);
         }
         box_remove(0, 0);
         box_remove(1, 0);
         box_remove(2, 0);
         box_remove(4, 0);
         box_remove(4, 1);
         box_remove(0, 2);
         box_remove(4, 2);
         box_remove(0, 3);
         box_remove(0, 4);
         box_remove(1, 4);
      }
   }

module piece_5()
{
   color("ORANGE")
      difference()
      {
         union()
         {
             cube([5 * CUBE_THICKNESS, 5 * CUBE_THICKNESS, CUBE_THICKNESS]);

         }
         box_remove(3, 0);
         box_remove(4, 0);
         box_remove(4, 1);
         box_remove(0, 3);
         box_remove(0, 4);
         box_remove(1, 4);
         box_remove(2, 4);
         
      }
      
      if(SHOW)
      {
        translate([-MOVE , -MOVE ,  -MOVE])
          color("RED")
            box(0,0);
      }
   }

module piece_6()
{
   color("BLUE")
      difference()
      {
         union()
         {
             cube([5 * CUBE_THICKNESS, 5 * CUBE_THICKNESS, CUBE_THICKNESS]);
         }
         box_remove(0, 0);
         box_remove(1, 0);
         box_remove(0, 1);
         box_remove(0, 2);
         box_remove(4, 2);
         box_remove(4, 3);
         box_remove(1, 4);
         box_remove(4, 4);
      }
   }

module box(x, y, lessLeft = 0, lessRight = 0,lessUp = 0,lessDown = 0)
{
   translate([x * CUBE_THICKNESS + lessLeft * THINNER, y * CUBE_THICKNESS + lessUp * THINNER, 0])
      cube([CUBE_THICKNESS - (lessLeft + lessRight) * THINNER, CUBE_THICKNESS - (lessUp + lessDown) * THINNER, CUBE_THICKNESS]);
}

module box_remove(x, y)
{
   translate([x * CUBE_THICKNESS - THINNER, y * CUBE_THICKNESS - THINNER, -.01])
      cube([CUBE_THICKNESS + 2 * THINNER, CUBE_THICKNESS + 2 * THINNER, CUBE_THICKNESS + .02]);
}

