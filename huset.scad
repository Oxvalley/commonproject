include <Z:/3D/openscad-git/oxvalley.scad>;
include<Z:/3D/openscad-git/sumup.scad>
overlap=1;
overlap_scaled = scale(overlap);
house_length_scaled = scale(11440);
house_width_scaled = scale(7230);
wall_extra_height_scaled = scale(150);
wall_heigth_scaled = scale(2150)+ wall_extra_height_scaled;
door_height_scaled = scale(1900);
door_width = 830;
door_degrees_open = 5;
window_width_scaled = scale(1000);
window_heigth_scaled = scale(1200);
window_above_floor_scaled = scale(1000);
wall_half_width_scaled = scale(100);
floor_thickness_scaled = scale(300);
// Not scaled
stair_points = [[-25,0],[295,0],[295,-20],[277,-36],[195,-36],[195,-190],[170,-190],[170,-36],[-25,-36],[-25,0]];
stair_side_points = [[0,0],[0,200],[1700,1800],[1700,0],[0,0]];
wine_x = 1740;
living_x = 5420;
living_y = 4210;
toilet_x = 1710;
toilet_y = 900;
toilet_door_x = 390;
laundry_x=3830;
laundry_y=2900;
bathroom_x=3330;
garage_x=3680;
garage_y=4350;
outside_garage_y=930;
middle_room_y=1780;
stair_closet_y=920;
basement_stairs_y=910;
balcony_x=3970;
balcony_y=2140;
linas_x=2260;
linas_y=3070;
hallway_x = 2470;
hallway_y=1320;
anton_y=2110;
anton_x=3530;
toilet_ground_floor_y = 1580;
living_room_ground_floor_x=4480;
living_room_ground_floor_y=3960;
window_hight_ground_floor=1230;
add_furniture = false;
function scale(millimeter) = millimeter/100;
function unscale(millimeter) = millimeter/scale(1);
// Start of house

living_room()

difference()
{
  *basement();
  //translate([scale(1000), scale(3000), scale(-400)])
  //cube([scale(14000), scale(14000), scale(14000)]);
}
translate([scale(0),scale(15000), scale(0)])
{
  *ground_floor();
  
  
}

module living_room()
{
  difference()
  {
    union()
    {
      translate([194, 302,0])
      {
        room(5420,4210);
        
        // Chimney
        translate([0, 130, 0])
        {
          room(240, 1600);
        } // end translate
        
        window("North", 421, 220, 1620, 750, 460);
        window("North", 421, 318, 1620, 760, 460);
        door("North", 100,"Left", 70, 180, 10);
        // TODO add door in

        door("East", 100,"Right",83,190,5);
        // TODO add door out
        door("East", 100,"Left",83,190,5);
        // Bathtub
        #add_stl_scaled("Barbie_Bathtub.STL", 8, 1200, 600, 840, 90, 0, 0);
        
        // Wall between stairs at main door
        translate([0, 130, 0])
        {
          cube([85, 19, 80]);
        } // end translate
        
        // Hole
        
        translate([20, 150, 0])
        {
          rotate([90, 0, 0])
          {
            cylinder([5, 8]);
          } // end rotate
        } // end translate
        
      } // end translate
      
      // Inline code start
      // Stair sides
      translate([234,100,20])
      stair_side(-90,180,0);
      translate([234,12,20])
      stair_side(-90,180,0);
      
      // Inline code end
      
      
      
      
    } // end module// Extra
  } // end union
  
  // Remove items below
  // TODO Special for remove
  {
    translate([0, 150, 0])
    {
      cube([85, 19, 80]);
    } // end translate
  } // end remove
} // end difference


module ground_floor()
{
  outer_wall_ground_floor();
  antons_room();
  cleaning_closet();
  hall();
  living_room_ground_floor();
  linas_room();
  hallway();
  toilet_ground_floor();
  kitchen();
  bedroom();
  balcony();
}
module antons_room()
{
  x = anton_x;
  y = anton_y;
  translate([-wall_half_width_scaled,house_width_scaled-scale(y), scale(0)])
  {
    sumUp()
    {
      add()
        room(x-unscale(wall_half_width_scaled), y-unscale(wall_half_width_scaled), true);
      door("S", 2750, y, 730, "Left",-5);
      remove()
      {
        window("N", y, 1840, 880, 1330, window_hight_ground_floor);
      }
    }
  }
}
module linas_room()
{
  x = linas_x;
  y = linas_y;
  translate([scale(0),scale(0), scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      door("E", 2270, x, 730, "Left");
      remove()
      {
        window("W", x, 1010, 990, 1350, window_hight_ground_floor);
      }
    }
  }
}
module hallway()
{
  x = hallway_x;
  y = hallway_y;
  translate([scale(linas_x)+2*wall_half_width_scaled,scale(toilet_ground_floor_y)+2*wall_half_width_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      chimney();
      door("N", 50, y, 750);
      door("S", 80, y, 630);
      door("W", 610, x, 730);
    }
  }
}
module cleaning_closet()
{
  x = 520;
  y = anton_y-unscale(wall_half_width_scaled);
  translate([scale(anton_x),house_width_scaled-scale(y)-wall_half_width_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y,true);
      door("N", 50, y, 750);
      door("S", 100, y, 530);
      door("W", 610, x, 730);
    }
  }
}
module chimney()
{
  x = 400;
  y = hallway_y-2*unscale(wall_half_width_scaled);
  translate([scale(hallway_x-x)-wall_half_width_scaled,scale(0), scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y, true);
    }
  }
}
module toilet_ground_floor()
{
  x = hallway_x;
  y = toilet_ground_floor_y;
  translate([scale(linas_x)+2*wall_half_width_scaled,scale(0), scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      door("N", 80, y, 630, "Left");
      remove()
      {
        window("S", y, 700, 1460, 670, 570);
      }
    }
  }
}
module kitchen()
{
  x = 2580;
  y = linas_y;
  translate([scale(linas_x+hallway_x)+4*wall_half_width_scaled,scale(0), scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      door("N", 680, y, 730);
      remove()
      {
        window("S", y, 610, 870, 1140, window_hight_ground_floor);
      }
    }
  }
}
module hall()
{
  x = unscale(house_length_scaled)-living_room_ground_floor_x-4*unscale(wall_half_width_scaled);
  y = unscale(house_width_scaled)-linas_y-2*unscale(wall_half_width_scaled);
  translate([scale(0),scale(linas_y)+2*wall_half_width_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      door("W", 200, x, 900);
      door("S", 3300, y, 730);
      door("S", 5300, y, 730);
      door("E", 600, x, 730);
      remove()
      {
        window("N", y, 4710, 960, 1340, window_hight_ground_floor);
      }
    }
  }
}
module bedroom()
{
  x = 3350;
  y = linas_y;
  translate([house_length_scaled-scale(x)-2*wall_half_width_scaled,scale(0), scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      door("N", 380, y, 900);
      remove()
      {
        window("S", y, 350, 860, 1430, window_hight_ground_floor);
      }
    }
  }
}
module living_room_ground_floor()
{
  x = living_room_ground_floor_x;
  y = living_room_ground_floor_y;
  translate([house_length_scaled-scale(x)-2*wall_half_width_scaled,scale(linas_y)+2*wall_half_width_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      door("N", 710, y, 700);
      door("W", 370, x, 1440);
      door("S", 1070, y, 900);
      remove()
      {
        window("N", y, 1490, 830, 1360, 1270);
      }
    }
  }
}
module basement()
{
  outer_wall();
  wine_cellar();
  living_room();
  toilet();
  stair_closet();
  laundry();
  bathroom();
  outside_garage();
  garage();
  middle_room();
  computer_room();
  basement_stairs();
}
module add_stl_scaled(filname, zoom , x, y, z = floor_thickness_scaled, rotate_x=0, rotate_y=0, rotate_z=0)
{
  zoom_scaled=scale(zoom);
  translate([scale(x),scale(y), scale(z)])
    scale([zoom_scaled, zoom_scaled, zoom_scaled])
      rotate([rotate_x, rotate_y, rotate_z])
      {
        if(add_furniture)
        {
          import(filname, convexity=3);
        }
      }
}
module outer_wall()
{
  x = unscale(house_length_scaled);
  y = unscale(house_width_scaled+2*wall_half_width_scaled);
  translate([-wall_half_width_scaled,-wall_half_width_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y,extra_height_scaled=-wall_extra_height_scaled);
      remove()
      {
        // Living room
        window("N", y, 2260, 1620, 750, 460);
        window("N", y, 5240, 1620, 760, 460);
        // Bath room
        window("S", y, 5480, 1660, 790, 470);
        // Landry room
        window("S", y, 2270, 1700, 800, 430);
        window("W", x, 1100, 1700, 800, 450);
        // Outside garage
        translate([6*wall_half_width_scaled+scale(laundry_x+bathroom_x),scale(0)-overlap_scaled, floor_thickness_scaled])
          cube([scale(garage_x), wall_half_width_scaled+2*overlap_scaled, wall_heigth_scaled+overlap_scaled]);
      }
      // Computer room
      door("N", 7650, y, 830);
    }
  }
}
module outer_wall_ground_floor()
{
  x = unscale(house_length_scaled);
  y = unscale(house_width_scaled+2*wall_half_width_scaled);
  translate([-wall_half_width_scaled,-wall_half_width_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y,extra_height_scaled=-wall_extra_height_scaled);
      door("N", 8370, y, 750);
      remove()
      {
        // Make edges in the bottom
        translate([wall_half_width_scaled,wall_half_width_scaled, scale(0)-overlap_scaled])
        {
          // Under main area
          cube([scale(x)-2*wall_half_width_scaled, scale(y)-2*wall_half_width_scaled, wall_extra_height_scaled+overlap_scaled]);
          // Under balcony
          translate([scale(x-balcony_x) ,scale(y)-3*wall_half_width_scaled, -overlap_scaled])
          {
            cube([scale(balcony_x)-2*wall_half_width_scaled,  5*wall_half_width_scaled+ overlap_scaled, wall_extra_height_scaled+overlap_scaled]);
          }
        }
        // Linas room
         window("W", y, 1110, 990, 1350, 1230);
        // Antons rum
        #window("N", y, 2000, 1340, 930, 540);
        // Bathroom
        #window("S", y, 3290, 1660, 790, 470);
        // Kitchen
        #window("S", y, 5890, 1660, 790, 470);
        // Bedroom
        #window("S", y, 8270, 1700, 800, 430);
        // Hall
        #window("N", y, 4500, 1340, 930, 540);
        // Living room
        #window("N", y, 9970, 1700, 800, 430);


      }
      // Computer room
      //door("N", 7100, y, 750);
    }
  }
}
module basement_stairs()
{
  x = wine_x;
  y = basement_stairs_y;
  translate([scale(0),scale(laundry_y+stair_closet_y)+3*wall_half_width_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      remove()
      {
        // Wall where stair comes down
        translate([scale(x)+wall_half_width_scaled-overlap_scaled, wall_half_width_scaled,floor_thickness_scaled])
          cube([4*wall_half_width_scaled+2*overlap_scaled, scale(y), wall_heigth_scaled+2*overlap_scaled]);
      }
      addAfterRemoving()
      {
        for ( i = [0 : 8] )
        {
          stair(i*200,i*190);
        }
        // Small floor inside main door
        translate([scale(95), scale(-930),floor_thickness_scaled+scale(1600)])
          cube([scale(845), scale(1960), scale(150)]);
        translate([scale(3180), scale(-50),floor_thickness_scaled+scale(1540)])
          rotate([0, 0, 180])
          {
            // Four upper stairs
            for ( i = [0 : 2] )
            {
              stair(i*200,i*190);
            }
          }
          // Stair sides
          translate([scale(2340),scale(1000), scale(200)])
            stair_side(-90,180,0);
          translate([scale(2340),scale(120), scale(200)])
            stair_side(-90,180,0);
      }
      remove()
      {
        // Wall between stairs at main door
        translate([scale(95), scale(-50),floor_thickness_scaled+scale(1750)])
          cube([scale(845), scale(200), scale(900)]);
      }
    }
  }
}
module stair(trans_y, trans_z)
{
  zoom=scale(1);
  translate([scale(2080-trans_y),scale(560), scale(400+trans_z)])
    scale([zoom,zoom,zoom])
      rotate([90, 0, 0])
        linear_extrude(height =  840,  center = true)
          polygon(stair_points);
}
module stair_side(rotate_x=0,rotate_y=0,rotate_z=0)
{
  zoom=scale(1);
  scale([zoom,zoom,zoom])
    rotate([rotate_x, rotate_y, rotate_z])
      linear_extrude(height =  40,  center = true)
        polygon(stair_side_points);
}
module computer_room()
{
  x = 3970;
  y = 2140;
  translate([scale(laundry_x+bathroom_x+garage_x-x)+4*wall_half_width_scaled,scale(garage_y+middle_room_y+outside_garage_y)+3*wall_half_width_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      remove()
      {
        window("N", y, 700, 1500, 920, 540);
        window("N", y, 2290, 1500, 920, 540);
      }
      door("S", 320, y, 750);
      add()
      {
        // Outer walls
        //East
        translate([-wall_half_width_scaled, scale(0),scale(0)])
          cube([wall_half_width_scaled, scale(y)+3*wall_half_width_scaled, wall_heigth_scaled- wall_extra_height_scaled+floor_thickness_scaled]);
        // North
        translate([scale(0), scale(y)+2*wall_half_width_scaled,scale(0)])
          cube([scale(x)+3*wall_half_width_scaled,wall_half_width_scaled,  wall_heigth_scaled- wall_extra_height_scaled+floor_thickness_scaled]);
        // West
        translate([scale(x)+2*wall_half_width_scaled, scale(0),scale(0)])
          cube([wall_half_width_scaled, scale(y)+3*wall_half_width_scaled, wall_heigth_scaled- wall_extra_height_scaled+floor_thickness_scaled]);
      }
      remove()
      {
        // North outer wall windows
        window("N", y+unscale(wall_half_width_scaled), 700, 1500, 920, 540);
        window("N", y+unscale(wall_half_width_scaled), 2290, 1500, 920, 540);
      }
    }
  }
}
module balcony()
{
  x = balcony_x;
  y = balcony_y;
  translate([scale(laundry_x+bathroom_x+garage_x-x)+4*wall_half_width_scaled,scale(garage_y+middle_room_y+outside_garage_y)+3*wall_half_width_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      //        remove()
      //        {
      // Under balcony
      //     translate([2*wall_half_width_scaled,-2*wall_half_width_scaled, -overlap_scaled])
      //  {
      //   cube([scale(balcony_x)-2*wall_half_width_scaled, scale(balcony_y)+ 2*wall_half_width_scaled+ overlap_scaled, wall_extra_height_scaled+overlap_scaled]);
      // }
      //        }
      door("S", 1000, y, 750, "Left");
      add()
      {
        // Outer walls
        //East
        translate([-wall_half_width_scaled, scale(0),scale(0)])
          cube([wall_half_width_scaled, scale(y)+3*wall_half_width_scaled, wall_heigth_scaled- wall_extra_height_scaled+floor_thickness_scaled]);
        // North
        translate([scale(0), scale(y)+2*wall_half_width_scaled,scale(0)])
          cube([scale(x)+3*wall_half_width_scaled,wall_half_width_scaled,  wall_heigth_scaled- wall_extra_height_scaled+floor_thickness_scaled]);
        // West
        translate([scale(x)+2*wall_half_width_scaled, scale(0),scale(0)])
          cube([wall_half_width_scaled, scale(y)+3*wall_half_width_scaled, wall_heigth_scaled- wall_extra_height_scaled+floor_thickness_scaled]);
      }
      //        remove()
      //       {
      // North outer wall windows
      // window("N", y+unscale(wall_half_width_scaled), 700, 1500, 920, 540);
      // window("N", y+unscale(wall_half_width_scaled), 2290, 1500, 920, 540);
      //     }
    }
  }
}
module middle_room()
{
  x = garage_x;
  y = middle_room_y;
  translate([scale(laundry_x+bathroom_x)+4*wall_half_width_scaled,scale(garage_y+outside_garage_y)+2*wall_half_width_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      door("N", 0, y, 830,"Right");
      door("S", 80, y, 730);
      door("W", 1030, x, 800);
    }
  }
}
module garage()
{
  x = garage_x;
  y = garage_y;
  translate([scale(laundry_x+bathroom_x)+4*wall_half_width_scaled,scale(outside_garage_y), scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      remove()
      {
        window("S", y, 1510, 1340, 930, 540);
        window("S", y, 310, 1340, 930, 540);
      }
      door("N", 80, y, 750,"Right");
    }
  }
}
module outside_garage()
{
  x = garage_x;
  y = outside_garage_y;
  translate([scale(laundry_x+bathroom_x)+4*wall_half_width_scaled,scale(0)-overlap_scaled, scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      remove()
      {
        window("N", y, 1510, 1340, 930, 540);
        window("N", y, 310, 1340, 930, 540);
        translate([1*wall_half_width_scaled,scale(0)-overlap_scaled, floor_thickness_scaled])
          cube([scale(garage_x), wall_half_width_scaled+2*overlap_scaled, wall_heigth_scaled+overlap_scaled]);
      }
    }
  }
}
module bathroom()
{
  x = bathroom_x;
  y = laundry_y;
  translate([scale(laundry_x)+2*wall_half_width_scaled,scale(0), scale(0)])
  {
    sumUp()
    {
      add()
      {
        room(x, y);
        // Chimney
        translate([scale(0),scale(laundry_y-1600), scale(0)])
          room(240, 1600);
        // Bathtub
        add_stl_scaled("Barbie_Bathtub.STL", 8, 1200, 600, 840,90,0,0);
        // Shower
        add_stl_scaled("shower.stl", 1, 3645, 3030, 300,0,0,-90);
        // Wicker chair
        add_stl_scaled("wicker chair.stl", 1, 3120, 860, 300,0,0,-135);
      }
      remove()
      {
        window("S", y, 1350, 1660, 790, 470);
      }
      door("N", 550, y, 830);
    }
  }
}
module laundry()
{
  x = laundry_x;
  y = laundry_y;
  translate([scale(0),scale(0), scale(0)])
  {
    sumUp()
    {
      add()
      {
        room(x, y);
        // Washing machine
        add_stl_scaled("washing machine.stl", 15.5, 150, 3200, 300);
        add_stl_scaled("Lid washing machine.stl",15.5, 150, 3200, 300);
        // Dryer
        add_stl_scaled("washing machine.stl", 15.5, 150, 3200, 1180);
        add_stl_scaled("Lid washing machine.stl",15.5, 150, 3200, 1180);
        // Sink
        add_stl_scaled("sink.stl", 1, 3000, -3100, 840,0,0,40);
      }
      remove()
      {
        window("S", y, 2170, 1700, 800, 430);
        window("W", x, 1000, 1700, 800, 450);
      }
      door("N", 2300, y, 830);
    }
  }
}
module toilet()
{
  x = 1710;
  y =  toilet_y;
  translate([scale(wine_x+living_x-x)+wall_half_width_scaled,house_width_scaled-scale(living_y)-wall_half_width_scaled+scale(51), scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y, true);
      // Door opening
      remove()
        translate([scale(toilet_door_x)+2*wall_half_width_scaled, scale(y)+wall_half_width_scaled, floor_thickness_scaled])
          cube([scale(660), wall_half_width_scaled*3+overlap_scaled, door_height_scaled]);
    }
  }
}
module stair_closet()
{
  x = wine_x;
  y = stair_closet_y;
  translate([scale(0),house_width_scaled-scale(living_y), scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      remove()
      {
        // Wall between stairs at main door
        translate([scale(95), scale(y-20)+wall_half_width_scaled,floor_thickness_scaled+scale(1740)])
          cube([scale(845), scale(190), scale(800)]);
      }
      door("E", 270, x, 630);
    }
  }
}
module living_room()
{
  x = living_x;
  y =  living_y;
  translate([scale(wine_x)+2*wall_half_width_scaled,house_width_scaled-scale(y), scale(0)])
  {
    sumUp()
    {
      add()
        room(x, y);
      remove()
      {
        window("N", y, 220, 1620, 750, 460);
        window("N", y, 3180, 1620, 760, 460);
        // Stairs wall
        translate([scale(0)-overlap_scaled, scale(stair_closet_y)+2*wall_half_width_scaled+scale(20),floor_thickness_scaled])
          cube([wall_half_width_scaled+2*overlap_scaled, scale(basement_stairs_y)+0.6*wall_half_width_scaled, wall_heigth_scaled+2*overlap_scaled]);
      }
      door("E", 3350, x, 800);
      door("S", 360,  y, 830, "Right",-20);
      door("S", 2640, y, 830, "Left",-10);
      door("W", 2900, x, 630, "Right");
      door("W", 280, x, 630, "Left", -60);
      translate([scale(x-toilet_x)-4*wall_half_width_scaled, scale(toilet_y)+2*wall_half_width_scaled, scale(0)])
        door("S", toilet_door_x+unscale(2*wall_half_width_scaled), y-toilet_y, 750, "Left");
    }
  }
}
module wine_cellar()
{
  x = wine_x;
  y = 2000;
  translate([scale(0),house_width_scaled-scale(y), scale(0)])
  {
    sumUp()
    {
      add()
      {
        room(x, y);
        // Wine shelfs
        add_stl_scaled("wine shelf.stl", 1, 660, 70, 300,0,0,90);
      }
      door("E", 680, x, 630);
    }
  }
}
module window(direction, xy=0, trans_xy, trans_z=window_above_floor_scaled,width = window_width_scaled, height = window_heigth_scaled)
{
  if (direction == "E")
  {
    translate([scale(xy)+wall_half_width_scaled-overlap_scaled, scale(trans_xy)+wall_half_width_scaled, floor_thickness_scaled+ scale(trans_z)])
      cube([wall_half_width_scaled+2*overlap_scaled, scale(width), scale(height)]);
  }
  if (direction == "W")
  {
    translate([-overlap_scaled, scale(trans_xy)+wall_half_width_scaled, floor_thickness_scaled+ scale(trans_z)])
      cube([wall_half_width_scaled+2*overlap_scaled, scale(width), scale(height)]);
  }
  if (direction == "S")
  {
    translate([scale(trans_xy)+wall_half_width_scaled, -2*wall_half_width_scaled, floor_thickness_scaled+ scale(trans_z)])
      cube([scale(width), 4*wall_half_width_scaled, scale(height)]);
  }
  if (direction == "N")
  {
    translate([scale(trans_xy)+wall_half_width_scaled, scale(xy)+wall_half_width_scaled-overlap_scaled, floor_thickness_scaled+ scale(trans_z)])
      cube([scale(width), wall_half_width_scaled+2*overlap_scaled, scale(height)]);
  }
}
module door(direction , distance, xy=0, width = door_width, hung = "NoDoor", angle = door_degrees_open)
{
  remove()
    door_opening(direction , distance, xy, width);
  addAfterRemoving()
    open_door(direction , distance, xy, width, hung, angle);
}
module open_door(direction , distance, xy=0, width = door_width, hung = "NoDoor", angle = door_degrees_open)
{
  if (direction == "E")
  {
    if (hung == "Left")
    {
      // {{{{{{{{{{{{
                        /*
                                   {{{{{{{{{{{{ */
      translate([scale(xy)+wall_half_width_scaled*2-scale(width), scale(distance+width)+wall_half_width_scaled*2, floor_thickness_scaled])
        rotate([0,0,-90-angle])
          cube([wall_half_width_scaled, scale(width), door_height_scaled]);
    }
    if (hung == "Right")
    {
      translate([scale(xy-width)+wall_half_width_scaled*2, scale(distance)+wall_half_width_scaled, floor_thickness_scaled])
        rotate([0,0,-90+angle])
          cube([wall_half_width_scaled, scale(width), door_height_scaled]);
    }
  }
  if (direction == "W")
  {
    if (hung == "Left")
    {
      translate([scale(0), scale(distance)+wall_half_width_scaled, floor_thickness_scaled])
        rotate([0,0,-90-angle])
          cube([wall_half_width_scaled, scale(width), door_height_scaled]);
    }
    if (hung == "Right")
    {
      translate([-wall_half_width_scaled, scale(distance+width)+wall_half_width_scaled, floor_thickness_scaled])
        rotate([0,0,-90+angle])
          cube([wall_half_width_scaled, scale(width), door_height_scaled]);
    }
  }
  if (direction == "S")
  {
    if (hung == "Left")
    {
      translate([scale(distance+width)+wall_half_width_scaled,scale(0), floor_thickness_scaled])
        rotate([0,0,0-angle])
          cube([wall_half_width_scaled, scale(width), door_height_scaled]);
    }
    if (hung == "Right")
    {
      translate([scale(distance)+wall_half_width_scaled, scale(0), floor_thickness_scaled])
        rotate([0,0,angle])
          cube([wall_half_width_scaled, scale(width), door_height_scaled]);
    }
  }
  if (direction == "N")
  {
    if (hung == "Left")
    {
      translate([scale(distance)+wall_half_width_scaled*2, scale(xy)++wall_half_width_scaled*2, floor_thickness_scaled])
        rotate([0,0,180-angle])
          cube([wall_half_width_scaled, scale(width), door_height_scaled]);
    }
    if (hung == "Right")
    {
      translate([scale(distance+width)+wall_half_width_scaled, scale(xy)+wall_half_width_scaled*2, floor_thickness_scaled])
        rotate([0,0,180+angle])
          cube([wall_half_width_scaled, scale(width), door_height_scaled]);
    }
  }
}
module door_opening(direction , distance, xy=0, width = door_width)
{
  if (direction == "E")
  {
    translate([scale(xy)+wall_half_width_scaled-overlap_scaled, scale(distance)+wall_half_width_scaled, floor_thickness_scaled])
      cube([wall_half_width_scaled+2*overlap_scaled, scale(width), door_height_scaled]);
  }
  if (direction == "W")
  {
    translate([-overlap_scaled, scale(distance)+wall_half_width_scaled, floor_thickness_scaled])
      cube([wall_half_width_scaled+2*overlap_scaled, scale(width), door_height_scaled]);
  }
  if (direction == "S")
  {
    translate([scale(distance)+wall_half_width_scaled, -overlap_scaled, floor_thickness_scaled])
      cube([scale(width), wall_half_width_scaled+2*overlap_scaled, door_height_scaled]);
  }
  if (direction == "N")
  {
    translate([scale(distance)+wall_half_width_scaled, scale(xy)+wall_half_width_scaled-overlap_scaled, floor_thickness_scaled])
      cube([scale(width), wall_half_width_scaled+2*overlap_scaled, door_height_scaled]);
  }
}
module room(x , y, doubleThickWalls = false, extra_height_scaled=0 )
{
  if(doubleThickWalls==false)
  {
    difference()
    {
      cube([scale(x)+wall_half_width_scaled*2, scale(y)+wall_half_width_scaled*2, wall_heigth_scaled+floor_thickness_scaled+extra_height_scaled]);
      
      translate([wall_half_width_scaled, wall_half_width_scaled, floor_thickness_scaled])
        cube([scale(x), scale(y), wall_heigth_scaled+extra_height_scaled+overlap_scaled]);
    }
  }
  else
  {
    difference()
    {
      cube([scale(x)+wall_half_width_scaled*4, scale(y)+wall_half_width_scaled*4, wall_heigth_scaled+floor_thickness_scaled+extra_height_scaled]);
      translate([wall_half_width_scaled*2, wall_half_width_scaled*2, floor_thickness_scaled])
        cube([scale(x), scale(y), wall_heigth_scaled+extra_height_scaled+overlap]);
    }
  }
}
