// Generated 2024-01-04 01:21 by CubePuzzler
PUZZLE_PIXELS = 4;
CUBE_THICKNESS = 5;
NONE = -1;
XPLUS = 0;
YMINUS = 1;
YPLUS = 2;
XMINUS = 3;
ZPLUS = 4;
ZMINUS = 5;

Position = [NONE, XMINUS, YMINUS, ZPLUS, YMINUS, XPLUS, ZMINUS, XMINUS, ZMINUS, ZMINUS];
SizesArray = [[1,4,2], [3,3,3], [2,2,2], [2,2,2], [3,1,3], [2,3,1], [1,4,3], [4,2,1], [2,2,2], [2,2,2]];

StartPosition = [[0,0,2], [1,1,0], [1,2,2], [2,0,1], [0,0,1], [2,0,3], [0,0,0], [0,0,0], [2147483647,2147483647,2147483647], [2147483647,2147483647,2147483647]];

show_all_cubescube();

module piece_1()
{
   // NONE
   // Dimensions = [1,4,2]
   // StartPixel = [0,0,2]
   color("GREEN")
   {
      write_pixel(0, 2, 3);
      write_pixel(0, 1, 3);
      write_pixel(0, 3, 3);
      write_pixel(0, 0, 3);
      write_pixel(0, 1, 2);
   }
}

module piece_2()
{
   // XMINUS
   // Dimensions = [3,3,3]
   // StartPixel = [1,1,0]
   color("YELLOW")
   {
      write_pixel(3, 3, 1);
      write_pixel(3, 2, 1);
      write_pixel(2, 2, 1);
      write_pixel(2, 1, 1);
      write_pixel(3, 3, 0);
      write_pixel(1, 1, 1);
      write_pixel(2, 2, 0);
      write_pixel(1, 2, 1);
      write_pixel(3, 2, 2);
      write_pixel(1, 3, 1);
      write_pixel(1, 2, 0);
   }
}

module piece_3()
{
   // YMINUS
   // Dimensions = [2,2,2]
   // StartPixel = [1,2,2]
   color("BLACK")
   {
      write_pixel(1, 3, 3);
      write_pixel(1, 3, 2);
      write_pixel(2, 3, 3);
      write_pixel(1, 2, 2);
   }
}

module piece_4()
{
   // ZPLUS
   // Dimensions = [2,2,2]
   // StartPixel = [2,0,1]
   color("PINK")
   {
      write_pixel(3, 0, 1);
      write_pixel(2, 0, 1);
      write_pixel(2, 0, 2);
      write_pixel(3, 1, 1);
   }
}

module piece_5()
{
   // YMINUS
   // Dimensions = [3,1,3]
   // StartPixel = [0,0,1]
   color("ORANGE")
   {
      write_pixel(1, 0, 3);
      write_pixel(2, 0, 3);
      write_pixel(1, 0, 2);
      write_pixel(1, 0, 1);
      write_pixel(0, 0, 2);
   }
}

module piece_6()
{
   // XPLUS
   // Dimensions = [2,3,1]
   // StartPixel = [2,0,3]
   color("BLUE")
   {
      write_pixel(2, 1, 3);
      write_pixel(3, 1, 3);
      write_pixel(3, 0, 3);
      write_pixel(3, 2, 3);
   }
}

module piece_7()
{
   // ZMINUS
   // Dimensions = [1,4,3]
   // StartPixel = [0,0,0]
   color("RED")
   {
      write_pixel(0, 3, 2);
      write_pixel(0, 3, 1);
      write_pixel(0, 2, 2);
      write_pixel(0, 2, 1);
      write_pixel(0, 1, 1);
      write_pixel(0, 0, 1);
      write_pixel(0, 2, 0);
   }
}

module piece_8()
{
   // XMINUS
   // Dimensions = [4,2,1]
   // StartPixel = [0,0,0]
   color("GRAY")
   {
      write_pixel(3, 0, 0);
      write_pixel(2, 0, 0);
      write_pixel(1, 0, 0);
      write_pixel(0, 0, 0);
      write_pixel(0, 1, 0);
   }
}

module piece_9()
{
   // ZMINUS
   // Dimensions = [2,2,2]
   // StartPixel = [2147483647,2147483647,2147483647]
   color("BROWN")
   {
   }
}

module piece_10()
{
   // ZMINUS
   // Dimensions = [2,2,2]
   // StartPixel = [2147483647,2147483647,2147483647]
   color("WHITE")
   {
   }
}


module show_all_cubescube()
{
  for(i=[0:10])
    translate([0, 12 * i * CUBE_THICKNESS,0])
      show_cube(i, Position[i], SizesArray[i],, StartPosition[i])
      {
        piece_1();
        piece_2();
        piece_3();
        piece_4();
        piece_5();
        piece_6();
        piece_7();
        piece_8();
        piece_9();
        piece_10();
      }
}

module show_cube(id, position, sizes, startPixel)
{
  if (id >= 1)
  {
    for(i=[0:id-1])
      children(i);
  }

  if(position == NONE)
  {
    children(id);
  }

  if(position == XPLUS)
  {
    translate([(PUZZLE_PIXELS-sizes[0]+1) * CUBE_THICKNESS, (0) * CUBE_THICKNESS, (0) * CUBE_THICKNESS])
      children(id);

    translate([(PUZZLE_PIXELS-sizes[0]+7) * CUBE_THICKNESS, (startPixel[1]+sizes[1]/2) * CUBE_THICKNESS, (startPixel[2]+sizes[2]/2) * CUBE_THICKNESS])
      rotate([0,-90,0])
        arrow();
  }

  if(position == XMINUS)
  {
    translate([(PUZZLE_PIXELS-sizes[0]-5) * CUBE_THICKNESS, (0) * CUBE_THICKNESS, (0) * CUBE_THICKNESS])
      children(id);

    translate([(PUZZLE_PIXELS-sizes[0]-7) * CUBE_THICKNESS, (startPixel[1]+sizes[1]/2) * CUBE_THICKNESS, (startPixel[2]+sizes[2]/2) * CUBE_THICKNESS])
      rotate([0,90,0])
        arrow();
  }

  if(position == YPLUS)
  {
    translate([(0) * CUBE_THICKNESS, (sizes[1]+1) * CUBE_THICKNESS, (0) * CUBE_THICKNESS])
      children(id);

    translate([(startPixel[0]+sizes[0]/2) * CUBE_THICKNESS, (sizes[1]+3) * CUBE_THICKNESS, (startPixel[2]+sizes[2]/2) * CUBE_THICKNESS])
      rotate([90,0,0])
        arrow();
  }

  if(position == YMINUS)
  {
    translate([(0) * CUBE_THICKNESS, (PUZZLE_PIXELS-sizes[1]-5) * CUBE_THICKNESS, (0) * CUBE_THICKNESS])
      children(id);

    translate([(startPixel[0]+sizes[0]/2) * CUBE_THICKNESS, (PUZZLE_PIXELS-sizes[1]-7) * CUBE_THICKNESS, (startPixel[2]+sizes[2]/2) * CUBE_THICKNESS])
      rotate([-90,0,0])
        arrow();
  }

  if(position == ZPLUS)
  {
    translate([(0) * CUBE_THICKNESS, (0) * CUBE_THICKNESS, (sizes[2]+1) * CUBE_THICKNESS])
      children(id);

    translate([(startPixel[0]+sizes[0]/2) * CUBE_THICKNESS, (startPixel[1]+sizes[1]/2) * CUBE_THICKNESS, (startPixel[2] + 3) * CUBE_THICKNESS])
      rotate([0,180,0])
        arrow();
  }

  if(position == ZMINUS)
  {
    translate([(0) * CUBE_THICKNESS, (0) * CUBE_THICKNESS, (PUZZLE_PIXELS - sizes[2]-5) * CUBE_THICKNESS])
      children(id);

    translate([(startPixel[0]+sizes[0]/2) * CUBE_THICKNESS, (startPixel[1]+sizes[1]/2) * CUBE_THICKNESS, (PUZZLE_PIXELS - sizes[2]-7) * CUBE_THICKNESS])
      rotate([0,0,0])
        arrow();
  }
}

module arrow()
{
  cylinder($fn=64, d=1.5,h=3);

  translate([0,0,3])
    cylinder($fn=64, r1=1.5, r2=0 ,h=2);
}

module write_pixel(x, y, z)
{
   translate([x * CUBE_THICKNESS , y * CUBE_THICKNESS , z * CUBE_THICKNESS])
      cube([CUBE_THICKNESS, CUBE_THICKNESS, CUBE_THICKNESS]);
}

