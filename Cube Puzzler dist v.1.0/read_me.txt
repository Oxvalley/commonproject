How to use Cube Puzzler v. 1.0
========================================

1. Install OpenScad:
https://openscad.org/downloads.html

2. Install java8:
https://developers.redhat.com/products/openjdk/download


How to run Cube Puzzler
-----------------------

It can be started in many ways:

From Linux:
bash "run Cube Puzzler v. 1.0.sh" 

From Windows 1:
Double-click "run Cube Puzzler v. 1.0.exe"

From Windows 2:
Double-click "Cube Puzzler v. 1.0.jar"

From Windows 3:
Double-click "run Cube Puzzler v. 1.0.cmd"
(If it do not start, open the file in a text editor and add path to your java8 
installation as value to JAVA_HOME variable in line 1. End path with a forward 
slash (/) )


The application wants to know 3 things:
1) The number of small cubes on each side
2) The total width in millimeter of any side of the total cube.
3) The number of solutions to choose from.

Each created solution is compared by these demands:

* If two pieces look the same, the solution is removed. 
(Pieces can be turned in 8 ways. All are tested to find a match.)

* The more sides that look like any other side on any of the six pieces,
the more certain that solution will be selected.

The solution is created as a file:
cube_puzzel_<Date & Time>.scad

If Openscad is installed, OpenScad will be opened with this file.

* Hit F5 to see the solution (with intentionally mismatched pieces) and the pieces.

* To just see the pieces, change:
SHOW_SOLUTION=true;
to
SHOW_SOLUTION=false;

* Hit F5 again to see the pieces.

To create a stl-file
--------------------

* Hit F6
* Wait for about 20 seconds until a sound is played and the pieces are shown
* Use menu File -> Export to STL, or the toolbar icon marked "STL" to give the STL a name.

Use the STL in your normal printing software.








