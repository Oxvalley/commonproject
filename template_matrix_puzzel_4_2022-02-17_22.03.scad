// Generated 2022-02-17 22:03 by CubePuzzler
CUBE_THICKNESS = 5;
NONE = -1;
XPLUS = 0;
YMINUS = 1;
YPLUS = 2;
XMINUS = 3;
ZPLUS = 4;
ZMINUS = 5;

Position = [NONE, ZMINUS, YMINUS, XPLUS, XMINUS, XPLUS, XPLUS, YMINUS, YMINUS, XPLUS];
SizesArray = [[2,2,1], [2,1, 3], [2,1, 3], [2,1, 3], [2,1, 3], [2,1, 3], [2,1, 3], [2,1, 3], [2,1, 3], [2,1, 3]];

show_all_cubescube();

module piece_1()
{
  // ZPLUS
  // Sizes = [2,2,1]
  color("GREEN")
  {
    write_pixel(3, 3, 3);
    write_pixel(3, 2, 3);
    write_pixel(3, 3, 2);
    write_pixel(2, 2, 3);
    write_pixel(2, 2, 2);
    write_pixel(3, 1, 3);
    write_pixel(1, 2, 3);
    write_pixel(3, 1, 2);
  }
}
module piece_2()
{
  // ZMINUS
  // Sizes = [2,2,1]
  color("YELLOW")
  {
    write_pixel(1, 0, 0);
    write_pixel(2, 0, 0);
    write_pixel(1, 1, 0);
    write_pixel(0, 0, 0);
    write_pixel(1, 1, 1);
    write_pixel(0, 0, 1);
    write_pixel(2, 1, 0);
    write_pixel(3, 0, 0);
  }
}
module piece_3()
{
  // YMINUS
  color("BLACK")
  {
    write_pixel(1, 0, 1);
    write_pixel(2, 0, 1);
    write_pixel(2, 1, 1);
    write_pixel(3, 0, 1);
    write_pixel(3, 0, 2);
    write_pixel(3, 0, 3);
    write_pixel(1, 0, 2);
    write_pixel(0, 0, 2);
  }
}
module piece_4()
{
  // XPLUS
  color("PINK")
  {
    write_pixel(2, 0, 2);
    write_pixel(2, 1, 2);
    write_pixel(2, 0, 3);
    write_pixel(2, 1, 3);
    write_pixel(1, 1, 2);
    write_pixel(1, 1, 3);
    write_pixel(0, 1, 2);
    write_pixel(0, 1, 3);
  }
}
module piece_5()
{
  // XMINUS
  color("ORANGE")
  {
    write_pixel(0, 2, 3);
    write_pixel(0, 3, 3);
    write_pixel(0, 2, 2);
    write_pixel(0, 2, 1);
    write_pixel(0, 2, 0);
    write_pixel(0, 3, 0);
    write_pixel(1, 2, 0);
    write_pixel(1, 3, 0);
  }
}
module piece_6()
{
  // XPLUS
  color("BLUE")
  {
    write_pixel(3, 3, 1);
    write_pixel(3, 3, 0);
    write_pixel(3, 2, 1);
    write_pixel(2, 3, 0);
    write_pixel(2, 3, 1);
    write_pixel(1, 3, 1);
    write_pixel(0, 3, 1);
    write_pixel(3, 1, 1);
  }
}
module piece_7()
{
  // XPLUS
  color("RED")
  {
    write_pixel(0, 1, 1);
  }
}
module piece_8()
{
  // YMINUS
  color("GRAY")
  {
    write_pixel(0, 3, 2);
  }
}
module piece_9()
{
  // YMINUS
  color("BROWN")
  {
    write_pixel(1, 2, 1);
    write_pixel(1, 2, 2);
    write_pixel(1, 3, 2);
    write_pixel(2, 3, 2);
    write_pixel(2, 3, 3);
    write_pixel(2, 2, 1);
    write_pixel(2, 2, 0);
  }
}
module piece_10()
{
  // XPLUS
  color("WHITE")
  {
    write_pixel(3, 1, 0);
    write_pixel(3, 2, 0);
  }
}

module show_all_cubescube()
{
  for(i=[0:10])
    translate([0, 12 * i * CUBE_THICKNESS,0])
      show_cube(i, Position[i], SizesArray[i])
      {
        piece_1();
        piece_2();
        piece_3();
        piece_4();
        piece_5();
        piece_6();
        piece_7();
        piece_8();
        piece_9();
        piece_10();
      }
}

module show_cube(id = 8, position = 0, Sizes)
{
  if (id >= 1)
  {
    for(i=[0:id-1])
      children(i);
  }

  if(position == NONE)
  {
    children(id);
  }

  if(position == XPLUS)
  {
    translate([Sizes[0] * CUBE_THICKNESS,0,0])
      children(id);

    translate([(Sizes[0]+3) * CUBE_THICKNESS,Sizes[1],Sizes[2]])
      rotate([0,0,0])
        arrow();
  }

  if(position == XMINUS)
  {
    translate([-Sizes[0] * CUBE_THICKNESS,0,0])
      children(id);

    translate([(-Sizes[0]-3) * CUBE_THICKNESS,0,0])
      rotate([0,0,0])
        arrow();
  }

  if(position == YPLUS)
  {
    translate([0, Sizes[0] * CUBE_THICKNESS,0])
      children(id);

    translate([0, (Sizes[0]+3) * CUBE_THICKNESS,0])
      rotate([0,0,0])
        arrow();
  }

  if(position == YMINUS)
  {
    translate([0, - Sizes[0] * CUBE_THICKNESS,0])
      children(id);

    translate([2* CUBE_THICKNESS, (-Sizes[0]-2) * CUBE_THICKNESS,2* CUBE_THICKNESS])
      rotate([-90,0,0])
        arrow();
  }

  if(position == ZPLUS)
  {
    translate([0, 0, Sizes[0] * CUBE_THICKNESS])
      children(id);

    translate([0, 0, (Sizes[0]+3) * CUBE_THICKNESS])
      rotate([0,0,0])
        arrow();
  }

  if(position == ZMINUS)
  {
    translate([0, 0, -Sizes[0] * CUBE_THICKNESS])
      children(id);

    translate([2* CUBE_THICKNESS,1* CUBE_THICKNESS,(-Sizes[0]-2) * CUBE_THICKNESS])
      rotate([0,0,0])
        arrow();
  }
}

module arrow()
{
  cylinder($fn=64, d=1.5,h=3);

  translate([0,0,3])
    cylinder($fn=64, r1=1.5, r2=0 ,h=2);
}

module write_pixel(x, y, z)
{
  translate([x * CUBE_THICKNESS , y * CUBE_THICKNESS , z * CUBE_THICKNESS])
    cube([CUBE_THICKNESS, CUBE_THICKNESS, CUBE_THICKNESS]);
}


