include <../oxvalley.scad>;

// Cogwheels
// 6 mars 2021
// 11:54

function apart(cog_count_1,cog_count_2)=round_half((cog_count_1+cog_count_2)*1.22);

function cog_count(apart, cog_count_2)=round((apart/1.22)-cog_count_2);

function round_half(value)=round(value*2)/2;

// Distance in mm between two cog wheels with number of tags
* echo(apart(16,30));

// Number of cogs on wheel 2 when distance in mm and cog count og wheel 1
* echo(cog_count(58,17));

*pin(40);

*pin_hole_test();

*bottom();

*bottom_test();

*holder("A3", 70, 0, 21, 32, 41, 50);
*holder("A4", 130, 0, 21, 32, 41, 50, 71, 82, 91, 100);

*wheel(31);

*pin_nob();

*pin_distance();

*hair_clip();

rotate([0,180,0])
  *thread_spool();

*hook();

rotate([180,0,0])
  *crank();

//crank_test();

*crank2();

model1();

module holderA3()
{
  holder("A3", 70, 0, 21, 32, 41, 50);
}

module holderA4()
{
  holder("A4", 130, 0, 21, 32, 41, 50, 71, 82, 91, 100);
}

module model1()
{
  bottom(); // Always level 0

  // Level 0
  place_pin(0,1,0);
  place_pin(1,1,0);
  place_pin(2,1,0);
  place_holder_a4(0,1,0);
  place_wheel(x=0, y=1 ,x_in=94, cogs=8);
  place_wheels(x=0, y=1, cogs1=24, cogs2=17, x_in=53, pin_out=14,,rotate_degrees1=7);

  translate([0,0,190])
    *cube([17,17,17]);

  // Level 1
  place_pin(0,1,1);
  place_pin(1,1,1);
  place_holder_a3(0,1,1);
  place_wheel(x=0, y=1,level=1 ,cogs=30, x_in=53, pin_out=14,front_side=false);

  // Level 2
  place_pin(0,1,2);
  place_pin(1,1,2);
  place_holder_a3(0,1,2);
  place_wheel(x=0, y=1,level=2 ,cogs=17,rotate_degrees=10, x_in=53, pin_out=14,front_side=false);

  // level 3
  place_pin(0,1,3);
  place_pin(1,1,3);
  place_holder_a4(-1,1,3);
  place_wheel(x=0, y=1,level=3 ,cogs=31,rotate_degrees=7, x_in=53, pin_out=27, pin_length=47,front_side=false);
  place_thread_spool(x=0, y=1, level=3 ,x_in=53);
} // Hej

module place_wheel(x=0, y=0, level=0, cogs=10, x_in=5, pin_out=23, pin_length=40, rotate_degrees = 0, add_clips=true, front_side=true)
{
  margin_x=2;
  margin_y=3;
  margin_z=53.5;
  step_y=14;
  step_x=60;
  step_z=59;

  x_pos=margin_x+x*step_x+x_in;
  y_pos=margin_y+y*step_y;
  z_pos=margin_z+level*step_z;

  translate([x_pos,y_pos+pin_length-pin_out,z_pos])
    rotate([90,0,0])
      pin(pin_length);

  if(front_side)
  {

    translate([x_pos,y_pos-3,z_pos])
      rotate([-90,0,0])
        pin_distance();

    translate([x_pos,y_pos-8,z_pos])
      rotate([-90,rotate_degrees,0])
        wheel(cogs);

    if(add_clips)
    {
      translate([x_pos,y_pos-8.2,z_pos])
        rotate([-90,180,180])
          hair_clip_open();

      translate([x_pos,y_pos+8.2,z_pos])
        rotate([-90,180,0])
          hair_clip_open();
    }
  }
  else
  {
    // Backside
    translate([x_pos,y_pos+11,z_pos])
      rotate([90,0,0])
        pin_distance();

    translate([x_pos,y_pos+11,z_pos])
      rotate([-90,rotate_degrees,0])
        wheel(cogs);

    if(add_clips)
    {
      translate([x_pos,y_pos-.3,z_pos])
        rotate([-90,180,180])
          hair_clip_open();

      translate([x_pos,y_pos+16.1,z_pos])
        rotate([-90,180,0])
          hair_clip_open();
    }
  }
}

module place_thread_spool(x=0, y=0, level=0, x_in=5, y_in=0,spool_length= 11, mirror_spool=false)
{
  margin_x=2;
  margin_y=3;
  margin_z=53.5;
  step_y=14;
  step_x=60;
  step_z=59;

  x_pos=margin_x+x*step_x+x_in;
  y_pos=margin_y+y*step_y;
  z_pos=margin_z+level*step_z;

  translate([x_pos,y_pos-y_in-1,z_pos])
    rotate([90,0,0])
    {
      if(mirror_spool)
      {
        translate([0,0,3.5])
          mirror([0,1,0])
            thread_spool(spool_length);
      }
      else
      {
        translate([0,0,spool_length+y_in+10.5])
          mirror([0,1,0])
            rotate([180,0,0])
              thread_spool(spool_length);
      }
    }
  }


  module place_wheels(x=0, y=0, level=0, cogs1=10, cogs2=20, x_in=5, pin_out=23, pin_length=40,rotate_degrees1 = 0,rotate_degrees2 = 0, add_clips=true)
  {
    margin_x=2;
    margin_y=3;
    margin_z=53.5;
    step_y=14;
    step_x=60;
    step_z=59;

    x_pos=margin_x+x*step_x+x_in;
    y_pos=margin_y+y*step_y;
    z_pos=margin_z+level*step_z;

    translate([x_pos,y_pos+pin_length-pin_out,z_pos])
      rotate([90,0,0])
        pin(pin_length);

    translate([x_pos,y_pos-3,z_pos])
      rotate([-90,0,0])
        pin_distance();

    translate([x_pos,y_pos-8,z_pos])
      rotate([-90,rotate_degrees1,0])
        wheel(cogs1);

    // Backside
    translate([x_pos,y_pos+11,z_pos])
      rotate([90,0,0])
        pin_distance();

    translate([x_pos,y_pos+11,z_pos])
      rotate([-90,rotate_degrees2,0])
        wheel(cogs2);

    if(add_clips)
    {
      translate([x_pos,y_pos-8.2,z_pos])
        rotate([-90,180,180])
          hair_clip_open();

      translate([x_pos,y_pos+16.1,z_pos])
        rotate([-90,180,0])
          hair_clip_open();
    }
  }


  module place_holder_a3(x=0, y=0, level=0)
  {
    margin_x=2;
    margin_y=11;
    margin_z=45;
    step_y=14;
    step_x=60;
    step_z=59;
    pin_start_height=3;

    translate([margin_x+x*step_x,margin_y+y*step_y,margin_z+level*step_z])
      rotate([90,0,0])
        holderA3();
  }

  module place_holder_a4(x=0, y=0, level=0)
  {
    margin_x=2;
    margin_y=11;
    margin_z=45;
    step_y=14;
    step_x=60;
    step_z=59;
    pin_start_height=3;

    translate([margin_x+x*step_x,margin_y+y*step_y,margin_z+level*step_z])
      rotate([90,0,0])
        holderA4();
  }


  module place_pin(x=0, y=0, level=0)
  {
    margin=7;
    step_y=14;
    step_x=60;
    step_z=59;
    pin_start_height=3;

    translate([margin+x*step_x,margin+y*step_y, pin_start_height+level*step_z])
      pin();
  }

  module crank2()
  {
    pin_nob(height=10.3);

    translate([0,-2,10])
      cube([18,4, 3]);

    translate([16,0,10])
      cylinder(h=13,d=4);

  }
  /*
  module hair_clip7(width=3, inner_diameter=5.6, outer_diameter=8.5)
  {
    rotate([0,0,-25])
      hair_clip5(width, inner_diameter, outer_diameter);
    translate([0,0,0])
      mirror([1,0,0])
        rotate([0,0,-25])
          hair_clip5(width, inner_diameter, outer_diameter);
  }
  */


  module hair_clip(width=3, inner_diameter=5.4, outer_diameter=9)
  {
    color("black")
    {
      hair_clip3(width, inner_diameter, outer_diameter);
      translate([2,0,0])
        mirror([1,0,0])
          hair_clip3(width, inner_diameter, outer_diameter);
    }
  }

  module hair_clip_open(width=3, inner_diameter=5.4, outer_diameter=9)
  {
    color("black")
      rotate([0,0,10])
      {
        hair_clip3(width, inner_diameter, outer_diameter);
        translate([-.5,0,0])
          mirror([1,0,0])
            rotate([0,0,20])
              hair_clip3(width, inner_diameter, outer_diameter);
      }
    }

    module hair_clip3(width=3, inner_diameter=5.6, outer_diameter=8.5)
    {
      difference()
      {
        union()
        {
          hair_clip2(width,, inner_diameter, outer_diameter);
        }
        translate([-11,-8.5,-0.01])
          cube([12,19, width+.02]);

      }
    }

    /*
    module hair_clip6(width=3, inner_diameter=5.6, outer_diameter=8.5)
    {
      difference()
      {
        union()
        {
          hair_clip7(width, inner_diameter, outer_diameter);

          translate([0,-6.6,0])
            cylinder(h=width,d=outer_diameter);


        }
        translate([-12,-6,-1])
          *cube([12,19, width+2]);

        translate([0,-7,-1])
          cylinder(h=width+2,d=inner_diameter);

        translate([-0.25,-5,-1])
          cube([.5,4, width+2]);


      }
    }


    module hair_clip4(width=3, inner_diameter=5.6, outer_diameter=8.5)
    {
      difference()
      {
        union()
        {
          hair_clip6(width, inner_diameter,outer_diameter );

          translate([-.5,0,0])
            mirror([1,0,0])
              hair_clip6(width, inner_diameter,outer_diameter );

          translate([0,-7,0])
            cylinder(h=width,d=outer_diameter);


        }
        translate([-0.25,-5,-1])
          cube([.5,4, width+2]);

        translate([0,-7,-1])
          cylinder(h=width+2,d=inner_diameter);


      }
    }

    module hair_clip5(width=3, inner_diameter=5.6, outer_diameter=8.5)
    {

      difference()
      {
        union()
        {
          translate([0,-5,0])
            *cylinder(h=width,d=outer_diameter);


          translate([0,0,0])
            cylinder(h=width,d=outer_diameter);

          translate([0,5,0])
            cylinder(h=width,d=outer_diameter);

        }

        // scale([1, 1.5, 1])
        translate([0,-5,-overlap])
          *cylinder(h=width+2,d=inner_diameter);


        translate([0,0,-overlap])
          cylinder(h=width+2,d=inner_diameter);

        translate([0,5,-overlap])
          cylinder(h=width+2,d=inner_diameter);

        translate([-6,5.5,-1])
          cube([12,4, width+2]);

        translate([0,-5,-1])
          cube([6,14, width+2]);

      }
    }
    */


    module hair_clip2(width=3, inner_diameter=5.6, outer_diameter=8.5)
    {

      difference()
      {
        union()
        {
          translate([0,-5,0])
            cylinder(h=width,d=outer_diameter);


          translate([0,0,0])
            cylinder(h=width,d=outer_diameter);

          translate([0,5,0])
            cylinder(h=width,d=outer_diameter);

        }

        // scale([1, 1.5, 1])
        translate([0,-5,-overlap])
          cylinder(h=width+2,d=inner_diameter);


        translate([0,0,-overlap])
          cylinder(h=width+2,d=inner_diameter);

        translate([0,5,-overlap])
          cylinder(h=width+2,d=inner_diameter);

        translate([-6,-11.5,-1])
          cube([12,6, width+2]);
      }
    }

    module crank_test(length=12)
    {
      difference()
      {
        rotate([180,0,0])
          union()
          {
            *cylinder(h=length,d=6);

            cube([10,11,5]);
          }

          translate([5,5,-1])
            rotating_hole();
      }

      translate([5,5,21.5])
        crank();
    }

    module crank(length=28)
    {
      diameter=6;
      length2=length+.76;
      diameter2=diameter-.5;
      end_height=length2*.1;
      end_diameter=diameter2*1.7;
      cone_max_diameter = diameter2*1.4;
      cone_height=length2*.11;
      gap=diameter2*.3;
      gap_start_height=length2*.7;
      gap_width=cone_max_diameter*1.1;
      gap_length= length2-gap_start_height+cone_height-2;

      difference()
      {
        union()
        {
          translate([0,0,22.5])
            cylinder(h=end_height,d=end_diameter);


          rotate([0,0,0])
            translate([0,0,end_height])
              cylinder(h=length2,d=diameter2);

          rotate([0,0,0])
            translate([0,0,end_height+length2])
              cylinder(h=cone_height,r1=cone_max_diameter/2, r2=0);
        }
        translate([-gap/2,-gap_width/2, gap_start_height+end_height+2.4])
          cube([gap,gap_width,gap_length]);
      }
    }

    module pin_distance(height=3.2,diameter=12, diameter2=5 )
    {
      color("grey")
      {
        nobbs=16;

        difference()
        {
          union()
          {
            cylinder(h=height,r1=diameter/2, r2=diameter2/2);

          }

          translate([0,0,-overlap])
            scale([1.2, 1.2, 2])
              pin_hole();
        }
      }
    }

    module hook()
    {
      // Upper loop
      translate([0, 0, 14])
        rotate([90,90,0])
          rotate_extrude(convexity = 10, $fn = 100)
            translate([3, 0, 0])
              circle(r = 1.2, $fn = 100);

      // Heavy section in the middle
      translate([0,0,5])
        sphere(6);

      // Hook
      translate([2, 0, -6])
        rotate([90,90,0])
          hook_hook();

    }

    module hook_hook()
    {
      difference()
      {
        union()
        {
          rotate_extrude(convexity = 10, $fn = 100)
            translate([5.5, 0, 0])
              circle(r = 1.5, $fn = 100);
        }
        rotate([0,0,45])
          translate([-4,0,-1.5])
            cube([8,8,3]);
      }
    }

    module thread_spool(length=11, inner_diameter=9, outer_diameter=16)
    {
      edge_thickness=4;

      difference()
      {
        union()
        {
          translate([0,0,0])
            cylinder(h=edge_thickness,r2=inner_diameter/2, r1 = outer_diameter/2);

          translate([0,0,edge_thickness])
            cylinder(h=length,d=inner_diameter);

          translate([0,0,edge_thickness+length])
            cylinder(h=edge_thickness,r1=inner_diameter/2, r2 = outer_diameter/2);

          translate([7.5,0,edge_thickness*1.55+length])
            rotate([90,0,0])
              *thread_spool_loop();
        }

        translate([0,0,-overlap])
          pin_hole(edge_thickness*2+length+overlap*2);

        // Hole to fasten thread
        translate([2,-6.15,-1])
          rotate([-10,0,0])
            cylinder(h=6,d=2);

        translate([2,-5.5,-1])
          sphere(2.1);
      }
    }

    module thread_spool_loop()
    {
      rotate_extrude(convexity = 10, $fn = 100)
        translate([1.2, 0, 0])
          circle(r = .6, $fn = 100);
    }


    module bottom(length=140, width=40)
    {
      color("grey")
      {
        margin = 7;

        difference()
        {
          union()
          {
            cube([length,width,3]);

            for(x=[margin:60:length])
            {
              for(y=[margin:14:width])
              {
                translate([x,y,3])
                  pin_hole6_deluxe();
              }
            }
          }
        }
      }
    }


    module bottom_test(length=80, width=13)
    {
      margin = 7;

      difference()
      {
        union()
        {
          cube([length,width,3]);

          for(i=[0:6])
          {
            translate([7+11*i,7,3])
              pin_hole6_deluxe();
          }
        }
      }
    }

    module pin_hole6_deluxe(length=13, diameter=6.2, thickness=4)
    {
      difference()
      {
        union()
        {
          cylinder(h=length,d=diameter+thickness, $fn=6);
        }

        translate([0,0,overlap])
          inner_hole6(length, diameter);
      }
    }


    module inner_hole6(length=13, diameter=6.2)
    {
      cylinder(h=length,d=diameter, $fn=6);
    }

    module holder(name="", length=70, hole1=20, hole2=55, hole3=-1, hole4=-1, hole5=-1, hole6=-1, hole7=-1, hole8=-1, hole9=-1)
    {
      color("cyan")
      {


        margin = 12;
        width=17;
        thickness=8;

        difference()
        {
          union()
          {
            cube([length,width,thickness]);

            translate([1,.9,7.5])
              linear_extrude(1)
                text(name, font="Comic Sans MS:style=bold", size=4.2, halign="left");

            for(x=[5:60:length])
            {
              translate([x, width-1, thickness/2])
                rotate([-90,0,0])
                  pin_hole6_deluxe(thickness=3);

              translate([x, 1, thickness/2])
                rotate([90,0,0])
                  pin_hole6_deluxe(thickness=3);
            }
          }

          if (hole1 >= 0)
          {
            translate([hole1+margin,width/2,-overlap])
              rotating_hole(10);
          }

          if (hole2 >= 0)
          {
            translate([hole2+margin,width/2,-overlap])
              rotating_hole(10);
          }

          if (hole3 >= 0)
          {
            translate([hole3+margin,width/2,-overlap])
              rotating_hole(10);
          }

          if (hole4 >= 0)
          {
            translate([hole4+margin,width/2,-overlap])
              rotating_hole(10);
          }

          if (hole5 >= 0)
          {
            translate([hole5+margin,width/2,-overlap])
              rotating_hole(10);
          }

          if (hole6 >= 0)
          {
            translate([hole6+margin,width/2,-overlap])
              rotating_hole(10);
          }

          if (hole7 >= 0)
          {
            translate([hole7+margin,width/2,-overlap])
              rotating_hole(10);
          }

          if (hole8 >= 0)
          {
            translate([hole8+margin,width/2,-overlap])
              rotating_hole(10);
          }

          if (hole9 >= 0)
          {
            translate([hole9+margin,width/2,-overlap])
              rotating_hole(10);
          }
        }
      }
    }

    module pin_hole_test()
    {
      difference()
      {
        union()
        {
          cube([47,15,5]);
        }

        translate([6,7,-overlap])
          pin_hole6(diameter=6.4);

        translate([14,7,-overlap])
          pin_hole6(diameter=6.45);

        translate([22,7,-overlap])
          pin_hole6(diameter=6.5);

        translate([30,7,-overlap])
          pin_hole6(diameter=6.55);

        translate([41,7,-overlap])
          pin_hole6(diameter=6.6);
      }
    }

    module pin_hole(length=11)
    {
      scale([1.06, 1.06, 1])
        pin(length);
    }

    module pin_hole6(length=11, diameter=6.45)
    {
      cylinder(h=length,d=diameter, $fn=6);
    }


    module pin(length=30)
    {
      color("red")
      {

        pin_blade(length);

        rotate([0,0,60])
          pin_blade(length);

        rotate([0,0,120])
          pin_blade(length);
      }
    }

    module pin_blade(length=30)
    {
      translate([-2.9,-.77,0])
        cube([5.8,1.54,length]);
    }

    module wheel(tags=30, distance=false)
    {
      color("gold")
      {
        diameter=tags*2.143;
        holes= round(tags/2.2);
        holes_distance=round(360/holes);
        height=5;

        //holes=14;

        difference()
        {
          union()
          {
            cylinder(h=height,d=diameter);

            for(i=[0:360/tags:360])
            {
              translate([sin(i)*diameter/2.05,cos(i)*diameter/2.05,0])
                rotate([0,0,-i])
                  cog();
            }

            if(distance)
            {
              translate([0,0,height])
                pin_distance();
            }

          }

          translate([0,0,-overlap])
            pin_hole();

          if(tags >= 10)
          {
            for(i=[0:holes_distance:360])
            {
              translate([sin(i)*diameter/3.05,cos(i)*diameter/3.05,-overlap])
                rotate([0,0,-i])
                  rotating_hole();
            }
          }
        }
      }
    }

    module pin_nob(diameter=12, height=5)
    {
      nobbs=16;

      difference()
      {
        union()
        {
          cylinder(h=height,d=diameter);

          for(i=[0:360/nobbs:360])
          {
            translate([sin(i)*diameter/2,cos(i)*diameter/2,0])
              cylinder(h=height,d=diameter/10);
          }
        }

        translate([0,0,-overlap])
          scale([.995, .995, 1])
            pin_hole();
      }
    }

    module rotating_hole(length=7)
    {
      cylinder(h=length,d=6.2);
    }

    module cog()
    {
      hull()
      {
        translate([-2,0,0])
          cube([4,2,5]);

        translate([0,5,0])
          cylinder(h=5,d=2.2);
      }
    }
